#!/usr/bin/env bash

if [ "$(whoami)" != "root" ] 
then
    echo "Please run this script as root"
    exit 1
fi

cd quickfix-v.1.14.4

./bootstrap
./configure
make && make check && sudo make install

