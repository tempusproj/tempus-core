#!/usr/bin/env bash

if [ "$(whoami)" != "root" ] 
then
    echo "Please run this script as root"
    exit 1
fi

cd wavelib
cmake .
make
cp Bin/libwavelib.a /usr/local/lib/
mkdir -p /usr/local/include/wavelib/header
cp header/wavelib.h /usr/local/include/wavelib/header
