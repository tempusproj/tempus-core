#include "../include/dbutils.h"
#include "../include/DaoTestFixture.h"
#include <cstdlib>
#include <cstdio>
#include <memory>
#include <stdexcept>
#include <iostream>
#include <regex>

bool DaoTestFixture::DoPerformanceTests = false;

std::string exec(char const * cmd) {
    int pec;
    return exec(cmd, pec);
}

std::string exec(char const * cmd, int & procExitCode)
{
    char buffer[128];
    std::string result = "";
    std::shared_ptr<FILE> pipe(popen(cmd, "r"), [&procExitCode](FILE* what){procExitCode = pclose(what);});
    if (!pipe) throw std::runtime_error("popen() failed!");
    while (!feof(pipe.get())) {
        if (fgets(buffer, 128, pipe.get()) != NULL)
            result += buffer;
    }
    return result;
}

bool erase_after(std::string & where, char what)
{
    std::string::size_type pos = where.find(what);
    if (pos == std::string::npos)
        return false;

    where.erase(where.begin() + pos, where.end());
    return true;
}

/******************************************************************************/

constexpr char const * TestEnv::ConfigFiles[];

bool TestEnv::initTestDb(char const * dbName)
{
    dbScriptsPath.reset();
    char const * ctmp = std::getenv(EnvDbScriptsPathVar);
    if(ctmp != NULL)
        dbScriptsPath = boost::make_optional( ctmp ) ;

    if( !dbScriptsPath )
    {
        std::string tmp = exec("dirname `find ../../ -name 'init_db.sh'`");
        erase_after(tmp, '\n');
        if(!tmp.empty())
            dbScriptsPath.reset(tmp);
    }

    if( !dbScriptsPath )
    {
        std::cerr << "Cannot find the db scripts path." << std::endl;
        return false;
    }

    std::string cl = *dbScriptsPath + "/init_db.sh " + dbName + " 2>&1";

    std::string expect = exec ("find ~/ -maxdepth 1 -name runwithpass.sh");
    erase_after(expect, '\n');

    if(!expect.empty())
        cl = expect + " " + cl;

    int pec;
    std::string out = exec(cl.c_str(), pec);

    if(pec != 0)
    {
        std::cerr << "init_db.sh exit code is not 0" << std::endl;
        return false;
    }

    std::regex regex("error", std::regex::icase);
    std::smatch match;

    if (std::regex_search(out, match, regex))
    {
        std::cerr << "Errors found in the init_db.sh output: " << out << std::endl;
        return false;
    }

    return true;
}

bool TestEnv::prepareSvrConfig(char const * dbName, std::string const & daoType, int max_loop_count)
{
    /**************************************************************************/
    // Copying config files
    int pec;
    std::stringstream str_cp; str_cp << "cp ";

    for (char const * cf : ConfigFiles)
        str_cp << "../config/" << cf << " ";

    str_cp << " .";

    exec(str_cp.str().c_str(), pec);
    if( pec )
        return false;


    /**************************************************************************/
    // Setting up db credentials
    {
        std::stringstream str_sed;
        str_sed << "sed -ri 's/(dbname=|user=|password=)([a-zA-Z0-9_]+)/\\1" << dbName << "/g' ";

        for (char const * cf : ConfigFiles)
        {
            std::stringstream str1;
            str1 << str_sed.str() << cf;
            std::string out = exec(str1.str().c_str(), pec);
            if(pec)
                return false;
        }
    }

    /**************************************************************************/
    // Setting up dao type

    {
        std::stringstream str_sed;
        str_sed << "sed -ri 's/(DAO_TYPE[ \\t]*=[ \\t]*)([a-zA-Z0-9_]+)/\\1" << daoType << "/g' ";

        for (char const * cf : ConfigFiles)
        {
            std::stringstream str1;
            str1 << str_sed.str() << cf;
            std::string out = exec(str1.str().c_str(), pec);
            if(pec)
                return false;
        }
    }

/**************************************************************************/
    // Setting up iteration number

    {
        std::stringstream str_sed;
        str_sed << "sed -ri 's/(MAX_LOOP_COUNT[ \\t]*=[ \\t]*)([a-zA-Z0-9_]+)/\\1" << max_loop_count << "/g' daemon.config";
        std::string out = exec(str_sed.str().c_str(), pec);
        if(pec)
            return false;
    }

    return true;
}

#include <unistd.h>

bool TestEnv::run_daemon()
{
    auto cpid = fork();

    if (cpid == 0)
    {
        //Child process;
        int exit_code = execl("./SVRDaemon", "./SVRDaemon", "--config", "daemon.config", NULL);
        if(exit_code != -1)
            exit_code = 0;
        else
            exit_code = 1;
        exit(exit_code);
    }
    else if (cpid > 0)
    {
        int status;
        auto w = waitpid(cpid, &status, WUNTRACED | WCONTINUED);
        if (w == -1)
            throw std::runtime_error("Cannot wait until child process ends.");

        return status == 0;
    }
    else
        throw std::runtime_error("Cannot fork a child process");
}
