#include "include/DaoTestFixture.h"
#include <memory>

#include <model/User.hpp>
#include <model/Request.hpp>
#include <model/Dataset.hpp>
#include <model/InputQueue.hpp>

using svr::datamodel::MultivalRequest;

TEST_F(DaoTestFixture, BasicWhiteboxTest)
{
    // Latest data in the IQ    open high low close
    // 2015-05-20 10:52:00 2015-11-17 12:54:52 9   1.11033000000000004 1.11044000000000009 1.11026000000000002 1.11026000000000002

    std::string const default_user_name("svrwave");
    bigint const default_dataset_id = 100;
    bpt::ptime const nw = bpt::second_clock::local_time();
    bpt::ptime const default_request_time = bpt::from_iso_string("20150520T105300");
    bpt::time_duration const default_resolution = bpt::seconds(60);
    u_int64_t default_decremental_distance = 2000;

    std::map<std::pair<std::string, std::string>, std::vector<SVRParameters_ptr>> parameters_pairs
            = aci.svr_parameters_service.get_all_by_dataset_id(default_dataset_id);
    for(auto parameters_pair : parameters_pairs)
        for(auto parameters : parameters_pair.second)
        {
            parameters->set_svr_decremental_distance(default_decremental_distance);
            aci.svr_parameters_service.save(parameters);
        }

    MultivalRequest_ptr request = MultivalRequest_ptr( new
        MultivalRequest(bigint(0), default_user_name, default_dataset_id, nw
            , default_request_time, default_request_time + default_resolution * 15, default_resolution.total_seconds()
            , "{open,high,low,close}")
    );

    aci.request_service.save(request);

    aci.flush_dao_buffers();

    EXPECT_TRUE(tdb.run_daemon());

    std::vector<MultivalResponse_ptr>  results = aci.request_service.get_multival_results
    (
          default_user_name
        , default_dataset_id
        , default_request_time
        , default_request_time + default_resolution * 15
        , default_resolution.total_seconds()
    );

    ASSERT_NE(0UL, results.size());

    /* Real reference data  open high low close
    2015-05-20 10:53:00     1.11009000000000002     1.11024999999999996     1.11004999999999998     1.11014999999999997
    2015-05-20 10:54:00     1.1101399999999999      1.11016000000000004     1.10953999999999997     1.10953999999999997
    2015-05-20 10:55:00     1.10949999999999993     1.10951                 1.10899999999999999     1.10924
    2015-05-20 10:56:00     1.10925000000000007     1.10949000000000009     1.10895999999999995     1.10916999999999999
    2015-05-20 10:57:00     1.10915999999999992     1.10962000000000005     1.10901999999999989     1.10958000000000001
    2015-05-20 10:58:00     1.10956999999999995     1.10962999999999989     1.10925999999999991     1.10932000000000008
    2015-05-20 10:59:00     1.1093599999999999      1.10997999999999997     1.1093599999999999      1.10990000000000011
    2015-05-20 11:00:00     1.10992000000000002     1.11131000000000002     1.10990999999999995     1.11112999999999995
    2015-05-20 11:01:00     1.11114000000000002     1.11182000000000003     1.11112000000000011     1.11172000000000004
    2015-05-20 11:02:00     1.11173999999999995     1.11192000000000002     1.11124000000000001     1.11138000000000003
    2015-05-20 11:03:00     1.11136999999999997     1.1117999999999999      1.11125999999999991     1.11158000000000001
    2015-05-20 11:04:00     1.11158000000000001     1.11220000000000008     1.11153999999999997     1.11176000000000008
    2015-05-20 11:05:00     1.1117999999999999      1.11182000000000003     1.11129999999999995     1.11146999999999996
    2015-05-20 11:06:00     1.11149000000000009     1.11158000000000001     1.11125000000000007     1.11126999999999998
    */

    /* Predicted values as of Aug 02, 2016
                             open high low close
    2015-May-20 10:53:00 1.04863 1.05413 1.05016 1.05302
    2015-May-20 10:54:00 1.05894 1.05599 1.05613 1.05472
    2015-May-20 10:55:00 1.05466 1.05491 1.0543  1.05486
    2015-May-20 10:56:00 1.05369 1.05456 1.05392 1.05489
    2015-May-20 10:57:00 1.05374 1.05457 1.05369 1.05452
    2015-May-20 10:58:00 1.05369 1.05434 1.05346 1.05413
    2015-May-20 10:59:00 1.05363 1.05391 1.05327 1.05369
    2015-May-20 11:00:00 1.05359 1.05345 1.05311 1.05325
    2015-May-20 11:01:00 1.05354 1.05283 1.05295 1.0527
    2015-May-20 11:02:00 1.05381 1.05231 1.05309 1.05235
    2015-May-20 11:03:00 1.05415 1.05211 1.05338 1.05227
    2015-May-20 11:04:00 1.05484 1.05242 1.05411 1.0527
    2015-May-20 11:05:00 1.0556  1.05343 1.05516 1.05373
    2015-May-20 11:06:00 1.05789 1.05559 1.05757 1.05589
     */

    /* abs(real-predicted)/real)
    0.0553648803	0.0505471741	0.0539525247	0.0514615142
    0.0461203092	0.0487947683	0.0481370658	0.0494078627
    0.0494276701	0.0492109129	0.0493237151	0.0490245574
    0.0500878972	0.049509234	0.0496320877	0.0489374938
    0.0499657398	0.0496115787	0.0498908947	0.0496223796
    0.0503618519	0.04982742	0.0503038061	0.0497511989
    0.0502361722	0.0505144237	0.0505606836	0.0506442022
    0.0507514055	0.0520646804	0.0511753205	0.0520911145
    0.0518386522	0.0530571495	0.0523525812	0.0530889073
    0.0521075072	0.0536099719	0.0523289298	0.0531141464
    0.0514860038	0.0536877136	0.0520850206	0.0533564836
    0.0510444592	0.0537493257	0.0516670565	0.0531229762
    0.0505486598	0.0525174938	0.050517412	0.0519492204
    0.0482235558	0.050369744	0.0483059618	0.0498348736

    MAX: 0.0553648803
    */

    const auto& fis = bpt::from_iso_string;

    typedef std::map<bpt::ptime, std::map<std::string, double>> result_map;

    result_map const reference_data
    {
      {fis("20150520T105300"), {{"open", 1.11009000000000002L}, {"high", 1.11024999999999996L}, {"low", 1.11004999999999998L}, {"close", 1.11014999999999997L}}},
      {fis("20150520T105400"), {{"open", 1.1101399999999999L }, {"high", 1.11016000000000004L}, {"low", 1.10953999999999997L}, {"close", 1.10953999999999997L}}},
      {fis("20150520T105500"), {{"open", 1.10949999999999993L}, {"high", 1.10951L            }, {"low", 1.10899999999999999L}, {"close", 1.10924L            }}},
      {fis("20150520T105600"), {{"open", 1.10925000000000007L}, {"high", 1.10949000000000009L}, {"low", 1.10895999999999995L}, {"close", 1.10916999999999999L}}},
      {fis("20150520T105700"), {{"open", 1.10915999999999992L}, {"high", 1.10962000000000005L}, {"low", 1.10901999999999989L}, {"close", 1.10958000000000001L}}},
      {fis("20150520T105800"), {{"open", 1.10956999999999995L}, {"high", 1.10962999999999989L}, {"low", 1.10925999999999991L}, {"close", 1.10932000000000008L}}},
      {fis("20150520T105900"), {{"open", 1.1093599999999999L }, {"high", 1.10997999999999997L}, {"low", 1.1093599999999999L }, {"close", 1.10990000000000011L}}},
      {fis("20150520T110000"), {{"open", 1.10992000000000002L}, {"high", 1.11131000000000002L}, {"low", 1.10990999999999995L}, {"close", 1.11112999999999995L}}},
      {fis("20150520T110100"), {{"open", 1.11114000000000002L}, {"high", 1.11182000000000003L}, {"low", 1.11112000000000011L}, {"close", 1.11172000000000004L}}},
      {fis("20150520T110200"), {{"open", 1.11173999999999995L}, {"high", 1.11192000000000002L}, {"low", 1.11124000000000001L}, {"close", 1.11138000000000003L}}},
      {fis("20150520T110300"), {{"open", 1.11136999999999997L}, {"high", 1.1117999999999999L }, {"low", 1.11125999999999991L}, {"close", 1.11158000000000001L}}},
      {fis("20150520T110400"), {{"open", 1.11158000000000001L}, {"high", 1.11220000000000008L}, {"low", 1.11153999999999997L}, {"close", 1.11176000000000008L}}},
      {fis("20150520T110500"), {{"open", 1.1117999999999999L }, {"high", 1.11182000000000003L}, {"low", 1.11129999999999995L}, {"close", 1.11146999999999996L}}},
      {fis("20150520T110600"), {{"open", 1.11149000000000009L}, {"high", 1.11158000000000001L}, {"low", 1.11125000000000007L}, {"close", 1.11126999999999998L}}}
    };

    result_map actual_data;

    for(auto const & result : results)
        actual_data[result->value_time][result->value_column] = result->value;

    for(auto actd : actual_data)
    {
        std::cout << bpt::to_simple_string(actd.first);
        for(auto vls : actd.second)
            std::cout << "\t" << vls.first << " " << vls.second;
        std::cout << "\n";
    }

    ASSERT_EQ(reference_data.size(), actual_data.size());

    double max_diff = 0;

    for(auto const & ref_pairs : reference_data)
        for(auto const & ref_val : ref_pairs.second)
        {
            auto act_iter = actual_data[ref_pairs.first].find(ref_val.first);
            ASSERT_NE(act_iter, actual_data[ref_pairs.first].end());

            double diff = fabs(ref_val.second - act_iter->second) / ref_val.second;

            max_diff = std::max(diff, max_diff);
        }

    std::cout << "BasicWhiteboxTest:: max value diff: " << max_diff << std::endl;

    ASSERT_LT(max_diff, 0.1L);
}
