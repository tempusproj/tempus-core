#ifndef DBUTILS_H
#define DBUTILS_H

#include <string>
#include <boost/optional.hpp>

using OptString = boost::optional<std::string>;

std::string exec(char const * cmd);
std::string exec(char const * cmd, int & procExitCode);

bool erase_after(std::string & where, char what);

struct TestEnv
{
    OptString dbScriptsPath;
    bool dbInitialized;

    static constexpr char const * EnvDbScriptsPathVar   {"TEMPUS_DB_SCRIPTS_PATH"};
    static constexpr char const * ConfigFiles[]         {"app.config", "daemon.config"};
    static constexpr char const * TestDbUserName        {"tempustest"};
    static constexpr char const * AppConfigFile         {"app.config"};

    bool initTestDb(char const * dbName);

    bool prepareSvrConfig(char const * dbName, std::string const & daoType, int max_loop_count);

    bool run_daemon();
};

#endif /* DBUTILS_H */

