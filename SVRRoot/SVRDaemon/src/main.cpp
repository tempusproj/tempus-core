#include "appcontext.hpp"
#include "DaemonFacade.hpp"
#include <boost/program_options.hpp>
#include <model/Request.hpp>

using namespace std;
using namespace svr::daemon;
using namespace svr::context;
using namespace svr::datamodel;

void save_test_deamon_data()
{
    std::string user_name = "svrwave";
    std::string input_queue_table_name = "q_svrwave_eurusd_60";
    InputQueue_ptr p_input_queue = AppContext::get_instance().input_queue_service.get_queue_metadata(input_queue_table_name);
    Dataset_ptr p_dataset = std::make_shared<Dataset>(0, "Deamon_test_dataset", user_name, p_input_queue, std::vector<InputQueue_ptr>(),
                                                        Priority::Normal, "", 4, "sym7");
    p_dataset->set_is_active(true);

    SVRParameters svr_parameters = SVRParameters(0, 0, p_input_queue->get_table_name(), "", 0,
                                                 1.39535, 0.00173067, 0.1, 0.108544, 3500, 0.508475,
                                                 kernel_type_e::RBF, 10);


    std::map<std::pair<std::string, std::string>, std::vector<SVRParameters_ptr>> ensembles_svr_parameters;
    for (std::string column_name : p_input_queue->get_value_columns())
    {
        std::vector<SVRParameters_ptr> vec_svr_parameters;
        for (size_t decon_level = 0; decon_level <= p_dataset->get_swt_levels(); ++decon_level)
        {
            svr_parameters.set_decon_level(decon_level);
            svr_parameters.set_input_queue_column_name(column_name);
            vec_svr_parameters.push_back(std::make_shared<SVRParameters>(svr_parameters));
        }
        auto key = std::make_pair(input_queue_table_name, column_name);
        ensembles_svr_parameters[key] = vec_svr_parameters;
    }

    p_dataset->set_ensembles_svr_parameters(ensembles_svr_parameters);
    bpt::ptime newest_time = AppContext::get_instance().input_queue_service.find_newest_record(p_input_queue)->get_value_time();
    AppContext::get_instance().dataset_service.save(p_dataset);

    MultivalRequest_ptr p_request = make_shared<MultivalRequest>(
        MultivalRequest(bigint(0), user_name, p_dataset->get_id(), bpt::second_clock::local_time(),
            newest_time + p_input_queue->get_resolution(), newest_time + p_input_queue->get_resolution()*5, p_input_queue->get_resolution().total_seconds(), "{open,close,high,low}")
    );

//    AppContext::get_instance().request_service.save(p_request);
}

std::string parse(int argc, char** argv)
{
    boost::program_options::options_description gen_desc = boost::program_options::options_description("Deamon options");
    gen_desc.add_options()
        ("help",     "produce help message")
        ("config",   boost::program_options::value<std::string>()->default_value("daemon.config"),
                    "Path to file with SQL configuration for daemon");

    boost::program_options::variables_map vm;
    // parse command line
    boost::program_options::store(boost::program_options::command_line_parser(argc, argv).options(gen_desc).run(), vm);
    if (vm.count("help") || !vm.count("config"))
    {
        cout << gen_desc << "\n";
        exit(0);
    }
    if (vm["config"].as<std::string>().empty())
    {
        throw std::invalid_argument("empty path to config file");
    }
    AppContext::init_instance(vm["config"].as<std::string>().c_str());
    return vm["config"].as<std::string>();
}

namespace{
    static AppContextDeleter appContextDeleter;
}

int main(int argc, char** argv)
{

    
    shared_ptr<DaemonFacade> p_daemon_facade;
    try
    {
        std::string config_path = parse(argc, argv);
//    save_test_deamon_data();
        p_daemon_facade = make_shared<DaemonFacade>(config_path);
    }
    catch(std::invalid_argument& e)
    {
        LOG4_ERROR(e.what());
        return 1;
    }

    p_daemon_facade->start_loop();

    LOG4_INFO("Daemon process finishing");
    return 0;
}
