#include <unistd.h>
#include <stdlib.h>
#include <syslog.h>
#include <sys/stat.h>
#include <signal.h>
#include <queue>

#include "common/Logging.hpp"
#include "appcontext.hpp"
#include "daemon-config.hpp"
#include "DaemonFacade.hpp"

#include <model/Request.hpp>

using namespace svr::datamodel;
using namespace svr::dao;
using namespace svr::business;
using namespace svr::context;
using namespace svr::common;
using namespace svr::daemon;
using namespace bpt;
using namespace std;


namespace svr {
namespace daemon {


std::string DaemonFacade::S_MAX_LOOP_COUNT              = "MAX_LOOP_COUNT";
std::string DaemonFacade::S_DECONSTRUCTING_FRAME_SIZE   = "DECONSTRUCTING_FRAME_SIZE";
std::string DaemonFacade::S_FILL_MISSING_QUEUE_VALUES   = "FILL_MISSING_QUEUE_VALUES";
std::string DaemonFacade::S_LOOP_INTERVAL               = "LOOP_INTERVAL_MS";
std::string DaemonFacade::S_DAEMONIZE                   = "DAEMONIZE";


DaemonFacade::DaemonFacade(const string &app_properties_path) : loop_count(0) {
    initialize(app_properties_path);
}


void DaemonFacade::initialize(const string &app_properties_path) {
    // parse necessary daemon configuration parameters
    loop_interval = AppContext::get_instance().app_properties.get_property<long>(app_properties_path, S_LOOP_INTERVAL, "1000");
    daemonize = AppContext::get_instance().app_properties.get_property<bool>(app_properties_path, S_DAEMONIZE, "1");
    max_loop_count = AppContext::get_instance().app_properties.get_property<size_t>(app_properties_path, S_MAX_LOOP_COUNT, "-1");
}


void DaemonFacade::do_fork() {
    pid_t pid;

    /* Catch, ignore and handle signals */
    //TODO: Implement a working signal handler */
    signal(SIGCHLD, SIG_IGN);
    signal(SIGHUP, SIG_IGN);


    /* Fork off the parent process */
    pid = fork();
    if (pid < 0) {
        exit(EXIT_FAILURE);
    }

    /* If we got a good PID, then
    we can exit the parent process.
    */
    if (pid > 0) {
        LOG4_INFO("Exiting parent process");
        exit(EXIT_SUCCESS);
    }

    /* Open the log file */
    openlog ("SVRDaemon", LOG_PID, LOG_DAEMON);
    syslog(LOG_NOTICE, "Daemon process started");

    /* Change the file mode mask */
    umask(0);

    /* Create a new SID for the child process */
    if (setsid() < 0) {
        /* Log the failure */
        syslog(LOG_ERR, "Cannot create new SID!");
        exit(EXIT_FAILURE);
    }

    /* Close out the standard file descriptors */
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
}


struct save_response {
    const std::string &value_column;
    const MultivalRequest_ptr &latest_request;

    save_response(const std::string &value_column_, const MultivalRequest_ptr &latest_request_) :
        value_column(value_column_), latest_request(latest_request_)
    {}

    void operator() (const std::pair<const boost::posix_time::ptime, std::shared_ptr<svr::datamodel::DataRow> >& resp)
    {
        if (resp.first < latest_request->value_time_start || resp.first > latest_request->value_time_end) {
            // TODO Filter out invalid responsed by request id matching not by time
            LOG4_DEBUG("Skipping response: " << resp.second->to_string());
            return;
        }

        MultivalResponse_ptr response = std::make_shared<MultivalResponse>
                (0, latest_request->get_id(), resp.first, value_column, resp.second->get_values()[0]);

        LOG4_DEBUG(response->to_string());
        try {
            AppContext::get_instance().request_service.save(response); // Requests get marked processed here
        } catch (const std::exception &e) {
            LOG4_ERROR("Failed saving response: " << e.what());
        }
    }
};

// TODO Break up
void DaemonFacade::process_multival_requests(const User_ptr& p_user, const Dataset_ptr& p_dataset)
{
    // Dataset and associated queues should be initialized in process_datasets
    InputQueue_ptr &p_input_queue = p_dataset->get_input_queue();
    if (p_input_queue->get_data().empty())
        LOG4_WARN("Input queue data is empty " << p_input_queue->get_logical_name());

    const size_t frame_size = swt_levels_to_frame_length(p_dataset->get_swt_levels());
    auto active_requests = AppContext::get_instance().request_service.get_active_multival_requests(*p_user, *p_dataset, *p_input_queue);

    for(MultivalRequest_ptr &p_active_request: active_requests)
    {
        const bpt::ptime start_predict_time = p_active_request->value_time_start;
        const bpt::ptime end_predict_time = p_active_request->value_time_end;
        bool request_answered = true;
        std::vector<std::string> columns = from_sql_array(p_active_request->value_columns);
        for (std::string const &value_column: columns)
        {
            Ensemble_ptr p_ensemble = p_dataset->get_ensemble(value_column);
            if (!p_ensemble) {
                LOG4_WARN("Ensemble for column " << value_column << " could not be found. Skipping ..");
                request_answered = false;
                continue;
            }

            const size_t predict_count =
                    bpt::time_duration(end_predict_time - start_predict_time).total_seconds() / p_input_queue->get_resolution().total_seconds();
            auto p_decon_queue_predicted = p_ensemble->get_decon_queue()->clone_empty();
            prepare_queue(p_dataset, start_predict_time, end_predict_time, p_decon_queue_predicted, predict_count);
            std::vector<DataRowContainer_ptr> aux_decon_data_predicted;
            for (auto &p_aux_decon_queue: p_ensemble->get_aux_decon_queues()) {
                auto p_aux_decon_queue_predicted = p_aux_decon_queue->clone_empty();
                prepare_queue(
                        p_dataset, start_predict_time, end_predict_time, p_aux_decon_queue_predicted, predict_count);
                aux_decon_data_predicted.push_back(std::make_shared<DataRowContainer>(p_aux_decon_queue_predicted->get_data()));
            }
            try {
                // predict ensembles
                AppContext::get_instance().model_service.predict(
                        p_ensemble->get_models(),
                        bpt::time_period(start_predict_time, end_predict_time),
                        p_input_queue->get_resolution(),
                        p_dataset->get_max_lookback_time_gap(),
                        p_decon_queue_predicted->get_data(),
                        aux_decon_data_predicted
                );
            } catch (const std::runtime_error &ex) {
                LOG4_ERROR("Predicting request failed. " << ex.what());
                request_answered = false;
                continue;
            }

            // unscale decon queue
            AppContext::get_instance().dq_scaling_factor_service.scale(p_dataset, true, p_ensemble);
            DataRow::Container::iterator end_frame_iter = p_ensemble->get_decon_queue()->get_data().find(end_predict_time);
            DataRow::Container::iterator start_frame_iter = p_ensemble->get_decon_queue()->get_data().find(start_predict_time);
            if (end_frame_iter == p_ensemble->get_decon_queue()->get_data().end() || start_frame_iter == p_ensemble->get_decon_queue()->get_data().end()) {
                LOG4_ERROR("Couldn't find predicted values! Aborting.");
                request_answered = false;
                continue;
            }

            auto dist = std::distance(start_frame_iter, end_frame_iter);
            if (dist < off_t(frame_size)) std::advance(start_frame_iter, dist - frame_size);
            DataRow::Container reconstructed_data = AppContext::get_instance().decon_queue_service.reconstruct(
                    datarow_range(start_frame_iter, end_frame_iter, p_ensemble->get_decon_queue()->get_data()),
                    p_dataset->get_swt_wavelet_name(),
                    swt_levels_to_frame_length(p_dataset->get_swt_levels())
            );
            if (reconstructed_data.empty())
            {
                LOG4_ERROR("Empty reconstructed data.");
                request_answered = false;
                continue;
            }

            for_each(reconstructed_data.begin(), reconstructed_data.end(), save_response(value_column, p_active_request));
            request_answered &= true;
        }

        if (request_answered) AppContext::get_instance().request_service.force_finalize(p_active_request);
        else LOG4_WARN("Failed answering request with id " << p_active_request->get_id());
    }
}

void DaemonFacade::prepare_queue(
        const Dataset_ptr &p_dataset,
        const ptime &start_predict_time,
        const ptime &end_predict_time,
        DeconQueue_ptr &p_decon_queue,
        const size_t predict_count) const
{
    const size_t frame_size = swt_levels_to_frame_length(p_dataset->get_swt_levels());
    auto p_input_queue = AppContext::get_instance().input_queue_service.get_queue_metadata(p_decon_queue->get_input_queue_table_name());
    p_input_queue->set_data(
            AppContext::get_instance().input_queue_service.get_queue_data(
                    p_decon_queue->get_input_queue_table_name(), start_predict_time, end_predict_time));
    p_input_queue->update_data(AppContext::get_instance().input_queue_service.get_latest_queue_data(
                    p_input_queue->get_table_name(),
                    2 * frame_size + predict_count + 1, // TODO remove 2 * frame size
                    start_predict_time),
                               false); /* don't overwrite already existing data */

    DataRow::Container decon_data;
    AppContext::get_instance().decon_queue_service.deconstruct(
            p_input_queue, p_dataset, p_decon_queue->get_input_queue_column_name(), decon_data);
    p_decon_queue->update_data(decon_data, false);

    (void) find_nearest_before(p_decon_queue->get_data(), start_predict_time - p_input_queue->get_resolution(), p_dataset->get_max_lookback_time_gap());
}


bool process_ensemble(const Dataset_ptr &p_dataset, InputQueue_ptr &p_input_queue,
                                    Ensemble_ptr &p_ensemble)
{
    LOG4_DEBUG("Training ensemble for column " << p_ensemble->get_decon_queue()->get_input_queue_column_name() <<
               " of input queue " << p_ensemble->get_decon_queue()->get_input_queue_table_name());

    // deconstruct InputQueue columns
    p_ensemble->get_decon_queue()->update_data(
            AppContext::get_instance().decon_queue_service.deconstruct(
                    p_input_queue,
                    p_dataset,
                    p_ensemble->get_decon_queue()->get_input_queue_column_name())->get_data());

    // scale decon queues
    AppContext::get_instance().dq_scaling_factor_service.scale(p_dataset, false, p_ensemble);

    // TODO: synchronize threads here when auxiliary columns are implemented
    // train ensemble
    AppContext::get_instance().ensemble_service.train(
            p_ensemble,
            p_dataset->get_ensembles_svr_parameters()[p_ensemble->get_key_pair()],
            p_dataset->get_max_lookback_time_gap());
    /*
     * TODO Enable save and trim after ASYNC DB code confirmed working.
    AppContext::get_instance().decon_queue_service.save(ensemble->get_decon_queue());
     *  Trim unneeded data when we save decon queues. Until then leave commented out. Maybe don't use trimming at all.
    p_dataset->get_ensembles()[ix]->get_decon_queue()->trim(ensemble_train_start_times[ix], ensemble_train_end_times[ix]);
     */
    return true;
}

// TODO Rewrite and reduce complexity
void DaemonFacade::process_dataset(Dataset_ptr& p_dataset)
{
    LOG4_DEBUG("Processing dataset " << p_dataset->get_id());

    if (!p_dataset->get_input_queue())
        p_dataset->set_input_queue(AppContext::get_instance().input_queue_service.get_queue_metadata(
                                       p_dataset->get_input_queue()->get_table_name()));

    if (p_dataset->get_ensembles().empty())
    {
        LOG4_DEBUG("Dataset ensembles empty. Initializing.");
        // create ensembles with deconstructed data (true argument)
        AppContext::get_instance().dataset_service.load_ensembles(p_dataset);
        // p_dataset->set_ensembles(AppContstart_predict_time - p_input_queue->get_resolutionext::get_instance().ensemble_service.init_ensembles_from_dataset(p_dataset));
        /* TODO Enable after ASYNC fully implemented and tested
        AppContext::ensemble_service.save_ensembles(p_dataset->get_ensembles());
        */
    }
    if (p_dataset->get_ensembles().empty()) {
        LOG4_ERROR("No ensembles for dataset.");
        return;
    }
    size_t frame_size = swt_levels_to_frame_length(p_dataset->get_swt_levels());

    /* Initialize input queue */
    InputQueue_ptr p_input_queue {p_dataset->get_input_queue()};

    /* Start time and end time are used to trim decon queues after deconstructing
     * a whole frame further below in this method.
     */
    std::vector<bpt::ptime> ensemble_train_start_times(p_dataset->get_ensembles().size());
    std::vector<bpt::ptime> ensemble_train_end_times(p_dataset->get_ensembles().size());
    const bpt::ptime input_queue_newest_value_time = AppContext::get_instance().input_queue_service.find_newest_record(p_input_queue)->get_value_time();

    std::vector<Ensemble_ptr> &dataset_ensembles = p_dataset->get_ensembles();
    /* Determine start and end training time range for every ensemble based on every model parameters. */
    for (std::vector<Ensemble_ptr>::size_type ix_ens = 0; ix_ens < dataset_ensembles.size(); ++ix_ens)
    {
        std::vector<SVRParameters_ptr> ensemble_parameters =
                p_dataset->get_ensembles_svr_parameters()[dataset_ensembles[ix_ens]->get_key_pair()];

        if (!AppContext::get_instance().ensemble_service.is_ensemble_input_queue(dataset_ensembles[ix_ens], p_input_queue) || ensemble_parameters.empty()) {
            LOG4_ERROR("SVR parameters for ensemble " << ix_ens << " are not present. Skipping gathering of train data.");
            ensemble_train_start_times[ix_ens] = ensemble_train_end_times[ix_ens] = input_queue_newest_value_time;
            continue;
        }

        size_t largest_decrement_distance_decon_queue = 0;
        size_t largest_lag_count = 0;
        for (auto &p_svr_parameters : ensemble_parameters) {
            if (p_svr_parameters->get_svr_decremental_distance() > largest_decrement_distance_decon_queue)
                largest_decrement_distance_decon_queue = p_svr_parameters->get_svr_decremental_distance();
            if (p_svr_parameters->get_lag_count() > largest_lag_count)
                largest_lag_count = p_svr_parameters->get_lag_count();
        }

        bpt::ptime ensemble_last_modeled_value_time;
        if (dataset_ensembles[ix_ens]->get_models().empty()) { /* Initial train case */
            ensemble_train_start_times[ix_ens] = input_queue_newest_value_time - p_input_queue->get_resolution() * (largest_decrement_distance_decon_queue + largest_lag_count);
            ensemble_train_end_times[ix_ens] = input_queue_newest_value_time;
        } else if ((ensemble_last_modeled_value_time = dataset_ensembles[ix_ens]->get_models()[0]->get_last_modeled_value_time()) == input_queue_newest_value_time) { /* No new values */
            /* Model is 0 because all models in an ensemble should have the same last modeled value time */

            LOG4_WARN("No new data in input queue to process for ensemble " << ix_ens);
            ensemble_train_start_times[ix_ens] = ensemble_train_end_times[ix_ens] = input_queue_newest_value_time;
        } else { /* Retrain existing models */
            ensemble_train_start_times[ix_ens] = ensemble_last_modeled_value_time - p_input_queue->get_resolution() * largest_lag_count;
            ensemble_train_end_times[ix_ens] = input_queue_newest_value_time;
        }
    }

    bpt::ptime oldest_start_train_time = *std::min_element(ensemble_train_start_times.begin(), ensemble_train_start_times.end());
    bpt::time_duration longest_training_period = input_queue_newest_value_time - oldest_start_train_time;
    if (oldest_start_train_time == input_queue_newest_value_time)
    {
        LOG4_INFO("No new data in input queue for any ensemble " << p_input_queue->get_logical_name() << " to process. Aborting.");
        return;
    }

    size_t input_queue_data_limit = std::max(static_cast<size_t>(longest_training_period.total_seconds() / p_input_queue->get_resolution().total_seconds()), frame_size);

    LOG4_DEBUG("Retrieving last " << input_queue_data_limit << " values from input queue " << p_input_queue->get_table_name() << " input queue last value time " <<
               input_queue_newest_value_time << " oldest start train time " << oldest_start_train_time);

    p_input_queue->set_data(AppContext::get_instance().input_queue_service.get_latest_queue_data(
            p_input_queue->get_table_name(),
            input_queue_data_limit));
    /* TODO Implement getting data for aux input queues */

    if(p_input_queue->get_data().size() < frame_size)
    {
        LOG4_WARN("Not enough data " << p_input_queue->get_data().size() << " values in input queue " << p_input_queue->get_logical_name() << " to process. Aborting.");
        return;
    }

    // Process columns and ensembles
    std::vector<std::future<bool>>  process_ensemble_futures;

    for (Ensemble_ptr &p_ensemble: p_dataset->get_ensembles())
        if (AppContext::get_instance().ensemble_service.is_ensemble_input_queue(p_ensemble, p_input_queue))
            process_ensemble_futures.push_back(std::async(
                    std::launch::async, process_ensemble, std::ref(p_dataset), std::ref(p_input_queue), std::ref(p_ensemble)));

    for (auto &f: process_ensemble_futures) f.get();
    // AppContext::get_instance().ensemble_service.save_ensembles(p_dataset->get_ensembles(), false);

    LOG4_END();
}


void DaemonFacade::start_loop()
{
    // configure the daemon
    if (daemonize)
    {
        do_fork();
        LOG4_INFO("Daemon process started");
    }

    syslog(LOG_NOTICE, "Daemon loop started");
    svr::business::DatasetService::UserDatasetPairs datasets;

    while(continue_loop())
    {
        AppContext::get_instance().dataset_service.update_active_datasets(datasets);

        for(svr::business::DatasetService::DatasetUsers &dsu: datasets)
        {
            process_dataset(dsu.dataset);

            for(User_ptr &user: dsu.users)
                process_multival_requests(user, dsu.dataset);
        }
    }
}

bool DaemonFacade::continue_loop() {
    // wait some time before next iteration
    this_thread::sleep_for(std::chrono::milliseconds(loop_interval));
    if(max_loop_count < 0) return true;
    return (loop_count++) < max_loop_count ;
}


}
}
