#pragma once

#include "model/User.hpp"
#include "model/InputQueue.hpp"
#include "model/Dataset.hpp"

namespace svr{
namespace daemon{

class DaemonFacade {

private:
    void initialize(const std::string &app_properties_path);
    bool continue_loop();
    void do_fork();

    void process_multival_requests(const User_ptr& user, const Dataset_ptr& dataset);
    void process_dataset(Dataset_ptr& dataset);
//    bool process_ensemble(const Dataset_ptr &p_dataset, InputQueue_ptr &p_input_queue, Ensemble_ptr &p_ensemble);

public:
    DaemonFacade(const std::string &app_properties_path);
    void start_loop();

private:
    bool daemonize;
    long loop_interval;
    long loop_count;
    long max_loop_count;


    static std::string S_MAX_LOOP_COUNT;
    static std::string S_DECONSTRUCTING_FRAME_SIZE;
    static std::string S_FILL_MISSING_QUEUE_VALUES;
    static std::string S_LOOP_INTERVAL;
    static std::string S_DAEMONIZE;

    void prepare_queue(
            const Dataset_ptr &p_dataset,
            const boost::posix_time::ptime &start_predict_time,
            const boost::posix_time::ptime &end_predict_time,
            DeconQueue_ptr &p_decon_queue,
            const size_t predict_count) const;
};

}
}

