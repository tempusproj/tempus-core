#include <util/TimeUtils.hpp>

using namespace std;

namespace svr {
namespace common {

const std::vector<size_t> sec_per_unit {24*60*60, 60*60, 60, 1}; // day, hour, minute, second

bpt::seconds date_time_string_to_seconds(const string &date_time)
{
    boost::char_separator<char> separator(",: ");
    boost::tokenizer<boost::char_separator<char>> tokens(date_time, separator);

    size_t time_period {0};
    std::vector<size_t>::const_iterator i_sec_per_unit {sec_per_unit.cbegin()};
    for(auto& t:tokens)
        time_period += stoll(t) * (*i_sec_per_unit++);

    if(i_sec_per_unit != sec_per_unit.cend())
        throw std::runtime_error("incorrect date_time " + date_time);

    return bpt::seconds(time_period); /* TODO in future move code to milliseconds once FIX is here */
}

bpt::time_period adjust_time_period_to_frame_size(const bpt::time_period & time_range,
                                                  const bpt::time_duration & resolution,
                                                  const size_t frame_size)
{
    return bpt::time_period(
                time_range.begin(),
                time_range.end() +
                bpt::minutes(frame_size -
                             time_range.length().total_seconds() / resolution.total_seconds() % frame_size));
}


} // namespace common
} // namespace svr

