#include <cmath>
#include "common/types.hpp"

namespace svr{
namespace common{

size_t frame_size_to_swt_levels(size_t input_len)
{
    int i, j;
    i = (int) floor(log((double) input_len) / log(2.0));

    // check how many times (maximum i times) input_len is divisible by 2
    for (j = 0; j <= i; ++j) {
        if ((input_len & 0x1) == 1) return j;
        input_len >>= 1;
    }
    return (i > 0) ? i : 0;
}

size_t get_lower_swt_level(size_t level, size_t neighbour_levels_count){

    size_t half_count = neighbour_levels_count / 2;

    if(level == 0 || half_count == 0){
        return level;
    }
    return level - half_count;
}

size_t get_higher_swt_level(size_t level, size_t neighbour_levels_count, size_t total_levels){

    size_t half_count = neighbour_levels_count / 2;

    if(level == 0 || half_count == 0){
        return level;
    }

    if(level + half_count <= total_levels){
        return level + half_count;
    }

    return total_levels;
}

std::vector<size_t> get_adjacent_indexes(size_t level, double ratio, size_t level_count){

    if (level_count == 0/* || level == 0*/) //in case of 0 ajdacent ratio
    {
        return std::vector<size_t>{level};
    }

    std::vector<size_t> level_indexes;

    size_t sum_count = static_cast<size_t>(level_count * ratio);
    size_t half_count = static_cast<size_t>(sum_count / 2.);
    int min_index = int (std::max(int(level) - int(half_count), 1));
    int max_index = int (std::min(int(level) + int(half_count), int(level_count) - 1));
    for(; min_index <= max_index; ++min_index)
        level_indexes.push_back((size_t) min_index);
    return level_indexes;

}

size_t swt_levels_to_frame_length(size_t swt_levels) {
    size_t level = 0, result = 1;
    while (level++ < swt_levels)
        result *= 2;
    return result;
}

bool is_power_of_two(size_t value)
{
    return !(value == 0) && !(value & (value - 1));
}

double get_uniform_random_value()
{
    return double(rand()) /  RAND_MAX;
}

std::vector<double> get_uniform_random_vector(const size_t size)
{
    std::vector<double> result(size);
    for_each(result.begin(), result.end(), [](double& val) {val = get_uniform_random_value();});
    return result;
}

std::vector<double> get_uniform_random_vector(const std::pair<std::vector<double>, std::vector<double> > &boundaries)
{
    std::vector<double> random_vector {get_uniform_random_vector(boundaries.first.size())};
    for(size_t i = 0; i < boundaries.first.size() && i < boundaries.second.size(); ++i)
        random_vector[i] = random_vector[i] * (boundaries.second[i] - boundaries.first[i]) + boundaries.first[i];

    return random_vector;
}

std::vector<double> operator*(const std::vector<double> &v1, const double &m) {
    std::vector<double> ret(v1.size());
    transform(v1.begin(), v1.end(), ret.begin(),
              [&m](double val) -> double { return val * m; });
    return ret;
}

std::vector<double> operator*(const double &m, const std::vector<double> &v1) {
    return v1 * m;
}

std::vector<double> operator*(const std::vector<double> &v1, const std::vector<double> &v2) {
    assert(v1.size() == v2.size());
    std::vector<double> ret(v1.size());
    transform(v1.begin(), v1.end(), v2.begin(), ret.begin(),
              [](double val1, double val2) -> double { return val1 * val2; });
    return ret;
}

std::vector<double> operator+(const std::vector<double> &v1, const std::vector<double> &v2) {
    assert(v1.size() == v2.size());
    std::vector<double> ret(v1.size());
    transform(v1.begin(), v1.end(), v2.begin(), ret.begin(),
              [](double val1, double val2) -> double { return val1 + val2; });
    return ret;
}

std::vector<double> operator-(const std::vector<double> &v1, const std::vector<double> &v2) {
    assert(v1.size() == v2.size());
    std::vector<double> ret(v1.size());
    transform(v1.begin(), v1.end(), v2.begin(), ret.begin(),
              [](double val1, double val2) -> double { return val1 - val2; });
    return ret;
}

std::vector<double> operator-(const std::vector<double> &v)
{
    std::vector<double> result {v};
    std::for_each(result.begin(), result.end(), [](double& x) {x *= -1.0;});

    return result;
}

std::vector<double> operator^(const std::vector<double> &v, const double a)
{
    assert(a >= 0.0);
    std::vector<double> result {v};
    std::for_each(result.begin(), result.end(), [=](double& x) {x = std::pow(x, a);});

    return result;
}

std::vector<double> operator^(const double a, const std::vector<double> &v)
{
    assert(a >= 0.0);
    std::vector<double> result {v};
    std::for_each(result.begin(), result.end(), [=](double& x) {x = std::pow(a, x);});

    return result;
}

}
}
