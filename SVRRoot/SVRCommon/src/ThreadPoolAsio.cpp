#include <util/ThreadPoolAsio.hpp>

namespace svr {
namespace common {


ThreadPoolAsio::ThreadPoolAsio(size_t size)
    : size_(size), work_(io_service_)
{
}

ThreadPoolAsio::~ThreadPoolAsio()
{
    stop();
}

void ThreadPoolAsio::start()
{
    for (size_t i = 0; i < size_; ++i) {
        increment();
    }
}

void ThreadPoolAsio::increment()
{
    grp_.create_thread(boost::bind(&boost::asio::io_service::run, &io_service_));
}

void ThreadPoolAsio::stop()
{
    io_service_.stop();
    grp_.join_all();
}

size_t ThreadPoolAsio::size() const
{
    return grp_.size();
}


} // namespace common
} // namespace svr