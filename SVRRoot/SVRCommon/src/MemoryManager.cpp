#include <util/MemoryManager.hpp>
#include <common/Logging.hpp>
#include <chrono>
#include <thread>
#include <stdio.h>
#include <sys/sysinfo.h>
#include <unistd.h>

namespace svr{
namespace common{

std::mutex memory_manager::lock;
bool memory_manager::mem_available;
std::condition_variable memory_manager::mem_cv;
memory_manager_state memory_manager::m_state;

void memory_manager::check_ram_memory()
{
    mem_available = true;
    m_state = memory_manager_state::WORK;

    while (true)
    {
        struct sysinfo mem_info;
        sysinfo(&mem_info);

        double ram_left = static_cast<double>(mem_info.freeram) / mem_info.totalram;
        if ((ram_left < MIN_RAM_THRESH && mem_available) || (ram_left >= MIN_RAM_THRESH && !mem_available))
        {
          mem_available = !mem_available;
          LOG4_INFO("changed mem_available to " << mem_available << " ram_left" << ram_left << " free ram [GB]" << static_cast<double>(mem_info.freeram)/mem_info.mem_unit/GB_RAM_UNIT_DIVIDER << " / total ram [GB]" << static_cast<double>(mem_info.totalram)/mem_info.mem_unit/GB_RAM_UNIT_DIVIDER);
          mem_cv.notify_all();
        }

        if(memory_manager_state::SHUTDOWN == m_state)
            break;

        std::this_thread::sleep_for(std::chrono::seconds(MEM_CHECK_INTERVAL));
    }
}

}
}
