#include "util/PropertiesFileReader.hpp"
#include "common/Logging.hpp"
#include <boost/algorithm/string/predicate.hpp>
#include "util/StringUtils.hpp"

using namespace std;

// this is inherited dependency for logging output level
int SVR_LOG_LEVEL;

namespace svr {
namespace common {

const string PropertiesFileReader::SQL_PROPERTIES_DIR_KEY = "SQL_PROPERTIES_DIR";
const string PropertiesFileReader::LOG_LEVEL_KEY = "LOG_LEVEL";
const string PropertiesFileReader::DAO_TYPE_KEY = "DAO_TYPE";
const string PropertiesFileReader::COMMENT_CHARS = "#";
const string PropertiesFileReader::DONT_UPDATE_R_MATRIX = "DONT_UPDATE_R_MATRIX";
const string PropertiesFileReader::MAIN_COLUMNS_AUX = "MAIN_COLUMNS_AUX";
const string PropertiesFileReader::MAX_SMO_ITERATIONS = "MAX_SMO_ITERATIONS";
const string PropertiesFileReader::CASCADE_REDUCE_RATIO = "CASCADE_REDUCE_RATIO";
const string PropertiesFileReader::MAX_SEGMENT_SIZE = "MAX_SEGMENT_SIZE";


size_t PropertiesFileReader::read_property_file(string property_file_name)
{
    LOG4_DEBUG("Reading properties from file: " << property_file_name);
	MessageProperties::mapped_type params;

	ifstream is_file(property_files_location + property_file_name);

	if(!is_file.is_open()) {
		throw std::invalid_argument("Cannot read properties file: " + property_files_location + property_file_name);
	}
	string line;
	string multi_line;
	bool is_multi_line = false;

	while (getline(is_file, line)) {
		trim(line);
		if(line.size() == 0) {
			is_multi_line = false;
			multi_line.clear();
			continue;
		};

		if(is_comment(line)){
			continue;
		}

		if(is_multiline(line)){
			is_multi_line = true;
			multi_line += line.substr(0, line.size()-1);
			continue;
		}

		if(is_multi_line){ // final line of multiline property
			line = multi_line + line;
			is_multi_line = false;
			multi_line.clear();
		}
		istringstream is_line(line);
		string key;
		if (getline(is_line, key, this->delimiter)) {
			string value;
			if (getline(is_line, value)){
				params[trim(key)] = trim(value);
			}
		}
	}

	size_t items = params.size();
	this->property_files[property_file_name] = params;
	LOG4_DEBUG("Read total of " << items << " from property file " << property_files_location << property_file_name);
	return items;
}

// TODO Move hardcoded values to header file
PropertiesFileReader::PropertiesFileReader(const string& app_config_file, char delimiter):
		delimiter(delimiter), dao_type(ConcreteDaoType::PgDao)
{
	read_property_file(app_config_file);

	property_files_location = get_property<string>(app_config_file, SQL_PROPERTIES_DIR_KEY);

	set_global_log_level(get_property<string>(app_config_file, LOG_LEVEL_KEY));

	std::string sdao_type = get_property<std::string>(app_config_file, DAO_TYPE_KEY);
	if (sdao_type == "async") dao_type = ConcreteDaoType::AsyncDao;

	dont_update_r_matrix_ = get_property<bool>(app_config_file, DONT_UPDATE_R_MATRIX, "0");

    main_columns_aux_ = get_property<bool>(app_config_file, MAIN_COLUMNS_AUX, "0");

	max_smo_iterations_ = get_property<size_t>(app_config_file, MAX_SMO_ITERATIONS, "300000");

	cascade_reduce_ratio_ = get_property<double>(app_config_file, CASCADE_REDUCE_RATIO, "0.6");

	max_segment_size_ = get_property<size_t>(app_config_file, MAX_SEGMENT_SIZE, "2000");
}

ConcreteDaoType PropertiesFileReader::get_dao_type() const
{
    return dao_type;
}

const MessageProperties::mapped_type& PropertiesFileReader::read_properties(const string &property_file)
{
    if(this->property_files.count(property_file) || read_property_file(property_file))
        return this->property_files[property_file];
    static MessageProperties::mapped_type empty;
    return empty;
}


void PropertiesFileReader::set_global_log_level(const std::string &log_level_value)
{
    if(strcasecmp(log_level_value.c_str(), "ALL") == 0)
        SVR_LOG_LEVEL = LOG_LEVEL_T::TRACE;
    else if(strcasecmp(log_level_value.c_str(), "TRACE") == 0)
        SVR_LOG_LEVEL = LOG_LEVEL_T::TRACE;
    else if(strcasecmp(log_level_value.c_str(), "DEBUG") == 0)
        SVR_LOG_LEVEL = LOG_LEVEL_T::DEBUG;
    else if(strcasecmp(log_level_value.c_str(), "INFO") == 0)
        SVR_LOG_LEVEL = LOG_LEVEL_T::INFO;
    else if(strcasecmp(log_level_value.c_str(), "WARN") == 0)
        SVR_LOG_LEVEL = LOG_LEVEL_T::WARN;
    else if(strcasecmp(log_level_value.c_str(), "ERROR") == 0)
        SVR_LOG_LEVEL = LOG_LEVEL_T::ERR;
    else if(strcasecmp(log_level_value.c_str(), "FATAL") == 0)
        SVR_LOG_LEVEL = LOG_LEVEL_T::FATAL;
}

bool PropertiesFileReader::is_comment(const std::string& line)
{
	return boost::starts_with(line, COMMENT_CHARS);
}

bool PropertiesFileReader::is_multiline(const std::string &line)
{
	if(line.size() == 0){
		return false;
	}

	bool even_slash_count = true;
	auto c = line.rbegin();
	while(c != line.rend() && *c == '\\'){
		even_slash_count = !even_slash_count;
		c++;
	}
	return !even_slash_count;
}

std::string PropertiesFileReader::get_property_value(
		const std::string &property_file, const std::string &key, std::string default_value)
{
	LOG4_BEGIN();

	if(this->property_files.count(property_file) == 0 && read_properties(property_file).size() == 0) return default_value;
	if(this->property_files.count(property_file) && this->property_files[property_file].count(key))
		default_value = this->property_files[property_file][key];
	LOG4_TRACE("Found property " << property_file << " " << key << " is " << default_value);

	LOG4_END();

	return default_value;
}
} /* namespace common */
} /* namespace svr */
