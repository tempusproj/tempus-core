#pragma once

#include "common/types.hpp"
#include "numeric"

namespace svr{
namespace common{

size_t swt_levels_to_frame_length(size_t swt_levels);

size_t frame_size_to_swt_levels(size_t input_len);

size_t get_lower_swt_level(size_t level, size_t neighbour_levels_count);

size_t get_higher_swt_level(size_t level, size_t neighbour_levels_count, size_t total_levels);

std::vector<size_t> get_adjacent_indexes(size_t level, double ratio, size_t level_count);

bool is_power_of_two(size_t value);

#define INF 9.9e99

template<class T>
T ABS(T X) {
    if (X >= 0)
        return X;
    else
        return -X;
}

template<class T>
T SIGN(T X) {
    if (X >= 0)
        return 1;
    else
        return -1;
}

double get_uniform_random_value();

std::vector<double> get_uniform_random_vector(const size_t size);

// TODO: rewrite with iterators
std::vector<double> get_uniform_random_vector(const std::pair<std::vector<double>, std::vector<double>>& boundaries);


// TODO: rewrite with template
template<typename T>
std::vector<size_t> argsort(const std::vector<T>& v)
{
    std::vector<size_t> result (v.size());
    std::iota(result.begin(), result.end(), 0);

    std::sort(result.begin(), result.end(), [&](size_t i, size_t j) {return v[i] < v[j];});

    return result;
}

std::vector<double> operator* (const std::vector<double>& v1, const double& m);

std::vector<double> operator* (const double& m, const std::vector<double>& v1);

std::vector<double> operator* (const std::vector<double>& v1, const std::vector<double>& v2);

std::vector<double> operator+ (const std::vector<double>& v1, const std::vector<double>& v2);

std::vector<double> operator- (const std::vector<double>& v1, const std::vector<double>& v2);

std::vector<double> operator-(const std::vector<double>& v);

std::vector<double> operator^(const std::vector<double>& v, const double a);

std::vector<double> operator^(const double a, const std::vector<double>& v);

}
}
