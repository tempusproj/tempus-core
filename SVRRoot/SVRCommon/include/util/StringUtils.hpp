#pragma once

#include <string>
#include <algorithm>
#include "common/types.hpp"

namespace svr {
namespace common {

static const std::string dd_separator {".."};
static const std::string cm_separator {","};

static inline std::string &ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
    return s;
}

// trim from end
static inline std::string &rtrim( std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
    return s;
}

// trim from both ends
static inline std::string &trim(std::string &s) {
    return ltrim(rtrim(s));
}

static inline std::string tolower(std::string str) {
    std::transform(str.begin(), str.end(), str.begin(), ::tolower);
    return str;
}

static inline std::string toupper(std::string str){
    std::transform(str.begin(), str.end(), str.begin(), ::toupper);
    return str;
}

template<typename T>
static inline std::string deep_to_string(const std::vector<T>& v){

    if(v.size() == 0){
        return "";
    }

    std::stringstream ss;

    for(size_t i = 0; i < v.size()-1; i++){
        ss << v[i] << ", ";
    }

    ss << v[v.size()-1];

    return ss.str();
}

std::vector<std::string> split(std::string str, std::string regex);

std::vector<std::string> from_sql_array(std::string array_str);

std::string demangle(const char *mangled);

inline bool ignoreCaseEquals(const std::string& lhs, const std::string &rhs){
    return tolower(lhs) == tolower(rhs);
}

std::string make_table_identifier(std::string toBeIdentifier, char replaceChar = '_');

std::string make_md5_hash(const std::string& in);

std::string to_mt4_date(const bpt::ptime& time);

std::map<std::string, std::string> json_to_map(const std::string &json_str);

std::string map_to_json(const std::map<std::string, std::string> &value);

std::vector<size_t> parse_string_range(const std::string& parameter_string);
std::vector<std::string> parse_string_range(const std::string& parameter_string, const std::vector<std::string>& set_parameters);


} // namespace common
} // namespace svr
