#pragma once

#include <common/types.hpp>

namespace svr {
namespace common {

enum class ConcreteDaoType{ PgDao, AsyncDao };

class PropertiesFileReader
{

	MessageProperties property_files;
	char delimiter;
	std::string property_files_location;
	ConcreteDaoType dao_type;
	bool dont_update_r_matrix_;
	bool main_columns_aux_;
	size_t max_smo_iterations_;
	double cascade_reduce_ratio_;
	size_t max_segment_size_;

	size_t read_property_file(std::string property_file_name);
	void set_global_log_level(const std::string& log_level_value);
	bool is_comment(const std::string& line);
	bool is_multiline(const std::string& line);

	std::string get_property_value(const std::string &property_file, const std::string &key, std::string default_value);

public:
	virtual ~PropertiesFileReader(){}

	explicit PropertiesFileReader(const std::string& app_config_file, char delimiter = '=');
	const MessageProperties::mapped_type& read_properties(const std::string &property_file) ;

	template<typename T>
	T get_property(const std::string &property_file, const std::string &key, std::string default_value = "")
    {
        return boost::lexical_cast<T>(get_property_value(property_file, key, default_value));
	}

	ConcreteDaoType get_dao_type() const;

	bool get_dont_update_r_matrix() const { return dont_update_r_matrix_; }
	bool get_main_columns_aux() const { return main_columns_aux_; }
	size_t get_max_smo_iterations() const { return max_smo_iterations_; }
	double get_cascade_reduce_ratio() const { return cascade_reduce_ratio_; }
	size_t get_max_segment_size() const { return max_segment_size_; }

private:
    static const std::string SQL_PROPERTIES_DIR_KEY;
    static const std::string LOG_LEVEL_KEY;
    static const std::string COMMENT_CHARS;
    static const std::string DAO_TYPE_KEY;
	static const std::string DONT_UPDATE_R_MATRIX;
	static const std::string MAIN_COLUMNS_AUX;
	static const std::string MAX_SMO_ITERATIONS;
	static const std::string CASCADE_REDUCE_RATIO;
	static const std::string MAX_SEGMENT_SIZE;
};

} /* namespace common */
} /* namespace svr */

using MessageSource_ptr = std::shared_ptr<svr::common::PropertiesFileReader>;
