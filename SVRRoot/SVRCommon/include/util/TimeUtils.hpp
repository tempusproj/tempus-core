#pragma once

#include "common/types.hpp"

namespace svr {
namespace common {

boost::posix_time::seconds date_time_string_to_seconds(const std::string & date_time);

bpt::time_period adjust_time_period_to_frame_size(const bpt::time_period & time_range,
                                                  const bpt::time_duration & resolution,
                                                  const size_t frame_size);

} // namespace common
} // namespace svr
