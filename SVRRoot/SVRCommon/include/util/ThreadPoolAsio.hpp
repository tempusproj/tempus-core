/* 
 * File:   ThreadPoolAsio.hpp
 * Author: S.Georgiev
 *
 * Thread Pool using boost ASIO
 * 
 * Created on August 30, 2016, 12:01 PM
 */

#pragma once

#include <boost/asio.hpp>
#include <boost/thread/thread.hpp>

#include <atomic>
#include <functional>
#include <future>
#include <memory>

namespace svr {
namespace common {

class ThreadPoolAsio {
public:
    explicit ThreadPoolAsio(size_t size);
    virtual ~ThreadPoolAsio();

    void start();
    void stop();

    void increment();

    size_t size() const;

    template<class T>
    std::future<T> post(std::function<T()> fn) {
        std::shared_ptr<std::promise<T> > prom(new std::promise<T>());
        
        io_service_.post([prom, fn]() -> void {
            prom->set_value(fn());
        });
        
        return prom->get_future();
    }

private:
    size_t size_;
    boost::thread_group grp_;
    boost::asio::io_service io_service_;
    boost::asio::io_service::work work_;
};


} // namespace common
} // namespace svr
