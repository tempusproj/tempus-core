#ifndef MEMORYMANAGER_HPP
#define	MEMORYMANAGER_HPP

#include <mutex>
#include <condition_variable>

namespace svr{
namespace common{
    
#define MIN_RAM_THRESH 0.05
#define MEM_CHECK_INTERVAL 5
#define GB_RAM_UNIT_DIVIDER (1024*1024*1024)

enum class memory_manager_state { WORK, SHUTDOWN };

struct memory_manager
{
    static bool mem_available;
    static std::mutex lock;
    static std::condition_variable mem_cv;
    static memory_manager_state m_state;
    static void check_ram_memory();
};
}
}

#endif	/* MEMORYMANAGER_HPP */

