#pragma once

#include <map>
#include <string>
#include <vector>
#include <boost/date_time.hpp>

using bigint = uint64_t;
using MessageProperties = std::map<std::string, std::map<std::string, std::string>>;

namespace bpt = boost::posix_time;
