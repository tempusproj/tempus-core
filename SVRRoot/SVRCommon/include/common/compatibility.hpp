/*
 * memory.hpp
 *
 *  Created on: Jul 27, 2014
 *      Author: vg
 */

#pragma once

#include <cmath>
#include <limits>
#include <memory>
#include <sstream>
#include <vector>
#include <map>

namespace std{

template<typename T, typename ...Args>
std::unique_ptr<T> make_unique( Args&& ...args )
{
    return std::unique_ptr<T>( new T( std::forward<Args>(args)... ) );
}

#ifdef __CYGWIN__
template<typename T>
string to_string(const T& obj){
    stringstream ss;
    ss << obj;
    return ss.str();
}
#endif
} // namespace std

namespace svr{
namespace common{

void print_stacktrace();

#define ELEMCOUNT(array) (sizeof(array)/sizeof(array[0]))


/* Return codes */
typedef size_t uint_rc_t;
#define SVR_RC_GENERAL_ERROR (std::numeric_limits<size_t>::max())


const long multi_div = 100000; // number of zeros tell the number of decimal points to assume when comparing doubles

// this is needed because there are some issues when comparing double values
// @see http://stackoverflow.com/questions/17333/most-effective-way-for-float-and-double-comparison
bool Equals(const double& lhs, const double& rhs);

double Round(const double& dbl);

template<typename T> std::vector<std::shared_ptr<T>> inline
clone_shared_ptr_elements(const std::vector<std::shared_ptr<T>> &arg)
{
    std::vector<std::shared_ptr<T>> res;
    for (const std::shared_ptr<T> &p_elem: arg) res.push_back(std::make_shared<T>(*p_elem));
    return res;
}

template<typename K, typename T> std::map<K, std::shared_ptr<T>> inline
clone_shared_ptr_elements(const std::map<K, std::shared_ptr<T>> &arg)
{
    std::map<K, std::shared_ptr<T>> res;
    for (const auto &pair: arg) res.emplace(K(pair.first), std::make_shared<T>(*pair.second));
    return res;
}

} // namespace common
} // namespace svr

