/*
 * common.hpp
 *
 *  Created on: Jul 27, 2014
 *      Author: vg
 */

#pragma once

// common external dependencies
#include <iostream>
#include <fstream> // file stream
#include <sstream> // (i/o)stringstream
#include <memory> // for shared and unique pointers
#include <typeinfo>

// common internal dependencies
#include "common/compatibility.hpp" // defines missing c++11 features or replacement for another
#include "common/Logging.hpp"
#include "common/constants.hpp"
#include "common/types.hpp"
#include "common/exceptions.hpp"

// boost
#include <boost/lexical_cast.hpp>



