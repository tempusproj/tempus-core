/*
 * Logging.hpp
 *
 *  Created on: Jul 27, 2014
 *      Author: vg
 */

#pragma once

#include <boost/date_time.hpp>
#include <boost/format.hpp>
#include <thread>

class Out {
public:
    Out(const std::string &fileName) {
        this->fileName = fileName;
        std::ofstream f;
        f.open(fileName, std::ofstream::out | std::ofstream::trunc);
        f.close();
    }
    template<class T>
    Out& operator<<(T value)
    {
        std::ofstream f;
        f.open(fileName, std::ios_base::app);
        f << value;
        f.close();
        return *this;
    }
    template<class T>
    void rewrite(T value) {
        std::ofstream f;
        f.open(fileName);
        f << value;
        f.close();
    }

private:
    std::string fileName;
};

namespace bpt = boost::posix_time;

#define FILE_NAME (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)

#define OUTPUT_FORMAT boost::format("%|-20| %|_25| #%|-4| [%|-15|] %|-5| %|-5| %s")
enum LOG_LEVEL_T{TRACE=1, DEBUG=2, INFO=3, WARN=4, ERR=5, FATAL=6};
extern int SVR_LOG_LEVEL;

#define __THREAD_ID__ (std::this_thread::get_id())

#ifdef ENABLE_DEBUGGING

#define LOG4_BEGIN() do{ if(SVR_LOG_LEVEL <= LOG_LEVEL_T::TRACE) std::cout << OUTPUT_FORMAT % bpt::second_clock::local_time() % FILE_NAME % __LINE__ % __FUNCTION__ % __THREAD_ID__ % "Begin." % "" << std::endl;} while(0)
#define LOG4_END() do{ if(SVR_LOG_LEVEL <= LOG_LEVEL_T::TRACE) std::cout << OUTPUT_FORMAT % bpt::second_clock::local_time() % FILE_NAME % __LINE__ % __FUNCTION__ % __THREAD_ID__ % "End." % "" << std::endl;} while(0)
#define LOG4_TRACE(msg) do{ if(SVR_LOG_LEVEL <= LOG_LEVEL_T::TRACE) std::cout << OUTPUT_FORMAT % bpt::second_clock::local_time() % FILE_NAME % __LINE__ % __FUNCTION__  % __THREAD_ID__ % "TRACE: " % msg << std::endl;} while(0)
#define LOG4_DEBUG(msg) do{ if(SVR_LOG_LEVEL <= LOG_LEVEL_T::DEBUG) std::cout << OUTPUT_FORMAT % bpt::second_clock::local_time() % FILE_NAME % __LINE__ % __FUNCTION__ % __THREAD_ID__ % "DEBUG: " % msg << std::endl;} while(0)
#define LOG4_INFO(msg)  do{ if(SVR_LOG_LEVEL <= LOG_LEVEL_T::INFO) std::cout << OUTPUT_FORMAT % bpt::second_clock::local_time() % FILE_NAME % __LINE__ % __FUNCTION__ % __THREAD_ID__ % "INFO: " % msg << std::endl;} while(0)
#define LOG4_WARN(msg)  do{ if(SVR_LOG_LEVEL <= LOG_LEVEL_T::WARN) std::cout << OUTPUT_FORMAT % bpt::second_clock::local_time() % FILE_NAME % __LINE__ % __FUNCTION__ % __THREAD_ID__ % "WARNING: " % msg << std::endl;} while(0)
#define LOG4_ERROR(msg) do{ if(SVR_LOG_LEVEL <= LOG_LEVEL_T::ERR) std::cerr << OUTPUT_FORMAT % bpt::second_clock::local_time() % FILE_NAME % __LINE__ % __FUNCTION__ % __THREAD_ID__ % "ERROR: " % msg << std::endl;} while(0)
#define LOG4_FATAL(msg) do{ if(SVR_LOG_LEVEL <= LOG_LEVEL_T::FATAL) std::cerr << OUTPUT_FORMAT % bpt::second_clock::local_time() % FILE_NAME % __LINE__ % __FUNCTION__ % __THREAD_ID__ % "FATAL: " % msg << std::endl;} while(0)

#else

#define LOG4_BEGIN()
#define LOG4_END()
#define LOG4_TRACE(msg) {}
#define LOG4_DEBUG(msg) {}
#define LOG4_INFO(msg) {}
#define LOG4_WARN(msg) {}
#define LOG4_ERROR(msg) {}
#define LOG4_FATAL(msg) {}

#endif

#define PROFILE_EXEC_TIME(X, M_NAME) {bpt::ptime _startTime_ = bpt::microsec_clock::local_time(); X; LOG4_INFO("Execution time of " << M_NAME << " is " << bpt::microsec_clock::local_time() - _startTime_);}
