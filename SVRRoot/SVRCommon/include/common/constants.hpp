#pragma once

namespace svr{
namespace common{

double const DEFAULT_MISSING_VALUE = 0.0;
const double DEFAULT_VALUE_TICK_VOLUME = 1.0;

const std::string INPUT_QUEUE_TABLE_NAME_PREFIX = "q";
const std::string DECON_QUEUE_TABLE_NAME_PREFIX = "z";

const std::string DECON_QUEUE_COLUMN_PREFIX = "level_";

const std::string MT4_DATE_TIME_FORMAT = "%Y.%m.%d %H:%M";
}
}
