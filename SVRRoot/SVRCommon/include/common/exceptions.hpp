#include <exception>

#pragma once

namespace svr{
namespace common{

class invalid_data : public std::logic_error{
public:
    invalid_data(const std::string& msg) : std::logic_error(msg){}

};

class insufficient_data : public std::range_error{
public:
    insufficient_data(const std::string& msg) : std::range_error(msg){}

};

class invalid_request : public std::logic_error{
public:
    invalid_request(const std::string& msg) : std::logic_error(msg){}
};

class corrupted_model : public std::runtime_error{
public:
    corrupted_model(const std::string& msg) : std::runtime_error(msg){}
};

}
}
