#include <DAO/IQScalingFactorDAO.hpp>
#include "PgDAO/PgIQScalingFactorDAO.hpp"
#include "DAO/IQScalingFactorRowMapper.hpp"

namespace svr {
namespace dao {

IQScalingFactorDAO* IQScalingFactorDAO::build(svr::common::PropertiesFileReader& sqlProperties,
                                              svr::dao::DataSource& dataSource,
                                              svr::common::ConcreteDaoType daoType)
{
    if(daoType == svr::common::ConcreteDaoType::AsyncDao)
    {
        throw std::runtime_error("Not implemented yet");
        // TODO: implement AsyncDQScalingFactorDAO
        //return new AsyncIQScalingFactorDAO(sqlProperties, dataSource);
    }
    else
    {
        return new PgIQScalingFactorDAO(sqlProperties, dataSource);
    }

    return nullptr;
}

IQScalingFactorDAO::IQScalingFactorDAO(svr::common::PropertiesFileReader& sqlProperties,
                                       svr::dao::DataSource& dataSource) :
    AbstractDAO(sqlProperties, dataSource, "IQScalingFactorDAO.properties")
{}

}
}
