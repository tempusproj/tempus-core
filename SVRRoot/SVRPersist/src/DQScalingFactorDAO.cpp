#include "DAO/DQScalingFactorDAO.hpp"
#include "PgDAO/PgDQScalingFactorDAO.hpp"
#include "DAO/DQScalingFactorRowMapper.hpp"

namespace svr {
namespace dao {

DQScalingFactorDAO* DQScalingFactorDAO::build(svr::common::PropertiesFileReader& sqlProperties,
                                              svr::dao::DataSource& dataSource,
                                              svr::common::ConcreteDaoType daoType)
{
    if(daoType == svr::common::ConcreteDaoType::AsyncDao)
    {
        throw std::runtime_error("Not implemented yet");
        // TODO: implement AsyncDQScalingFactorDAO
        //return new AsyncDQScalingFactorDAO(sqlProperties, dataSource);
    }
    else
    {
        return new PgDQScalingFactorDAO(sqlProperties, dataSource);
    }

    return nullptr;
}

DQScalingFactorDAO::DQScalingFactorDAO(svr::common::PropertiesFileReader& sqlProperties,
                                       svr::dao::DataSource& dataSource) :
    AbstractDAO(sqlProperties, dataSource, "DQScalingFactorDAO.properties")
{}

}
}
