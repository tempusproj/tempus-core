#include <thread>
#include "common/Logging.hpp"
#include "DAO/ScopedTransaction.hpp"

namespace svr{
	namespace dao{

	    ScopedTransaction::ScopedTransaction(pqxx::lazyconnection *connection, boost::recursive_mutex& trx_mutex, bool commit_on_destroy)
			    : commit_on_destroy(commit_on_destroy), trx_lock(trx_mutex) {
            trx = new pqxx::work(*connection);
            LOG4_TRACE("Opening new transaction from thread " << std::this_thread::get_id() << " trx " << trx);
        }

	    ScopedTransaction::~ScopedTransaction() {

            if (trx != nullptr){
                if(commit_on_destroy) {
                    LOG4_TRACE("Finishing transaction " << trx << " with autocommit enabled from thread " <<  std::this_thread::get_id());
                    try {
                        trx->commit();
                    } catch (const std::exception &e) {
                        LOG4_ERROR("Cannot commit transaction " << trx << ": " << e.what() << " thread " << std::this_thread::get_id());
                    } catch (...) {
                        LOG4_FATAL("Unrecoverable error occurred: Cannot commit transaction!");
                    }
                } else {
                    LOG4_TRACE("Finishing transaction " << trx << " without commiting anything from thread " <<  std::this_thread::get_id());
                }
                delete trx;
            }
        }
	}
}
