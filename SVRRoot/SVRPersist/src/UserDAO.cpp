#include "DAO/UserDAO.hpp"
#include "PgDAO/PgUserDAO.hpp"
#include "AsyncDAO/AsyncUserDAO.hpp"

using svr::datamodel::User;
using std::make_shared;
using svr::common::ConcreteDaoType;

namespace svr {
namespace dao {

UserDAO * UserDAO::build(svr::common::PropertiesFileReader& sqlProperties, svr::dao::DataSource& dataSource, ConcreteDaoType daoType)
{
    if(daoType == ConcreteDaoType::AsyncDao)
        return new AsyncUserDAO(sqlProperties, dataSource);
    return new PgUserDAO(sqlProperties, dataSource);
}

UserDAO::UserDAO(svr::common::PropertiesFileReader& sqlProperties, svr::dao::DataSource& dataSource)
:AbstractDAO(sqlProperties, dataSource, "UserDAO.properties")
{}

}
}
