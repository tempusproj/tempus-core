#include <DAO/DecrementTaskDAO.hpp>
#include "PgDAO/PgDecrementTaskDAO.hpp"
#include "AsyncDAO/AsyncDecrementTaskDAO.hpp"

namespace svr {
namespace dao {

DecrementTaskDAO * DecrementTaskDAO::build(svr::common::PropertiesFileReader& sqlProperties, svr::dao::DataSource& dataSource, svr::common::ConcreteDaoType daoType)
{
    if(daoType == svr::common::ConcreteDaoType::AsyncDao)
        return new AsyncDecrementTaskDAO(sqlProperties, dataSource);
    return new PgDecrementTaskDAO(sqlProperties, dataSource);
}


DecrementTaskDAO::DecrementTaskDAO(svr::common::PropertiesFileReader& sqlProperties, svr::dao::DataSource& dataSource)
: AbstractDAO(sqlProperties, dataSource, "DecrementTaskDAO.properties")
{}

} // namespace dao
} // namespace svr
