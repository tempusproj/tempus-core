#include <DAO/RequestDAO.hpp>
#include <DAO/DataSource.hpp>
#include "PgDAO/PgRequestDAO.hpp"
#include "AsyncDAO/AsyncRequestDAO.hpp"

namespace svr {
namespace dao {

RequestDAO::RequestDAO(svr::common::PropertiesFileReader& sqlProperties, svr::dao::DataSource& dataSource)
: AbstractDAO(sqlProperties, dataSource, "RequestDAO.properties")
{
}

RequestDAO * RequestDAO::build(svr::common::PropertiesFileReader& sqlProperties, svr::dao::DataSource& dataSource, svr::common::ConcreteDaoType daoType)
{
    if(daoType == svr::common::ConcreteDaoType::AsyncDao)
        return new AsyncRequestDAO(sqlProperties, dataSource);
    return new PgRequestDAO(sqlProperties, dataSource);
}

}
}

