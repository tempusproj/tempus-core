#include "PgDAO/PgDeconQueueDAO.hpp"
#include "AsyncDAO/AsyncDeconQueueDAO.hpp"

using svr::common::ConcreteDaoType;

namespace svr{
namespace dao{

DeconQueueDAO * DeconQueueDAO::build(svr::common::PropertiesFileReader& sqlProperties, svr::dao::DataSource& dataSource, ConcreteDaoType daoType)
{
    if(daoType == ConcreteDaoType::AsyncDao)
        return new AsyncDeconQueueDAO(sqlProperties, dataSource);
    return new PgDeconQueueDAO(sqlProperties, dataSource);
}

DeconQueueDAO::DeconQueueDAO(svr::common::PropertiesFileReader& sqlProperties, svr::dao::DataSource& dataSource)
: AbstractDAO(sqlProperties, dataSource, "DeconQueueDAO.properties")
{}

}
}
