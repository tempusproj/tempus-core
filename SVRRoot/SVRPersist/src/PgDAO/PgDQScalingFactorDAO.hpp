#pragma once

#include <DAO/DQScalingFactorDAO.hpp>

namespace svr {
namespace dao {

class PgDQScalingFactorDAO : public DQScalingFactorDAO
{
public:
    explicit PgDQScalingFactorDAO(svr::common::PropertiesFileReader& sqlProperties,
                                  svr::dao::DataSource& dataSource);

    virtual bigint get_next_id();
    virtual bool exists(const bigint id);
    virtual int save(const DQScalingFactor_ptr& dQscalingFactor);
    virtual int remove(const DQScalingFactor_ptr& dQscalingFactor);
    virtual std::vector<DQScalingFactor_ptr> find_all_by_dataset_id(const bigint dataset_id);
};

}
}
