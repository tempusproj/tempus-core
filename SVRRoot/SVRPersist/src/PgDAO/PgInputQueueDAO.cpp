#include "PgInputQueueDAO.hpp"

#include "DAO/InputQueueRowRowMapper.hpp"
#include "DAO/DataRowRowMapper.hpp"
#include "model/InputQueue.hpp"
#include "DAO/InputQueueRowMapper.hpp"
#include "DAO/DataSource.hpp"

namespace svr {
namespace dao {

PgInputQueueDAO::PgInputQueueDAO(svr::common::PropertiesFileReader& sql_properties, svr::dao::DataSource& data_source)
: InputQueueDAO(sql_properties, data_source) {
}

InputQueue_ptr PgInputQueueDAO::get_queue_metadata(const std::string &user_name, const std::string &logical_name, const bpt::time_duration &resolution) {

    InputQueueRowMapper row_mapper;
    std::string sql = AbstractDAO::get_sql("get_queue_metadata");
    return data_source.query_for_object(&row_mapper, sql, user_name, logical_name, resolution);
}

InputQueue_ptr PgInputQueueDAO::get_queue_metadata(const std::string &table_name) {
    InputQueueRowMapper row_mapper;
    return data_source.query_for_object(&row_mapper, get_sql("get_queue_metadata_by_table_name"), table_name);
}

std::vector<DataRow_ptr> PgInputQueueDAO::get_queue_data_by_table_name(
        const std::string &table_name,
        const bpt::ptime &time_from,
        const bpt::ptime &time_to,
        size_t limit)
{
    LOG4_DEBUG("Reading data from queue " << table_name);
    DataRowRowMapper row_mapper;

    std::string sql = (boost::format(AbstractDAO::get_sql("get_queue_data_by_table_name")) % table_name).str();

    if (limit != 0) sql += " LIMIT " + pqxx::to_string(limit);

    return data_source.query_for_array(&row_mapper, sql, time_from, time_to, limit);
}

std::vector<DataRow_ptr> PgInputQueueDAO::get_latest_queue_data_by_table_name(
        const std::string &table_name,
        const size_t limit,
        const bpt::ptime &last_time)
{
    LOG4_DEBUG("Reading data from queue " << table_name);
    DataRowRowMapper row_mapper;

    std::string sql = (boost::format(AbstractDAO::get_sql("get_latest_queue_data_by_table_name")) % table_name).str();

    return data_source.query_for_array(&row_mapper, sql, last_time, limit);
}

size_t PgInputQueueDAO::save_metadata(const InputQueue_ptr& p_input_queue)
{
    std::string sql = AbstractDAO::get_sql("create_queue_table");
    std::string table_name = p_input_queue->get_table_name();
    p_input_queue->set_table_name(table_name);

    LOG4_DEBUG("Saving InputQueue with table name: " << table_name);
    std::string value_column_sql;

    for (const std::string& columnName : p_input_queue->get_value_columns())
        value_column_sql += "\"" + columnName + "\" double precision DEFAULT 0, ";

    boost::format sql_format(sql);
    sql_format % table_name % value_column_sql % table_name;

    sql = sql_format.str();
    LOG4_TRACE("CREATE TABLE SQL: " << sql);

    int ret;

    { // transaction
        ScopedTransaction_ptr trx = data_source.open_transaction();

        data_source.update(sql);

        p_input_queue->set_table_name(table_name);

        sql = AbstractDAO::get_sql("save_metadata");

        ret = data_source.update(sql,
                table_name,
                p_input_queue->get_logical_name(),
                p_input_queue->get_owner_user_name(),
                p_input_queue->get_description(),
                p_input_queue->get_resolution(),
                p_input_queue->get_legal_time_deviation(),
                p_input_queue->get_time_zone_offset(),
                p_input_queue->get_value_columns()
                );
    }

    return ret;
}

size_t PgInputQueueDAO::save(const InputQueue_ptr& inputQueue)
{
    long ret = 0;

    bool exist = exists(inputQueue);
    if (exist)
        ret = update_metadata(inputQueue);
    else
        ret = save_metadata(inputQueue);

    if(inputQueue->get_data().size() > 0)
    {
        ret = save_data(inputQueue);
    }

    return ret;
}

size_t PgInputQueueDAO::save_data(const InputQueue_ptr& inputQueue)
{
    std::vector<std::vector<std::string>> data;
    std::vector<bpt::ptime const *> value_times;
    for(const auto& row : inputQueue->get_data())
    {
        data.push_back(row.second->to_tuple());
        value_times.push_back(&row.first);
    }

    data_source.cleanup_queue_table(inputQueue->get_table_name(), value_times);
    return data_source.batch_update(inputQueue->get_table_name(), data);
}

size_t PgInputQueueDAO::update_metadata(const InputQueue_ptr& inputQueue) {

    LOG4_DEBUG("Updating InputQueue table metadata: " << inputQueue->get_table_name());
    std::string sql = AbstractDAO::get_sql("update_metadata");

    return data_source.update(sql,
            inputQueue->get_logical_name(),
            inputQueue->get_description(),
            inputQueue->get_legal_time_deviation(),
            inputQueue->get_time_zone_offset(),
            inputQueue->get_table_name()
            );
}

bool PgInputQueueDAO::exists(const std::string tableName) {
    return 1 == data_source.query_for_type<long>(AbstractDAO::get_sql("exists"), tableName);
}

bool PgInputQueueDAO::exists(const std::string& user_name,
        const std::string logical_name, const bpt::time_duration& resolution) {
    return exists(svr::datamodel::InputQueue::make_queue_table_name(user_name, logical_name, resolution));
}

bool PgInputQueueDAO::exists(const InputQueue_ptr& p_input_queue)
{
    if (p_input_queue == nullptr) return false;
    std::string table_name = p_input_queue->get_table_name();
    p_input_queue->set_table_name(table_name);
    return exists(table_name);
}

size_t PgInputQueueDAO::remove(const InputQueue_ptr& p_input_queue)
{
    std::string table_name = svr::datamodel::InputQueue::make_queue_table_name(
                p_input_queue->get_owner_user_name(), p_input_queue->get_logical_name(), p_input_queue->get_resolution());

    LOG4_DEBUG("Removing InputQueue with table name " << table_name);

    std::string sql = get_sql("remove_queue_table");

    boost::format sql_format(sql);
    sql_format % table_name;
    sql = sql_format.str();

    data_source.update(sql, table_name);
    return data_source.update(get_sql("remove_queue_metadata"), table_name);

}

DataRow_ptr PgInputQueueDAO::find_oldest_record(const InputQueue_ptr& p_input_queue) {
    std::string sql_format = get_sql("find_oldest_record");
    InputQueueRowRowMapper row_mapper;

    boost::format sql(sql_format);
    sql % p_input_queue->get_table_name();

    return data_source.query_for_object(&row_mapper, sql.str());
}

DataRow_ptr PgInputQueueDAO::find_newest_record(const InputQueue_ptr& queue) {
    std::string sql_format = get_sql("find_newest_record");
    InputQueueRowRowMapper row_mapper;

    boost::format sql(sql_format);
    sql % queue->get_table_name();

    return data_source.query_for_object(&row_mapper, sql.str());
}

std::vector<std::shared_ptr<std::string>> PgInputQueueDAO::get_db_table_column_names(const InputQueue_ptr& queue)
{
    std::string query = get_sql("get_db_table_column_names");

    InputQueueDbTableColumnsMapper row_mapper;
    return data_source.query_for_array(& row_mapper, query, queue->get_table_name());
}

std::vector<InputQueue_ptr> PgInputQueueDAO::get_all_user_queues(const std::string &user_name) {
    InputQueueRowMapper row_mapper;
    return data_source.query_for_array(&row_mapper, get_sql("get_all_user_queues"), user_name);
}

bool PgInputQueueDAO::row_exists(const std::string& tableName, const bpt::ptime &valueTime) {
    std::string sql_format = get_sql("row_exists");
    boost::format sql(sql_format);
    sql % tableName;

    return data_source.query_for_type<bool>(sql.str(), valueTime);
}

/******************************************************************************/
/******************************************************************************/
/******************************************************************************/

struct MissedHoursrow_mapper : public IRowMapper<bpt::ptime> {

    std::shared_ptr<bpt::ptime> mapRow(const pqxx::tuple& rowSet) {
        if (rowSet.empty())
            return {
        };
        return std::make_shared<bpt::ptime>(rowSet[0].as<bpt::ptime>());
    }
};

OptionalTimeRange PgInputQueueDAO::get_missing_hours(InputQueue_ptr const &queue, TimeRange const & fromRange) {
    std::string sql_format = get_sql("get_missing_hours_start");
    boost::format sqlStart(sql_format);
    sqlStart % queue->get_table_name();

    static MissedHoursrow_mapper row_mapper;

    auto const sec = queue->get_resolution().total_seconds();
    std::shared_ptr<bpt::ptime> start = data_source.query_for_object(&row_mapper, sqlStart.str(), fromRange.second, fromRange.first, sec);

    if (!start)
        return OptionalTimeRange();

    sql_format = get_sql("get_missing_hours_end");
    boost::format sqlEnd(sql_format);
    sqlEnd % queue->get_table_name();

    std::shared_ptr<bpt::ptime> end = data_source.query_for_object(&row_mapper, sqlEnd.str(), *start, fromRange.first, sec);
    if (!end)
        return OptionalTimeRange();

    return OptionalTimeRange(std::make_pair(*end + queue->get_resolution(), *start + queue->get_resolution()));
}

void PgInputQueueDAO::mark_hours_missing(InputQueue_ptr const & queue, std::string const & start, std::string const & end) {
    std::string sql_format = get_sql("mark_hours_missing");
    data_source.update(sql_format, queue->get_table_name(), start, end);
}

void PgInputQueueDAO::purge_missing_hours(InputQueue_ptr const & queue) {
    std::string sql_format = get_sql("purge_missing_hours");
    data_source.update(sql_format, queue->get_table_name());
}

}
}
