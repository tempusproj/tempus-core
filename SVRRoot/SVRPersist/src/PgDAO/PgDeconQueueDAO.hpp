#ifndef PGDECONQUEUEDAO_H
#define PGDECONQUEUEDAO_H

#include <DAO/DeconQueueDAO.hpp>

namespace svr {
namespace dao {

class PgDeconQueueDAO : public DeconQueueDAO
{
public:
    explicit PgDeconQueueDAO(svr::common::PropertiesFileReader& sql_properties, svr::dao::DataSource& data_source);

    DeconQueue_ptr          get_decon_queue_by_table_name(const std::string &tableName);

    std::vector<DataRow_ptr> get_data(const std::string& deconQueueTableName, const bpt::ptime& timeFrom = bpt::min_date_time, const bpt::ptime& timeTo = bpt::max_date_time, size_t limit = 0);
    std::vector<DataRow_ptr> get_latest_data(const std::string& deconQueueTableName, bpt::ptime const &timeTo = bpt::max_date_time, size_t limit = 0);
    std::vector<DataRow_ptr> get_data_having_update_time_greater_than(const std::string& deconQueueTableName, const bpt::ptime& updateTime, long limit = 0);

    bool exists(const std::string& tableName);
    bool exists(const DeconQueue_ptr& deconQueue);

    void save(const DeconQueue_ptr& deconQueue);
    int save_metadata(const DeconQueue_ptr& deconQueue);
    long save_data(const DeconQueue_ptr& deconQueue);

    int remove(const DeconQueue_ptr& deconQueue);
    int clear(const DeconQueue_ptr& deconQueue);

    long count(const DeconQueue_ptr& deconQueue);

    bool decon_table_needs_recreation(DeconQueue_ptr const & existing_queue, DeconQueue_ptr const & new_queue);

private:
    void create_decon_table_no_trx(DeconQueue_ptr const & decon_queue); //no_trx means caller code takes care of transactions
    void remove_decon_table_no_trx(DeconQueue_ptr const & decon_queue);
    int  update_metadata_no_trx(DeconQueue_ptr const & decon_queue);

    size_t get_db_column_number( DeconQueue_ptr const & queue );
    size_t get_col_nr(DeconQueue_ptr const & queue);
};

} }

#endif /* PGDECONQUEUEDAO_H */

