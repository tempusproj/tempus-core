#include "PgDQScalingFactorDAO.hpp"
#include <DAO/DataSource.hpp>
#include <DAO/DQScalingFactorRowMapper.hpp>

namespace svr {
namespace dao {

PgDQScalingFactorDAO::PgDQScalingFactorDAO(svr::common::PropertiesFileReader& sqlProperties,
                                           svr::dao::DataSource& dataSource) :
    DQScalingFactorDAO(sqlProperties, dataSource)
{}

bigint PgDQScalingFactorDAO::get_next_id()
{
    return data_source.query_for_type<bigint>(AbstractDAO::get_sql("get_next_id"));
}

bool PgDQScalingFactorDAO::exists(const bigint id)
{
    return data_source.query_for_type<int>(AbstractDAO::get_sql("exists_by_id"), id) == 1;
}

int PgDQScalingFactorDAO::save(const DQScalingFactor_ptr& dQscalingFactor)
{
    if(!dQscalingFactor->get_id())
    {
        dQscalingFactor->set_id(get_next_id());

        return data_source.update(AbstractDAO::get_sql("save"),
                                  dQscalingFactor->get_id(),
                                  dQscalingFactor->get_dataset_id(),
                                  dQscalingFactor->get_input_queue_table_name(),
                                  dQscalingFactor->get_input_queue_column_name(),
                                  dQscalingFactor->get_wavelet_level(),
                                  dQscalingFactor->get_scaling_factor()
                                  );
    }
    else
    {
        return data_source.update(AbstractDAO::get_sql("update"),
                                  dQscalingFactor->get_id(),
                                  dQscalingFactor->get_dataset_id(),
                                  dQscalingFactor->get_input_queue_table_name(),
                                  dQscalingFactor->get_input_queue_column_name(),
                                  dQscalingFactor->get_wavelet_level(),
                                  dQscalingFactor->get_scaling_factor()
                                  );
    }


}

int PgDQScalingFactorDAO::remove(const DQScalingFactor_ptr& dQscalingFactor)
{
    if(dQscalingFactor->get_id() == 0)
        return 0;

    return data_source.update(AbstractDAO::get_sql("remove"), dQscalingFactor->get_id());
}

std::vector<DQScalingFactor_ptr> PgDQScalingFactorDAO::find_all_by_dataset_id(const bigint dataset_id)
{
    DQScalingFactorRowMapper rowMaper;
    return data_source.query_for_array(&rowMaper, AbstractDAO::get_sql("find_all_by_dataset_id"), dataset_id);
}

}
}
