#include "PgDeconQueueDAO.hpp"
#include "util/ValidationUtils.hpp"
#include "common/constants.hpp"
#include "util/StringUtils.hpp"
#include "appcontext.hpp"
#include <DAO/DataRowRowMapper.hpp>
#include <DAO/DeconQueueRowMapper.hpp>
#include <DAO/DataSource.hpp>

using svr::common::DECON_QUEUE_TABLE_NAME_PREFIX;
using svr::common::make_table_identifier;
using svr::common::reject_empty;

namespace svr { namespace dao {

PgDeconQueueDAO::PgDeconQueueDAO(svr::common::PropertiesFileReader& sqlProperties, svr::dao::DataSource& dataSource)
: DeconQueueDAO(sqlProperties, dataSource)
{}

DeconQueue_ptr PgDeconQueueDAO::get_decon_queue_by_table_name(const std::string &tableName) {
    DeconQueueRowMapper rowMapper;
    return data_source.query_for_object(&rowMapper, AbstractDAO::get_sql("get_decon_queue_by_table_name"), tableName);
}

std::vector<DataRow_ptr> PgDeconQueueDAO::get_data(const std::string& deconQueueTableName, bpt::ptime const &timeFrom, bpt::ptime const &timeTo, size_t limit) {
    reject_empty(deconQueueTableName);

    boost::format sqlFormat (AbstractDAO::get_sql("get_data"));
    sqlFormat % deconQueueTableName;

    std::string sql = sqlFormat.str();

    if(limit != 0){
        sql += " LIMIT " + std::to_string(limit);
    }

    DataRowRowMapper rowMapper;

    return data_source.query_for_array(&rowMapper, sql, timeFrom, timeTo);
}

std::vector<DataRow_ptr> PgDeconQueueDAO::get_latest_data(const std::string& deconQueueTableName, bpt::ptime const &timeTo, size_t limit) {
    reject_empty(deconQueueTableName);

    boost::format sqlFormat (AbstractDAO::get_sql("get_latest_data"));
    sqlFormat % deconQueueTableName;

    std::string sql = sqlFormat.str();

    DataRowRowMapper rowMapper;

    return data_source.query_for_array(&rowMapper, sql, timeTo, limit);
}

bool PgDeconQueueDAO::exists(const DeconQueue_ptr& deconQueue) {
    return exists(deconQueue->get_table_name());
}

bool PgDeconQueueDAO::exists(const std::string &tableName) {
    if(tableName == ""){
        return false;
    }

    return data_source.query_for_type<int>(AbstractDAO::get_sql("decon_queue_exists"), tableName) == 1 &&
            data_source.query_for_type<int>(AbstractDAO::get_sql("table_exists"), tableName) == 1;
}

void PgDeconQueueDAO::save(DeconQueue_ptr const &deconQueue)
{
    save_metadata(deconQueue);
    save_data(deconQueue);
}

namespace {

    size_t const na = size_t(-1);

} // namespace



bool PgDeconQueueDAO::decon_table_needs_recreation(DeconQueue_ptr const & existing_queue, DeconQueue_ptr const & new_queue)
{

    if(    existing_queue->get_input_queue_column_name() != new_queue->get_input_queue_column_name()
        || existing_queue->get_dataset_id() != new_queue->get_dataset_id()
        || get_col_nr(existing_queue) != get_col_nr(new_queue)
      )
        return true;

    return false;
}


int PgDeconQueueDAO::save_metadata(DeconQueue_ptr const &p_decon_queue) {

    int ret;

    DeconQueue_ptr existing = get_decon_queue_by_table_name(p_decon_queue->get_table_name());

    if(existing)
    {
        ScopedTransaction_ptr tx = data_source.open_transaction();
        if(decon_table_needs_recreation(p_decon_queue, existing))
        {
            remove_decon_table_no_trx(p_decon_queue);
            ret = update_metadata_no_trx(p_decon_queue);
            create_decon_table_no_trx(p_decon_queue);
            return ret;
        }
        ret = update_metadata_no_trx(p_decon_queue);
        return ret;
    }
    else
    {
        ScopedTransaction_ptr tx = data_source.open_transaction();
        ret = data_source.update(AbstractDAO::get_sql("save_metadata"),
                                 p_decon_queue->get_table_name(),
                                 p_decon_queue->get_input_queue_table_name(),
                                 p_decon_queue->get_input_queue_column_name(),
                                 p_decon_queue->get_dataset_id()
                                 );
        create_decon_table_no_trx(p_decon_queue);
    }

    return ret;
}

int PgDeconQueueDAO::remove(DeconQueue_ptr const &deconQueue) {
    ScopedTransaction_ptr trx = data_source.open_transaction();

    std::string dropTableSql = AbstractDAO::get_sql("remove_decon_queue_table");
    boost::format sqlFormat(dropTableSql);
    sqlFormat % deconQueue->get_table_name();

    data_source.update(sqlFormat.str());

    return data_source.update(AbstractDAO::get_sql("remove_decon_queue"),
        deconQueue->get_table_name()
    );
}

long PgDeconQueueDAO::save_data(const DeconQueue_ptr& deconQueue)
{
    if( deconQueue->get_data().size() > 0){
        std::vector<std::vector<std::string>> tuples;

        std::vector<bpt::ptime const *> value_times;
        for(const auto& row : deconQueue->get_data())
        {
            tuples.push_back(row.second->to_tuple());
            value_times.push_back(&row.first);
        }

        data_source.cleanup_queue_table(deconQueue->get_table_name(), value_times);
        data_source.batch_update(deconQueue->get_table_name(), tuples);
    }
    return 0;
}

std::vector<DataRow_ptr> PgDeconQueueDAO::get_data_having_update_time_greater_than(const std::string &deconQueueTableName, const bpt::ptime &updateTime, long limit) {
    DataRowRowMapper rowMapper;
    std::string sql = get_sql("get_data_having_update_time_greater_than");
    boost::format sqlFormat(sql);
    sqlFormat % deconQueueTableName;

    return data_source.query_for_array(&rowMapper, sqlFormat.str(), updateTime, limit);
}

int PgDeconQueueDAO::clear(const DeconQueue_ptr &deconQueue) {

    boost::format sqlFormat(get_sql("clear_table"));
    sqlFormat % deconQueue->get_table_name();

    return data_source.update(sqlFormat.str());
}

long PgDeconQueueDAO::count(const DeconQueue_ptr &deconQueue) {
    boost::format sqlFormat(get_sql("count"));
    sqlFormat % deconQueue->get_table_name();

    return data_source.query_for_type<long>(sqlFormat.str());
}

void PgDeconQueueDAO::create_decon_table_no_trx(const DeconQueue_ptr& decon_queue)
{
    std::string create_table_sql = AbstractDAO::get_sql("create_decon_table");

    LOG4_DEBUG("Creating new DeconQueue table with name: " << decon_queue->get_table_name());

    if (decon_queue->get_data().empty())
    {
        LOG4_ERROR("DeconQueue is empty. Need some data because we need number of deconstructed levels.");
        throw std::logic_error("PgDeconQueueDAO::create_decon_table: DeconQueue is empty. Need some data because we need number of deconstructed levels.");
    }

    std::string level_columns_sql;
    for(size_t level = 0; level < decon_queue->get_data().cbegin()->second->get_values().size(); ++level)
    {
        level_columns_sql += svr::common::DECON_QUEUE_COLUMN_PREFIX
                        + std::to_string(level)
                        + " double precision NOT NULL DEFAULT 0, ";
    }

    boost::format sql_format(create_table_sql);
    sql_format % decon_queue->get_table_name() % level_columns_sql % decon_queue->get_table_name();

    LOG4_TRACE("CREATE DECON TABLE SQL: " << sql_format.str());

    data_source.update(sql_format.str());
}

void PgDeconQueueDAO::remove_decon_table_no_trx(DeconQueue_ptr const & decon_queue)
{
    std::string dropTableSql = AbstractDAO::get_sql("remove_decon_queue_table");
    boost::format sqlFormat(dropTableSql);
    sqlFormat % decon_queue->get_table_name();

    data_source.update(sqlFormat.str());
}

int  PgDeconQueueDAO::update_metadata_no_trx(DeconQueue_ptr const & decon_queue)
{
    return data_source.update(AbstractDAO::get_sql("update_metadata"),
                            decon_queue->get_input_queue_table_name(),
                            decon_queue->get_input_queue_column_name(),
                            decon_queue->get_dataset_id(),
                            decon_queue->get_table_name()
                            );
}

size_t PgDeconQueueDAO::get_col_nr(DeconQueue_ptr const & queue)
{
    size_t result = na;
    if(!queue->get_data().empty() && !queue->get_data().begin()->second->get_values().empty())
        result = queue->get_data().begin()->second->get_values().size();

    if(result != na)
        return result;

    result = get_db_column_number(queue);

    if(result != na)
        return result;

    Dataset_ptr dataset = svr::context::AppContext::get_instance().dataset_service.get_by_id(queue->get_dataset_id());
    if(dataset)
        result = dataset->get_swt_levels() + 1;

    return result;
}


size_t PgDeconQueueDAO::get_db_column_number( DeconQueue_ptr const & queue ) {
    auto ret = data_source.query_for_type<int>(   AbstractDAO::get_sql("get_db_column_number")
                                                , queue->get_table_name()
                                                );
    if(ret < 3)
        return na;

    return ret - 3;
}


}
}
