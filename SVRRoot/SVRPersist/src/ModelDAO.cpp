#include <DAO/ModelDAO.hpp>
#include "PgDAO/PgModelDAO.hpp"
#include "AsyncDAO/AsyncModelDAO.hpp"

namespace svr {
namespace dao {

ModelDAO * ModelDAO::build(svr::common::PropertiesFileReader& sqlProperties, svr::dao::DataSource& dataSource, svr::common::ConcreteDaoType daoType)
{
    if(daoType == svr::common::ConcreteDaoType::AsyncDao)
        return new AsyncModelDAO(sqlProperties, dataSource);
    return new PgModelDAO(sqlProperties, dataSource);
}

ModelDAO::ModelDAO(svr::common::PropertiesFileReader& sqlProperties, svr::dao::DataSource& dataSource)
: AbstractDAO(sqlProperties, dataSource, "ModelDAO.properties")
{}


}
}
