#include <DAO/DatasetDAO.hpp>
#include "PgDAO/PgDatasetDAO.hpp"
#include "AsyncDAO/AsyncDatasetDAO.hpp"

namespace svr {
namespace dao {

DatasetDAO * DatasetDAO::build(svr::common::PropertiesFileReader& sqlProperties, svr::dao::DataSource& dataSource, svr::common::ConcreteDaoType daoType)
{
    if(daoType == svr::common::ConcreteDaoType::AsyncDao)
        return new AsyncDatasetDAO(sqlProperties, dataSource);
    return new PgDatasetDAO(sqlProperties, dataSource);
}


DatasetDAO::DatasetDAO(svr::common::PropertiesFileReader& sqlProperties, svr::dao::DataSource& dataSource)
:AbstractDAO(sqlProperties, dataSource, "DatasetDAO.properties")
{}

}
}
