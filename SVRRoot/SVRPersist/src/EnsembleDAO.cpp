#include <DAO/EnsembleDAO.hpp>
#include "PgDAO/PgEnsembleDAO.hpp"
#include "AsyncDAO/AsyncEnsembleDAO.hpp"

namespace svr {
namespace dao {

EnsembleDAO * EnsembleDAO::build(svr::common::PropertiesFileReader& sqlProperties, svr::dao::DataSource& dataSource, svr::common::ConcreteDaoType daoType)
{
    if(daoType == svr::common::ConcreteDaoType::AsyncDao)
        return new AsyncEnsembleDAO(sqlProperties, dataSource);
    return new PgEnsembleDAO(sqlProperties, dataSource);
}

EnsembleDAO::EnsembleDAO(svr::common::PropertiesFileReader& sqlProperties, svr::dao::DataSource& dataSource)
:AbstractDAO(sqlProperties, dataSource, "EnsembleDAO.properties")
{}

}
}
