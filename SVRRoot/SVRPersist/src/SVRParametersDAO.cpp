#include <DAO/SVRParametersDAO.hpp>
#include "PgDAO/PgSVRParametersDAO.hpp"
#include "AsyncDAO/AsyncSVRParametersDAO.hpp"

namespace svr{
namespace dao{

SVRParametersDAO * SVRParametersDAO::build(svr::common::PropertiesFileReader& sqlProperties, svr::dao::DataSource& dataSource, svr::common::ConcreteDaoType daoType)
{
    if(daoType == svr::common::ConcreteDaoType::AsyncDao)
        return new AsyncSVRParametersDAO(sqlProperties, dataSource);
    return new PgSVRParametersDAO(sqlProperties, dataSource);
}

SVRParametersDAO::SVRParametersDAO(svr::common::PropertiesFileReader& sqlProperties, svr::dao::DataSource& dataSource)
: AbstractDAO(sqlProperties, dataSource, "SVRParametersDAO.properties")
{}

}
}
