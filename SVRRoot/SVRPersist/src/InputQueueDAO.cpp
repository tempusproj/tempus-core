#include "DAO/InputQueueDAO.hpp"
#include "PgDAO/PgInputQueueDAO.hpp"
#include "AsyncDAO/AsyncInputQueueDAO.hpp"
#include <model/InputQueue.hpp>

namespace svr {
namespace dao {

using svr::common::ConcreteDaoType;

InputQueueDAO * InputQueueDAO::build(svr::common::PropertiesFileReader& sqlProperties, svr::dao::DataSource& dataSource, ConcreteDaoType daoType)
{
    if(daoType == ConcreteDaoType::AsyncDao)
        return new AsyncInputQueueDAO(sqlProperties, dataSource);
    return new PgInputQueueDAO(sqlProperties, dataSource);
}

InputQueueDAO::InputQueueDAO(svr::common::PropertiesFileReader& sql_properties, svr::dao::DataSource& data_source)
: AbstractDAO(sql_properties, data_source, "InputQueueDAO.properties")
{}

} /* namespace dao */
} /* namespace svr */

