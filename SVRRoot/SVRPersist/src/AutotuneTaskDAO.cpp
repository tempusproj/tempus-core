#include <DAO/AutotuneTaskDAO.hpp>
#include "PgDAO/PgAutotuneTaskDAO.hpp"
#include "AsyncDAO/AsyncAutotuneTaskDAO.hpp"

namespace svr {
namespace dao {

AutotuneTaskDAO * AutotuneTaskDAO::build(svr::common::PropertiesFileReader& sqlProperties, svr::dao::DataSource& dataSource, svr::common::ConcreteDaoType daoType)
{
    if(daoType == svr::common::ConcreteDaoType::AsyncDao)
        return new AsyncAutotuneTaskDAO(sqlProperties, dataSource);
    return new PgAutotuneTaskDAO(sqlProperties, dataSource);
}


AutotuneTaskDAO::AutotuneTaskDAO(svr::common::PropertiesFileReader& sqlProperties, svr::dao::DataSource& dataSource)
:AbstractDAO(sqlProperties, dataSource, "AutotuneTaskDAO.properties")
{}


} /* namespace dao */
} /* namespace svr */
