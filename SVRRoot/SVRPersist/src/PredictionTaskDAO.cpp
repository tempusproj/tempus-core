#include <DAO/PredictionTaskDAO.hpp>
#include "PgDAO/PgPredictionTaskDAO.hpp"
#include "AsyncDAO/AsyncPredictionTaskDAO.hpp"

namespace svr {
namespace dao {

PredictionTaskDAO * PredictionTaskDAO::build(svr::common::PropertiesFileReader& sqlProperties, svr::dao::DataSource& dataSource, svr::common::ConcreteDaoType daoType)
{
    if(daoType == svr::common::ConcreteDaoType::AsyncDao)
        return new AsyncPredictionTaskDAO(sqlProperties, dataSource);
    return new PgPredictionTaskDAO(sqlProperties, dataSource);
}

PredictionTaskDAO::PredictionTaskDAO(svr::common::PropertiesFileReader& sqlProperties, svr::dao::DataSource& dataSource)
: AbstractDAO(sqlProperties, dataSource, "PredictionTaskDAO.properties")
{}

} /* namespace dao */
} /* namespace svr */
