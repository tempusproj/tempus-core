/*
 * UserRowMapper.cpp
 *
 *  Created on: Jul 27, 2014
 *      Author: vg
 */

#include "DAO/UserRowMapper.hpp"
#include "util/StringUtils.hpp"

using namespace svr::datamodel;
using namespace std;
using namespace svr::common;

namespace svr {
namespace dao {

User_ptr UserRowMapper::mapRow(const pqxx::tuple& rowSet) {
	if(rowSet.empty()){
		LOG4_ERROR("No row returned from database!");
		return nullptr;
	}
	User::ROLE role;
	svr::datamodel::Priority priority = svr::datamodel::Priority::Normal;

	if(! rowSet["priority"].is_null()) {
		priority = static_cast<svr::datamodel::Priority>(rowSet["priority"].as<int>());
	}

	if(!rowSet["role"].is_null() && ignoreCaseEquals(rowSet["role"].as<string>(), "admin"))
		role = User::ROLE::ADMIN;
	else role = User::ROLE ::USER;

	return make_shared<User>(
			rowSet["user_id"].as<bigint>(),
			rowSet["username"].as<string>(),
			rowSet["email"].as<string>(),
			rowSet["password"].as<string>(),
			rowSet["name"].is_null()	?	"" : rowSet["name"].as<string>(),
			role,
			priority
	);
}

} /* namespace dao */
} /* namespace svr */

