#include <thread>

#include "DAO/DataSource.hpp"

namespace svr{
namespace dao{

//using namespace pqxx;
using namespace std;

DataSource::~DataSource() {
    if (_connection.get() != nullptr && _connection->is_open()) {
        LOG4_DEBUG("Closing database connection...");
        _connection->disconnect();
    }
}

DataSource::DataSource(const std::string& connectionString, bool commit_on_scope_exit) {
    LOG4_DEBUG("Opening connection using connection string: \"" << connectionString << "\"");
    try {
        this->_connection = std::make_shared<pqxx::lazyconnection>(connectionString);
        this->_connection->set_variable("search_path", "svr,public");
        this->statementPreparerTemplate = std::make_shared<StatementPreparerDBTemplate>(this->_connection);
        this->commit_on_scope_exit = commit_on_scope_exit;
    }catch (const std::exception& e){
        LOG4_FATAL(e.what());
        throw e;
    }
}

void DataSource::reopen_connection()
{
    unsigned int sleep_seconds = 1;
    unsigned int inc_sleep_seconds = 30;
    unsigned int max_attempt_numb = 30;
    unsigned int attempt_numb = 0;
    while (!_connection->is_open() && attempt_numb <= 30)
    {
        LOG4_INFO("Attempt number to create new transaction : " << attempt_numb);
        try
        {
            _connection->activate();
        }
        catch (const pqxx::pqxx_exception &e)
        {
            LOG4_ERROR("Can't open transaction fro thread " <<  std::this_thread::get_id()
                       << " due error: " << e.base().what());
            LOG4_INFO("Sleep for " << sleep_seconds);
            sleep(sleep_seconds);
            // increament counters
            if (attempt_numb % 10 == 0) sleep_seconds += inc_sleep_seconds;
            ++attempt_numb;
        }
    }
    if (!_connection->is_open())
        throw std::runtime_error("Can't connect to db after " + std::to_string(max_attempt_numb)
                                 + " attempts");
}

ScopedTransaction_ptr DataSource::open_transaction(){
    return open_transaction(commit_on_scope_exit);
}

ScopedTransaction_ptr DataSource::open_transaction(bool auto_commit){
    ScopedTransaction_ptr trx;
    RecursiveLock _{trx_mutex};

    if (!_connection->is_open())
    {
        reopen_connection();
    }

    if(current_trx.expired()){
        LOG4_TRACE("Opening new transaction for thread " << std::this_thread::get_id()
                   << " with autocommit set to " << auto_commit);
        trx = std::make_shared<ScopedTransaction>(_connection.get(), trx_mutex, auto_commit);
        current_trx = trx;
    }
    else
    {
        trx = current_trx.lock();
        LOG4_TRACE("Reusing existing tranasction for thread " << std::this_thread::get_id() << " trx " << trx.get());
    }
    return trx;
}

long DataSource::batch_update(const std::string& tableName, const vector<vector<string> >& data)
{
    if (data.size() == 0) {
        LOG4_ERROR("Cannot save: No data present to save/update!");
        throw std::invalid_argument("Invalid or no data present!");
    }
    if (tableName.length() == 0) {
        LOG4_ERROR("Cannot save: No table name specified to write into!");
        throw std::invalid_argument("Invalid table name!");
    }

    ScopedTransaction_ptr trx = open_transaction();
    try {
        unsigned long ret = 0;
        pqxx::tablewriter tw(*trx->getTrx(), tableName);

        for (const std::vector<std::string> &row : data) {
            tw.insert(row);
            ++ret;
        }

        tw.complete();
        return ret;

    } catch (const pqxx::pqxx_exception& e) {
        LOG4_FATAL(e.base().what());
        throw;
    }
}

void DataSource::cleanup_queue_table(const std::string &tableName, std::vector<bpt::ptime const *> const & value_times)
{
    if(value_times.empty())
        return;

    ScopedTransaction_ptr trx = open_transaction();

    size_t const batch_size = 1000;

    for(size_t i = 0; i < value_times.size(); i += batch_size)
    {
        std::ostringstream ostr;
        ostr << "select cleanup_queue('" << tableName << "', '{";
        auto ivt = value_times.begin() + i;
        ostr << bpt::to_simple_string(**ivt); ++ivt;

        size_t j = 0;
        for(auto end = value_times.end(); ivt != end && j < batch_size; ++ivt, ++j )
            ostr << "," << bpt::to_simple_string(**ivt);

        ostr << "}'::timestamp[]);";

        try
        {
            trx->getTrx()->exec(ostr.str());
        }
        catch (const std::exception& e)
        {
            LOG4_FATAL(e.what());
            throw e;
        }
    }
}


}
}

