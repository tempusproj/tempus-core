#ifndef ASYNCINPUTQUEUEDAO_HPP
#define ASYNCINPUTQUEUEDAO_HPP

#include <DAO/InputQueueDAO.hpp>

namespace svr {
namespace dao {

class AsyncInputQueueDAO : public InputQueueDAO
{
public:
    explicit AsyncInputQueueDAO(svr::common::PropertiesFileReader& sql_properties, svr::dao::DataSource& data_source);
    ~AsyncInputQueueDAO();

    std::vector<InputQueue_ptr> get_all_user_queues(const std::string& user_name);
    InputQueue_ptr get_queue_metadata(const std::string &userName, const std::string &logicalName, const bpt::time_duration &resolution);
    InputQueue_ptr get_queue_metadata(const std::string &tableName);

    std::vector<DataRow_ptr> get_queue_data_by_table_name(const std::string &tableName,
            const bpt::ptime &timeFrom = bpt::min_date_time,
            const bpt::ptime &timeTo = bpt::max_date_time,
            size_t limit = 0);
    std::vector<DataRow_ptr> get_latest_queue_data_by_table_name(const std::string &tableName,
            const size_t limit = 0, const bpt::ptime &last_time = bpt::max_date_time);

    size_t save(const InputQueue_ptr& queue);
    size_t save_data(const InputQueue_ptr& queue);
    size_t save_metadata(const InputQueue_ptr& queue);
    size_t update_metadata(const InputQueue_ptr& queue);
    size_t remove(const InputQueue_ptr& queue);

    bool exists(const std::string tableName);
    bool exists(const InputQueue_ptr& input_queue);
    bool exists(const std::string& userName, const std::string logicalName, const bpt::time_duration& resolution);

    DataRow_ptr find_oldest_record(const InputQueue_ptr& queue);
    DataRow_ptr find_newest_record(const InputQueue_ptr& queue);

    std::vector<std::shared_ptr<std::string>> get_db_table_column_names(const InputQueue_ptr& queue);

    OptionalTimeRange get_missing_hours(InputQueue_ptr const &, TimeRange const &);
    void mark_hours_missing(InputQueue_ptr const &, std::string const &, std::string const &);
    void purge_missing_hours(InputQueue_ptr const & queue);
private:
    struct AsyncImpl;
    AsyncImpl & pImpl;

};

} }
#endif /* ASYNCINPUTQUEUEDAO_HPP */

