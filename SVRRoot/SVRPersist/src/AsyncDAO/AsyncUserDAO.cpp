#include "AsyncUserDAO.hpp"

#include "../PgDAO/PgUserDAO.hpp"
#include "AsyncImplBase.hpp"

#include "model/User.hpp"
#include <common/Logging.hpp>
#include "common/ScopeExit.hpp"

namespace svr { namespace dao {

namespace {
    static bool cmp_primary_key(User_ptr const & lhs, User_ptr const & rhs)
    {
        return reinterpret_cast<unsigned long>(lhs.get()) * reinterpret_cast<unsigned long>(rhs.get())
                && lhs->get_user_name() == rhs->get_user_name();
    }
    static bool cmp_whole_value(User_ptr const & lhs, User_ptr const & rhs)
    {
        return reinterpret_cast<unsigned long>(lhs.get()) * reinterpret_cast<unsigned long>(rhs.get())
                && *lhs == *rhs;
    }
}

struct AsyncUserDAO::AsyncImpl
    : AsyncImplBase<User_ptr, decltype(std::ptr_fun(cmp_primary_key)), decltype(std::ptr_fun(cmp_whole_value)), PgUserDAO>
{
    AsyncImpl(svr::common::PropertiesFileReader& sqlProperties, svr::dao::DataSource& dataSource)
    :AsyncImplBase(sqlProperties, dataSource, std::ptr_fun(cmp_primary_key), std::ptr_fun(cmp_whole_value), 10, 10)
    {}

    void store(User_ptr user)
    {
        std::lock_guard<std::mutex> lg(pgMutex);
        if(pgDao.exists(user->get_user_name()))
            pgDao.update(user);
        else
        {
            user->set_id(pgDao.get_next_id());
            pgDao.save(user);
        }
    }
};

AsyncUserDAO::AsyncUserDAO(svr::common::PropertiesFileReader& sqlProperties, svr::dao::DataSource& dataSource)
: UserDAO(sqlProperties, dataSource), pImpl(*new AsyncImpl(sqlProperties, dataSource))
{}

AsyncUserDAO::~AsyncUserDAO()
{
    delete & pImpl;
}

User_ptr AsyncUserDAO::get_by_user_name(const std::string& user_name)
{
    User_ptr user{ std::make_shared<svr::datamodel::User>() };
    user->set_user_name(user_name);

    pImpl.seekAndCache(user, &PgUserDAO::get_by_user_name, user_name);
    return user;
}

std::vector<User_ptr> AsyncUserDAO::get_all_users()
{
    std::lock_guard<std::mutex> lg(pImpl.pgMutex);
    return pImpl.pgDao.get_all_users();
}

std::vector<User_ptr> AsyncUserDAO::get_all_users_by_priority()
{
    std::lock_guard<std::mutex> lg(pImpl.pgMutex);
    return pImpl.pgDao.get_all_users_by_priority();
}

bigint AsyncUserDAO::get_next_id()
{
    std::lock_guard<std::mutex> lg(pImpl.pgMutex);
    return pImpl.pgDao.get_next_id();
}

bool AsyncUserDAO::exists(std::string const & userName)
{
    User_ptr user{ std::make_shared<svr::datamodel::User>() };
    user->set_user_name(userName);

    if(pImpl.cached(user))
        return true;

    std::lock_guard<std::mutex> lg(pImpl.pgMutex);
    return pImpl.pgDao.exists(userName);
}

int AsyncUserDAO::save(const User_ptr& user)
{
    pImpl.cache(user);
    return 1;
}

int AsyncUserDAO::update(const User_ptr& user)
{
    pImpl.cache(user);
    return 1;
}

int AsyncUserDAO::remove(const User_ptr& user)
{
    return pImpl.remove(user);
    return 1;
}

bool AsyncUserDAO::login(const std::string& user_name, const std::string& enc_password)
{
    std::lock_guard<std::mutex> lg(pImpl.pgMutex);
    return pImpl.pgDao.login(user_name, enc_password);
}

} }
