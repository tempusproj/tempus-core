#include "AsyncIQScalingFactorDAO.hpp"
#include "AsyncImplBase.hpp"
#include <model/IQScalingFactor.hpp>
#include <DAO/DataSource.hpp>
#include <DAO/IQScalingFactorRowMapper.hpp>
#include "../PgDAO/PgIQScalingFactorDAO.hpp"

namespace svr {
namespace dao {

namespace
{
    static bool cmp_primary_key(IQScalingFactor_ptr const & lhs, IQScalingFactor_ptr const & rhs)
    {
        return reinterpret_cast<unsigned long>(lhs.get()) * reinterpret_cast<unsigned long>(rhs.get())
                && lhs->get_id() == rhs->get_id();
    }
    static bool cmp_whole_value(IQScalingFactor_ptr const & lhs, IQScalingFactor_ptr const & rhs)
    {
        return reinterpret_cast<unsigned long>(lhs.get()) * reinterpret_cast<unsigned long>(rhs.get())
                && *lhs == *rhs;
    }
}

struct AsyncIQScalingFactorDAO::AsyncImpl
    : AsyncImplBase<IQScalingFactor_ptr, decltype(std::ptr_fun(cmp_primary_key)), decltype(std::ptr_fun(cmp_whole_value)), PgIQScalingFactorDAO>
{
    AsyncImpl(svr::common::PropertiesFileReader& sqlProperties, svr::dao::DataSource& dataSource)
    :AsyncImplBase(sqlProperties, dataSource, std::ptr_fun(cmp_primary_key), std::ptr_fun(cmp_whole_value), 10, 10)
    {}
};

AsyncIQScalingFactorDAO::AsyncIQScalingFactorDAO(svr::common::PropertiesFileReader& sqlProperties, svr::dao::DataSource& dataSource)
: IQScalingFactorDAO (sqlProperties, dataSource), pImpl(* new AsyncImpl(sqlProperties, dataSource))
{}

AsyncIQScalingFactorDAO::~AsyncIQScalingFactorDAO()
{
    delete & pImpl;
}

bigint AsyncIQScalingFactorDAO::get_next_id()
{
    return data_source.query_for_type<bigint>(AbstractDAO::get_sql("get_next_id"));
}

bool AsyncIQScalingFactorDAO::exists(bigint id)
{
    return data_source.query_for_type<int>(AbstractDAO::get_sql("exists_by_id"), id) == 1;
}

int AsyncIQScalingFactorDAO::save(const IQScalingFactor_ptr& iQScalingFactor)
{
    if(!iQScalingFactor->get_id())
    {
        iQScalingFactor->set_id(get_next_id());
        return data_source.update(AbstractDAO::get_sql("save"),
                                  iQScalingFactor->get_dataset_id(),
                                  iQScalingFactor->get_input_queue_table_name(),
                                  iQScalingFactor->get_scaling_factor(),
                                  iQScalingFactor->get_id()
                                  );
    }
    return data_source.update(AbstractDAO::get_sql("update"),
                              iQScalingFactor->get_dataset_id(),
                              iQScalingFactor->get_input_queue_table_name(),
                              iQScalingFactor->get_scaling_factor(),
                              iQScalingFactor->get_id()
                              );

}

int AsyncIQScalingFactorDAO::remove(const IQScalingFactor_ptr& iQScalingFactor)
{
    if(iQScalingFactor->get_id() == 0)
        return 0;

    return data_source.update(AbstractDAO::get_sql("remove"), iQScalingFactor->get_id());
}

std::vector<IQScalingFactor_ptr> svr::dao::AsyncIQScalingFactorDAO::find_all_by_dataset_id(const bigint dataset_id)
{
    IQScalingFactorRowMapper rowMaper;
    return data_source.query_for_array(&rowMaper, AbstractDAO::get_sql("find_all_by_dataset_id"), dataset_id);
}


}
}

