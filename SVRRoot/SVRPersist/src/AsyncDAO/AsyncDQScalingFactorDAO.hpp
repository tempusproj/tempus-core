#pragma once

#include <DAO/DQScalingFactorDAO.hpp>

namespace svr {
namespace dao {

class AsyncDQScalingFactorDAO : public DQScalingFactorDAO
{
public:
    explicit AsyncDQScalingFactorDAO(svr::common::PropertiesFileReader& sqlProperties, svr::dao::DataSource& dataSource);
    ~AsyncDQScalingFactorDAO();

    virtual bigint get_next_id();
    virtual bool exists(const bigint id);
    virtual int save(const DQScalingFactor_ptr& dQScalingFactor);
    virtual int remove(const DQScalingFactor_ptr& dQscalingFactor);
    virtual std::vector<DQScalingFactor_ptr> find_all_by_dataset_id(const bigint dataset_id);

private:
    class AsyncImpl;
    AsyncImpl & pImpl;
};

}
}
