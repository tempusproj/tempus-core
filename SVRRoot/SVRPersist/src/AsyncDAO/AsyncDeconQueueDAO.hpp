#ifndef ASYNCDECONQUEUEDAO_HPP
#define ASYNCDECONQUEUEDAO_HPP

#include <DAO/DeconQueueDAO.hpp>

namespace svr {
namespace dao {

class AsyncDeconQueueDAO : public DeconQueueDAO
{
public:
    explicit AsyncDeconQueueDAO(svr::common::PropertiesFileReader& sql_properties, svr::dao::DataSource& data_source);
    ~AsyncDeconQueueDAO();

    DeconQueue_ptr          get_decon_queue_by_table_name(const std::string &tableName);

    std::vector<DataRow_ptr> get_data(const std::string& deconQueueTableName, const bpt::ptime& timeFrom = bpt::min_date_time, const bpt::ptime& timeTo = bpt::max_date_time, size_t limit = 0);
    std::vector<DataRow_ptr> get_latest_data(const std::string& deconQueueTableName, const bpt::ptime& timeTo = bpt::max_date_time, size_t limit = 0); /* TODO Andrey test please :) */
    std::vector<DataRow_ptr> get_data_having_update_time_greater_than(const std::string& deconQueueTableName, const bpt::ptime& updateTime, long limit = 0);

    bool exists(const std::string& tableName);
    bool exists(const DeconQueue_ptr& deconQueue);

    void save(const DeconQueue_ptr& deconQueue);

    int remove(const DeconQueue_ptr& deconQueue);
    int clear(const DeconQueue_ptr& deconQueue);

    long count(const DeconQueue_ptr& deconQueue);
private:
    struct AsyncImpl;
    AsyncImpl & pImpl;
};

} }


#endif /* ASYNCDECONQUEUEDAO_HPP */

