#include "AsyncDecrementTaskDAO.hpp"
#include "AsyncImplBase.hpp"
#include <model/DecrementTask.hpp>
#include <DAO/DataSource.hpp>
#include <DAO/DecrementTaskRowMapper.hpp>
#include "../PgDAO/PgDecrementTaskDAO.hpp"

namespace svr {
namespace dao {

namespace
{
    static bool cmp_primary_key(DecrementTask_ptr const & lhs, DecrementTask_ptr const & rhs)
    {
        return reinterpret_cast<unsigned long>(lhs.get()) * reinterpret_cast<unsigned long>(rhs.get())
                && lhs->get_id() == rhs->get_id();
    }
    static bool cmp_whole_value(DecrementTask_ptr const & lhs, DecrementTask_ptr const & rhs)
    {
        return reinterpret_cast<unsigned long>(lhs.get()) * reinterpret_cast<unsigned long>(rhs.get())
                && *lhs == *rhs;
    }
}

struct AsyncDecrementTaskDAO::AsyncImpl
    : AsyncImplBase<DecrementTask_ptr, decltype(std::ptr_fun(cmp_primary_key)), decltype(std::ptr_fun(cmp_whole_value)), PgDecrementTaskDAO>
{
    AsyncImpl(svr::common::PropertiesFileReader& sqlProperties, svr::dao::DataSource& dataSource)
    :AsyncImplBase(sqlProperties, dataSource, std::ptr_fun(cmp_primary_key), std::ptr_fun(cmp_whole_value), 10, 10)
    {}
};

AsyncDecrementTaskDAO::AsyncDecrementTaskDAO(svr::common::PropertiesFileReader& sqlProperties, svr::dao::DataSource& dataSource)
: DecrementTaskDAO (sqlProperties, dataSource), pImpl(* new AsyncImpl(sqlProperties, dataSource))
{}

AsyncDecrementTaskDAO::~AsyncDecrementTaskDAO()
{
    delete & pImpl;
}

bigint AsyncDecrementTaskDAO::get_next_id()
{
    return data_source.query_for_type<bigint>(AbstractDAO::get_sql("get_next_id"));
}

bool AsyncDecrementTaskDAO::exists(bigint id)
{
    return data_source.query_for_type<int>(AbstractDAO::get_sql("exists_by_id"), id) == 1;
}

int AsyncDecrementTaskDAO::save(const DecrementTask_ptr& decrementTask)
{
    if(!decrementTask->get_id())
    {
        decrementTask->set_id(get_next_id());
        return data_source.update(AbstractDAO::get_sql("save"),
                                  decrementTask->get_id(),
                                  decrementTask->get_dataset_id(),
                                  decrementTask->get_start_task_time(),
                                  decrementTask->get_end_task_time(),
                                  decrementTask->get_start_train_time(),
                                  decrementTask->get_end_train_time(),
                                  decrementTask->get_start_validation_time(),
                                  decrementTask->get_end_validation_time(),
                                  decrementTask->get_parameters(),
                                  decrementTask->get_status(),
                                  decrementTask->get_decrement_step(),
                                  decrementTask->get_vp_sliding_direction(),
                                  decrementTask->get_vp_slide_count(),
                                  decrementTask->get_vp_slide_period_sec().total_seconds(),
                                  decrementTask->get_values(),
                                  decrementTask->get_suggested_value()
                                  );
    }
    return data_source.update(AbstractDAO::get_sql("update"),
                              decrementTask->get_dataset_id(),
                              decrementTask->get_start_task_time(),
                              decrementTask->get_end_task_time(),
                              decrementTask->get_start_train_time(),
                              decrementTask->get_end_train_time(),
                              decrementTask->get_start_validation_time(),
                              decrementTask->get_end_validation_time(),
                              decrementTask->get_parameters(),
                              decrementTask->get_status(),
                              decrementTask->get_decrement_step(),
                              decrementTask->get_vp_sliding_direction(),
                              decrementTask->get_vp_slide_count(),
                              decrementTask->get_vp_slide_period_sec().total_seconds(),
                              decrementTask->get_values(),
                              decrementTask->get_suggested_value(),
                              decrementTask->get_id()
                              );

}

int AsyncDecrementTaskDAO::remove(const DecrementTask_ptr& decrementTask)
{
    if(decrementTask->get_id() == 0)
        return 0;

    return data_source.update(AbstractDAO::get_sql("remove"), decrementTask->get_id());
}

DecrementTask_ptr AsyncDecrementTaskDAO::get_by_id(bigint id)
{
    DecrementTaskRowMapper rowMaper;
    return data_source.query_for_object(&rowMaper, AbstractDAO::get_sql("get_by_id"), id);
}

}
}

