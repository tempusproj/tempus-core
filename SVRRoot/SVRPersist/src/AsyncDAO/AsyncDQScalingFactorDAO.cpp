#include "AsyncDQScalingFactorDAO.hpp"
#include "AsyncImplBase.hpp"
#include <model/DQScalingFactor.hpp>
#include <DAO/DataSource.hpp>
#include <DAO/DQScalingFactorRowMapper.hpp>
#include "../PgDAO/PgDQScalingFactorDAO.hpp"

namespace svr {
namespace dao {

namespace
{
    static bool cmp_primary_key(DQScalingFactor_ptr const & lhs, DQScalingFactor_ptr const & rhs)
    {
        return reinterpret_cast<unsigned long>(lhs.get()) * reinterpret_cast<unsigned long>(rhs.get())
                && lhs->get_id() == rhs->get_id();
    }
    static bool cmp_whole_value(DQScalingFactor_ptr const & lhs, DQScalingFactor_ptr const & rhs)
    {
        return reinterpret_cast<unsigned long>(lhs.get()) * reinterpret_cast<unsigned long>(rhs.get())
                && *lhs == *rhs;
    }
}

struct AsyncDQScalingFactorDAO::AsyncImpl
    : AsyncImplBase<DQScalingFactor_ptr, decltype(std::ptr_fun(cmp_primary_key)), decltype(std::ptr_fun(cmp_whole_value)), PgDQScalingFactorDAO>
{
    AsyncImpl(svr::common::PropertiesFileReader& sqlProperties, svr::dao::DataSource& dataSource)
    :AsyncImplBase(sqlProperties, dataSource, std::ptr_fun(cmp_primary_key), std::ptr_fun(cmp_whole_value), 10, 10)
    {}
};

AsyncDQScalingFactorDAO::AsyncDQScalingFactorDAO(svr::common::PropertiesFileReader& sqlProperties, svr::dao::DataSource& dataSource)
: DQScalingFactorDAO (sqlProperties, dataSource), pImpl(* new AsyncImpl(sqlProperties, dataSource))
{}

AsyncDQScalingFactorDAO::~AsyncDQScalingFactorDAO()
{
    delete & pImpl;
}

bigint AsyncDQScalingFactorDAO::get_next_id()
{
    return data_source.query_for_type<bigint>(AbstractDAO::get_sql("get_next_id"));
}

bool AsyncDQScalingFactorDAO::exists(bigint id)
{
    return data_source.query_for_type<int>(AbstractDAO::get_sql("exists_by_id"), id) == 1;
}

int AsyncDQScalingFactorDAO::save(const DQScalingFactor_ptr& dQScalingFactor)
{
    if(!dQScalingFactor->get_id())
    {
        dQScalingFactor->set_id(get_next_id());
        return data_source.update(AbstractDAO::get_sql("save"),
                                  dQScalingFactor->get_id(),
                                  dQScalingFactor->get_dataset_id(),
                                  dQScalingFactor->get_input_queue_table_name(),
                                  dQScalingFactor->get_input_queue_column_name(),
                                  dQScalingFactor->get_wavelet_level(),
                                  dQScalingFactor->get_scaling_factor()
                                  );
    }
    return data_source.update(AbstractDAO::get_sql("update"),
                              dQScalingFactor->get_dataset_id(),
                              dQScalingFactor->get_input_queue_table_name(),
                              dQScalingFactor->get_input_queue_column_name(),
                              dQScalingFactor->get_wavelet_level(),
                              dQScalingFactor->get_scaling_factor(),
                              dQScalingFactor->get_id()
                              );

}

int AsyncDQScalingFactorDAO::remove(const DQScalingFactor_ptr& dQScalingFactor)
{
    if(dQScalingFactor->get_id() == 0)
        return 0;

    return data_source.update(AbstractDAO::get_sql("remove"), dQScalingFactor->get_id());
}

std::vector<DQScalingFactor_ptr> svr::dao::AsyncDQScalingFactorDAO::find_all_by_dataset_id(const bigint dataset_id)
{
    DQScalingFactorRowMapper rowMaper;
    return data_source.query_for_array(&rowMaper, AbstractDAO::get_sql("find_all_by_dataset_id"), dataset_id);
}


}
}

