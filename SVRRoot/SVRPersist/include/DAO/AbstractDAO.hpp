#pragma once

#include "common/types.hpp"
#include "util/PropertiesFileReader.hpp"


namespace svr {
namespace dao {

class DataSource;

class AbstractDAO {
    svr::common::PropertiesFileReader& sql_properties;
    const std::string propsFileName;
protected:
    DataSource& data_source;

    virtual ~AbstractDAO();
    explicit AbstractDAO(svr::common::PropertiesFileReader& sql_properties, DataSource& data_source, std::string propsFileName);

    virtual std::string get_sql(const std::string& sqlKey);
public:
    DataSource& get_data_source(){ return data_source; }
};

} /* namespace dao */
} /* namespace svr */

using AbstractDAO_ptr = std::shared_ptr<svr::dao::AbstractDAO>;
