#pragma once

#include "DAO/IRowMapper.hpp"
#include "model/IQScalingFactor.hpp"

namespace svr {
namespace dao {

class IQScalingFactorRowMapper : public IRowMapper<svr::datamodel::IQScalingFactor>
{
private:
    //empty

public:
    IQScalingFactor_ptr mapRow(const pqxx::tuple& rowSet) override
    {
        return std::make_shared<svr::datamodel::IQScalingFactor>(
                    rowSet["id"].as<bigint>(),
                    rowSet["dataset_id"].is_null() ? 0 : rowSet["dataset_id"].as<bigint>(),
                    rowSet["input_queue_table_name"].is_null() ? "" : rowSet["input_queue_table_name"].as<std::string>(),
                    rowSet["scaling_factor"].is_null() ? 1.0 : rowSet["scaling_factor"].as<double>()
                );
    }

};

}
}
