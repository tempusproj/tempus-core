#pragma once

#include "DAO/IRowMapper.hpp"
#include "model/PredictionTask.hpp"

namespace svr{
namespace dao{

class PredictionTaskRowMapper : public IRowMapper<svr::datamodel::PredictionTask>{
public:
    PredictionTask_ptr mapRow(const pqxx::tuple& rowSet) override {

        return std::make_shared<svr::datamodel::PredictionTask>(
                rowSet["id"].as<bigint>(),
                rowSet["dataset_id"].is_null() ? 0 : rowSet["dataset_id"].as<bigint>(),
                rowSet["start_time"].is_null() ? bpt::ptime() : bpt::time_from_string(rowSet["start_time"].as<std::string>()),
                rowSet["end_time"].is_null() ? bpt::ptime() : bpt::time_from_string(rowSet["end_time"].as<std::string>()),
                rowSet["start_prediction_time"].is_null() ? bpt::ptime() : bpt::time_from_string(rowSet["start_prediction_time"].as<std::string>()),
                rowSet["end_prediction_time"].is_null() ? bpt::ptime() : bpt::time_from_string(rowSet["end_prediction_time"].as<std::string>()),
                rowSet["status"].is_null() ? 0 : rowSet["status"].as<int>(),
                rowSet["mse"].is_null() ? 0 : rowSet["mse"].as<double>()
        );
    }
};
}
}
