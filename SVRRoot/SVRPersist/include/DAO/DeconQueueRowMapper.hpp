#pragma once

#include "DAO/IRowMapper.hpp"
#include "model/DeconQueue.hpp"

namespace svr {
namespace dao {

class DeconQueueRowMapper : public IRowMapper<svr::datamodel::DeconQueue> {
public:
    DeconQueue_ptr mapRow(const pqxx::tuple &rowSet) override {

        return std::make_shared<svr::datamodel::DeconQueue>(
                rowSet["table_name"].as<std::string>(),
                rowSet["input_queue_table_name"].as<std::string>(),
                rowSet["input_queue_column_name"].as<std::string>(),
                rowSet["dataset_id"].as<bigint>()
        );
    }
};
}
}
