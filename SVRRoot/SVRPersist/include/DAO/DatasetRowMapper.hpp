#pragma once

#include "DAO/IRowMapper.hpp"
#include "model/Dataset.hpp"

namespace svr{
namespace dao{

class DatasetRowMapper : public IRowMapper<svr::datamodel::Dataset>{
public:
    Dataset_ptr mapRow(const pqxx::tuple& rowSet) override{

        using svr::common::ignoreCaseEquals;

        svr::datamodel::Priority priority = svr::datamodel::Priority::Normal;

        try {
            rowSet.column_number("priority"); // if not -> throw exception
            priority = rowSet["priority"].is_null() ? svr::datamodel::Priority::Normal
                                                    : static_cast<svr::datamodel::Priority>(rowSet["priority"].as<int>());
        }
        catch(...)
        {}
//        InputQueue_ptr p_input_queue;
//        if (!rowSet["input_data_table_name"].is_null()) {
//            p_input_queue = std::make_shared<svr::datamodel::InputQueue>(svr::datamodel::InputQueue());
//            p_input_queue->set_table_name(rowSet["input_data_table_name"].as<std::string>());
//        }
        return std::make_shared<svr::datamodel::Dataset>(
                rowSet["id"].as<bigint>(),
                rowSet["dataset_name"].is_null() ? "" : rowSet["dataset_name"].as<std::string>(),
                rowSet["user_name"].is_null() ? "" : rowSet["user_name"].as<std::string>(),
                rowSet["main_input_queue_table_name"].is_null() ? "" : rowSet["main_input_queue_table_name"].as<std::string>(),
                svr::common::from_sql_array(rowSet["aux_input_queues_table_names"].is_null()
                                            ? "" : rowSet["aux_input_queues_table_names"].as<std::string>()),
                priority,
                rowSet["description"].is_null() ? "" : rowSet["description"].as<std::string>(),
                rowSet["swt_levels"].is_null() ? 0 : rowSet["swt_levels"].as<size_t>(),
                rowSet["swt_wavelet_name"].is_null() ? "" : rowSet["swt_wavelet_name"].as<std::string>(),
                rowSet["max_gap"].is_null() ? bpt::time_duration() : bpt::duration_from_string(rowSet["max_gap"].as<std::string>()),
                std::vector<Ensemble_ptr>(),
                rowSet["is_active"].is_null() ? false : rowSet["is_active"].as<bool>()
        );
    }
};

class UserDatasetRowMapper : public IRowMapper<std::pair<std::string, Dataset_ptr>>{
    DatasetRowMapper datasetRowMapper;
public:
    std::shared_ptr<std::pair<std::string, Dataset_ptr>> mapRow(const pqxx::tuple& rowSet) override {
        return std::make_shared<std::pair<std::string, Dataset_ptr>>
        (
              rowSet["linked_user_name"].as<std::string>()
            , datasetRowMapper.mapRow(rowSet)
        );
    }
};

}
}
