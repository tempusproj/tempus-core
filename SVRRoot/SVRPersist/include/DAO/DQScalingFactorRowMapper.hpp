#pragma once

#include "DAO/IRowMapper.hpp"
#include "model/DQScalingFactor.hpp"

namespace svr {
namespace dao {

class DQScalingFactorRowMapper : public IRowMapper<svr::datamodel::DQScalingFactor>
{
private:
    //empty

public:
    DQScalingFactor_ptr mapRow(const pqxx::tuple& rowSet) override
    {
        return std::make_shared<svr::datamodel::DQScalingFactor>(
                    rowSet["id"].as<bigint>(),
                    rowSet["dataset_id"].is_null() ? 0 : rowSet["dataset_id"].as<bigint>(),
                    rowSet["input_queue_table_name"].is_null() ? "" : rowSet["input_queue_table_name"].as<std::string>(),
                    rowSet["input_queue_column_name"].is_null() ? "" : rowSet["input_queue_column_name"].as<std::string>(),
                    rowSet["wavelet_level"].is_null() ? 0 : rowSet["wavelet_level"].as<size_t>(),
                    rowSet["scaling_factor"].is_null() ? 1.0 : rowSet["scaling_factor"].as<double>()
                );
    }

};

}
}
