#pragma once

#include <DAO/AbstractDAO.hpp>

namespace svr { namespace datamodel { class ScalingFactor; } }
using ScalingFactor_ptr = std::shared_ptr<svr::datamodel::ScalingFactor>;

namespace svr {
namespace dao {

class ScalingFactorDAO : public AbstractDAO
{
public:
    static ScalingFactorDAO * build(svr::common::PropertiesFileReader& sql_properties, svr::dao::DataSource& data_source, svr::common::ConcreteDaoType daoType);

    explicit ScalingFactorDAO(svr::common::PropertiesFileReader& sql_properties, svr::dao::DataSource& data_source);

    virtual bigint get_next_id()= 0;
    virtual bool exists(const bigint id) = 0;
    virtual int save(const ScalingFactor_ptr& scalingTask) = 0;
    virtual int remove(const ScalingFactor_ptr& scalingTask) = 0;
    virtual ScalingFactor_ptr get_by_id(const bigint id) = 0;
    virtual std::vector<ScalingFactor_ptr> find_all_by_dataset_id(const bigint dataset_id) = 0;
};

}
}

using ScalingFactorDAO_ptr = std::shared_ptr<svr::dao::ScalingFactorDAO>;
