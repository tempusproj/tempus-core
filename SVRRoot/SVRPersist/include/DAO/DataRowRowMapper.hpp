#pragma once

#include <pqxx/tuple.hxx>
#include "DAO/IRowMapper.hpp"
#include "model/DataRow.hpp"

namespace svr {
namespace dao {

class DataRowRowMapper : public IRowMapper<svr::datamodel::DataRow> {
public:
    DataRow_ptr mapRow(const pqxx::tuple &rowSet) override {

        std::vector<double> levels;

        for(unsigned int colIdx = 3; colIdx < rowSet.size(); colIdx++){
            levels.push_back(rowSet.at(colIdx).as<double>());
        }

        return std::make_shared<svr::datamodel::DataRow>(
                bpt::time_from_string(rowSet["value_time"].as<std::string>()),
                bpt::time_from_string(rowSet["update_time"].as<std::string>()),
                rowSet["tick_volume"].as<double>(),
                levels
        );
    }
};
}
}