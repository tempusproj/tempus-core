/*
 * InputQueueRowRowMapper.hpp
 *
 *  Created on: Aug 7, 2014
 *      Author: vg
 */

#pragma once

#include "DAO/IRowMapper.hpp"
#include "model/DataRow.hpp"

namespace svr {
namespace dao {

class InputQueueRowRowMapper: public IRowMapper<svr::datamodel::DataRow> {
public:

    DataRow_ptr mapRow(const pqxx::tuple& rowSet) override{

		std::vector<double> values;
		for(auto values_iter = rowSet.begin() + 4; values_iter != rowSet.end(); values_iter++){
			values.push_back(svr::common::Round(values_iter.as<double>()));
		}

		return std::make_shared<svr::datamodel::DataRow>(
			bpt::time_from_string(rowSet["value_time"].as<std::string>()),
			bpt::time_from_string(rowSet["update_time"].as<std::string>()),
			svr::common::Round(rowSet["tick_volume"].as<double>()),
			values
		);
	}
};

} /* namespace dao */
} /* namespace svr */
