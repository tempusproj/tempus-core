/*
 * common.hpp
 *
 *  Created on: Jul 27, 2014
 *      Author: vg
 */

#pragma once
// external dependencies
#include "common/types.hpp"
#include <pqxx/pqxx>
#include "util/CompressionUtils.hpp"
//#include <pqxx/except.hxx>
#include <boost/thread.hpp>
#include "misc/boostPqxxConvertor.hpp"

using Lock = boost::lock_guard<boost::mutex>;
using RecursiveLock = boost::lock_guard<boost::recursive_mutex>;
using ParamsMap = std::map<std::string, std::string>;
