#pragma once

#include "DAO/IRowMapper.hpp"
#include "model/SVRParameters.hpp"

namespace svr{
namespace dao{

class SVRParametersRowMapper : public IRowMapper<svr::datamodel::SVRParameters>{
public:
    SVRParameters_ptr mapRow(const pqxx::tuple& rowSet) override{

        return std::make_shared<svr::datamodel::SVRParameters>(
                rowSet["id"].is_null() ? 0 : rowSet["id"].as<bigint>(),
                rowSet["dataset_id"].is_null() ? 0 : rowSet["dataset_id"].as<bigint>(),
                rowSet["input_queue_table_name"].is_null() ? std::string() : rowSet["input_queue_table_name"].as<std::string>(),
                rowSet["input_queue_column_name"].is_null() ? std::string() : rowSet["input_queue_column_name"].as<std::string>(),
                rowSet["decon_level"].is_null() ? 0 : rowSet["decon_level"].as<size_t>(),
                rowSet["svr_c"].is_null() ? 1.0 : rowSet["svr_c"].as<double>(),
                rowSet["svr_epsilon"].is_null() ? 1.0 : rowSet["svr_epsilon"].as<double>(),
                rowSet["svr_kernel_param"].is_null() ? 1.0 : rowSet["svr_kernel_param"].as<double>(),
                rowSet["svr_kernel_param2"].is_null() ? 1.0 : rowSet["svr_kernel_param2"].as<double>(),
                rowSet["svr_decremental_distance"].is_null() ? 1 : rowSet["svr_decremental_distance"].as<bigint>(),
                rowSet["svr_adjacent_levels_ratio"].is_null() ? 1.0 : rowSet["svr_adjacent_levels_ratio"].as<double>(),
                rowSet["svr_kernel_type"].is_null() ? datamodel::kernel_type_e::RBF :
                    static_cast<datamodel::kernel_type_e>(rowSet["svr_kernel_type"].as<size_t>()),
                rowSet["lag_count"].is_null() ? 0 : rowSet["lag_count"].as<size_t>()
        );
    }
};
}
}
