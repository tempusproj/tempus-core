#pragma once

#include "common.hpp"
#include "model/User.hpp"
#include "model/Dataset.hpp"
#include "OnlineSVR.tcc"

namespace bpt = boost::posix_time;

namespace svr {
namespace dao {

class StatementPreparerDBTemplate {

	std::shared_ptr<pqxx::lazyconnection> connection;

	/**
	 * Converts ptime object into postgres <code>timestamp without time zone</code>.
	 * example output:<code>'18-6-2011 21:59:40'::timestamp
	 * @param time the datetime object
	 * @return string representing datetime in the format expected by the underlying database
	 */

    std::string escape(std::nullptr_t);
	std::string escape(const std::shared_ptr<svr::OnlineSVR>& model);
	std::string escape(const svr::datamodel::Priority& priority);
	std::string escape(const bpt::ptime&);
	std::string escape(const bpt::time_duration&);
	std::string escape(const std::string&);
	std::string escape(const char* s);
	inline std::string escape(char c){std::string result; result += "'"; result += c; result += "'"; return result;}
	std::string escape(const bool& );
	inline std::string escape(const long v) { return std::to_string(v);}
	inline std::string escape(const bigint v){ return std::to_string(v);}
	std::string escape(svr::datamodel::User::ROLE role);
	// handle "escaping" of non-escapable values (eg. of type double, int, float, etc.)
	template<typename T>	inline std::string escape(T t){
		return boost::lexical_cast<std::string>(t);
	}
	template<typename InputIterator> inline std::vector<std::string> escape(InputIterator begin, InputIterator end){
		std::vector<std::string> r;
		for(;begin != end; begin++){
			std::string tmp = escape(*begin);
            size_t pos = tmp.find_first_of("'");
            if (pos != std::string::npos)
            {
                tmp.replace(pos, 1, "\"");
                pos = tmp.find_last_of("'");
                if (pos != std::string::npos)
                    tmp.replace(pos, 1, "\"");
            }
			r.push_back(tmp);
		}
		return r;
	}
	template<typename T> inline std::string escape(const std::vector<T>& v){
		std::stringstream ss;
		std::vector<std::string> vals = escape(begin(v), end(v));
		ss << "'{";
		if(vals.size() > 0)
			ss << vals[0];
		for(size_t colNum = 1; colNum < vals.size(); colNum++)
			ss << ", " << vals[colNum];
		ss << "}'";

		return ss.str();
	}
	// handle recursive call when no other var args are present
	// (overload of variadic template function prepareStatement)
	std::string prepareStatement(const char* format);

public:

	StatementPreparerDBTemplate(std::shared_ptr<pqxx::lazyconnection> connection);
	virtual ~StatementPreparerDBTemplate(){}

	/**
	 * function for escaping of strings with variable number of arguments.
	 * Note that only char and string (i.e. const char *) are really escaped
	 * @param format is the format string consisting of ? chars which replace one argument
	 * @param value this is automatically resolved to the first argument from the vararg. list
	 * @param Fargs this is sort of an array of (possibly) different types
	 * @return the query string with replaced ? with the escaped arguments
	 */
	template<typename T, typename... Targs>
	std::string prepareStatement(const char* format, T value, Targs... Fargs){
		std::string s;
		for (; *format != '\0'; format++) {
			if (*format == '?') { // ? is the argument placeholder which will be replaced with concrete escaped value
				s += (std::string) (escape(value));
				s += (std::string) (prepareStatement(format + 1, Fargs...)); // recursive call
				return s;
			}
			s += *format;
		}
		return s;
	}
	/**
	 * Overload of the function with the same name for more convenient invocation
	 */
	template<typename T, typename... Targs>
	std::string prepareStatement(const std::string& format, T value, Targs... Fargs){
		return prepareStatement(format.c_str(), value, Fargs...);
	}

	std::string prepareStatement(const std::string &format){
		return format;
	}
};

} /* namespace dao */
} /* namespace svr */

using StatementPreparerDBTemplate_ptr = std::shared_ptr<svr::dao::StatementPreparerDBTemplate>;

