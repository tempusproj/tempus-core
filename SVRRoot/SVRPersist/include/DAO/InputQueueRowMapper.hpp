#pragma once

#include "common.hpp"
#include <util/StringUtils.hpp>
#include "IRowMapper.hpp"
#include <model/InputQueue.hpp>

namespace svr {
namespace dao {

class InputQueueRowMapper : public IRowMapper<svr::datamodel::InputQueue>{
public:
	InputQueueRowMapper(){}
	virtual ~InputQueueRowMapper(){}

	InputQueue_ptr mapRow(const pqxx::tuple& rowSet) override{

            if(rowSet["table_name"].is_null())
                throw std::runtime_error("Cannot map a row with empty table_name");

                InputQueue_ptr result = std::make_shared<svr::datamodel::InputQueue>(
                rowSet["table_name"].as<std::string>(),
                rowSet["logical_name"].is_null() ? "" : rowSet["logical_name"].as<std::string>(),
                rowSet["user_name"].is_null()   ? "" : rowSet["user_name"].as<std::string>(),
                rowSet["description"].is_null() ? "" : rowSet["description"].as<std::string>(),
                rowSet["resolution"].is_null()  ? bpt::time_duration()
                                                : rowSet["resolution"].as<bpt::time_duration>(),
                rowSet["legal_time_deviation"].is_null() ? bpt::time_duration()
                                                         : rowSet["legal_time_deviation"].as<bpt::time_duration>(),
                rowSet["timezone"].is_null()    ? bpt::time_duration()
                                                : rowSet["timezone"].as<bpt::time_duration>(),
                rowSet["value_columns"].is_null() ? std::vector<std::string>()
                                                   : svr::common::from_sql_array(rowSet["value_columns"].as<std::string>())
		);

		LOG4_DEBUG(result->to_string());
		return result;
	}

};


class InputQueueDbTableColumnsMapper : public IRowMapper<std::string>{
public:
	InputQueueDbTableColumnsMapper(){}
	virtual ~InputQueueDbTableColumnsMapper(){}

	std::shared_ptr<std::string> mapRow(const pqxx::tuple& rowSet) override
        {
            return std::make_shared<std::string>(rowSet[0].as<std::string>());
        }
};

} /* namespace dao */
} /* namespace svr */

