#pragma once

#include "common.hpp"


namespace svr{
	namespace dao{
	    class ScopedTransaction{
		    bool commit_on_destroy;
		    pqxx::work* trx;
            RecursiveLock trx_lock;
		    ScopedTransaction(const ScopedTransaction&) = delete;
		    void operator= (const ScopedTransaction& ) = delete;

	    public:
            explicit ScopedTransaction(pqxx::lazyconnection* connection, boost::recursive_mutex& trx_mutex, bool commit_on_destroy = true);

		    ~ScopedTransaction();

		    inline pqxx::work *getTrx(){return trx;}
	    };
	}
}

using ScopedTransaction_ptr = std::shared_ptr<svr::dao::ScopedTransaction>;