#pragma once

#include "common.hpp"
#include "DAO/IRowMapper.hpp"
#include "DAO/StatementPreparerDBTemplate.hpp"
#include "DAO/ScopedTransaction.hpp"

namespace svr {
namespace dao {

class DataSource {

private:
    std::shared_ptr<pqxx::lazyconnection> _connection;
    StatementPreparerDBTemplate_ptr statementPreparerTemplate;
    bool commit_on_scope_exit;
    boost::recursive_mutex trx_mutex;
    std::weak_ptr<ScopedTransaction> current_trx;

public:

    explicit DataSource(const std::string &connectionString, bool commit_on_scope_exit = true);

    virtual ~DataSource();

    void reopen_connection();
    ScopedTransaction_ptr open_transaction();
    ScopedTransaction_ptr open_transaction(bool auto_commit);

    inline StatementPreparerDBTemplate_ptr &getStatementPreparerTemplate() {
        return statementPreparerTemplate;
    }

    template<typename T, typename ...Args>
    std::shared_ptr<T> query_for_object(IRowMapper<T> *rowMapper,
            const std::string &sql, Args &&... args) {
        using namespace pqxx;

        if (sql.length() == 0) {
            LOG4_ERROR("Query cannot be null!");
            throw std::invalid_argument("Invalid SQL query passed!");
        }
        std::shared_ptr<T> object(nullptr);
        std::string query;
        try {

            ScopedTransaction_ptr trx = open_transaction();
            query = this->statementPreparerTemplate->prepareStatement(sql, args...);
            pqxx::result result;
            result = trx->getTrx()->exec(query);
            if(!result.empty())
                object = rowMapper->mapRow(result.at(0));

        } catch (const std::exception &ex) {
            auto what = ex.what();
            LOG4_ERROR("DataSource::query_for_object: exception " << what << " while executing [" << query << "] ");
            throw ex;
        }
        return object;
    }

    template<typename T, class ...Args>
    T query_for_type(const std::string &sql, Args &&... args){
        using namespace pqxx;
        if (sql.length() == 0) {
            LOG4_ERROR("Query cannot be null!");
            throw std::invalid_argument("Invalid SQL query passed!");
        }
        LOG4_DEBUG("Query for " << svr::common::demangle(typeid(T).name()) << " [" << sql << "]");
        std::string query;
        try {

            ScopedTransaction_ptr trx = open_transaction();
            query = this->statementPreparerTemplate->prepareStatement(sql, args...);
            pqxx::result result;
            result = trx->getTrx()->exec(query);
            if(result.empty())
                return T();
            else
                return result.at(0).at(0).as<T>();

        } catch (const std::exception &ex) {
            auto what = ex.what();
            LOG4_ERROR("DataSource::query_for_type: exception " << what << " while executing [" << query << "] ");
            throw ex;
        }
    }

    template<typename T, class ...Args>
    std::vector<T> query_for_type_array(const std::string &sql, Args &&... args){
        using namespace pqxx;
        if (sql.length() == 0) {
            LOG4_ERROR("Query cannot be null!");
            throw std::invalid_argument("Invalid SQL query passed!");
        }
        LOG4_DEBUG("Query for array of " << svr::common::demangle(typeid(T).name()) << " [" << sql << "]");
        std::string query;
        try {

            ScopedTransaction_ptr trx = open_transaction();
            query = this->statementPreparerTemplate->prepareStatement(sql, args...);
            pqxx::result result;
            result = trx->getTrx()->exec(query);

            std::vector<T> res;

            for(const auto& row : result){
                res.push_back(row.at(0).as<T>());
            }
            return res;

        } catch (const std::exception &ex) {
            auto what = ex.what();
            LOG4_ERROR("DataSource::query_for_type_array: exception " << what << " while executing [" << query << "] ");
            throw ex;
        }
    }

    template<typename ...Args>
    int update(const std::string &sql, Args &&... args) {
        using namespace pqxx;
        if (sql.length() == 0) {
            LOG4_ERROR("Query cannot be null!");
            throw std::invalid_argument("Invalid SQL query passed!");
        }
        LOG4_DEBUG("Update [" << sql << "]");
        std::string query;
        try {

            ScopedTransaction_ptr trx = open_transaction();
            query = this->statementPreparerTemplate->prepareStatement(sql, args...);
            pqxx::result result;

            result = trx->getTrx()->exec(query);
            return result.affected_rows();

        } catch (const pqxx::pqxx_exception &ex) {
            LOG4_ERROR("DataSource::update: exception " << ex.base().what() << " while executing " << query << "." );
            throw;
        }
    }

    template<typename T, typename ...Args>
    std::vector<std::shared_ptr<T>> query_for_array(IRowMapper<T> *rowMapper, const std::string &sql, Args &&...args) {
        using namespace pqxx;

        if (sql.length() == 0) {
            LOG4_ERROR("Query cannot be null!");
            throw std::invalid_argument("Invalid SQL query passed!");
        }
        std::vector<std::shared_ptr<T>> result;
        std::string query;
        try {
            query = this->statementPreparerTemplate->prepareStatement(sql, args...);

            ScopedTransaction_ptr trx = open_transaction();
            pqxx::result rawResult = trx->getTrx()->exec(query);

            for (const pqxx::tuple &row : rawResult) {
                result.push_back(rowMapper->mapRow(row));
            }
        } catch (const pqxx::pqxx_exception &ex) {
            LOG4_ERROR("DataSource::query_for_array: exception " << ex.base().what() << " while executing " << query << "." );
            throw;
        }

        return result;
    }

    long batch_update(
            const std::string &tableName,
            const std::vector<std::vector<std::string> > &data);

    void cleanup_queue_table(const std::string &tableName, std::vector<bpt::ptime const *> const & value_times);
};


} /* namespace dao */
} /* namespace svr */

using DataSource_ptr = std::shared_ptr<svr::dao::DataSource>;

