#pragma once

#include "DAO/AbstractDAO.hpp"

namespace svr { namespace datamodel { class DQScalingFactor; } }
using DQScalingFactor_ptr = std::shared_ptr<svr::datamodel::DQScalingFactor>;

namespace svr {
namespace dao {

class DQScalingFactorDAO : public AbstractDAO
{
public:
    static DQScalingFactorDAO* build(svr::common::PropertiesFileReader& sqlProperties,
                                      svr::dao::DataSource& dataSource,
                                      svr::common::ConcreteDaoType daoType);

    explicit DQScalingFactorDAO(svr::common::PropertiesFileReader& sqlProperties,
                                svr::dao::DataSource& dataSource);

    virtual bigint get_next_id() = 0;
    virtual bool exists(const bigint id) = 0;
    virtual int save(const DQScalingFactor_ptr& dQscalingFactor) = 0;
    virtual int remove(const DQScalingFactor_ptr& dQscalingFactor) = 0;
    virtual std::vector<DQScalingFactor_ptr> find_all_by_dataset_id(const bigint dataset_id) = 0;
};

} // namespace dao
} // namespace svr

using ScalingFactorDAO_ptr = std::shared_ptr<svr::dao::DQScalingFactorDAO>;
