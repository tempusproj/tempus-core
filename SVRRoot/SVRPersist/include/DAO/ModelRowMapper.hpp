#pragma once

#include <sstream>
#include "DAO/IRowMapper.hpp"
#include "model/Model.hpp"
#include "OnlineSVR.tcc"
#include <pqxx/binarystring>
#include "util/CompressionUtils.hpp"

namespace svr
{
namespace dao
{

class ModelRowMapper : public IRowMapper<svr::datamodel::Model>
{
public:
    Model_ptr mapRow(const pqxx::tuple &rowSet) override
    {
        std::shared_ptr<svr::OnlineSVR> model = nullptr;

        bool skip_loading_model = false;
        try { // TODO Rewrite this check
            (void) rowSet.column_number("model_binary");
        } catch (...) {
            skip_loading_model = true;
        }
        if (!skip_loading_model && !rowSet["model_binary"].is_null()) {
            pqxx::binarystring binary_model(rowSet["model_binary"]);
            std::stringstream ss;
            const char * pBinaryCompressed = reinterpret_cast<const char *>(binary_model.data());
            std::string decompressed = svr::common::decompress(pBinaryCompressed, binary_model.size());
            ss.rdbuf()->pubsetbuf(const_cast<char *>(decompressed.c_str()), decompressed.size());
            model = std::make_shared<svr::OnlineSVR>(ss);
        }
        std::vector<size_t> learning_levels;
        if (!rowSet["learning_levels"].is_null()) {
            std::vector<std::string> learning_levels_str = svr::common::from_sql_array(
                    rowSet["learning_levels"].as<std::string>());
            learning_levels.reserve(learning_levels_str.size());
            std::transform(learning_levels_str.begin(), learning_levels_str.end(),
                           std::back_inserter(learning_levels),
                           [](const std::string &val){return std::stoi(val);});
        }

        return std::make_shared<svr::datamodel::Model>(
                rowSet["id"].as<bigint>(),
                rowSet["ensemble_id"].is_null() ? 0 : rowSet["ensemble_id"].as<bigint>(),
                rowSet["decon_level"].is_null() ? 0 : rowSet["decon_level"].as<bigint>(),
                learning_levels,
                model,
                rowSet["last_modified_time"].is_null() ? bpt::not_a_date_time : bpt::time_from_string(
                        rowSet["last_modified_time"].as<std::string>()),
                rowSet["last_modeled_value_time"].is_null() ? bpt::not_a_date_time : bpt::time_from_string(
                        rowSet["last_modeled_value_time"].as<std::string>())
        );
    }
};
}
}
