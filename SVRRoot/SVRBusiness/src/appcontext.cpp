#include "appcontext.hpp"
#include "main-config.hpp"

#include "common.hpp"
#include <common/types.hpp>
#include <util/PropertiesFileReader.hpp>

#include <DAO/DataSource.hpp>
#include <DAO/AutotuneTaskDAO.hpp>
#include <DAO/DatasetDAO.hpp>
#include <DAO/DeconQueueDAO.hpp>
#include <DAO/DeconQueueDAO.hpp>
#include <DAO/DecrementTaskDAO.hpp>
#include <DAO/EnsembleDAO.hpp>
#include <DAO/InputQueueDAO.hpp>
#include <DAO/ModelDAO.hpp>
#include <DAO/RequestDAO.hpp>
#include <DAO/PredictionTaskDAO.hpp>
#include <DAO/SVRParametersDAO.hpp>
#include <DAO/IQScalingFactorDAO.hpp>
#include <DAO/DQScalingFactorDAO.hpp>
#include <DAO/UserDAO.hpp>

#include "../../SVRPersist/src/AsyncDAO/StoreBufferController.hpp"

#include <boost/interprocess/sync/named_semaphore.hpp>

namespace svr{
namespace context{

    AppContext * AppContext::p_instance = nullptr;

    struct StoreBufferInitializer
    {
        StoreBufferInitializer() { svr::dao::StoreBufferController::initInstance(); }
        ~StoreBufferInitializer(){ svr::dao::StoreBufferController::destroyInstance(); }
    };

    struct AppContext::AppContextImpl: StoreBufferInitializer
    {
        svr::common::PropertiesFileReader & appProperties;

        svr::dao::DataSource & dataSource;
        svr::dao::UserDAO & userDao;
        svr::dao::InputQueueDAO & inputQueueDao;
        svr::dao::SVRParametersDAO & svrParametersDao;
        svr::dao::DatasetDAO & datasetDao;
        svr::dao::DeconQueueDAO & deconQueueDao;
        svr::dao::EnsembleDAO & ensembleDao;
        svr::dao::ModelDAO & modelDao;
        svr::dao::RequestDAO & requestDao;
        svr::dao::PredictionTaskDAO & predictionTaskDao;
        svr::dao::AutotuneTaskDAO & autotuneTaskDao;
        svr::dao::DecrementTaskDAO & decrementTaskDao;
        svr::dao::IQScalingFactorDAO & iQScalingFactorDao;
        svr::dao::DQScalingFactorDAO & dQScalingFactorDao;

        std::thread memory_manager_thread;

        AppContextImpl(const std::string& config_path)
        :  appProperties(*new svr::common::PropertiesFileReader(config_path))
        , dataSource(*new svr::dao::DataSource(appProperties.get_property<std::string>(config_path, "CONNECTION_STRING"), true))

        , userDao(*svr::dao::UserDAO::                      build(appProperties, dataSource,
                                                                  appProperties.get_dao_type()))
        , inputQueueDao(*svr::dao::InputQueueDAO::          build(appProperties, dataSource,
                                                                  appProperties.get_dao_type()))
        , svrParametersDao(*svr::dao::SVRParametersDAO::    build(appProperties, dataSource,
                                                                  appProperties.get_dao_type()))
        , datasetDao(*svr::dao::DatasetDAO::                build(appProperties, dataSource,
                                                                  appProperties.get_dao_type()))
        , deconQueueDao(*svr::dao::DeconQueueDAO::          build(appProperties, dataSource,
                                                                  appProperties.get_dao_type()))
        , ensembleDao(*svr::dao::EnsembleDAO::              build(appProperties, dataSource,
                                                                  appProperties.get_dao_type()))
        , modelDao(*svr::dao::ModelDAO::                    build(appProperties, dataSource,
                                                                  appProperties.get_dao_type()))
        , requestDao(*svr::dao::RequestDAO::                build(appProperties, dataSource,
                                                                  appProperties.get_dao_type()))
        , predictionTaskDao(*svr::dao::PredictionTaskDAO::  build(appProperties, dataSource,
                                                                  appProperties.get_dao_type()))
        , autotuneTaskDao(*svr::dao::AutotuneTaskDAO::      build(appProperties, dataSource,
                                                                  appProperties.get_dao_type()))
        , decrementTaskDao(*svr::dao::DecrementTaskDAO::    build(appProperties, dataSource,
                                                                  appProperties.get_dao_type()))
        , iQScalingFactorDao(*svr::dao::IQScalingFactorDAO::build(appProperties, dataSource,
                                                                  appProperties.get_dao_type()))
        , dQScalingFactorDao(*svr::dao::DQScalingFactorDAO::build(appProperties, dataSource,
                                                                  appProperties.get_dao_type()))
        {
            memory_manager_thread = std::thread(svr::common::memory_manager::check_ram_memory);

            if(appProperties.get_dao_type() == svr::common::ConcreteDaoType::AsyncDao)
                svr::dao::StoreBufferController::getInstance().startPolling();
        }
        ~AppContextImpl()
        {
            if(appProperties.get_dao_type() == svr::common::ConcreteDaoType::AsyncDao )
                svr::dao::StoreBufferController::getInstance().stopPolling();

            svr::common::memory_manager::m_state = svr::common::memory_manager_state::SHUTDOWN;
            memory_manager_thread.join();

            delete &dQScalingFactorDao; delete &iQScalingFactorDao;
            delete &decrementTaskDao; delete &autotuneTaskDao; delete &predictionTaskDao;
            delete &requestDao; delete &modelDao; delete &ensembleDao; delete &deconQueueDao; delete &datasetDao;
            delete &svrParametersDao; delete &inputQueueDao; delete &userDao; delete &dataSource; delete &appProperties;
        }

        void flush_dao_buffers()
        {
            svr::dao::StoreBufferController::getInstance().flush();
        }
    };


    AppContext::AppContext(const std::string& config_path)
    : p_impl(*new AppContextImpl(config_path))
    , app_properties(p_impl.appProperties)
    , user_service(*new svr::business::UserService(p_impl.userDao))
    , input_queue_service(*new svr::business::InputQueueService(p_impl.inputQueueDao))
    , svr_parameters_service(*new svr::business::SVRParametersService(p_impl.svrParametersDao))
    , model_service(*new svr::business::ModelService(p_impl.modelDao))
    , decon_queue_service(*new svr::business::DeconQueueService(p_impl.deconQueueDao, input_queue_service))
    , ensemble_service(*new svr::business::EnsembleService(p_impl.ensembleDao, model_service, decon_queue_service))
    , dataset_service(*new svr::business::DatasetService(p_impl.datasetDao, ensemble_service, svr_parameters_service))
    , request_service(*new svr::business::RequestService(p_impl.requestDao))
    , authentication_provider(*new svr::business::LocalAuthenticationProvider(user_service))
    , prediction_task_service(*new svr::business::PredictionTaskService(p_impl.predictionTaskDao))
    , autotune_task_service(*new svr::business::AutotuneTaskService(p_impl.autotuneTaskDao))
    , decrement_task_service(*new svr::business::DecrementTaskService(p_impl.decrementTaskDao))
    , iq_scaling_factor_service(*new svr::business::IQScalingFactorService(p_impl.iQScalingFactorDao))
    , dq_scaling_factor_service(*new svr::business::DQScalingFactorService(p_impl.dQScalingFactorDao))
    {}

    AppContext::~AppContext()
    {
        delete &dq_scaling_factor_service; delete &iq_scaling_factor_service;
        delete &decrement_task_service; delete &autotune_task_service; delete &prediction_task_service;
        delete &authentication_provider; delete &request_service;
        delete &model_service; delete &ensemble_service; delete &decon_queue_service; delete &dataset_service;
        delete &svr_parameters_service; delete &input_queue_service; delete &user_service;
        delete &p_impl;
    }

    void AppContext::init_instance(std::string configPath)
    {
        if(AppContext::p_instance)
            throw std::runtime_error("AppContext instance has already been initialized");

        AppContext::p_instance = new AppContext(configPath);
    }

    void AppContext::destroy_instance()
    {
        delete AppContext::p_instance;
        AppContext::p_instance = nullptr;
    }

    void AppContext::flush_dao_buffers()
    {
        p_impl.flush_dao_buffers();
    }


    AppContextDeleter::~AppContextDeleter()
    {
        AppContext::destroy_instance();
    }
}
}


