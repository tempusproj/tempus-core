#include "SVRParametersService.hpp"
#include "util/ValidationUtils.hpp"
#include "DAO/SVRParametersDAO.hpp"
#include "model/SVRParameters.hpp"


using namespace svr::common;

namespace svr {
namespace business {

SVRParameters_ptr SVRParametersService::get_svr_parameter_by_id(bigint id)
{
    return svr_parameters_dao.get_by_id(id);
}


bool SVRParametersService::exists(const SVRParameters_ptr &svr_parameters)
{
    reject_nullptr(svr_parameters);
    return exists(svr_parameters->get_id());
}


bool SVRParametersService::exists(bigint svr_parameters_id)
{
    return svr_parameters_dao.exists(svr_parameters_id);
}

int SVRParametersService::save(const SVRParameters_ptr &svr_parameters)
{
    reject_nullptr(svr_parameters);
    return svr_parameters_dao.save(svr_parameters);
}


int SVRParametersService::remove(const SVRParameters_ptr &svr_parameters)
{
    reject_nullptr(svr_parameters);
    return svr_parameters_dao.remove(svr_parameters);
}


int SVRParametersService::remove_by_dataset(bigint dataset_id)
{
    return svr_parameters_dao.remove_by_dataset_id(dataset_id);
}


std::map<std::pair<std::string, std::string>, std::vector<SVRParameters_ptr>> SVRParametersService::get_all_by_dataset_id(bigint dataset_id)
{
    std::vector<SVRParameters_ptr> vec_svr_parameters = svr_parameters_dao.get_all_svrparams_by_dataset_id(dataset_id);
    std::map<std::pair<std::string, std::string>, std::vector<SVRParameters_ptr>> map_svr_parameters;
    for (SVRParameters_ptr &svr_parameters : vec_svr_parameters)
    {
        map_svr_parameters[std::make_pair(svr_parameters->get_input_queue_table_name(),
                                          svr_parameters->get_input_queue_column_name())].push_back(svr_parameters);
    }

    return map_svr_parameters;
}

std::vector<double> SVRParametersService::to_vector(const SVRParameters_ptr& svr_parameters) const
{
    std::vector<double> result;

    result.push_back(svr_parameters->get_svr_C());
    result.push_back(svr_parameters->get_svr_epsilon());
    result.push_back(svr_parameters->get_svr_kernel_param());
    result.push_back(svr_parameters->get_svr_kernel_param2());
    result.push_back(svr_parameters->get_svr_adjacent_levels_ratio());
    result.push_back(static_cast<double>(svr_parameters->get_lag_count()));

    return result;
}

}
}
