#include <utility>

#include <util/ValidationUtils.hpp>
#include "appcontext.hpp"

#include "util/TimeUtils.hpp"

#include "model/Dataset.hpp"
#include "DAO/DatasetDAO.hpp"
#include "EnsembleService.hpp"
#include "DatasetService.hpp"
#include "model/User.hpp"
#include <algorithm>

using namespace svr::common;
using namespace svr::datamodel;
using namespace svr::context;

namespace svr{
namespace business{


void DatasetService::load_ensembles(Dataset_ptr &p_dataset)
{
    if (!p_dataset) {
        LOG4_ERROR("Dataset is null! Aborting.");
        return;
    }

    bigint id = p_dataset->get_id();
    p_dataset->set_ensembles_svr_parameters(svr_parameters_service.get_all_by_dataset_id(id));
    std::vector<Ensemble_ptr> ensembles = ensemble_service.get_all_by_dataset_id(id);
    p_dataset->set_ensembles(ensembles);
}


bool DatasetService::check_ensembles_svr_parameters(const Dataset_ptr &p_dataset)
{
    std::vector<Ensemble_ptr> &ensembles = p_dataset->get_ensembles();
    return !((ensembles.size() != 0)
             && (p_dataset->get_ensembles_svr_parameters().size() != ensembles.size()));
}


Dataset_ptr DatasetService::get_by_id(bigint datasetId)
{
    Dataset_ptr p_dataset = dataset_dao.get_by_id(datasetId);
    if (p_dataset != nullptr)
    {
        p_dataset->set_input_queue(AppContext::get_instance().input_queue_service.get_queue_metadata(
                p_dataset->get_input_queue()->get_table_name()));
        for (InputQueue_ptr &p_aux_input_queue : p_dataset->get_aux_input_queues())
        {
            p_aux_input_queue = AppContext::get_instance().input_queue_service.get_queue_metadata(p_aux_input_queue->get_table_name());
        }
        load_ensembles(p_dataset);
    }
    return p_dataset;
}


bool DatasetService::save(Dataset_ptr const &p_dataset)
{
    if (!p_dataset) {
        LOG4_ERROR("Dataset is null! Aborting.");
        return false;
    }

    dataset_dao.save(p_dataset);
    // TODO Remove deleting of svr parameters at every save.
    svr_parameters_service.remove_by_dataset(p_dataset->get_id());
    // save svr_parametes
    for (auto &pair_vec_svr_parameters : p_dataset->get_ensembles_svr_parameters())
    {
        for (auto &svr_parameters : pair_vec_svr_parameters.second)
        {
            svr_parameters->set_dataset_id(p_dataset->get_id());
            svr_parameters_service.save(svr_parameters);
        }
    }
    // save ensembles
    if (!p_dataset->get_ensembles().empty())
    {
        ensemble_service.remove_by_dataset_id(p_dataset->get_id());
        // check
        if (!check_ensembles_svr_parameters(p_dataset)) {
            LOG4_ERROR("SVRParameters in ensembles and in dataset are not equal");
        }
        if (!p_dataset->get_ensembles().empty()
            && !ensemble_service.save_ensembles(p_dataset->get_ensembles(), true))
            return false;
    }
    return true;
}


bool DatasetService::exists(Dataset_ptr const &dataset)
{
    reject_nullptr(dataset);
    return dataset_dao.exists(dataset->get_id());
}


bool DatasetService::exists(int dataset_id)
{
    return dataset_dao.exists(dataset_id);
}


int DatasetService::remove(Dataset_ptr const &dataset)
{
    reject_nullptr(dataset);
    svr_parameters_service.remove_by_dataset(dataset->get_id());
    for (Ensemble_ptr ensemble : dataset->get_ensembles())
    {
        ensemble_service.remove(ensemble);
    }
    return dataset_dao.remove(dataset);
}


bool DatasetService::link_user_to_dataset(User_ptr const & user, Dataset_ptr const & dataset)
{
    return dataset_dao.link_user_to_dataset(user->get_user_name(), dataset);
}


bool DatasetService::unlink_user_from_dataset(User_ptr const & user, Dataset_ptr const & dataset)
{
    return dataset_dao.unlink_user_from_dataset(user->get_user_name(), dataset);
}

DatasetService::DatasetUsers::DatasetUsers(Dataset_ptr const & dataset, std::vector<User_ptr> && users)
: dataset(dataset), users(std::forward<std::vector<User_ptr>>(users))
{}


bool is_dataset_element_of(const Dataset_ptr &p_dataset, const svr::dao::DatasetDAO::UserDatasetPairs &active_datasets)
{
    /* TODO Implement 'is functionally equivalent' Dataset class operator
     * in order to avoid removing of dataset when only runtime modifiable parts have changed
     */
    for (auto &dataset_users: active_datasets)
        if (*p_dataset == *dataset_users.second)
            return true;

    return false;
}


void DatasetService::update_active_datasets(UserDatasetPairs &processed_user_dataset_pairs)
{
    svr::dao::DatasetDAO::UserDatasetPairs active_datasets = dataset_dao.get_active_datasets();

    auto new_end = std::remove_if(
                processed_user_dataset_pairs.begin(),
                processed_user_dataset_pairs.end(),
                [&active_datasets](DatasetUsers &processed_pair)->bool{ return !is_dataset_element_of(processed_pair.dataset, active_datasets); }
    );
    processed_user_dataset_pairs.erase(new_end, processed_user_dataset_pairs.end());

    for(auto & pudp: processed_user_dataset_pairs)
        pudp.users.clear();

    for(auto &dataset_pair: active_datasets)
    {
        auto compare_by_dataset_id = [&dataset_pair](UserDatasetPairs::value_type const &other)
        {
            if(dataset_pair.second && other.dataset)
                return dataset_pair.second->get_id() == other.dataset->get_id();
            throw std::invalid_argument("DatasetService::get_active_datasets: Null ptr value passed");
        };

        auto iter = std::find_if(processed_user_dataset_pairs.begin(), processed_user_dataset_pairs.end(), compare_by_dataset_id);
        if(iter == processed_user_dataset_pairs.end())
            processed_user_dataset_pairs.emplace_back(
                        UserDatasetPairs::value_type(
                            dataset_pair.second,
                            {AppContext::get_instance().user_service.get_user_by_user_name(dataset_pair.first)}
                            ));
        else
            iter->users.emplace_back( AppContext::get_instance().user_service.get_user_by_user_name(dataset_pair.first) );
    }

    auto compare_user_dataset_priority = [](UserDatasetPairs::value_type const & lhs, UserDatasetPairs::value_type const & rhs)
    {
        auto i_lhs = lhs.users.begin(), i_rhs = rhs.users.begin(), end_lhs = lhs.users.end(), end_rhs = rhs.users.end();

        for( ; i_lhs != end_lhs && i_rhs != end_rhs; ++i_lhs, ++i_rhs)
        {
            if((*i_lhs)->get_priority() > (*i_rhs)->get_priority())
                return true;
            if((*i_lhs)->get_priority() < (*i_rhs)->get_priority())
                return false;
        }

        if( lhs.dataset->get_priority() > rhs.dataset->get_priority() )
            return true;
        if( lhs.dataset->get_priority() < rhs.dataset->get_priority() )
            return false;

        return false;
    };

    std::sort(processed_user_dataset_pairs.begin(), processed_user_dataset_pairs.end(), compare_user_dataset_priority);
}


bool DatasetService::exists(const std::string &user_name, const std::string &dataset_name) {
    return dataset_dao.exists(user_name, dataset_name);
}


Dataset_ptr DatasetService::get_user_dataset(const std::string &user_name, const std::string &dataset_name) {
    Dataset_ptr p_dataset = dataset_dao.get_by_name(user_name, dataset_name);
    if (p_dataset != nullptr) {
        p_dataset->set_input_queue(AppContext::get_instance().input_queue_service.get_queue_metadata(
                                                p_dataset->get_input_queue()->get_table_name()));
        load_ensembles(p_dataset);
    }
    return p_dataset;
}


std::vector<DeconQueue_ptr>
DatasetService::run_dataset_aux(
        const Dataset_ptr &p_dataset,
        const boost::posix_time::time_period &training_range,
        const boost::posix_time::time_period &prediction_range)
{
    std::vector<DeconQueue_ptr> decon_queues;
    std::vector<std::vector<DeconQueue_ptr>> aux_decon_queues;

    // Train
    prepare_ensembles(p_dataset, training_range, decon_queues, aux_decon_queues);
    
    std::vector<std::future<void>> ensemble_train_futures;
    for (Ensemble_ptr& p_ensemble: p_dataset->get_ensembles())
        ensemble_train_futures.push_back(std::async(
                std::launch::async|std::launch::deferred,
                &EnsembleService::train,
                &ensemble_service,
                std::ref(p_ensemble),
                std::ref(p_dataset->get_ensembles_svr_parameters()[p_ensemble->get_key_pair()]),
                std::ref(p_dataset->get_max_lookback_time_gap()),
                std::numeric_limits<size_t>::max()));
    for (auto &f: ensemble_train_futures) f.get();

    // Predict
    prepare_ensembles(p_dataset, prediction_range, decon_queues, aux_decon_queues, false, false);
    
    std::vector<std::future<void>> ensemble_predict_futures;
    for (Ensemble_ptr& p_ensemble: p_dataset->get_ensembles())
        ensemble_predict_futures.push_back(std::async(
                std::launch::async, &EnsembleService::predict, std::ref(p_ensemble), std::ref(prediction_range),
                std::ref(p_dataset->get_input_queue()->get_resolution()), std::ref(p_dataset->get_max_lookback_time_gap())));
    for (auto &f: ensemble_predict_futures) f.get();


    AppContext::get_instance().dq_scaling_factor_service.scale(p_dataset, true);

    std::vector<DeconQueue_ptr> result_decon_queues;
    for (Ensemble_ptr &p_ensemble: p_dataset->get_ensembles())
        result_decon_queues.push_back(p_ensemble->get_decon_queue());

    return result_decon_queues;
}


void
DatasetService::prepare_ensembles(
        const Dataset_ptr &p_dataset,
        const boost::posix_time::time_period &range,
        std::vector<DeconQueue_ptr> decon_queues,
        std::vector<std::vector<DeconQueue_ptr>> aux_decon_queues,
        const bool trim,
        const bool init_ensembles)
{
    prepare_queue(p_dataset, p_dataset->get_input_queue(), range, trim, decon_queues);
    aux_decon_queues.resize(decon_queues.size()); // TODO Remove this bug
    for (size_t input_queue_ix = 0; input_queue_ix < p_dataset->get_aux_input_queues().size(); ++input_queue_ix)
        prepare_queue(p_dataset, p_dataset->get_aux_input_queues()[input_queue_ix], range, trim, aux_decon_queues[input_queue_ix]);
    if (init_ensembles)
        p_dataset->set_ensembles(
                ensemble_service.init_ensembles_from_dataset(p_dataset, decon_queues, aux_decon_queues));
    else
        ensemble_service.init_decon_data(p_dataset->get_ensembles(), decon_queues, aux_decon_queues);
    AppContext::get_instance().dq_scaling_factor_service.scale(p_dataset);
}


void
DatasetService::prepare_queue(
        const Dataset_ptr &p_dataset, const InputQueue_ptr &p_input_queue, const boost::posix_time::time_period &range,
        const bool trim, std::vector<DeconQueue_ptr> &decon_queues)
{
    const size_t frame_length = svr::common::swt_levels_to_frame_length(p_dataset->get_swt_levels());
    InputQueue_ptr p_new_input_queue = AppContext::get_instance().input_queue_service.clone_with_data(
            p_input_queue, range);
    if (p_new_input_queue->get_data().size() < frame_length) {
        p_new_input_queue->update_data(
                AppContext::get_instance().input_queue_service.get_latest_queue_data(
                        p_input_queue->get_table_name(),
                        2 * frame_length, // TODO Revise
                        range.end()),
                false);
    }
    std::vector<DeconQueue_ptr> new_decon_queues {AppContext::get_instance().decon_queue_service.deconstruct(p_new_input_queue, p_dataset)};
    if (trim)
        for (DeconQueue_ptr &p_decon_queue_it: new_decon_queues)
            p_decon_queue_it->trim(range.begin(), range.end()); // TODO Bug, trim to begin minus auto regression lag til end
    decon_queues.insert(decon_queues.end(), new_decon_queues.begin(), new_decon_queues.end());
}


datarow_range
DatasetService::find_iterators(
        const DeconQueue_ptr &p_decon_queue,
        const bpt::time_period &range,
        const SVRParameters &svr_parameters)
{
    LOG4_BEGIN();

    // Find start and end iterators
    auto start_iter = p_decon_queue->get_data().lower_bound(range.begin()); // TODO Replace with find nearest
    auto end_iter = p_decon_queue->get_data().lower_bound(range.end());
    if (start_iter == p_decon_queue->get_data().end() || end_iter == p_decon_queue->get_data().end())
        throw std::runtime_error(
                "Couldn't find training range " + bpt::to_simple_string(range) + " in decon data starting from " +
                bpt::to_simple_string(p_decon_queue->get_data().begin()->first) + " ending at " + bpt::to_simple_string(p_decon_queue->get_data().rbegin()->first) + ". Aborting.");

    // Trim to decrement distance
    auto range_distance = std::distance(start_iter, end_iter);
    if (range_distance > off_t(svr_parameters.get_svr_decremental_distance()))
        std::advance(start_iter, range_distance - svr_parameters.get_svr_decremental_distance());

    if (std::distance(p_decon_queue->get_data().begin(), start_iter) < static_cast<ssize_t>(svr_parameters.get_lag_count() + 1))
        throw std::runtime_error(
                "Could not collect enough data for lagged features. Starting value is " + std::to_string(std::distance(p_decon_queue->get_data().begin(), start_iter)) +
                " elements distance, while lag count is " + std::to_string(svr_parameters.get_lag_count()));
    std::advance(start_iter, -ssize_t(svr_parameters.get_lag_count() + 1));

    LOG4_END();
    return datarow_range(start_iter, end_iter, p_decon_queue->get_data());
}


bool
DatasetService::prepare_range(
        const DeconQueue_ptr &p_decon_queue,
        const std::vector<DeconQueue_ptr> &aux_decon_queues,
        const bpt::time_period &range,
        const SVRParameters &svr_parameters,
        datarow_range &out_decon_data,
        std::vector<datarow_range> &out_aux_decon_data)
{
    try {
        out_decon_data = find_iterators(p_decon_queue, range, svr_parameters);
        for (const DeconQueue_ptr &p_aux_decon_queue: aux_decon_queues)
            out_aux_decon_data.push_back(find_iterators(p_aux_decon_queue, range, svr_parameters));
    } catch (const std::runtime_error &ex) {
        LOG4_ERROR("Aborting. " << ex.what());
        return false;
    }

    return true;
}


bool
DatasetService::train_dataset_model(
        const Dataset_ptr &p_dataset,
        Model_ptr &p_model,
        const SVRParameters_ptr &p_svr_parameters,
        const boost::posix_time::time_period &training_range,
        const DeconQueue_ptr &p_decon_queue,
        const std::vector<DeconQueue_ptr> &aux_decon_queues)
{
    LOG4_BEGIN();

    // Prepare training data
    datarow_range main_data_range(p_decon_queue->get_data().begin(), p_decon_queue->get_data().end(), p_decon_queue->get_data());
    std::vector<datarow_range> aux_data_ranges;
    if (!prepare_range(p_decon_queue, aux_decon_queues, training_range, *p_svr_parameters, main_data_range, aux_data_ranges))
        return false;

    cascaded::layer_data training_data;
    try {
        // Create training data
        training_data = AppContext::get_instance().model_service.get_training_data(
                    main_data_range,
                    p_svr_parameters->get_lag_count(),
                    p_model->get_learning_levels(),
                    p_dataset->get_max_lookback_time_gap(),
                    p_model->get_decon_level(),
                    aux_data_ranges,
                    p_model->get_last_modeled_value_time());
    } catch (const std::runtime_error &ex) {
        LOG4_ERROR("Failed preparing training features matrix. " << ex.what() << ". Aborting training.");
        return false;
    }

    AppContext::get_instance().get_instance().model_service.train(
            *p_svr_parameters, p_model, training_data, main_data_range.rbegin()->first);

    delete training_data.training_matrix;
    delete training_data.reference_vector;

    LOG4_END();

    return true;
}


std::shared_ptr<Vector<double>>
DatasetService::run_dataset_column_model(
        const Dataset_ptr &p_dataset,
        const SVRParameters_ptr &p_svr_parameters,
        const boost::posix_time::time_period &training_range,
        const boost::posix_time::time_period &prediction_time_range,
        const size_t model_numb,
        const DeconQueue_ptr &p_decon_queue,
        const std::vector<DeconQueue_ptr> &aux_decon_queues)
{
    LOG4_BEGIN();

    Vector<double> *p_predicted_values = nullptr;
    std::vector<datarow_range> aux_data_ranges;
    const bpt::time_duration resolution = p_dataset->get_input_queue()->get_resolution();
    Model_ptr p_model = std::make_shared<Model>(
                        0, 0, model_numb,
                        get_adjacent_indexes(model_numb, p_svr_parameters->get_svr_adjacent_levels_ratio(), p_dataset->get_swt_levels() + 1), nullptr, bpt::min_date_time, bpt::min_date_time);
    
    // Train model
    if (!train_dataset_model(p_dataset, p_model, p_svr_parameters, training_range, p_decon_queue, aux_decon_queues)) {
        LOG4_ERROR("Failed training model for level " << model_numb << ". Aborting prediction.");
        p_model->reset();
        return std::shared_ptr<Vector<double>>(p_predicted_values);
    }
    // Predict from trained model
    // No need to prepare range, as range is sent to training method
    // prepare_range(p_decon_queue, aux_decon_queues, prediction_time_range, p_svr_parameters->get_lag_count(), validation_data, aux_data_ranges);
    for (const auto &p_aux_decon_queue: aux_decon_queues)
        aux_data_ranges.push_back(datarow_range(p_aux_decon_queue->get_data().begin(), p_aux_decon_queue->get_data().end(), p_aux_decon_queue->get_data()));

    auto p_main_decon_data = datarow_range(p_decon_queue->get_data().begin(), p_decon_queue->get_data().end(), p_decon_queue->get_data());
    p_predicted_values = AppContext::get_instance().model_service.predict(
                p_model, p_main_decon_data, aux_data_ranges, prediction_time_range, resolution, p_dataset->get_max_lookback_time_gap());

    if (p_predicted_values)
        LOG4_DEBUG("Predicted " << p_predicted_values->GetLength() << " values for range " << prediction_time_range);
    else
        LOG4_ERROR("Prediction failed.");

    p_model->reset();
    LOG4_END();
    return std::shared_ptr<Vector<double>>(p_predicted_values);
}

// x.toSVRParameters()
void DatasetService::set_svr_parameters(
        const Dataset_ptr &p_dataset,
        const std::vector<double> &parameter_values,
        const size_t model_number,
        const std::vector<datamodel::Bounds> &bounds,
        const std::string &column_name)
{
    std::vector<SVRParameters_ptr>& vec_svr_parameters =
            p_dataset->get_ensembles_svr_parameters()[std::make_pair(p_dataset->get_input_queue()->get_table_name(), column_name)];

    SVRParameters_ptr svr_params = vec_svr_parameters[model_number];

    if (bounds[model_number].is_tuned.svr_C)
    {
        if(parameter_values[0] > bounds[model_number].min_bounds.get_svr_C() &&
           parameter_values[0] < bounds[model_number].max_bounds.get_svr_C())
            svr_params->set_svr_C(parameter_values.at(0));
        else
        {
            if(parameter_values[0] < bounds[model_number].min_bounds.get_svr_C())
                svr_params->set_svr_C(bounds[model_number].min_bounds.get_svr_C());
            if(parameter_values[0] > bounds[model_number].max_bounds.get_svr_C())
                svr_params->set_svr_C(bounds[model_number].max_bounds.get_svr_C());
        }
    }

    if (bounds[model_number].is_tuned.svr_epsilon)
    {
        if(parameter_values[1] > bounds[model_number].min_bounds.get_svr_epsilon()
           && parameter_values[1] < bounds[model_number].max_bounds.get_svr_epsilon())
            svr_params->set_svr_epsilon(parameter_values[1]);
        else
        {
            if(parameter_values[1] < bounds[model_number].min_bounds.get_svr_epsilon())
                svr_params->set_svr_epsilon(bounds[model_number].min_bounds.get_svr_epsilon());
            if(parameter_values[1] > bounds[model_number].max_bounds.get_svr_epsilon())
                svr_params->set_svr_epsilon(bounds[model_number].max_bounds.get_svr_epsilon());
        }
    }

    if (bounds[model_number].is_tuned.svr_kernel_param)
    {
        if(parameter_values[2] > bounds[model_number].min_bounds.get_svr_kernel_param() &&
           parameter_values[2] < bounds[model_number].max_bounds.get_svr_kernel_param())
            svr_params->set_svr_kernel_param(parameter_values[2]);
        else
        {
            if(parameter_values[2] < bounds[model_number].min_bounds.get_svr_kernel_param())
                svr_params->set_svr_kernel_param(bounds[model_number].min_bounds.get_svr_kernel_param());
            if(parameter_values[2] > bounds[model_number].max_bounds.get_svr_kernel_param())
                svr_params->set_svr_kernel_param(bounds[model_number].max_bounds.get_svr_kernel_param());
        }
    }

    if (bounds[model_number].is_tuned.svr_kernel_param2)
    {
        if(parameter_values[3] > bounds[model_number].min_bounds.get_svr_kernel_param2() &&
           parameter_values[3] < bounds[model_number].max_bounds.get_svr_kernel_param2())
            svr_params->set_svr_kernel_param2(parameter_values[3]);
        else
        {
            if(parameter_values[3] < bounds[model_number].min_bounds.get_svr_kernel_param2())
                svr_params->set_svr_kernel_param2(bounds[model_number].min_bounds.get_svr_kernel_param2());
            if(parameter_values[3] > bounds[model_number].max_bounds.get_svr_kernel_param2())
                svr_params->set_svr_kernel_param2(bounds[model_number].max_bounds.get_svr_kernel_param2());
        }
    }

    if (bounds[model_number].is_tuned.svr_adjacent_levels_ratio)
    {
        if(parameter_values[4] > bounds[model_number].min_bounds.get_svr_adjacent_levels_ratio() &&
           parameter_values[4] < bounds[model_number].max_bounds.get_svr_adjacent_levels_ratio())
            svr_params->set_svr_adjacent_levels_ratio(parameter_values[4]);
        else
        {
            if(parameter_values[4] < bounds[model_number].min_bounds.get_svr_adjacent_levels_ratio())
                svr_params->set_svr_adjacent_levels_ratio(bounds[model_number].min_bounds.get_svr_adjacent_levels_ratio());
            if(parameter_values[4] > bounds[model_number].max_bounds.get_svr_adjacent_levels_ratio())
                svr_params->set_svr_adjacent_levels_ratio(bounds[model_number].max_bounds.get_svr_adjacent_levels_ratio());
        }
    }

    if (bounds[model_number].is_tuned.lag_count)
    {
        const size_t rounded_lag_count {static_cast<size_t>(std::round(parameter_values[5]))};

        if(rounded_lag_count > bounds[model_number].min_bounds.get_lag_count() &&
           rounded_lag_count < bounds[model_number].max_bounds.get_lag_count())
            svr_params->set_lag_count(rounded_lag_count);
        else
        {
            if(rounded_lag_count < bounds[model_number].min_bounds.get_lag_count())
                svr_params->set_lag_count(bounds[model_number].min_bounds.get_lag_count());
            if(rounded_lag_count > bounds[model_number].max_bounds.get_lag_count())
                svr_params->set_lag_count(bounds[model_number].max_bounds.get_lag_count());
        }
    }
}

} //business
} //svr
