#include "DQScalingFactorService.hpp"
#include "appcontext.hpp"



using namespace svr::common;
using namespace svr::context;

namespace svr {
namespace business {

bool DQScalingFactorService::exists(const DQScalingFactor_ptr& dq_scaling_factor)
{
    return dq_scaling_factor_dao.exists(dq_scaling_factor->get_id());
}


int DQScalingFactorService::save(const DQScalingFactor_ptr& dq_scaling_factor)
{
    return dq_scaling_factor_dao.save(dq_scaling_factor);
}


int DQScalingFactorService::remove(const DQScalingFactor_ptr& dq_scaling_factor)
{
    return dq_scaling_factor_dao.remove(dq_scaling_factor);
}


std::vector<DQScalingFactor_ptr> DQScalingFactorService::find_all_by_dataset_id(const bigint dataset_id)
{
    return dq_scaling_factor_dao.find_all_by_dataset_id(dataset_id);
}


std::vector<DQScalingFactor_ptr> DQScalingFactorService::calculate(
        const svr::datamodel::DeconQueue_ptr &p_decon_queue,
        const size_t dataset_id,
        const double alpha)
{
    LOG4_DEBUG("Calculating " << alpha << " truncated mean for scaling " <<
             p_decon_queue->get_input_queue_table_name() << " " << p_decon_queue->get_input_queue_column_name());
    if(p_decon_queue->get_data().empty())
    {
        LOG4_ERROR("Decon queue is empty");
        return std::vector<DQScalingFactor_ptr>();
    }

    std::vector<DQScalingFactor_ptr> result;
    std::vector<std::vector<double>> dq_matrix {AppContext::get_instance().decon_queue_service.to_matrix(p_decon_queue)};
    for(auto &decon_level: dq_matrix)
    {
        std::for_each(decon_level.begin(), decon_level.end(), [](double& x) {x = std::abs(x);});
        std::sort(decon_level.begin(), decon_level.end());
    }

    auto pos = (const size_t) std::round(dq_matrix[0].size() * (1.0 - alpha));

    for(size_t level = 0; level < dq_matrix.size(); ++level)
        result.push_back(
                    std::make_shared<svr::datamodel::DQScalingFactor>(
                             0,
                             dataset_id,
                             p_decon_queue->get_input_queue_table_name(),
                             p_decon_queue->get_input_queue_column_name(),
                             level,
                             dq_matrix[level][pos]));


    return result;
}


std::vector<DQScalingFactor_ptr> DQScalingFactorService::slice(
        const std::vector<DQScalingFactor_ptr> &scaling_factors,
        const size_t dataset_id,
        const std::string& input_queue_table_name,
        const std::string& input_queue_column_name)
{
    std::vector<DQScalingFactor_ptr> result;

    for(auto& scaling_factor : scaling_factors)
        if(scaling_factor->get_dataset_id() == dataset_id &&
           scaling_factor->get_input_queue_table_name() == input_queue_table_name &&
           scaling_factor->get_input_queue_column_name() == input_queue_column_name)
            result.push_back(scaling_factor);

    return result;
}

// TODO Parallelize
void DQScalingFactorService::scale_decon_queue(
        const svr::datamodel::DeconQueue_ptr& decon_queue,
        const std::vector<double>& scaling_factors,
        const bool unscale)
{
    for(auto& datarow : decon_queue->get_data())
    {
        std::vector<double> values {datarow.second->get_values()};
        for(size_t level = 0; level < values.size(); ++level)
            values[level] = unscale ? values[level] * scaling_factors[level] : values[level] / scaling_factors[level];
        datarow.second->set_values(values);
    }
}

// TODO Rewrite and check with slice() for needed scaling factors
void DQScalingFactorService::scale(
        const Dataset_ptr& p_dataset,
        const bool unscale,
        Ensemble_ptr p_ensemble)
{
    LOG4_BEGIN();

    if(p_dataset->get_input_queue()->get_data().empty())
    {
        LOG4_ERROR("Input queue is empty");
        return;
    }

    // If scaling factors are empty try to load them from DB
    if(p_dataset->get_dq_scaling_factors().empty())
        p_dataset->set_dq_scaling_factors(find_all_by_dataset_id(p_dataset->get_id()));

    // Otherwise calculate them and save to the DB
    if(p_dataset->get_dq_scaling_factors().empty()) calculate_dataset_scaling_factors(p_dataset);

    std::vector<Ensemble_ptr> ensembles_to_scale;
    if(!p_ensemble) ensembles_to_scale = p_dataset->get_ensembles();
    else ensembles_to_scale.push_back(p_ensemble);

    for (Ensemble_ptr &p_scaled_ensemble: ensembles_to_scale)
    {
        // Scale main decon queue
        scale_decon_queue(p_dataset, p_scaled_ensemble->get_decon_queue(), unscale);
        // Scale auxiliary decon queues
        for(auto& aux_decon_queue : p_scaled_ensemble->get_aux_decon_queues())
            scale_decon_queue(p_dataset, aux_decon_queue, unscale);
    }

    LOG4_END();
}

// Recalculate all scaling factors
void DQScalingFactorService::calculate_dataset_scaling_factors(const Dataset_ptr &p_dataset)
{
    LOG4_BEGIN();

    auto &aci = AppContext::get_instance();
    LOG4_INFO("Calculating scaling factors...");
    std::vector<DQScalingFactor_ptr> dq_scaling_factors;
    for (auto& p_ensemble: p_dataset->get_ensembles())
    {
        std::vector<DeconQueue_ptr> all_decon_queues;
        InputQueue_ptr p_input_queue = aci.input_queue_service.get_queue_metadata(
                p_ensemble->get_decon_queue()->get_input_queue_table_name());
        p_input_queue->set_data(aci.input_queue_service.get_queue_data(p_dataset->get_input_queue()->get_table_name()));
        all_decon_queues = aci.decon_queue_service.deconstruct(p_input_queue, p_dataset, false);

        for (const DeconQueue_ptr &p_all_data_decon_queue: all_decon_queues) {
            std::vector<DQScalingFactor_ptr> new_dataset_scaling_factors = calculate(p_all_data_decon_queue, p_dataset->get_id());
            dq_scaling_factors.insert(
                    dq_scaling_factors.end(), new_dataset_scaling_factors.begin(), new_dataset_scaling_factors.end());
        }
    }
    p_dataset->set_dq_scaling_factors(dq_scaling_factors);
    if (p_dataset->get_id())
        for(const auto &scaling_factor: dq_scaling_factors)
            save(scaling_factor);

    LOG4_END();
}

// Calculate scaling factors related to decon queue
void DQScalingFactorService::calculate_dataset_scaling_factors(
        const Dataset_ptr &p_dataset,
        const DeconQueue_ptr &p_decon_queue)
{
    LOG4_BEGIN();

    auto &aci = AppContext::get_instance();
    LOG4_INFO("Calculating scaling factors...");
    std::vector<DQScalingFactor_ptr> &dq_scaling_factors = p_dataset->get_dq_scaling_factors();
    std::vector<DeconQueue_ptr> all_decon_queues;
    InputQueue_ptr p_input_queue = aci.input_queue_service.get_queue_metadata(p_decon_queue->get_input_queue_table_name());
    p_input_queue->set_data(aci.input_queue_service.get_queue_data(p_decon_queue->get_input_queue_table_name()));
    all_decon_queues = aci.decon_queue_service.deconstruct(p_input_queue, p_dataset, false);

    for (const DeconQueue_ptr &p_all_data_decon_queue: all_decon_queues) {
        std::vector<DQScalingFactor_ptr> new_dataset_scaling_factors = calculate(p_all_data_decon_queue, p_dataset->get_id());
        dq_scaling_factors.insert(
                dq_scaling_factors.end(), new_dataset_scaling_factors.begin(), new_dataset_scaling_factors.end());
    }
    if (p_dataset->get_id())
        for(const auto &scaling_factor: dq_scaling_factors)
            save(scaling_factor);

    LOG4_END();
}


void
DQScalingFactorService::scale_decon_queue(
        const Dataset_ptr& p_dataset, DeconQueue_ptr& p_decon_queue, const bool unscale)
{
    LOG4_BEGIN();

    DataRowContainer all_input_data;
    InputQueue_ptr p_input_queue;
    std::vector<DeconQueue_ptr> all_decon_queues;
    const size_t all_scale_levels = p_dataset->get_swt_levels() + 1;

    // scale main DeconQueue
    std::vector<DQScalingFactor_ptr> decon_queue_scaling_factors = slice(
            p_dataset->get_dq_scaling_factors(), p_dataset->get_id(), p_decon_queue->get_input_queue_table_name(), p_decon_queue->get_input_queue_column_name());

    if (decon_queue_scaling_factors.size() != all_scale_levels) {
        LOG4_WARN("Dataset doesn't have scaling factors. Loading from database.");

        // try to load them from database
        p_dataset->set_dq_scaling_factors(this->find_all_by_dataset_id(p_dataset->get_id()));
        decon_queue_scaling_factors = slice(
                p_dataset->get_dq_scaling_factors(), p_dataset->get_id(), p_decon_queue->get_input_queue_table_name(), p_decon_queue->get_input_queue_column_name());

        // otherwise calculate them and save to the DB
        if (decon_queue_scaling_factors.size() != all_scale_levels)
            calculate_dataset_scaling_factors(p_dataset, p_decon_queue);
    }

    decon_queue_scaling_factors = slice(
            p_dataset->get_dq_scaling_factors(), p_dataset->get_id(), p_decon_queue->get_input_queue_table_name(), p_decon_queue->get_input_queue_column_name());
    if (decon_queue_scaling_factors.size() != all_scale_levels) {
        LOG4_ERROR("Failed calculating scaling factors. Aborting.");
        return;
    }
    std::vector<double> scaling_factors(all_scale_levels, 1.);
    for (const DQScalingFactor_ptr &p_scaling_factor: decon_queue_scaling_factors)
        scaling_factors[p_scaling_factor->get_wavelet_level()] = p_scaling_factor->get_scaling_factor();

    scale_decon_queue(p_decon_queue, scaling_factors, unscale);

    LOG4_END();
}

}
}
