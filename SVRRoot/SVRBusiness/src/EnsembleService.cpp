#include "EnsembleService.hpp"
#include <util/ValidationUtils.hpp>
#include <util/TimeUtils.hpp>
#include <util/MemoryManager.hpp>

#include <model/Ensemble.hpp>
#include <model/Dataset.hpp>
#include <DAO/EnsembleDAO.hpp>
#include "DeconQueueService.hpp"
#include "ModelService.hpp"
#include "InputQueueService.hpp"
#include "SVRParametersService.hpp"
#include <common/exceptions.hpp>

#include "appcontext.hpp"


using namespace svr::common;
using namespace svr::datamodel;

using namespace std;

namespace svr {
namespace business {


void EnsembleService::load_svr_params_and_models(Ensemble_ptr p_ensemble)
{
    reject_nullptr(p_ensemble);
    p_ensemble->set_models(model_service.get_all_models_by_ensemble_id(p_ensemble->get_id()));
}


Ensemble_ptr EnsembleService::get_ensemble_by_id(bigint ensemble_id)
{
    Ensemble_ptr p_ensemble = ensemble_dao.get_by_id(ensemble_id);
    load_svr_params_and_models(p_ensemble);

    return p_ensemble;
}


Ensemble_ptr EnsembleService::get_ensemble_by_dataset_and_decon_queue(
        const Dataset_ptr &p_dataset,
        const DeconQueue_ptr &p_decon_queue)
{
    reject_nullptr(p_dataset);
    reject_nullptr(p_decon_queue);
    reject_empty(p_decon_queue->get_table_name());

    return ensemble_dao.get_by_dataset_and_decon_queue(p_dataset, p_decon_queue);
}


bool
EnsembleService::is_ensemble_input_queue(
        const Ensemble_ptr &p_ensemble,
        const InputQueue_ptr &p_input_queue)
{

    bool res = std::any_of(
            p_input_queue->get_value_columns().begin(),
            p_input_queue->get_value_columns().end(),
            [&p_ensemble](const std::string &column_name)->bool{ return column_name == p_ensemble->get_decon_queue()->get_input_queue_column_name(); }
    );

    if (!res) LOG4_DEBUG(
                "Skipping ensemble for column " << p_ensemble->get_decon_queue()->get_input_queue_column_name() <<
                                                " of input queue " << p_ensemble->get_decon_queue()->get_input_queue_table_name() << ". Not a value column.");

    return res;
}


std::vector<Ensemble_ptr> EnsembleService::get_all_by_dataset_id(bigint dataset_id)
{
    LOG4_BEGIN();

    std::vector<Ensemble_ptr> ensembles = ensemble_dao.find_all_ensembles_by_dataset_id(dataset_id);
    for (Ensemble_ptr p_ensemble: ensembles) load_svr_params_and_models(p_ensemble);

    // Get all decon table names from ensembles
    std::map<std::string, DeconQueue_ptr> decon_queues;
    for (Ensemble_ptr &p_ensemble: ensembles) {
        DeconQueue_ptr p_decon_queue = p_ensemble->get_decon_queue();

        if (p_decon_queue && decon_queues.count(p_decon_queue->get_table_name()) == 0)
            decon_queues[p_decon_queue->get_table_name()] = nullptr;
    }

    // Load decon data
    for (std::pair<const std::string, DeconQueue_ptr> &it_decon_queue: decon_queues) {
        it_decon_queue.second = decon_queue_service.get_by_table_name(it_decon_queue.first);
        try {
            //decon_queue_service.load_decon_data(it_decon_queue.second);
        } catch (const std::exception &ex) {
            LOG4_ERROR("Failed loading data from decon queue. " << ex.what());
        }
    }

    // Set decon queues to ensembles
    for (Ensemble_ptr p_ensemble : ensembles) {
        if (p_ensemble->get_decon_queue())
            p_ensemble->set_decon_table(decon_queues[p_ensemble->get_decon_queue()->get_table_name()]);

        for (DeconQueue_ptr &p_aux_decon_queue: p_ensemble->get_aux_decon_queues())
            p_aux_decon_queue = decon_queues[p_aux_decon_queue->get_table_name()];
    }

    LOG4_END();

    return ensembles;
}


int EnsembleService::save(const Ensemble_ptr &p_ensemble)
{
    LOG4_BEGIN();

    if (!p_ensemble) {
        LOG4_ERROR("Ensemble is null! Aborting!");
        return 0;
    }

    if (p_ensemble->get_id() <= 0) {
        p_ensemble->set_id(ensemble_dao.get_next_id());
        for (Model_ptr &p_model: p_ensemble->get_models())
            p_model->set_ensemble_id(p_ensemble->get_id());
    }
    int ret_value = ensemble_dao.save(p_ensemble);
    model_service.remove_by_ensemble_id(p_ensemble->get_id());
    for (Model_ptr &p_model: p_ensemble->get_models())
        model_service.save(p_model);

    LOG4_END();

    return ret_value;
}


bool EnsembleService::save_ensembles(const std::vector<Ensemble_ptr> &ensembles, bool save_decon_queues)
{
    LOG4_BEGIN();

    if (save_decon_queues)
        for (const Ensemble_ptr &p_ensemble: ensembles)
            if (p_ensemble->get_decon_queue())
                decon_queue_service.save(p_ensemble->get_decon_queue());

    for (const Ensemble_ptr &p_ensemble: ensembles)
        if (save(p_ensemble) == 0)
            return false;

    LOG4_END();

    return true;
}


bool EnsembleService::exists(const Ensemble_ptr &ensemble) 
{
    reject_nullptr(ensemble);
    if (ensemble->get_id() <= 0) return false;
    return ensemble_dao.exists(ensemble->get_id());
}


int EnsembleService::remove_by_dataset_id(bigint dataset_id)
{
    std::vector<Ensemble_ptr> ensembles = this->get_all_by_dataset_id(dataset_id);
    //iterate only by decon_queue (not aux_decon) because each aux_decons must be in another ensemble as decon_queue
    int ret_val = 0;
    for (Ensemble_ptr p_ensemble : ensembles)
        ret_val += this->remove(p_ensemble);
    return ret_val;
}


int EnsembleService::remove(const Ensemble_ptr& p_ensemble) 
{
    reject_nullptr(p_ensemble);
    model_service.remove_by_ensemble_id(p_ensemble->get_id());
    int ret_val = ensemble_dao.remove(p_ensemble); // First we need to remove ensembles due "REFERENCE" in db
    if (p_ensemble->get_decon_queue() != nullptr)
        decon_queue_service.remove(p_ensemble->get_decon_queue());
    return ret_val;
}


std::vector<Ensemble_ptr> EnsembleService::init_ensembles_from_dataset(const Dataset_ptr &p_dataset)
{
    // checking data
    InputQueue_ptr p_queue = p_dataset->get_input_queue();
    if (p_dataset->get_ensembles_svr_parameters().size() <  p_queue->get_value_columns().size())
    {
        LOG4_ERROR("Not enough svr_parameters in dataset");
        return std::vector<Ensemble_ptr>();
    }
/* TODO We do not do this check.
    for (InputQueue_ptr p_aux_queue : p_dataset->get_aux_input_queues())
    {
        if (p_aux_queue->get_value_columns().size() != p_queue->get_value_columns().size())
        {
            LOG4_ERROR("Column numbers [" << p_aux_queue->get_value_columns().size() << "] in aux queue ["
                       << p_aux_queue->get_table_name() << "] not equal as in main queue ["
                       << p_queue->get_value_columns().size() << "]");
            return std::vector<Ensemble_ptr>();
        }
    }
*/
    // creating deconQueue metadata
    bpt::ptime curr_time = bpt::second_clock::local_time();
    static const DataRow default_datarow(curr_time, curr_time, 1.0, std::vector<double>(p_dataset->get_swt_levels() + 1, 1.0));

    std::vector<DeconQueue_ptr> decon_queues;
    std::vector<std::vector<DeconQueue_ptr>> aux_decon_queues;
    for (size_t i = 0; i < p_queue->get_value_columns().size(); i++)
    {
        std::string column_name = p_queue->get_value_columns()[i];
        svr::datamodel::DeconQueue_ptr p_decon = std::make_shared<DeconQueue>(
                std::string(), p_queue->get_table_name(), column_name, p_dataset->get_id());
        //add one DataRow for posibility saving DeconQueue
        p_decon->get_data()[curr_time] = std::make_shared<DataRow>(default_datarow);
        decon_queues.push_back(p_decon);
        std::vector<DeconQueue_ptr> aux_decon_queues_curent_column;
        aux_decon_queues_curent_column.reserve(p_dataset->get_aux_input_queues().size());
        for (InputQueue_ptr &p_aux_queue : p_dataset->get_aux_input_queues())
        {
            svr::datamodel::DeconQueue_ptr p_aux_decon = std::make_shared<DeconQueue>(
                        std::string(), p_aux_queue->get_table_name(), column_name, p_dataset->get_id());

            // Add one DataRow for posibility saving DeconQueue
            p_aux_decon->get_data()[curr_time] = std::make_shared<DataRow>(default_datarow);
            aux_decon_queues_curent_column.push_back(p_aux_decon);
        }
        aux_decon_queues.push_back(aux_decon_queues_curent_column);
    }

    return init_ensembles_from_dataset(p_dataset, decon_queues, aux_decon_queues);
}


std::vector<Ensemble_ptr> EnsembleService::init_ensembles_from_dataset(
        const Dataset_ptr &p_dataset,
        const std::vector<svr::datamodel::DeconQueue_ptr> &decon_queues,
        const std::vector<std::vector<svr::datamodel::DeconQueue_ptr>> &aux_decon_queues)
{
    LOG4_BEGIN();

    // checking data
    InputQueue_ptr p_queue = p_dataset->get_input_queue();
    size_t queue_column_names_size = p_queue->get_value_columns().size();
    if (p_dataset->get_ensembles_svr_parameters().size()
        != queue_column_names_size * (p_dataset->get_aux_input_queues().size() + 1)) // +1 for main input queue
    {
        LOG4_ERROR("Ensembles_svr_parameters size " << p_dataset->get_ensembles_svr_parameters().size()
                   << " and number of decons " << decon_queues.size() * queue_column_names_size << " aren't equal");
        return std::vector<Ensemble_ptr>();
    }

    // init ensembles
    std::vector<Ensemble_ptr> ensembles;
    for (size_t i = 0; i < decon_queues.size(); i++)
    {
        Ensemble_ptr p_ensemble;
        std::vector<DeconQueue_ptr> all_aux_decon_queues;

        // Add aux columns decon queues
        if (svr::context::AppContext::get_instance().app_properties.get_main_columns_aux())
            for (auto &p_decon_queue: decon_queues)
                if (p_decon_queue->get_input_queue_column_name() != decon_queues[i]->get_input_queue_column_name())
                    all_aux_decon_queues.push_back(p_decon_queue);

        // Add aux input decon queues
        for (auto &p_decon_queue: aux_decon_queues[i])
            all_aux_decon_queues.push_back(p_decon_queue);

        ensembles.push_back(std::make_shared<Ensemble>(
                0, p_dataset->get_id(), std::vector<Model_ptr>(), decon_queues[i], all_aux_decon_queues));
    }

    for (size_t aux_queue_numb = 0; aux_queue_numb < p_dataset->get_aux_input_queues().size(); ++aux_queue_numb)
        for (size_t model_numb = 0; model_numb < decon_queues.size(); ++model_numb)
            ensembles.push_back(std::make_shared<Ensemble>(
                    0, p_dataset->get_id(), std::vector<Model_ptr>(), aux_decon_queues[model_numb][aux_queue_numb]));

    LOG4_END();

    return ensembles;
}


// TODO Reconsider :)
void
EnsembleService::init_decon_data(
        std::vector<Ensemble_ptr> &ensembles,
        const std::vector<DeconQueue_ptr> &decon_queues,
        const std::vector<std::vector<DeconQueue_ptr>> &aux_decon_queues)
{
    LOG4_BEGIN();

    for (Ensemble_ptr &p_ensemble: ensembles) {
        for (const DeconQueue_ptr &p_decon_queue: decon_queues)
            if (p_ensemble->get_decon_queue()->get_input_queue_column_name() == p_decon_queue->get_input_queue_column_name() &&
                    p_ensemble->get_decon_queue()->get_input_queue_table_name() == p_decon_queue->get_input_queue_table_name()) {
                p_ensemble->get_decon_queue()->set_data(p_decon_queue->get_data());
                break;
            }
        for (DeconQueue_ptr &p_ensemble_decon_queue: p_ensemble->get_aux_decon_queues()) {
            /*
            for (const std::vector<DeconQueue_ptr> &aux_decon_queues_vec: aux_decon_queues)
                for (const DeconQueue_ptr &p_decon_queue: aux_decon_queues_vec)
                    if (p_ensemble_decon_queue->get_input_queue_table_name() == p_decon_queue->get_input_queue_table_name() &&
                        p_ensemble_decon_queue->get_input_queue_column_name() == p_decon_queue->get_input_queue_column_name())
                        p_ensemble_decon_queue->set_data(p_decon_queue->get_data());
            */
            // Aux columns from main decon queue
            for (const DeconQueue_ptr &p_decon_queue: decon_queues)
                if (p_ensemble_decon_queue->get_input_queue_table_name() == p_decon_queue->get_input_queue_table_name() &&
                    p_ensemble_decon_queue->get_input_queue_column_name() == p_decon_queue->get_input_queue_column_name()) {
                    p_ensemble_decon_queue->set_data(p_decon_queue->get_data());
                    break;
                }
        }
    }

    LOG4_END();
}


bool EnsembleService::exists(bigint ensemble_id)
{
    return ensemble_dao.exists(ensemble_id);
}


OnlineSVR_ptr EnsembleService::prepare_data_and_train_model(
        Ensemble_ptr &p_ensemble,
        const SVRParameters_ptr &p_svr_parameters, 
        std::vector<cascaded::layer_data> &models_train_data,
        const bpt::time_duration &max_gap,
        size_t model_counter)
{
    Model_ptr &p_model = p_ensemble->get_models().at(model_counter);
    LOG4_DEBUG("Training model " << p_model->to_string() << " with SVR parameters " << p_svr_parameters->to_string());

    auto start_iter = p_ensemble->get_decon_queue()->get_data().begin();
    off64_t distance = std::distance(start_iter, p_ensemble->get_decon_queue()->get_data().end());
    if (distance > (off64_t) p_svr_parameters->get_svr_decremental_distance())
        std::advance(start_iter, distance - p_svr_parameters->get_svr_decremental_distance());
    datarow_range main_data_range(start_iter, p_ensemble->get_decon_queue()->get_data().end(), p_ensemble->get_decon_queue()->get_data());
    //get aux_data_row vector
    std::vector<datarow_range> aux_data_ranges;
    for (const DeconQueue_ptr &p_aux_decon_queue : p_ensemble->get_aux_decon_queues()) {
        auto start_aux_iter = p_aux_decon_queue->get_data().begin();
        off64_t distance = std::distance(start_aux_iter, p_aux_decon_queue->get_data().end());
        if (distance > (off64_t) p_svr_parameters->get_svr_decremental_distance())
            std::advance(start_aux_iter, distance - p_svr_parameters->get_svr_decremental_distance());
        // TODO Add advance minus lag
        aux_data_ranges.push_back(
                datarow_range(start_aux_iter, p_aux_decon_queue->get_data().end(), p_aux_decon_queue->get_data()));
    }

    cascaded::layer_data training_data;
    try {
        // Create training data
        training_data = model_service.get_training_data(
                main_data_range,
                p_svr_parameters->get_lag_count(),
                p_model->get_learning_levels(),
                max_gap,
                model_counter,
                aux_data_ranges,
                p_model->get_last_modeled_value_time());
    } catch (const std::runtime_error &ex) {
        LOG4_ERROR(ex.what());
    }
    
    LOG4_DEBUG("Training model for decon level " << model_counter);
        
    models_train_data.push_back(training_data);
    model_service.train(*p_svr_parameters, p_model, training_data, main_data_range.rbegin()->first);
    return p_model->get_svr_model();
}

// TODO Train with a time range!
// TODO Split in two, one for per model train one for all models in ensemble
void EnsembleService::train(
        Ensemble_ptr &p_ensemble,
        const std::vector<SVRParameters_ptr> &model_parameters,
        const bpt::time_duration &max_gap,
        const size_t model_numb)
{
    LOG4_BEGIN();

    if (model_numb != std::numeric_limits<size_t>::max()) LOG4_DEBUG("Training only model #" << model_numb);

    const size_t models_count = model_parameters.size();
    std::vector<Model_ptr> models;
    std::vector<std::future<OnlineSVR_ptr>> futures;
    std::vector<cascaded::layer_data> models_train_data;

    // Initialize if models are non existant
    if(!p_ensemble->get_models().empty()) goto __train;

    LOG4_DEBUG("Ensemble models are empty. Initializing models with default values.");

    for (size_t model_counter = 0; model_counter < models_count; model_counter++)
    {
        SVRParameters_ptr p_svr_parameters = model_parameters.at(model_counter);
        const std::vector<size_t> adjacent_levels{get_adjacent_indexes(
                model_counter,
                p_svr_parameters->get_svr_adjacent_levels_ratio(),
                model_parameters.size())
        };

        auto p_model = std::make_shared<Model>(
                0, p_ensemble->get_id(), model_counter, adjacent_levels, nullptr, bpt::min_date_time, bpt::min_date_time);

        if (p_model->get_learning_levels().size() < 1) {
            LOG4_DEBUG("Reinitalizing model learning levels.");
            const std::vector<size_t> adjacent_levels {
                    get_adjacent_indexes(
                            model_counter,
                            p_svr_parameters->get_svr_adjacent_levels_ratio(),
                            model_parameters.size())
            };

            p_model->set_decon_level(model_counter);
            p_model->set_learning_levels(adjacent_levels);
        }

        models.push_back(p_model);
    }
    p_ensemble->set_models(models);

    __train:
    for (size_t model_counter = 0; model_counter < models_count; model_counter++)
    {
        // continue if we need train only one model and current current model_counter != model_numb which need to train
        if (model_numb != ALL_MODELS && model_counter != model_numb) {
            LOG4_DEBUG("Skipping training on model " << model_counter << " because we want model " <<
                                                     model_numb << " out of " << models_count << " models trained.");
            continue;
        }
        SVRParameters_ptr p_svr_parameters = model_parameters.at(model_counter);
        // std::unique_lock<std::mutex> l(svr::common::memory_manager::lock);
        // svr::common::memory_manager::mem_cv.wait(l, [](){return svr::common::memory_manager::mem_available;});
        futures.push_back(std::async(std::launch::async, &EnsembleService::prepare_data_and_train_model, this,
                std::ref(p_ensemble), std::ref(p_svr_parameters), std::ref(models_train_data), std::ref(max_gap), model_counter));
    }

    for(auto &f : futures)
    {
        OnlineSVR_ptr p_svr_model = f.get();
        Model_ptr p_model = p_ensemble->get_models().at(p_svr_model->get_svr_parameters().get_decon_level());
        p_model->set_svr_model(p_svr_model); /* Why is this necessary? We pass model by reference. */
        p_model->set_last_modeled_value_time(p_ensemble->get_decon_queue()->get_data().rbegin()->first);
        p_model->set_last_modified(bpt::second_clock::local_time());
    }

    LOG4_INFO("Finished training ensemble.");
    
    for (auto & data: models_train_data)
    {
        delete data.training_matrix;
        delete data.reference_vector;
    }

    LOG4_END();
}


// Warning predict in place
void EnsembleService::predict(
        Ensemble_ptr &p_ensemble,
        const boost::posix_time::time_period &range,
        const boost::posix_time::time_duration &resolution,
        const bpt::time_duration &max_gap)
{
// TODO Implement aux decon data prediction
// TODO Predict ensemble should return a decon queue of predicted values instead of writing in it's own decon queue
    LOG4_DEBUG("Request to predict time period " << bpt::to_simple_string(range));
    DataRow::Container &main_data = p_ensemble->get_decon_queue()->get_data(); // we write back forecasted data to the ensemble decon queue
    std::vector<DataRowContainer_ptr> aux_decon_data;
    for (const auto &decon_queue: p_ensemble->get_aux_decon_queues())
        aux_decon_data.push_back(std::make_shared<DataRowContainer>(decon_queue->get_data())); // TODO Remove copying of DataRowContainer
    svr::context::AppContext::get_instance().model_service.predict(p_ensemble->get_models(), range, resolution, max_gap, main_data, aux_decon_data);

    LOG4_END();
}

}
}
