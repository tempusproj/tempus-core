#include <future>

#include <appcontext.hpp>
#include "InputQueueService.hpp"

#include "DAO/InputQueueDAO.hpp"
#include "util/ValidationUtils.hpp"
#include <common/exceptions.hpp>
#include <DAO/ScopedTransaction.hpp>

#include <model/InputQueue.hpp>
#include <model/Dataset.hpp>

using namespace std;
using namespace bpt;
using namespace svr::common;
using namespace svr::datamodel;

namespace svr {
namespace business {

InputQueueService::InputQueueService(svr::dao::InputQueueDAO& inputQueueDao)
: input_queue_dao(inputQueueDao)
{}

InputQueueService::~InputQueueService()
{}

InputQueue_ptr
InputQueueService::get_queue_metadata(
        const string &userName,
        const string &logicalName,
        const time_duration &resolution)
{
    return input_queue_dao.get_queue_metadata(userName, logicalName, resolution);
}

InputQueue_ptr
InputQueueService::get_queue_metadata(const string &input_queue_table_name)
{
    return input_queue_dao.get_queue_metadata(input_queue_table_name);
}

svr::datamodel::DataRow::Container InputQueueService::get_queue_data(
        const string &table_name,
        const ptime &time_from,
        const ptime &time_to,
        size_t limit)
{
    map<ptime, DataRow_ptr> result;
    vector<DataRow_ptr> data;

    PROFILE_EXEC_TIME(data = input_queue_dao.get_queue_data_by_table_name(table_name, time_from, time_to, limit), "Getting queue data from DAO");

    PROFILE_EXEC_TIME(
                for (DataRow_ptr &row : data) {
                    result[row->get_value_time()] = row;
                },
                "Mapping Queue data by value_time");

    return result;
}

std::map<ptime, DataRow_ptr> InputQueueService::get_latest_queue_data(
        const string &table_name,
        const size_t limit,
        const bpt::ptime &last_time)
{
    map<ptime, DataRow_ptr> result;
    vector<DataRow_ptr> data;

    PROFILE_EXEC_TIME(
            data = input_queue_dao.get_latest_queue_data_by_table_name(table_name, limit, last_time), "Getting queue data from DAO");

    PROFILE_EXEC_TIME(
            for (DataRow_ptr &row: data) result[row->get_value_time()] = row, "Mapping Queue data by value_time");

    return result;
}

long InputQueueService::save(const InputQueue_ptr &p_input_queue)
{
    return input_queue_dao.save(p_input_queue);
}


bool InputQueueService::exists(
        const std::string &user_name,
        const std::string &logical_name,
        const bpt::time_duration &resolution)
{
    return input_queue_dao.exists(user_name, logical_name, resolution);
}


int InputQueueService::remove(const InputQueue_ptr& p_input_queue)
{
    reject_nullptr(p_input_queue);
    return input_queue_dao.remove(p_input_queue);
}

/* TODO Optimize! */
bpt::ptime InputQueueService::adjust_time_on_grid(
        const InputQueue_ptr &p_input_queue,
        const bpt::ptime &value_time)
{
    ptime epoch = from_time_t(0);
    time_duration duration_since_epoch = value_time - epoch;

    // check whether value_time may be present in two different timegrid slots
    // Hint: this can(should) be caught on object construction time.
    if (p_input_queue->get_resolution().total_seconds() / 2
            < p_input_queue->get_legal_time_deviation().total_seconds()) {
        throw invalid_argument(
                    "Invalid Resolution/Legal time deviation arguments!"
                    "Cannot calculate time on grid because value_time may be valid on two different timegrid slots!");
    }

    long seconds_on_grid_since_epoch = 0;

    // check if the value_time can fit on the previous time grid slot
    if ((duration_since_epoch.total_seconds() % p_input_queue->get_resolution().total_seconds())
            <= p_input_queue->get_legal_time_deviation().total_seconds()) {
        seconds_on_grid_since_epoch = duration_since_epoch.total_seconds()
                                - (duration_since_epoch.total_seconds() % p_input_queue->get_resolution().total_seconds());
    // else check if the value_time can fit on the next timegrid slot
    } else if (((
                seconds_on_grid_since_epoch =
                duration_since_epoch.total_seconds() + p_input_queue->get_legal_time_deviation().total_seconds()
            ) % p_input_queue->get_resolution().total_seconds())
            < p_input_queue->get_legal_time_deviation().total_seconds()) {
        seconds_on_grid_since_epoch -= seconds_on_grid_since_epoch
                % p_input_queue->get_resolution().total_seconds();
    } else
        return not_a_date_time;

    return from_time_t(0) + seconds(seconds_on_grid_since_epoch);
}


bool
InputQueueService::add_row(
        InputQueue_ptr& p_input_queue,
        DataRow_ptr p_row,
        bool concatenate)
{
    reject_nullptr(p_input_queue);
    reject_nullptr(p_row);

    bpt::ptime adjusted_value_time = adjust_time_on_grid(p_input_queue, p_row->get_value_time());
    if(adjusted_value_time == not_a_date_time) {
        LOG4_DEBUG("The following row doesn't fit in the queue: " << p_row->to_string());
        return false;
    }

    if(p_row->get_value_time() != adjusted_value_time) p_row->set_value_time(adjusted_value_time);

    DataRow::Container& data = p_input_queue->get_data();
    if (concatenate && !data.empty()) {
        for (ptime insert_time = data.rbegin()->first + p_input_queue->get_resolution();
             insert_time < adjusted_value_time;
             insert_time += p_input_queue->get_resolution()) {
            DataRow_ptr insert_row = make_shared<DataRow>(*p_row);
            insert_row->set_value_time(insert_time);
            data[insert_time] = insert_row;
        }
    }

    data[adjusted_value_time] = p_row;
    return true;
}


InputQueue_ptr
InputQueueService::clone_with_data(
        const InputQueue_ptr &p_input_queue,
        const time_period &time_range,
        const size_t minimum_rows_count)
{
    vector<DataRow_ptr> rows = input_queue_dao.get_queue_data_by_table_name(
                                                    p_input_queue->get_table_name(), time_range.begin(), time_range.end());

    InputQueue_ptr result_queue = p_input_queue->clone_empty();
    DataRow::Container &input_queue_data = result_queue->get_data();
    for (const DataRow_ptr &p_row: rows) input_queue_data.emplace(p_row->get_value_time(), p_row /* push the row pointer, no need to copy */);

    if(input_queue_data.size() < minimum_rows_count)
        throw insufficient_data(
                "Not enough data in inputQueue in time range " + bpt::to_simple_string(time_range) +
                " number of observations is lesser than " + std::to_string(minimum_rows_count));

    return result_queue;
}


DataRowContainer
InputQueueService::get_column_data(
        const InputQueue_ptr &queue,
        const string &column_name)
{
    LOG4_BEGIN();

    DataRow::Container column_data = queue->get_data();
    const size_t column_index = get_value_column_index(queue, column_name);
    DataRow::Container result;

    if (column_data.empty()) {
        LOG4_ERROR("Column data is empty. Aborting.");
        return result;
    }

    if (column_data.begin()->second->get_values().size() <= column_index || column_index < 0) {
        LOG4_ERROR("Column index out of bounds " << column_index << ", columns in input queue " <<
                   column_data.begin()->second->get_values().size() << ". Aborting.");
        return result;
    }

    for(auto& row: column_data)
    {
        if (!row.second) {
            LOG4_ERROR("Invalid row. Skipping.");
            continue;
        }

        result[row.first] = std::make_shared<DataRow>(
                    DataRow(row.second->get_value_time(),
                            row.second->get_update_time(),
                            row.second->get_tick_volume(),
        {row.second->get_values()[column_index]}));
    }

    LOG4_END();

    return result;
}

std::vector<std::string> InputQueueService::get_db_table_column_names(const InputQueue_ptr & queue)
{
    auto db_all_columns = input_queue_dao.get_db_table_column_names(queue);

    std::vector<std::string> result;

    auto iut = std::find_if( db_all_columns.begin(), db_all_columns.end(), [](std::shared_ptr<std::string> const & col){ return *col == "tick_volume";});
    if(iut == db_all_columns.end())
        return result;

    iut += 1;

    result.reserve(std::distance(iut, db_all_columns.end()));

    for(auto ir = result.begin(); iut != db_all_columns.end(); ++iut, ++ir)
        result.push_back(**iut);

    return result;
}

size_t InputQueueService::get_value_column_index(
        const InputQueue_ptr &p_input_queue, const std::string &column_name)
{
    const auto& cols = p_input_queue->get_value_columns();
    const auto pos = find(cols.begin(), cols.end(), column_name);
    if(pos == cols.end()) {
        LOG4_WARN("Column " << column_name << " is not part of input queue " << p_input_queue->get_table_name());
        return SVR_RC_GENERAL_ERROR;
    }
    return (size_t) std::abs(std::distance(cols.begin(), pos));
}


DataRow_ptr InputQueueService::find_oldest_record(const InputQueue_ptr &queue)
{
    reject_nullptr(queue);

    return input_queue_dao.find_oldest_record(queue);
}

DataRow_ptr InputQueueService::find_newest_record(const InputQueue_ptr &queue) {
    reject_nullptr(queue);

    return input_queue_dao.find_newest_record(queue);
}

std::vector<InputQueue_ptr> InputQueueService::get_all_user_queues(const std::string &user_name) {
    return input_queue_dao.get_all_user_queues(user_name);
}

size_t InputQueueService::save_data(const InputQueue_ptr &queue) {

    reject_nullptr(queue);
    reject_empty(queue->get_data());

    return input_queue_dao.save_data(queue);
}

svr::dao::OptionalTimeRange InputQueueService::get_missing_hours(const InputQueue_ptr &queue, svr::dao::TimeRange const & fromRange) const
{
    return input_queue_dao.get_missing_hours(queue, fromRange);
}

void InputQueueService::mark_hours_missing(InputQueue_ptr const & queue, std::string const & start, std::string const & end)
{
    input_queue_dao.mark_hours_missing(queue, start, end);
}

void InputQueueService::purge_missing_hours(InputQueue_ptr const & queue)
{
    input_queue_dao.purge_missing_hours(queue);
}

} /* namespace business */
} /* namespace svr */
