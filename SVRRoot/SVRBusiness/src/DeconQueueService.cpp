#include <model/FramesContainer.hpp>
#include <util/ValidationUtils.hpp>
#include <util/MathUtils.hpp>
#include "DeconQueueService.hpp"
#include "model/InputQueue.hpp"
#include "model/DeconQueue.hpp"
#include "model/Dataset.hpp"
#include "DAO/DeconQueueDAO.hpp"
#include "InputQueueService.hpp"
#include "WaveletTransform.hpp"
#include "appcontext.hpp"

using namespace std;
using namespace svr::datamodel;
using namespace svr::common;
using namespace bpt;

namespace svr{
namespace business{


DeconQueue_ptr DeconQueueService::get_by_table_name(const std::string &table_name)
{
    return decon_queue_dao.get_decon_queue_by_table_name(table_name);
}

DeconQueue_ptr DeconQueueService::get_queue_metadata(const std::string &table_name)
{
    return decon_queue_dao.get_decon_queue_by_table_name(table_name);
}

void DeconQueueService::save(DeconQueue_ptr const &p_decon_queue)
{
    reject_nullptr(p_decon_queue);
    decon_queue_dao.save(p_decon_queue);
}

bool DeconQueueService::exists(DeconQueue_ptr const &p_decon_queue)
{
    reject_nullptr(p_decon_queue);
    return decon_queue_dao.exists(p_decon_queue->get_table_name());
}

bool DeconQueueService::exists(const string& decon_queue_table_name)
{
    return decon_queue_dao.exists(decon_queue_table_name);
}

int DeconQueueService::remove(DeconQueue_ptr const &p_decon_queue)
{
    if (exists(p_decon_queue))
        return decon_queue_dao.remove(p_decon_queue);
    else
        return 0;
}


std::vector<DeconQueue_ptr>
DeconQueueService::deconstruct(
        const InputQueue_ptr &p_input_queue,
        const Dataset_ptr &dataset,
        const bool get_data_from_database)
{
    std::vector<DeconQueue_ptr> decon_queues;
    for (const std::string &column_name: p_input_queue->get_value_columns()) {
        auto p_decon_queue = deconstruct(p_input_queue, dataset, column_name, get_data_from_database);
        if (p_decon_queue) decon_queues.push_back(p_decon_queue);
    }
    return decon_queues;
}


DeconQueue_ptr
DeconQueueService::deconstruct(
        const InputQueue_ptr &p_input_queue,
        const Dataset_ptr &p_dataset,
        const std::string &column_name,
        const bool get_data_from_database)
{
    LOG4_BEGIN();
    if (svr::context::AppContext::get_instance().input_queue_service.get_value_column_index(p_input_queue, column_name) == SVR_RC_GENERAL_ERROR) {
        LOG4_ERROR("Column " << column_name << " not found in input queue " << p_input_queue->get_table_name());
        return nullptr;
    }

    DeconQueue_ptr p_decon_queue = make_shared<DeconQueue>(
            "",
            p_input_queue->get_table_name(),
            column_name,
            p_dataset->get_id());

    deconstruct(p_input_queue, p_dataset, column_name, p_decon_queue->get_data(), get_data_from_database);
    LOG4_END();

    return p_decon_queue;
}


void
DeconQueueService::deconstruct(
        const InputQueue_ptr &p_input_queue,
        const Dataset_ptr &p_dataset,
        const string &column_name,
        svr::datamodel::DataRow::Container &out_data,
        const bool get_data_from_database)
{
    const size_t frame_size_ = svr::common::swt_levels_to_frame_length(p_dataset->get_swt_levels());
    if(p_dataset->get_swt_levels() == 0) // TODO Optimize zero columns case for not get data from database
        // Skipping deconstruction as wavelet levels count is set to zero.
        out_data = input_queue_service.get_column_data(p_input_queue, column_name);
    else if (get_data_from_database)
        deconstruct(
                input_queue_service.get_column_data(p_input_queue, column_name),
                p_dataset->get_swt_wavelet_name(),
                frame_size_,
                out_data);
    else
        this->deconstruct(
                p_input_queue->get_data(),
                p_dataset->get_swt_wavelet_name(),
                frame_size_,
                out_data,
                context::AppContext::get_instance().input_queue_service.get_value_column_index(
                        p_input_queue, column_name));
}


inline void
DeconQueueService::copy_decon_frame_to_container(
        const DataRow::Container &data,
        DataRow::Container &data_container,
        const std::vector<bpt::ptime> &times,
        const std::vector<std::vector<double>> &decon_frame,
        size_t limit)
{
    if (limit > decon_frame.size()) limit = decon_frame.size();

    LOG4_DEBUG("Copying " << limit << " rows of " << decon_frame[0].size() <<
               " columns count with starting value time " << *times.begin() << " ending value time " << *times.rbegin());

    // TODO Optimize once DataRowContainer is implemented on top of viennacl::matrix
    for(size_t decon_row_index = 0; decon_row_index < limit; decon_row_index++)
        data_container[times[decon_row_index]] = std::make_shared<DataRow>(
                times[decon_row_index],
                bpt::second_clock::local_time(),
                data.at(times[decon_row_index])->get_tick_volume(),
                decon_frame[decon_row_index]);

    LOG4_END();
}

void
DeconQueueService::deconstruct(
        const DataRow::Container &data,
        const std::string &wavelet_name,
        const size_t frame_size,
        DataRow::Container &decon_data,
        const size_t input_column_index)
{
    LOG4_BEGIN();

    if (data.size() < frame_size)
    {
        LOG4_ERROR("Not enough data for deconstruction, data size is " << data.size() << " frame_size is " << frame_size);
        return;
    }

    if (input_column_index >= data.begin()->second->get_values().size()) {
        LOG4_ERROR("Column index out of bounds. Aborting.");
        return;
    }

    SWTransform swt(wavelet_name, frame_size, frame_size_to_swt_levels(frame_size));
    std::vector<double> frame(frame_size);
    std::vector<bpt::ptime> times;
    DataRowContainer::const_iterator row_iter = data.begin();
    for (size_t frame_element_index = 0; frame_element_index < frame_size; ++row_iter, frame_element_index++)
    {
        times.push_back(row_iter->first);
        frame[frame_element_index] = row_iter->second->get_values()[input_column_index];
    }

    {
        std::vector<std::vector<double>> decon_frame;
        swt.swt_frame(frame, decon_frame);
        size_t frame_mod = data.size() % frame_size;

        if (data.size() == frame_size) {
            copy_decon_frame_to_container(data, decon_data, times, decon_frame);
            return;
        }

        copy_decon_frame_to_container(data, decon_data, times, decon_frame, frame_mod);
        times.clear();
        row_iter = data.begin();
        std::advance(row_iter, frame_mod);
    }

    for (size_t frame_element_index = 0; row_iter != data.end(); ++row_iter)
    {
        times.push_back(row_iter->first);
        frame[frame_element_index++] = row_iter->second->get_values()[input_column_index];

        if (frame_element_index == frame_size)
        { // TODO Parallelize, put in a future
            std::vector<std::vector<double>> decon_frame;
            PROFILE_EXEC_TIME(
                    swt.swt_frame(frame, decon_frame),
                    "Forward stationary wavelet transform");
            copy_decon_frame_to_container(data, decon_data, times, decon_frame);
            frame_element_index = 0;
            times.clear();
        }
    }
    LOG4_END();
}

void
DeconQueueService::copy_recon_frame_to_container(
        const svr::datamodel::DataRow::Container &decon_data,
        const std::vector<bpt::ptime> &times,
        DataRow::Container &recon_data,
        const std::vector<double> &reconstructed_values,
        size_t limit)
{
    if (limit > reconstructed_values.size()) limit = reconstructed_values.size();

    LOG4_DEBUG("Copying " << limit << " rows with starting value time " << *times.begin() << " ending value time " << *times.rbegin());

    for(size_t j = 0; j < reconstructed_values.size() && j < times.size(); j++) {
        const auto row_iter = decon_data.find(times[j]);
        double tick_volume = row_iter == decon_data.end() ? DEFAULT_VALUE_TICK_VOLUME : row_iter->second->get_tick_volume();
        recon_data[times[j]] = std::make_shared<DataRow>(
                    DataRow(times[j],
                            bpt::second_clock::local_time(),
                            tick_volume,
        { reconstructed_values[j] } ));
    }

    LOG4_END();
}

void
DeconQueueService::copy_coefs_to_vector(
        const std::pair<bpt::ptime, std::shared_ptr<DataRow>> &row,
        const size_t frame_size,
        const size_t row_index,
        std::vector<bpt::ptime> &times,
        std::vector<double> &current_frame)
{
    LOG4_BEGIN();

    size_t wanted_frame_size = row.second->get_values().size() * frame_size;
    if (current_frame.size() < wanted_frame_size) {
        LOG4_ERROR("Output vector size " << current_frame.size() <<
                   " too small to contain all required values " << wanted_frame_size);
        return;
    }

    times.push_back(row.first);
    for (size_t level_ix = 0; level_ix < row.second->get_values().size(); ++level_ix)
        current_frame[row_index  +  level_ix * frame_size] = row.second->get_values()[level_ix];

    LOG4_END();
}

DataRowContainer DeconQueueService::reconstruct(
        const svr::datamodel::datarow_range &data,
        const string &wavelet_name,
        const size_t frame_size)
{
    LOG4_BEGIN();
    DataRow::Container out_data_container;
    size_t swt_levels = frame_size_to_swt_levels(frame_size);
    std::vector<double> current_frame(frame_size * (swt_levels + 1), 0);
    std::vector<bpt::ptime> times;
    DataRowContainer::const_iterator row_iter;
    DataRowContainer::const_iterator start_frame_iter;

    SWTransform swt(wavelet_name, frame_size, swt_levels);
    std::vector<double> reconstructed_values;

    if (frame_size < 1 || swt_levels == 0) {
        LOG4_DEBUG("Wavelet levels is set to zero. Frame reconstruction aborted.");
        goto __bail;
    }

    if (data.distance() < frame_size) {
        LOG4_ERROR("Not enough data " << data.distance() << " in container to form a frame of size " << frame_size << ". Aborting.");
        goto __bail;
    }

    row_iter = data.begin();
    for (size_t frame_element_index = 0; frame_element_index < frame_size; ++row_iter, frame_element_index++)
        copy_coefs_to_vector(*row_iter, frame_size, frame_element_index, times, current_frame);

    swt.iswt_frame(current_frame, reconstructed_values);
    copy_recon_frame_to_container(data.get_container(), times, out_data_container, reconstructed_values);

    if (data.get_container().size() == frame_size) goto __bail;

    times.clear();
    row_iter = data.begin();
    start_frame_iter = row_iter;
    std::advance(row_iter, data.distance() % frame_size);

    for (size_t frame_element_index = 0; row_iter != data.end(); ++row_iter)
    {
        copy_coefs_to_vector(*row_iter, frame_size, frame_element_index++, times, current_frame);
        if(frame_element_index < frame_size) continue;

        // We have a full frame here
        swt.iswt_frame(current_frame, reconstructed_values);
        copy_recon_frame_to_container(data.get_container(), times, out_data_container, reconstructed_values);
        times.clear();
        start_frame_iter = row_iter;
        frame_element_index = 0;
    }

__bail:
    LOG4_END();

    return out_data_container;
}


long DeconQueueService::load_decon_data(const DeconQueue_ptr decon_queue, const ptime &time_from, const ptime &time_to, size_t limit)
{
    reject_nullptr(decon_queue);
    vector<DataRow_ptr> data = decon_queue_dao.get_data(decon_queue->get_table_name(), time_from, time_to, limit);
    DataRowContainer &decon_queue_data = decon_queue->get_data();
    for(const DataRow_ptr &row: data) decon_queue_data[row->get_value_time()] = row;
    return data.size();
}

long DeconQueueService::load_latest_decon_data(const DeconQueue_ptr decon_queue, const ptime &time_to, size_t limit)
{
    LOG4_DEBUG("Loading " << limit << " values until " << bpt::to_simple_string(time_to) <<
                          " from decon queue " << decon_queue->get_table_name());
    reject_nullptr(decon_queue);
    vector<DataRow_ptr> data;
    try {
        data = decon_queue_dao.get_latest_data(decon_queue->get_table_name(), time_to, limit);
    } catch (const std::exception &ex) {
        LOG4_WARN("Error loading data from decon queue " << decon_queue->get_table_name() << ". " << ex.what());
        return 0;
    }
    DataRowContainer &decon_queue_data = decon_queue->get_data();
    for (const DataRow_ptr &row: data) decon_queue_data[row->get_value_time()] = row;
    LOG4_DEBUG("Retrieved " << data.size() << " rows.");
    return data.size();
}

int DeconQueueService::clear(const DeconQueue_ptr &decon_queue)
{
    reject_nullptr(decon_queue);
    return decon_queue_dao.clear(decon_queue);
}

long DeconQueueService::count(const DeconQueue_ptr &decon_queue)
{
    reject_nullptr(decon_queue);
    return decon_queue_dao.count(decon_queue);
}

std::vector<std::vector<double>> DeconQueueService::to_matrix(const DeconQueue_ptr &decon_queue)
{
    if(decon_queue->get_data().empty())
    {
        LOG4_ERROR("DQ is empty");
        return std::vector<std::vector<double>>();
    }

    const size_t swt_levels {decon_queue->get_data().begin()->second->get_values().size()};
    std::vector<std::vector<double>> result (swt_levels, std::vector<double>());

    for(auto& datarow : decon_queue->get_data())
    {
        const std::vector<double> row {datarow.second->get_values()};

        for(size_t level = 0; level < swt_levels; ++level)
        {
            result[level].push_back(row[level]);
        }
    }

    return result;
}

} // business
} // svr
