#include <string>
#include "LocalAuthenticationProvider.hpp"
#include <util/StringUtils.hpp>

namespace svr{
namespace business{


bool LocalAuthenticationProvider::login(const std::string &userName, const std::string password) {
    std::string encPassword = svr::common::make_md5_hash(password);
    return user_service.login(userName, encPassword);
}
}
}