#include "ModelService.hpp"
#include "util/ValidationUtils.hpp"
#include "appcontext.hpp"
#include "DAO/ModelDAO.hpp"
#include "common/exceptions.hpp"

#include <string>

using namespace svr::common;
using namespace svr::datamodel;

namespace svr {
namespace business {

ModelService::ModelService(svr::dao::ModelDAO &model_dao)
:model_dao(model_dao)
{
}

Model_ptr ModelService::get_model_by_id(bigint model_id)
{
    if (model_id != 0)
        return model_dao.get_by_id(model_id);
    else
        return nullptr;
}

int ModelService::save(const Model_ptr &model)
{
    reject_nullptr(model);
    if(!model->get_id())
        model->set_id(model_dao.get_next_id());
   return model_dao.save(model);
}

bool ModelService::exists(const Model_ptr & model)
{
    return model_dao.exists(model->get_id());
}

int ModelService::remove(const Model_ptr &model)
{
    reject_nullptr(model);
    return model_dao.remove(model);
}

int ModelService::remove_by_ensemble_id(bigint ensemble_id)
{
    return model_dao.remove_by_ensemble_id(ensemble_id);
}

std::vector<Model_ptr> ModelService::get_all_models_by_ensemble_id(bigint ensemble_id)
{
    try{
        return model_dao.get_all_ensemble_models(ensemble_id);
    } catch(...) {
        LOG4_ERROR("Cannot read models from the DB.");
        return {};
    }
}


Model_ptr ModelService::get_model(bigint ensemble_id, size_t decon_level)
{
    return model_dao.get_by_ensemble_id_and_decon_level(ensemble_id, decon_level);
}


void ModelService::prepare_adjacent_levels_row(
        const std::vector<size_t> &adjacent_levels,
        const DataRowContainer::const_iterator &start_iter,
        const DataRowContainer::const_iterator &end_iter,
        const bpt::time_duration &max_gap,
        Vector<double> &row)
{
    LOG4_TRACE("Processing rows with value time " << start_iter->first << " until " << std::prev(end_iter)->first);

    // add adjacent levels
    for (auto adjacent_level: adjacent_levels) {
        for(DataRowContainer::const_iterator row_iter = start_iter; row_iter != std::next(end_iter); ++row_iter)
            /* Add autoregression lag features for the level */
            if (row_iter != start_iter && row_iter->first - std::prev(row_iter)->first > max_gap)
                    throw std::runtime_error(
                            "Invalid row. Gap between " + bpt::to_simple_string(row_iter->first) + " and " +
                            bpt::to_simple_string(std::prev(row_iter)->first) + " too large. Omitting this vector.");
            else
                row.Add(row_iter->second->get_values()[adjacent_level]);
    }

    LOG4_END();
}


void
ModelService::prepare_aux_data_row(
        const std::vector<size_t> &adjacent_levels,
        const std::vector<datarow_range> &aux_ranges,
        const DataRowContainer::const_iterator &start_lag_iterator,
        const DataRowContainer::const_iterator &end_lag_iterator,
        const bpt::time_duration &max_gap,
        Vector<double> &row
        )
{
    LOG4_TRACE("Processing row with value time " << start_lag_iterator->first << " until " << std::prev(end_lag_iterator)->first);

    // Add aux data
    for (const auto &adjacent_level: adjacent_levels) {
        for (const datarow_range &aux_range: aux_ranges) {
            DataRowContainer::const_iterator local_aux_iterator = find_nearest_before(aux_range.get_container(), start_lag_iterator->first, max_gap);
            DataRowContainer::const_iterator local_iterator = start_lag_iterator;
            while (local_iterator != std::next(end_lag_iterator)) {
                if (local_iterator != start_lag_iterator && local_aux_iterator->first - std::prev(local_aux_iterator)->first > max_gap)
                    throw svr::common::insufficient_data(
                            "Auxiliary data, gap between " + bpt::to_simple_string(local_aux_iterator->first) +
                            " and " + bpt::to_simple_string(std::prev(local_aux_iterator)->first) + " too big, omitting the vector");

                if (local_aux_iterator->first - local_iterator->first > max_gap)
                    throw svr::common::insufficient_data(
                            "Missing some data in data or auxiliary data. Aux value time is " + bpt::to_simple_string(local_aux_iterator->first) +
                            " while main value time is " + bpt::to_simple_string(local_iterator->first));

                if (local_aux_iterator == aux_range.get_container().end() || local_aux_iterator->second->get_values().empty())
                    throw svr::common::insufficient_data(
                            "Missing aux data for value time " + bpt::to_simple_string(local_iterator->first));

                row.Add(local_aux_iterator->second->get_values()[adjacent_level]);
                ++local_aux_iterator;
                ++local_iterator;
            }
        }
    }

    LOG4_END();
}


void
ModelService::prepare_time_features(
        const bpt::ptime &value_time,
        Vector<double> &row)
{
    LOG4_TRACE("Processing row with value time " << value_time);

    /* Add time features as one hot encoded structures */
    for(size_t hour_of_day = 0; hour_of_day < 24; ++hour_of_day)
        hour_of_day == static_cast<size_t>(value_time.time_of_day().hours()) ? row.Add(1.0) : row.Add(0.0);

    for(size_t day_of_week = 0; day_of_week < 7; ++day_of_week)
        day_of_week == static_cast<size_t>(value_time.date().day_of_week()) ? row.Add(1.0) : row.Add(0.0);

    for(size_t day_of_month = 0; day_of_month < 31; ++day_of_month)
        day_of_month == static_cast<size_t>(value_time.date().day() - 1) ? row.Add(1.0) : row.Add(0.0);

    for(size_t month_of_year = 0; month_of_year < 12; ++month_of_year)
        month_of_year == static_cast<size_t>(value_time.date().month() - 1) ? row.Add(1.0) : row.Add(0.0);

    LOG4_END();
}


// TODO Rewrite
inline void prepare_tick_volume_features(
        const std::vector<double>::const_iterator it_tick_volume,
        const size_t lag,
        Vector<double>& row)
{
    std::vector<double>::const_iterator it {it_tick_volume};
    for(size_t i = 0; i < lag; ++i) row.Add(*it++);
}


cascaded::layer_data
ModelService::get_training_data(
        const datarow_range &main_data_range,
        const size_t lag,
        const std::vector<size_t> &adjacent_levels,
        const bpt::time_duration &max_gap,
        const size_t current_level,
        const std::vector<datarow_range> &aux_data_ranges,
        const bpt::ptime &last_modeled_value_time)
{
    LOG4_DEBUG("Preparing training main_data_range from " << bpt::to_simple_string(main_data_range.begin()->first) <<
               " until " << bpt::to_simple_string(std::prev(main_data_range.end())->first));

    Matrix<double> *p_train_features_matrix = new Matrix<double>();
    Vector<double> *p_labels_vector = new Vector<double>();

    // TODO check for lag during daemon work
    if (main_data_range.distance() <= lag)
        throw std::runtime_error(
                "Too small chunk of main_data_range " + std::to_string(main_data_range.distance()) +
                        " for creating train matrix with lag " + std::to_string(lag));

    DataRowContainer::const_iterator label_iter = main_data_range.begin(); // for response vector
    std::advance(label_iter, lag + 1); // One position after lag count is the label

    for(DataRowContainer::iterator feature_start_iter = main_data_range.begin();
        label_iter != main_data_range.end();
        ++feature_start_iter, ++label_iter)
    {
        /* Sanity check, as we have different lag count per different wavelet levels. */
        if (label_iter->first < last_modeled_value_time) {
            LOG4_TRACE("Skipping already modeled row with value time " << bpt::to_simple_string(label_iter->first));
            continue;
        }
        LOG4_TRACE("Adding row to training matrix with value time " << bpt::to_simple_string(label_iter->first));
        Vector<double> row;
        { /* Fill up features vector row */
            std::vector<size_t> aux_adjacent_levels(1, current_level); /* TODO experiment with other values or make an option */
            DataRowContainer::const_iterator feature_end_iter = feature_start_iter;
            std::advance(feature_end_iter, lag);

            try {
                prepare_adjacent_levels_row(adjacent_levels, feature_start_iter, feature_end_iter, max_gap, row);
            } catch (const std::runtime_error &ex) {
                LOG4_INFO("For row at " << bpt::to_simple_string(label_iter->first) <<
                          " can't assemble adjacent lag observations. Skipping. " << ex.what());
                continue;
            }

            try {
                prepare_aux_data_row(aux_adjacent_levels, aux_data_ranges, feature_start_iter, feature_end_iter, max_gap, row);
            } catch (const std::runtime_error &ex) {
                LOG4_INFO("For row at " << bpt::to_simple_string(label_iter->first) << " can't assemble lag observations. Skipping. " << ex.what());
                continue;
            }

            prepare_time_features(label_iter->first, row);
        }
        p_train_features_matrix->AddRowCopy(row);
        p_labels_vector->Add(label_iter->second->get_values()[current_level]);
        LOG4_TRACE("Added row successfully.");
    }
    cascaded::layer_data res;
    res.training_matrix = p_train_features_matrix;
    res.reference_vector = p_labels_vector;

    if (res.reference_vector->GetLength() < 1 || res.training_matrix->GetLengthRows() < 1)
        throw std::runtime_error("No new main_data_range to prepare for training for model.");

    LOG4_DEBUG("Prepared training features matrix of " << res.training_matrix->GetLengthRows() <<
               " rows and " << res.training_matrix->GetLengthCols() << " columns.");

    return res;
}


void
ModelService::get_prediction_vector(
        const datarow_range &main_range,
        const std::vector<size_t> &adjacent_levels,
        const size_t current_level,
        const std::vector<datarow_range> &aux_ranges,
        const bpt::time_duration &max_gap,
        Vector<double> &row)
{
    LOG4_BEGIN();
    std::vector<size_t> adjacent_aux_levels(1, current_level); // TODO Add config option and experiment with different values for adjacent levels
    prepare_adjacent_levels_row(adjacent_levels, main_range.begin(), main_range.end(), max_gap, row);
    prepare_aux_data_row(adjacent_aux_levels, aux_ranges, main_range.begin(), main_range.end(), max_gap, row);
    prepare_time_features(main_range.rbegin()->first, row);
    LOG4_END();
}


void
ModelService::train(
        const SVRParameters &svr_parameters,
        Model_ptr &p_model,
        const cascaded::layer_data &data,
        const bpt::ptime &last_modeled_value_time)
{
    LOG4_INFO("Starting training model for level " << p_model->get_decon_level());

    if (data.reference_vector->GetLength() < 1 || data.training_matrix->GetLengthRows() < 1) {
        LOG4_ERROR(
                "Labels vector length is " << data.reference_vector->GetLength() <<
                " training features matrix row count is " << data.training_matrix->GetLengthRows() <<
                ". Aborting training.");
        return;
    }

    if (!p_model->get_svr_model() ||
            context::AppContext::get_instance().app_properties.get_dont_update_r_matrix() ||
            u_int64_t(data.training_matrix->GetLengthRows()) > svr_parameters.get_svr_decremental_distance() / 2) // If new dataset is more than half of decrement distance, usually its faster to batch train the whole model from scratch
        train_batch(svr_parameters, p_model, data);
    else
        train_online(data, p_model->get_svr_model());
    p_model->set_last_modeled_value_time(last_modeled_value_time);
    p_model->set_last_modified(bpt::second_clock::local_time());

    LOG4_INFO(
            "Finished training model for level " << p_model->get_svr_model()->get_svr_parameters().get_decon_level() <<
            " input queue name " << p_model->get_svr_model()->get_svr_parameters().get_input_queue_table_name() <<
            " input queue column " << p_model->get_svr_model()->get_svr_parameters().get_input_queue_column_name() <<
            " samples trained number is " << p_model->get_svr_model()->get_samples_trained_number());
}


void
ModelService::train_online(const cascaded::layer_data &data, OnlineSVR_ptr &p_svr_model)
{
    LOG4_DEBUG("Starting online SVM train on " << data.reference_vector->GetLength() << " values.");
    PROFILE_EXEC_TIME(
                p_svr_model->Train(
                        data.training_matrix->GetRowCopy(data.training_matrix->GetLengthRows() - 1),
                        data.reference_vector->GetValue(data.reference_vector->GetLength() - 1)),
                "Online SVM train");

    while (u_int64_t(p_svr_model->get_samples_trained_number()) > p_svr_model->get_svr_parameters().get_svr_decremental_distance())
            p_svr_model->Forget(0); // remove the last observation
}


void
ModelService::train_batch(const SVRParameters &svr_parameters, Model_ptr &p_model, const cascaded::layer_data &data)
{
    OnlineSVR_ptr p_svr_model = std::make_shared<OnlineSVR>(svr_parameters);
    svr::batch::set_max_smo_iterations(
                context::AppContext::get_instance().app_properties.get_max_smo_iterations());
    svr::batch::set_update_r_matrix(
                !context::AppContext::get_instance().app_properties.get_dont_update_r_matrix());
    cascaded::set_max_segment_size(
                context::AppContext::get_instance().app_properties.get_max_segment_size());
    cascaded::set_cascade_layer_reduce_ratio(
            context::AppContext::get_instance().app_properties.get_cascade_reduce_ratio());

    LOG4_DEBUG("Starting batch SVM train on " << data.reference_vector->GetLength() << " values.");
    PROFILE_EXEC_TIME(
            svr::cascaded::cascade_iteration(
                    *data.training_matrix, *data.reference_vector, *p_svr_model),
            "Batch SVM train");

    p_model->set_svr_model(p_svr_model);
}


datarow_range ModelService::prepare_feat_range(
        DataRow::Container &data,
        const boost::posix_time::time_duration &max_gap,
        const boost::posix_time::ptime &feat_time,
        const ssize_t lag_count)
{
    auto it_end = find_nearest_before(data, feat_time, max_gap, lag_count);
    auto it_lookback = it_end;
    std::advance(it_lookback, -lag_count);
    return datarow_range(it_lookback, it_end, data);
}


void
ModelService::check_feature_data(
        const DataRow::Container &data,
        const DataRow::Container::const_iterator &iter,
        const bpt::time_duration &max_gap,
        const bpt::ptime &feat_time,
        const ssize_t lag_count)
{
    if (iter == data.end() || iter->first - feat_time > max_gap || std::distance(data.begin(), iter) < lag_count ) // We don't have lag count data
        throw svr::common::insufficient_data(
                "Can't find data for prediction features. Need " + std::to_string(lag_count) + " values until " + bpt::to_simple_string(feat_time) +
                ", data available is from " + bpt::to_simple_string(data.begin()->first) + " until " + bpt::to_simple_string(data.rbegin()->first));
}


void
ModelService::check_feature_data(
        const DataRow::Container &data,
        const DataRow::Container::const_iterator &iter,
        const bpt::time_duration &max_gap,
        const bpt::ptime &feat_time)
{
    if (iter == data.end() || iter->first - feat_time > max_gap)
        throw svr::common::insufficient_data(
                "Can't find data for prediction features. Needed value for " + bpt::to_simple_string(feat_time) +
                ", nearest data available is " + (iter == data.end() ? "not found" : "at " + bpt::to_simple_string(iter->first)));
}


double
ModelService::predict(
        const Model_ptr &p_model,
        DataRowContainer &main_decon_data,
        std::vector<DataRowContainer_ptr> &aux_data_rows_containers,
        const boost::posix_time::ptime &prediction_time,
        const boost::posix_time::time_duration &resolution,
        const bpt::time_duration &max_gap)
{
    LOG4_DEBUG("Predicting time " << prediction_time);

    if (main_decon_data.size() < 1) throw svr::common::insufficient_data("Decon queue is empty.");
    if (!p_model->get_svr_model()) throw std::runtime_error("Model is not initialized!");

    const auto feat_time = prediction_time - resolution;
    const auto lag_count = p_model->get_svr_model()->get_svr_parameters().get_lag_count();
    datarow_range lookback_range = prepare_feat_range(main_decon_data, max_gap, feat_time, lag_count);
    std::vector<datarow_range> aux_lookback_ranges;
    for (const DataRowContainer_ptr &p_aux_data: aux_data_rows_containers)
        aux_lookback_ranges.push_back(prepare_feat_range(*p_aux_data, max_gap, feat_time, lag_count));
    Vector<double> prediction_vector;
    get_prediction_vector(
            lookback_range, p_model->get_learning_levels(), p_model->get_decon_level(), aux_lookback_ranges, max_gap, prediction_vector);

#ifdef WHITEBOX_TEST
    std::stringstream ss_row ;
    for (int i = 0; i < prediction_vector.GetLength(); ++i)
        ss_row << prediction_vector.GetValue(i) << " ";

    LOG4_DEBUG("Row values " << prediction_vector.GetLength());
    LOG4_DEBUG(ss_row.str());
#endif /* WHITEBOX_TEST */

    double ret_val;
    PROFILE_EXEC_TIME(
            ret_val = p_model->get_svr_model()->Predict(&prediction_vector),
            "Predict a single value"
    );

    LOG4_END();

    return ret_val;
}


Vector<double> *
ModelService::predict(
        Model_ptr &p_model,
        datarow_range &main_data_range,
        std::vector<datarow_range> &aux_data_ranges,
        const boost::posix_time::time_period &prediction_range,
        const boost::posix_time::time_duration &resolution,
        const bpt::time_duration &max_gap)
{
    LOG4_BEGIN();

    if (main_data_range.distance() < 1) throw svr::common::insufficient_data("Decon queue is empty.");
    if (!p_model->get_svr_model()) throw std::runtime_error("Model is not initialized!");

    size_t lag_count = p_model->get_svr_model()->get_svr_parameters().get_lag_count();
    { // Do initial sanity checks of input data
        const auto feat_time = prediction_range.begin() - resolution;
        (void) find_nearest_before(main_data_range.get_container(), feat_time, max_gap, lag_count);
        for (auto &aux_data_range: aux_data_ranges)
            (void) find_nearest_before(aux_data_range.get_container(), feat_time, max_gap, lag_count);
    }

    LOG4_DEBUG("Predicting range " << prediction_range);
    Matrix<double> prediction_matrix;
    for (bpt::ptime prediction_time = prediction_range.begin(); prediction_range.end() > prediction_time; prediction_time = prediction_time + resolution) {
        const bpt::ptime feat_time = prediction_time - resolution;
        auto it_lag_end = find_nearest_before(main_data_range.get_container(), feat_time, max_gap);
        auto it_lag_start = it_lag_end;
        std::advance(it_lag_start, -ssize_t(lag_count));

        // Check aux_lookback_data vector
        /* TODO Decide do we really need this check now because it is done in prepare aux features
        for (const DataRowContainer_ptr &p_aux_data: aux_data_ranges)
            if (p_aux_data->find(lookback_data.begin()->first) == p_aux_data->end() || p_aux_data->find(lookback_data.rbegin()->first) == p_aux_data->end())
                throw svr::common::insufficient_data(
                        "Can't find aux data for range " + bpt::to_simple_string(lookback_data.begin()->first) + " to " + bpt::to_simple_string(lookback_data.rbegin()->first));
        */

        auto p_prediction_vector = new Vector<double>();
        get_prediction_vector(
                    datarow_range(it_lag_start, it_lag_end, main_data_range.get_container()),
                    p_model->get_learning_levels(), p_model->get_decon_level(), aux_data_ranges, max_gap, *p_prediction_vector);
        prediction_matrix.AddRowRef(p_prediction_vector);

#ifdef WHITEBOX_TEST
        std::stringstream ss_row ;
        for (int i = 0; i < p_prediction_vector.GetLength(); ++i)
            ss_row << p_prediction_vector.GetValue(i) << " ";

        LOG4_DEBUG("Row values " << p_prediction_vector.GetLength());
        LOG4_DEBUG(ss_row.str());
#endif /* WHITEBOX_TEST */
    }

    Vector<double> *result_values;
    PROFILE_EXEC_TIME(
            result_values = p_model->get_svr_model()->Predict(&prediction_matrix),
            "Predict a vector of values"
    );

    LOG4_END();

    return result_values;
}


void
ModelService::predict(
        const std::vector<Model_ptr> &models,
        const boost::posix_time::time_period &range,
        const boost::posix_time::time_duration &resolution,
        const boost::posix_time::time_duration &max_gap,
        DataRow::Container &decon_data,
        std::vector<DataRowContainer_ptr> &aux_decon_data)
{
    for (bpt::ptime prediction_time = range.begin();
         prediction_time <= range.end();
         prediction_time = prediction_time + resolution)
    {
        std::vector<double> predicted_values(models.size(), std::numeric_limits<double>::max());
        try {
            for (size_t model_counter = 0; model_counter < models.size(); model_counter++)
                predicted_values[model_counter] = predict(
                        models[model_counter], decon_data, aux_decon_data, prediction_time, resolution, max_gap);
            LOG4_DEBUG("Predicted coefficients for time " << prediction_time << "  " << deep_to_string(predicted_values));
        } catch (const std::exception &ex) {
            LOG4_ERROR("Failed predicting coefficients for time " << prediction_time << ". " << ex.what() << ". Skipping.");
            continue;
        }
        decon_data[prediction_time] = std::make_shared<DataRow>(
                prediction_time, bpt::second_clock::local_time(), DEFAULT_VALUE_TICK_VOLUME, predicted_values);
    }
}


} // business
} // svr
