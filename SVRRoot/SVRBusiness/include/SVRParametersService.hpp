#pragma once

#include <memory>
#include <common/types.hpp>

namespace svr { namespace dao { class  SVRParametersDAO; } }

namespace svr { namespace datamodel { class  SVRParameters; } }
using SVRParameters_ptr = std::shared_ptr<svr::datamodel::SVRParameters>;

namespace svr {
namespace business {

class SVRParametersService {

    svr::dao::SVRParametersDAO & svr_parameters_dao;

public:
    SVRParametersService(svr::dao::SVRParametersDAO &svr_parameters_dao):
        svr_parameters_dao(svr_parameters_dao)
    {}

    SVRParameters_ptr get_svr_parameter_by_id(bigint id);
    bool exists(const SVRParameters_ptr &svr_parameters);
    bool exists(bigint svr_parameters_id);

    int save(const SVRParameters_ptr &svr_parameters);
    int remove(const SVRParameters_ptr &svr_parameters);
    int remove(bigint svr_parameters_id);
    int remove_by_dataset(bigint dataset_id);

    std::map<std::pair<std::string, std::string>, std::vector<SVRParameters_ptr> > get_all_by_dataset_id(bigint dataset_id);

    std::vector<double> to_vector(const SVRParameters_ptr& svr_parameters) const;
};

}
}

using SVRParametersService_ptr = std::shared_ptr<svr::business::SVRParametersService>;
