#pragma once

#include <memory>
#include <vector>
#include <model/DataRow.hpp>

namespace svr { namespace dao { class DeconQueueDAO; } }

namespace svr { namespace datamodel {
    class DeconQueue;
    class InputQueue;
    class Dataset;
} }
using DeconQueue_ptr = std::shared_ptr<svr::datamodel::DeconQueue>;
using InputQueue_ptr = std::shared_ptr<svr::datamodel::InputQueue>;
using Dataset_ptr = std::shared_ptr<svr::datamodel::Dataset>;

namespace svr { namespace business { class InputQueueService; } }

namespace svr {
namespace business {

class DeconQueueService {

    svr::dao::DeconQueueDAO & decon_queue_dao;
    svr::business::InputQueueService & input_queue_service;

public:

    DeconQueueService(svr::dao::DeconQueueDAO & deconQueueDao,
                      svr::business::InputQueueService & input_queue_service)
            :
            decon_queue_dao(deconQueueDao),
            input_queue_service(input_queue_service)
    {}

    DeconQueue_ptr get_by_table_name(const std::string &table_name);
    DeconQueue_ptr get_queue_metadata(const std::string &table_name);

    long load_latest_decon_data(const DeconQueue_ptr deconQueue, const bpt::ptime &timeTo, size_t limit);
    long load_decon_data(const DeconQueue_ptr decon_queue, const bpt::ptime &time_from = bpt::min_date_time, const bpt::ptime &time_to = bpt::max_date_time, size_t limit = 0);
    DeconQueue_ptr clone_range(const DeconQueue_ptr &p_decon_queue, const bpt::time_period &time_range);

    void save(const DeconQueue_ptr &);

    bool exists(const DeconQueue_ptr & p_decon_queue);
    bool exists(const std::string& decon_queue_table_name);

    int remove(const DeconQueue_ptr &p_decon_queue);
    int clear(const DeconQueue_ptr &);

    long count(const DeconQueue_ptr &decon_queue);

    std::vector<DeconQueue_ptr>
    deconstruct(
            const InputQueue_ptr &p_input_queue,
            const Dataset_ptr &p_dataset,
            const bool get_data_from_database = true);

    DeconQueue_ptr
    deconstruct(
            const InputQueue_ptr &p_input_queue,
            const Dataset_ptr &p_dataset,
            const std::string &column_name,
            const bool get_data_from_database = true);

    void
    deconstruct(
            const InputQueue_ptr &p_input_queue,
            const Dataset_ptr &p_dataset,
            const std::string &column_name,
            svr::datamodel::DataRow::Container &out_data,
            const bool get_data_from_database = true);

    void
    deconstruct(
            const datamodel::DataRow::Container &data,
            const std::string &wavelet_name,
            const size_t frame_size,
            datamodel::DataRow::Container &decon_data,
            const size_t input_column_index = 0);

    static DataRowContainer
    reconstruct(
            const svr::datamodel::datarow_range &decon_queue,
            const std::string &wavelet_name,
            const size_t frame_size);

    static std::vector<std::vector<double>> to_matrix(const DeconQueue_ptr& decon_queue);


private:
    static void
    copy_decon_frame_to_container(
            const svr::datamodel::DataRow::Container &data,
            svr::datamodel::DataRow::Container &data_container,
            const std::vector<bpt::ptime> &times,
            const std::vector<std::vector<double>> &decon_frame,
            size_t limit = std::numeric_limits<size_t>::max());

    static void
    copy_coefs_to_vector(
            const std::pair<bpt::ptime, std::shared_ptr<svr::datamodel::DataRow>> &row,
            const size_t frame_size,
            const size_t row_index,
            std::vector<bpt::ptime> &times,
            std::vector<double> &current_frame);

    static void
    copy_recon_frame_to_container(
            const svr::datamodel::DataRow::Container &decon_data,
            const std::vector<bpt::ptime> &times,
            svr::datamodel::DataRow::Container &recon_data,
            const std::vector<double> &reconstructed_values,
            size_t limit = std::numeric_limits<size_t>::max());
};

} /* namespace business */
} /* namespace svr */

using DeconQueueService_ptr = std::shared_ptr <svr::business::DeconQueueService>;
