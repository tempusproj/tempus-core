#pragma once

#include "model/DQScalingFactor.hpp"
#include "DAO/DQScalingFactorDAO.hpp"
#include "model/Dataset.hpp"
#include "model/DeconQueue.hpp"

namespace svr {
namespace business {

#define DEFAULT_ALPHA_TRUNC (0.01)

class DQScalingFactorService
{
private:
    svr::dao::DQScalingFactorDAO &dq_scaling_factor_dao;
    
public:
    DQScalingFactorService(svr::dao::DQScalingFactorDAO& dq_scaling_factor_dao) :
        dq_scaling_factor_dao(dq_scaling_factor_dao)
    {}
    
    bool exists(const DQScalingFactor_ptr& dq_scaling_factor);
    bool exists_by_dataset_id(const bigint dataset_id);

    int save(const DQScalingFactor_ptr& scaling_factor);
    int remove(const DQScalingFactor_ptr& scaling_factor);

    std::vector<DQScalingFactor_ptr> find_all_by_dataset_id(const bigint dataset_id);

    std::vector<DQScalingFactor_ptr> calculate(const svr::datamodel::DeconQueue_ptr &decon_queue, const size_t dataset_id, const double alpha = DEFAULT_ALPHA_TRUNC);
    void scale(const Dataset_ptr& p_dataset, const bool unscale = false, Ensemble_ptr p_ensemble = nullptr);
    void scale_decon_queue(const Dataset_ptr& p_dataset, svr::datamodel::DeconQueue_ptr& p_decon_queue, const bool unscale = false);

    static void scale_decon_queue(
            const svr::datamodel::DeconQueue_ptr& decon_queue,
            const std::vector<double>& scaling_factors,
            const bool unscale = false);

    static std::vector<DQScalingFactor_ptr> slice(
            const std::vector<DQScalingFactor_ptr> &scaling_factors,
            const size_t dataset_id,
            const std::string& input_queue_table_name,
            const std::string& input_queue_column_name);

    void calculate_dataset_scaling_factors(const Dataset_ptr &p_dataset);

    void calculate_dataset_scaling_factors(const Dataset_ptr &p_dataset, const svr::datamodel::DeconQueue_ptr &p_decon_queue);
};

}
}

using DQScalingTaskService_ptr = std::shared_ptr<svr::business::DQScalingFactorService>;
