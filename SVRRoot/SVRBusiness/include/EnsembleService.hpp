#pragma once

#include <memory>
#include <common/types.hpp>
#include "cascaded_svm.h"
#include "model/DataRow.hpp"
#include "model/InputQueue.hpp"


namespace svr { namespace dao { class EnsembleDAO; } }
namespace svr { namespace business {
    class ModelService;
    class SVRParametersService;
    class DeconQueueService;

} }
namespace svr { namespace datamodel {
    class Ensemble;
    class Dataset;
    class DeconQueue;
    class SVRParameters;
} }

using Ensemble_ptr = std::shared_ptr<svr::datamodel::Ensemble>;
using Dataset_ptr = std::shared_ptr<svr::datamodel::Dataset>;
using DeconQueue_ptr = std::shared_ptr<svr::datamodel::DeconQueue>;
using SVRParameters_ptr = std::shared_ptr<svr::datamodel::SVRParameters>;


#define DEFAULT_MAX_GAP (24) /* hours */

#define ALL_MODELS (std::numeric_limits<size_t>::max()) /* train or predict all models, instead of a specific level one. */

namespace svr {
namespace business {

class EnsembleService {

    svr::dao::EnsembleDAO &ensemble_dao;
    svr::business::ModelService &model_service;
    svr::business::DeconQueueService &decon_queue_service;

    /**
     * @brief load decon_queue and aux_decon_queues by table names
     * @param ensemble shared pointer
     */
    void load_svr_params_and_models(Ensemble_ptr p_ensemble);
    OnlineSVR_ptr prepare_data_and_train_model(Ensemble_ptr &p_ensemble, 
        const SVRParameters_ptr &p_svr_parameters, 
        std::vector<svr::cascaded::layer_data> &models_train_data,
        const bpt::time_duration &max_gap,
        size_t model_counter);

public:

    EnsembleService(svr::dao::EnsembleDAO & ensembleDao,
            svr::business::ModelService & model_service,
            svr::business::DeconQueueService & decon_queue_service)
            :
            ensemble_dao(ensembleDao),
            model_service(model_service),
            decon_queue_service(decon_queue_service)
    {}

    Ensemble_ptr get_ensemble_by_id(bigint ensemble_id);
    Ensemble_ptr get_ensemble_by_dataset_and_decon_queue(const Dataset_ptr &p_dataset, const DeconQueue_ptr &p_decon_queue);

    std::vector<Ensemble_ptr> get_all_by_dataset_id(bigint dataset_id);

    int save(const Ensemble_ptr &p_ensemble);
    bool save_ensembles(const std::vector<Ensemble_ptr> &ensembles, bool save_decon_queues = false);
    bool exists(const Ensemble_ptr &ensemble);
    bool exists(bigint ensemble_id);
    int remove_by_dataset_id(bigint dataset_id);
    int remove(const Ensemble_ptr & p_ensemble);

 	//create ensembles with decons and svrParameters from dataset
    std::vector<Ensemble_ptr> init_ensembles_from_dataset(const Dataset_ptr &p_dataset);

    std::vector<Ensemble_ptr> init_ensembles_from_dataset(
            const Dataset_ptr &p_dataset,
            const std::vector<DeconQueue_ptr> &decon_queues,
            const std::vector<std::vector<DeconQueue_ptr> > &aux_decon_queues);

    void train(
            Ensemble_ptr &p_ensemble,
            const std::vector<SVRParameters_ptr> &vec_svr_parameters,
            const bpt::time_duration &max_gap,
            const size_t model_numb = std::numeric_limits<size_t>::max());

    static void
    predict(Ensemble_ptr &p_ensemble,
            const boost::posix_time::time_period &range,
            const boost::posix_time::time_duration &resolution,
            const bpt::time_duration &max_gap = bpt::hours(DEFAULT_MAX_GAP));

    void
    init_decon_data(
            std::vector<Ensemble_ptr> &ensembles,
            const std::vector<DeconQueue_ptr> &decon_queues,
            const std::vector<std::vector<DeconQueue_ptr>> &aux_decon_queues);

    static bool is_ensemble_input_queue(const Ensemble_ptr &p_ensemble, const svr::datamodel::InputQueue_ptr &p_input_queue);
};
} /* namespace business */
} /* namespace svr */

using EnsembleService_ptr = std::shared_ptr<svr::business::EnsembleService>;
