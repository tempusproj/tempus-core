#pragma once

#include <memory>
#include <common/types.hpp>
#include "cascaded_svm.h"
#include "model/DataRow.hpp"

namespace svr { namespace dao { class ModelDAO; } }
namespace svr { namespace datamodel { class Model; } }
using Model_ptr = std::shared_ptr<svr::datamodel::Model>;
using Model_vector = std::vector<Model_ptr>;


namespace svr {
namespace business {


class ModelService {
private:

    svr::dao::ModelDAO & model_dao;

    static void
    prepare_adjacent_levels_row(
            const std::vector<size_t> &adjacent_levels,
            const DataRowContainer::const_iterator &start_iter,
            const DataRowContainer::const_iterator &end_iter,
            const bpt::time_duration &max_gap,
            Vector<double> &row);

    static void
    prepare_aux_data_row(
            const std::vector<size_t> &adjacent_levels,
            const std::vector<svr::datamodel::datarow_range> &aux_ranges,
            const DataRowContainer::const_iterator &start_lag_iterator,
            const DataRowContainer::const_iterator &end_lag_iterator,
            const bpt::time_duration &max_gap,
            Vector<double> &row);

    static void
    prepare_time_features(
            const bpt::ptime &value_time,
            Vector<double> &row);

public:
    ModelService(svr::dao::ModelDAO &model_dao);

    Model_ptr get_model_by_id(bigint model_id);

    Model_ptr get_model(bigint ensemble_id, size_t decon_level);

    int save(const Model_ptr &p_model);

    bool exists(const Model_ptr &p_model);

    int remove(const Model_ptr &p_model);

    int remove_by_ensemble_id(bigint ensemble_id);

    std::vector<Model_ptr> get_all_models_by_ensemble_id(bigint ensemble_id);

    cascaded::layer_data get_training_data(
            const svr::datamodel::datarow_range &main_data_range,
            const size_t lag,
            const std::vector<size_t> &adjustent_levels,
            const bpt::time_duration &max_gap,
            const size_t current_level,
            const std::vector<svr::datamodel::datarow_range> &aux_data_ranges = std::vector<svr::datamodel::datarow_range>(),
            const bpt::ptime &last_modeled_value_time = bpt::min_date_time);

    static void
    get_prediction_vector(
            const svr::datamodel::datarow_range &main_range,
            const std::vector<size_t> &adjacent_levels,
            const size_t current_level,
            const std::vector<svr::datamodel::datarow_range> &aux_ranges,
            const bpt::time_duration &max_gap,
            svr::datamodel::Vector<double> &row);

    static void
    train(
            const SVRParameters &p_svr_parameters,
            Model_ptr &svr_model,
            const cascaded::layer_data &data,
            const bpt::ptime &last_modeled_value_time);

    static double
    predict(
            const Model_ptr &p_model,
            DataRowContainer &p_main_decon_data,
            std::vector<DataRowContainer_ptr> &aux_data_rows_containers,
            const boost::posix_time::ptime &prediction_time,
            const boost::posix_time::time_duration &resolution,
            const bpt::time_duration &max_gap);

    // Use only when adjacent level values are known for the predicted time range (eg. validation in paramtune)
    static Vector<double> *
    predict(
            Model_ptr &p_model,
            svr::datamodel::datarow_range &p_main_decon_data,
            std::vector<svr::datamodel::datarow_range> &aux_data_rows_container,
            const boost::posix_time::time_period &prediction_range,
            const boost::posix_time::time_duration &resolution,
            const bpt::time_duration &max_gap);

    // Predict all levels at once
    static void
    predict(const std::vector<Model_ptr> &models,
            const boost::posix_time::time_period &range,
            const boost::posix_time::time_duration &resolution,
            const boost::posix_time::time_duration &max_gap,
            svr::datamodel::DataRow::Container &decon_data,
            std::vector<DataRowContainer_ptr> &aux_decon_data);

    // A bit more expensive but checks for lag count values before found time
    static void
    check_feature_data(
            const svr::datamodel::DataRow::Container &data,
            const svr::datamodel::DataRow::Container::const_iterator &iter,
            const bpt::time_duration &max_gap,
            const bpt::ptime &feat_time,
            const ssize_t lag_count);

    static void
    check_feature_data(
            const svr::datamodel::DataRow::Container &data,
            const svr::datamodel::DataRow::Container::const_iterator &iter,
            const bpt::time_duration &max_gap,
            const bpt::ptime &feat_time);

    static svr::datamodel::datarow_range
    prepare_feat_range(
            svr::datamodel::DataRow::Container &data,
            const boost::posix_time::time_duration &max_gap,
            const boost::posix_time::ptime &feat_time,
            const ssize_t lag_count);

    static void
    train_batch(const SVRParameters &svr_parameters, Model_ptr &p_model, const cascaded::layer_data &data);

    static void
    train_online(const cascaded::layer_data &data, OnlineSVR_ptr &p_svr_model);
};

} /* namespace business */
} /* namespace svr */

using ModelService_ptr = std::shared_ptr <svr::business::ModelService>;
