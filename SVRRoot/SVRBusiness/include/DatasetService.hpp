#pragma once

#include <common/types.hpp>
#include <model/Dataset.hpp>

namespace svr { namespace dao { class DatasetDAO; } }
namespace svr { namespace business {
class EnsembleService;
class SVRParametersService;
} }
namespace svr { namespace datamodel { class DeconQueue; } }

using Dataset_ptr = std::shared_ptr<svr::datamodel::Dataset>;
using DeconQueue_ptr = std::shared_ptr<svr::datamodel::DeconQueue>;

namespace svr {
namespace business {

class DatasetService {

    svr::dao::DatasetDAO & dataset_dao;
    svr::business::EnsembleService & ensemble_service;
    svr::business::SVRParametersService & svr_parameters_service;

    bool train_dataset_model(const Dataset_ptr &p_dataset,
            Model_ptr &p_model,
            const SVRParameters_ptr &p_svr_parameters,
            const boost::posix_time::time_period &training_range,
            const DeconQueue_ptr &p_decon_queue,
            const std::vector<DeconQueue_ptr> &aux_decon_queues);

    svr::datamodel::datarow_range find_iterators(
            const DeconQueue_ptr &p_decon_queue,
            const boost::posix_time::time_period &range,
            const SVRParameters &svr_parameters);

    bool prepare_range(
            const DeconQueue_ptr &p_decon_queue,
            const std::vector<DeconQueue_ptr> &aux_decon_queues,
            const bpt::time_period &range,
            const SVRParameters &svr_parameters,
            svr::datamodel::datarow_range &out_decon_data,
            std::vector<svr::datamodel::datarow_range> &out_aux_decon_data);

public:

    DatasetService(svr::dao::DatasetDAO &datasetDao, svr::business::EnsembleService & ensemble_service,
                    svr::business::SVRParametersService & svr_parameters_service) :
            dataset_dao(datasetDao),
            ensemble_service(ensemble_service),
            svr_parameters_service(svr_parameters_service)
    {}

    Dataset_ptr get_by_id(bigint datasetId);
    Dataset_ptr get_user_dataset(const std::string& user_name, const std::string& dataset_name);
    void load_ensembles(Dataset_ptr &p_dataset);
    bool check_ensembles_svr_parameters(const Dataset_ptr &p_dataset);

    bool save(const Dataset_ptr &);

    bool exists(const Dataset_ptr &);
    bool exists(int dataset_id);
    bool exists(const std::string& userName, const std::string& datasetName);

    int remove(const Dataset_ptr &);

    bool link_user_to_dataset(User_ptr const & user, Dataset_ptr const & dataset);
    bool unlink_user_from_dataset(User_ptr const & user, Dataset_ptr const & dataset);

    struct DatasetUsers
    {
        Dataset_ptr dataset;
        std::vector<User_ptr> users;
        DatasetUsers(Dataset_ptr const & dataset, std::vector<User_ptr> && users);
    };
    typedef std::vector<DatasetUsers> UserDatasetPairs;

    void update_active_datasets(UserDatasetPairs &processed_user_dataset_pairs);

    std::vector<DeconQueue_ptr>
    run_dataset_aux(
            const Dataset_ptr& dataset,
            const bpt::time_period & training_range,
            const bpt::time_period & prediction_range);

    std::shared_ptr<Vector<double>>
    run_dataset_column_model(
            const Dataset_ptr &p_dataset,
            const SVRParameters_ptr &p_svr_parameters,
            const boost::posix_time::time_period &training_range,
            const boost::posix_time::time_period &prediction_time_range,
            const size_t model_numb,
            const DeconQueue_ptr &p_decon_queue,
            const std::vector<DeconQueue_ptr> &aux_decon_queues);

    static void
    set_svr_parameters(
            const Dataset_ptr &p_dataset,
            const std::vector<double> &parameter_values,
            const size_t model_number,
            const std::vector<datamodel::Bounds> &bounds,
            const std::string &column_name);

    void
    prepare_ensembles(
            const Dataset_ptr &p_dataset,
            const boost::posix_time::time_period &range,
            std::vector<DeconQueue_ptr> decon_queues,
            std::vector<std::vector<DeconQueue_ptr>> aux_decon_queues,
            const bool trim = true,
            const bool init_ensembles = true);

    static void
    prepare_queue(
            const Dataset_ptr &p_dataset,
            const InputQueue_ptr &p_input_queue,
            const boost::posix_time::time_period &range,
            const bool trim,
            std::vector<DeconQueue_ptr> &decon_queues);
};

} /* namespace business */
} /* namespace svr */

using DatasetService_ptr = std::shared_ptr <svr::business::DatasetService>;
