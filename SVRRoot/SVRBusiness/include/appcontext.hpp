#pragma once

#include <string>
#include "business.hpp"

namespace svr { namespace dao { class ScopedTransaction; } }
using ScopedTransaction_ptr = std::shared_ptr<svr::dao::ScopedTransaction>;

namespace svr { namespace common { class PropertiesFileReader; } }

namespace svr{
namespace context{

class AppContext{
private:
    class AppContextImpl;
    AppContextImpl & p_impl;

public:
    static inline AppContext& get_instance() { return *p_instance; }
    static void init_instance(std::string configPath);
    static void destroy_instance();

    svr::common::PropertiesFileReader &app_properties;

    svr::business::UserService &user_service;
    svr::business::InputQueueService &input_queue_service;
    svr::business::SVRParametersService &svr_parameters_service;
    svr::business::ModelService &model_service;
    svr::business::DeconQueueService &decon_queue_service;
    svr::business::EnsembleService &ensemble_service;
    svr::business::DatasetService &dataset_service;
    svr::business::RequestService &request_service;
    svr::business::AuthenticationProvider &authentication_provider;
    svr::business::PredictionTaskService &prediction_task_service;
    svr::business::AutotuneTaskService &autotune_task_service;
    svr::business::DecrementTaskService &decrement_task_service;
    svr::business::IQScalingFactorService & iq_scaling_factor_service;
    svr::business::DQScalingFactorService & dq_scaling_factor_service;

    void flush_dao_buffers();

private:
    static AppContext *p_instance;
    AppContext(const std::string& config_path);
    ~AppContext();
    AppContext(AppContext&)=delete;
    AppContext(AppContext&&)=delete;
    void operator=(AppContext&)=delete;
    void operator=(AppContext&&)=delete;
};

struct AppContextDeleter
{
    ~AppContextDeleter();
};


}
}
