#pragma once

#include "model/IQScalingFactor.hpp"
#include "DAO/IQScalingFactorDAO.hpp"
#include "model/Dataset.hpp"
#include "model/InputQueue.hpp"

namespace svr {
namespace business {

class IQScalingFactorService
{
private:
    svr::dao::IQScalingFactorDAO & iq_scaling_factor_dao_;

public:
    IQScalingFactorService(svr::dao::IQScalingFactorDAO& iq_scaling_factor_dao) :
        iq_scaling_factor_dao_(iq_scaling_factor_dao)
    {}

    bool exists(const IQScalingFactor_ptr& iq_scaling_factor);

    int save(const IQScalingFactor_ptr& iq_scaling_factor);
    int remove(const IQScalingFactor_ptr& iq_scaling_factor);

    std::vector<IQScalingFactor_ptr> find_all_by_dataset_id(const bigint dataset_id);

    std::vector<IQScalingFactor_ptr> calculate(const svr::datamodel::InputQueue_ptr &p_input_queue, const size_t dataset_id, const double alpha = 0.02);
    void scale(const Dataset_ptr& p_dataset, const bool unscale = false);
};

}
}

using IQScalingTaskService_ptr = std::shared_ptr<svr::business::IQScalingFactorService>;
