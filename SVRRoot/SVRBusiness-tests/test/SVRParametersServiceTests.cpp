#include "include/DaoTestFixture.h"
#include <iostream>

#include <model/User.hpp>
#include <model/InputQueue.hpp>
#include <model/DeconQueue.hpp>
#include <model/DataRow.hpp>
#include <model/Dataset.hpp>
#include <model/Ensemble.hpp>
#include <model/SVRParameters.hpp>

using svr::datamodel::SVRParameters;

TEST_F(DaoTestFixture, SVRParametersWorkflow)
{

    User_ptr user1 = std::make_shared<svr::datamodel::User>(
            bigint(), "DeconQueueTestUser", "DeconQueueTestUser@email", "DeconQueueTestUser", "DeconQueueTestUser", svr::datamodel::User::ROLE::ADMIN, svr::datamodel::Priority::High) ;

    aci.user_service.save(user1);

    InputQueue_ptr iq = std::make_shared<svr::datamodel::InputQueue>(
            "tableName", "inputQueue", user1->get_name(), "description", bpt::seconds(60), bpt::seconds(5), bpt::hours(-8), std::vector<std::string>{"up", "down", "left", "right"} );
    aci.input_queue_service.save(iq);

    Dataset_ptr ds = std::make_shared<svr::datamodel::Dataset>(0, "DeconQueueTestDataset", user1->get_user_name(), iq, std::vector<InputQueue_ptr>()
            , svr::datamodel::Priority::Normal, "", 4, "sym7");
    ds->set_is_active(true);

    DeconQueue_ptr dq = std::make_shared<svr::datamodel::DeconQueue>("DeconQueuetableName", iq->get_table_name(), "up", ds->get_id());

    dq->get_data()[bpt::time_from_string("2015-01-01 10:00:00")] =
        DataRow_ptr { new svr::datamodel::DataRow(bpt::time_from_string("2015-01-01 10:00:00"), bpt::time_from_string("2015-01-01 10:00:00"), 0.1, std::vector<double>{1, 2, 3, 4, 5} ) };

//    std::vector<Ensemble_ptr> ensembles = aci.ensemble_service.init_ensembles_from_dataset(ds, std::vector<DeconQueue_ptr>{dq}, std::vector<std::vector<DeconQueue_ptr>>());
//    ds->set_ensembles(ensembles);


    ASSERT_FALSE(aci.dataset_service.exists(ds));

    aci.dataset_service.save(ds);

    ASSERT_TRUE(aci.dataset_service.exists(ds));

    SVRParameters_ptr svrParams { new SVRParameters () };
    svrParams->set_dataset_id(ds->get_id());
    svrParams->set_input_queue_table_name("q_svrwave_eurusd_60");
    svrParams->set_decon_level(3);

    ASSERT_FALSE (aci.svr_parameters_service.exists(svrParams));

    ASSERT_EQ(1, aci.svr_parameters_service.save(svrParams));

    ASSERT_TRUE (aci.svr_parameters_service.exists(svrParams));

    ASSERT_EQ(1, aci.svr_parameters_service.remove(svrParams));

    ASSERT_FALSE (aci.svr_parameters_service.exists(svrParams));

    aci.dataset_service.remove(ds);
    aci.input_queue_service.remove(iq);
    aci.user_service.remove(user1);
}
