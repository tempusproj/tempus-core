#include "include/DaoTestFixture.h"
#include <iostream>
#include <model/User.hpp>
#include <model/InputQueue.hpp>
#include <model/DeconQueue.hpp>
#include <model/Dataset.hpp>
#include "include/InputQueueRowDataGenerator.hpp"

using svr::datamodel::Priority;
using svr::datamodel::kernel_type;
using svr::common::INPUT_QUEUE_TABLE_NAME_PREFIX;
using svr::common::make_table_identifier;

class EnsembleIntegrationTests : public DaoTestFixture {
protected:

    ScopedTransaction_ptr trx;

    User_ptr testUser;
    InputQueue_ptr testQueue;
    Dataset_ptr testDataset;
    std::vector<DeconQueue_ptr> deconQueues;
    std::vector<Ensemble_ptr> ensembles;
    std::map<std::pair<std::string, std::string>, std::vector<SVRParameters_ptr>> ensembles_svr_parameters;

    // test configuration
    long testDataNumberToGenerate = 1000;
    int testFrameSize = 256;

    // user details
    std::string userName = "test_user";
    std::string userRealName = "test user";
    std::string userEmail = "svruser@google.com";
    std::string newEmail = "svruser@sf.net";
    std::string userPassword = "svr123456";

    // queue details
    std::string queueName = "simple queue";
    bpt::time_duration resolution = bpt::seconds(60);
    bpt::time_duration legalTimeDeviation = bpt::seconds(5);
    bpt::hours timezoneOffset = bpt::hours(1);
    std::vector<std::string> valueColumns {"high", "low", "open", "close"};
    std::string expectedTableName = make_table_identifier(INPUT_QUEUE_TABLE_NAME_PREFIX + "_"
                                                     + userName + "_" + queueName + "_"
                                                     + std::to_string(resolution.total_seconds()));

    // dataset details
    std::string datasetName = "test dataset";
    Priority priority = Priority::Normal;
    size_t swtLevels = 1;
    std::string swtWaveletName = "db3";
    bpt::time_duration max_lookback_time_gap = bpt::hours(24);
    bool is_active = true;

    //vec_svr_parameters details

    SVRParameters svr_parameters = SVRParameters(0, 0, expectedTableName, "", 0,
                                                 2.05521, 0.16373, 1.4742, 1.40931, 1000, 0.357722, kernel_type::RBF, 10);

    void InitSvrParameters() {
        //init vec_svr_parameters
         for (std::string column : valueColumns)
         {
             std::vector<SVRParameters_ptr> vec_svr_parameters;
             svr_parameters.set_input_queue_column_name(column);
             for (size_t swt_level = 0; swt_level <= swtLevels; swt_level++)
             {
                 svr_parameters.set_decon_level(swt_level);
                 vec_svr_parameters.push_back(std::make_shared<SVRParameters>(svr_parameters));
             }
             auto key = std::make_pair(testQueue->get_table_name(), column);
             ensembles_svr_parameters[key] = vec_svr_parameters;
         }
    }

    void InitUser(){
        // init user
        testUser = std::make_shared<svr::datamodel::User>(0, userName, userEmail, userPassword, userRealName,
                                                          svr::datamodel::User::ROLE::USER);
    }

    void InitDataset(){
        testDataset = std::make_shared<svr::datamodel::Dataset>(0, datasetName, userName, testQueue, std::vector<InputQueue_ptr>(),
                                                   priority, "description", swtLevels, swtWaveletName,
                                                   max_lookback_time_gap, std::vector<Ensemble_ptr>(), is_active);
        testDataset->set_ensembles_svr_parameters(ensembles_svr_parameters);
    }

    void InitInputQueueData(){
        // initialize InputQueue object
        testQueue = std::make_shared<svr::datamodel::InputQueue>("", queueName, userName, "description", resolution,legalTimeDeviation,
                timezoneOffset, valueColumns);

        // init data generator
        InputQueueRowDataGenerator dataGenerator(aci.input_queue_service, testQueue,
                                                 valueColumns.size(), testDataNumberToGenerate);

        // generate some random data
        PROFILE_EXEC_TIME( while ( !dataGenerator.isDone() ) {
            DataRow_ptr row = dataGenerator();
            aci.input_queue_service.add_row(testQueue, row);
        }, "Generating " << testDataNumberToGenerate << " InputQueue rows");
        // should save all the data
    }

    void InitDeconQueueData(){
        bpt::ptime startTime = testQueue->get_data().begin()->first;
        bpt::ptime endTime = testQueue->get_data().rbegin()->first;

        ASSERT_FALSE(startTime.is_special());
        ASSERT_FALSE(endTime.is_special());

        LOG4_TRACE("Deconstructing test queue");
        deconQueues = aci.decon_queue_service.deconstruct(testQueue, testDataset);
        for (DeconQueue_ptr p_decon : deconQueues) {
            LOG4_DEBUG("Deconstructed Data Queue: " << p_decon->metadata_to_string());
        }
    }

    void InitEnsemble(){
        ensembles = aci.ensemble_service.init_ensembles_from_dataset(
                                                                            testDataset, deconQueues,
                                                                            std::vector<std::vector<DeconQueue_ptr>>());
        testDataset->set_ensembles(ensembles);

        ASSERT_EQ(ensembles.size(), deconQueues.size());
    }

    void saveDbData()
    {
        LOG4_TRACE("Saving test user");
        ASSERT_TRUE(aci.user_service.save(testUser) == 1);

        LOG4_TRACE("Saving test queue");
        ASSERT_TRUE(aci.input_queue_service.save(testQueue) > 0);

        LOG4_INFO(testQueue->get_table_name() << " is having data: "
                 << bpt::to_simple_string(testQueue->get_data().begin()->first) << " - "
                 << bpt::to_simple_string(testQueue->get_data().rbegin()->first));

        LOG4_TRACE("Saving test dataset");
        ASSERT_TRUE(aci.dataset_service.save(testDataset) == 1);

    }

    void removeDbData()
    {
        aci.dataset_service.remove(testDataset);
        aci.input_queue_service.remove(testQueue);
        aci.user_service.remove(testUser);
    }

    virtual void SetUp() override
    {
        InitUser();
        InitInputQueueData();
        InitSvrParameters();
        InitDataset();
        InitDeconQueueData();
        InitEnsemble();
        saveDbData();
    }

    virtual void TearDown() override {
//        removeDbData();
        trx = nullptr;
    }

};

TEST_F(EnsembleIntegrationTests, testEnsembleCRUD)
{
    ASSERT_EQ(testDataset->get_ensembles().size(), size_t(aci.ensemble_service.remove_by_dataset_id(testDataset->get_id())));
    for (size_t i = 0; i < ensembles.size(); ++i) {
        Ensemble_ptr p_ensemble = ensembles.at(i);
        PROFILE_EXEC_TIME(
                    aci.ensemble_service.train(
                        p_ensemble,
                        testDataset->get_ensembles_svr_parameters()[p_ensemble->get_key_pair()],
                    testDataset->get_max_lookback_time_gap()),
                "Retraining all models");
    }

    for (Ensemble_ptr p_ensemble : ensembles) {
        // for each decon level should be there a model
        ASSERT_EQ(p_ensemble->get_models().size(), testDataset->get_swt_levels() + 1);
    }

    PROFILE_EXEC_TIME(aci.ensemble_service.save_ensembles(ensembles, true), "Persisting ensemble models");

    for (Ensemble_ptr p_ensemble : ensembles)
    {
        ASSERT_NE(0UL, p_ensemble->get_id());
    }
}
