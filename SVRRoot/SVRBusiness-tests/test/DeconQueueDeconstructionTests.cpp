#include "include/DaoTestFixture.h"
#include <iostream>
#include <model/User.hpp>
#include <model/InputQueue.hpp>
#include <model/DeconQueue.hpp>
#include <model/Dataset.hpp>


namespace
{
    std::string new_datarow_container = "new_datarow_container";
}


std::vector<svr::datamodel::DataRow::Container> load_datarow_containers(std::string file_path);
void compare_datarow_containers(svr::datamodel::DataRow::Container const & lhs, svr::datamodel::DataRow::Container const & rhs);
std::vector<size_t> get_adjacent_indexes(size_t level, double ratio, size_t level_count);


TEST_F(DaoTestFixture, BasicDeconstructionTest)
{
    bpt::time_period times{bpt::time_from_string("2015-03-20 22:00:00"), bpt::time_from_string("2015-03-23 09:59:00")};
    std::string const test_iq_name = "q_svrwave_eurusd_60";

    InputQueue_ptr test_iq
        = aci.input_queue_service.get_queue_metadata(test_iq_name);

    const DataRowContainer test_iq_data
        = aci.input_queue_service.get_queue_data( test_iq_name
                                                , times.begin()
                                                , times.end());

    test_iq->set_data(test_iq_data);

    Dataset_ptr dataset_100
        = aci.dataset_service.get_user_dataset("svrwave", "eurusd");

    ////////////////////////////////////////////////////////////////////////////
    //
    // Test deconstruction
    //

    std::vector<DeconQueue_ptr> decon_queues = aci.decon_queue_service.deconstruct(test_iq, dataset_100);

//    std::ofstream decon_output("decon_output.txt");
//
//    for(DeconQueue_ptr dq : decon_queues)
//    {
//        decon_output << dq->data_to_string(-1) << "\n";
//        decon_output << new_datarow_container << "\n";
//    }
//
//    decon_output.close();

    std::string etalon_decon_output_path = exec("find ../../ -name etalon_decon_output.txt");
    erase_after(etalon_decon_output_path, '\n');

    std::vector<svr::datamodel::DataRow::Container> etalon_decon_containers = load_datarow_containers(etalon_decon_output_path);

    ASSERT_EQ(etalon_decon_containers.size(), decon_queues.size());

    auto ri = decon_queues.begin();

    for(auto ei = etalon_decon_containers.begin(); ri != decon_queues.end(); ++ei, ++ri)
        compare_datarow_containers(*ei, (*ri)->get_data());

    ////////////////////////////////////////////////////////////////////////////
    //
    // Test training data
    //

    size_t column_index = 0;
    size_t lookback_rows = 5;
    size_t model_number = 2;
    double adjacent_levels_ratio = 0.5;
    size_t swt_levels = 4;
    bpt::time_duration max_lookback_time_gap = bpt::hours(23);

    const std::vector<size_t> adjacent_levels{get_adjacent_indexes( model_number,
                                                                    adjacent_levels_ratio,
                                                                    swt_levels + 1)};

    /* TODO add auxilliary decon queue tests */
    auto training_data = aci.model_service.get_training_data(
            svr::datamodel::datarow_range(decon_queues[column_index]->get_data().begin(), decon_queues[column_index]->get_data().end(), decon_queues[column_index]->get_data()),
            lookback_rows,
            adjacent_levels,
            max_lookback_time_gap,
            model_number);

    std::shared_ptr<Matrix<double>> real_training_matrix = std::make_shared<Matrix<double>>(*training_data.training_matrix);
    std::shared_ptr<Vector<double>> real_response_vector = std::make_shared<Vector<double>>(*training_data.reference_vector);

//    training_matrix->Save("training_matrix_output.txt");
//    response_vector->Save("response_vector_output.txt");


    std::string etalon_training_matrix_output_path = exec("find ../../ -name etalon_training_matrix_output.txt");
    erase_after(etalon_training_matrix_output_path, '\n');
    std::shared_ptr<Matrix<double>> etalon_training_matrix = std::shared_ptr<Matrix<double>>(Matrix<double>::Load(etalon_training_matrix_output_path.c_str()));

    ASSERT_EQ(*real_training_matrix, *etalon_training_matrix);



    std::string etalon_response_vector_output_path = exec("find ../../ -name etalon_response_vector_output.txt");
    erase_after(etalon_response_vector_output_path, '\n');
    std::shared_ptr<Vector<double>> etalon_response_vector = std::shared_ptr<Vector<double>>(Vector<double>::Load(etalon_response_vector_output_path.c_str()));

    ASSERT_EQ(*real_response_vector, *etalon_response_vector);
}

void compare_datarow_containers(svr::datamodel::DataRow::Container const & lhs, svr::datamodel::DataRow::Container const & rhs)
{
    ASSERT_EQ(lhs.size(), rhs.size());

    static double const epsilon = std::numeric_limits<double>::epsilon();

    for(auto li = lhs.begin(), ri = rhs.begin(); li != lhs.end(); ++li, ++ri)
    {
        ASSERT_EQ(li->second->get_value_time(), ri->second->get_value_time());
        ASSERT_GT(epsilon, fabs(li->second->get_tick_volume() - ri->second->get_tick_volume() ) );
        ASSERT_EQ(li->second->get_values().size(), ri->second->get_values().size());

        for(auto ldi = li->second->get_values().begin(), rdi = ri->second->get_values().begin(); ldi != li->second->get_values().end(); ++ldi, ++rdi)
            ASSERT_GT(epsilon, fabs(*ldi - *rdi ) );
    }
}


std::vector<svr::datamodel::DataRow::Container> load_datarow_containers(std::string file_path)
{
    std::ifstream ifile(file_path.c_str());
    if(ifile.bad())
        throw std::runtime_error("load_datarow_containers: Error while openinig the file");

    std::vector<svr::datamodel::DataRow::Container> result;
    svr::datamodel::DataRow::Container current_container;

    size_t valuation_counter = -1;
    static size_t const valuate_every = 100;

    while(!ifile.eof())
    {
        try
        {
            ++valuation_counter;

            std::string line;
            std::getline(ifile, line);
            if(line.empty())
                continue;

            if(line == new_datarow_container)
            {
                result.push_back(current_container);
                current_container.clear();
                continue;
            }

            std::istringstream istr(line);
            std::string str_tmp1, str_tmp2;
            double dbl_tmp;

            //Reading value_time

            istr >> str_tmp1;
            if( ( valuation_counter % valuate_every ) == 0 && str_tmp1 != "ValueTime:")
                throw std::runtime_error("load_datarow_containers: Wrong file format");

            istr >> str_tmp1 >> str_tmp2;

            if(!str_tmp2.empty())
                str_tmp2.resize(str_tmp2.size()-1);

            DataRow_ptr row = DataRow_ptr(new svr::datamodel::DataRow);
            row->set_value_time(bpt::time_from_string(str_tmp1 + " " + str_tmp2));

            //Skipping update_time

            istr >> str_tmp1;
            if( ( valuation_counter % valuate_every ) == 0 && str_tmp1 != "UpdateTime:")
                throw std::runtime_error("load_datarow_containers: Wrong file format");

            istr >> str_tmp1 >> str_tmp2;

            //Reading data

            istr >> dbl_tmp;
            row->set_tick_volume(dbl_tmp);

            istr >> str_tmp1;
            if( ( valuation_counter % valuate_every ) == 0 && str_tmp1 != ",")
                throw std::runtime_error("load_datarow_containers: Wrong file format");

            std::vector<double> values;

            while(!istr.eof())
            {
                istr >> dbl_tmp;
                values.push_back(dbl_tmp);

                istr >> str_tmp1;
                if( ( valuation_counter % valuate_every ) == 0 && str_tmp1 != ",")
                    throw std::runtime_error("load_datarow_containers: Wrong file format");
            }
            row->set_values(values);

            current_container[row->get_value_time()] = row;
        }
        catch (...)
        {
            std::cerr << "load_datarow_containers: exception thrown while processing line #" << valuation_counter << " of file " << file_path << "\n";
            std::rethrow_exception(std::current_exception());
        }
    }
    return result;

}

std::vector<size_t> get_adjacent_indexes(size_t level, double ratio, size_t level_count){

    if (level_count == 0)
        return std::vector<size_t>{level};

    std::vector<size_t> level_indexes;

    size_t sum_count = static_cast<size_t>(level_count * ratio);
    size_t half_count = static_cast<size_t>(sum_count / 2.);
    int min_index = int (std::max(int(level) - int(half_count), 1));
    int max_index = int (std::min(int(level) + int(half_count), int(level_count) - 1));
    for(; min_index <= max_index; ++min_index)
        level_indexes.push_back(size_t(min_index));
    return level_indexes;

}
