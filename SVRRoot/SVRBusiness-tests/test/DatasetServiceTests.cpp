#include "include/DaoTestFixture.h"
#include <iostream>
#include <model/User.hpp>
#include <model/InputQueue.hpp>
#include <model/DeconQueue.hpp>
#include <model/Dataset.hpp>
#include "include/InputQueueRowDataGenerator.hpp"
#include <util/TimeUtils.hpp>

namespace
{
    long const testDataNumberToGenerate = 5000;
}

TEST_F(DaoTestFixture, DatasetWorkflow)
{
    User_ptr user1 = std::make_shared<svr::datamodel::User>(
            bigint(), "DeconQueueTestUser", "DeconQueueTestUser@email", "DeconQueueTestUser", "DeconQueueTestUser", svr::datamodel::User::ROLE::ADMIN, svr::datamodel::Priority::High) ;

    aci.user_service.save(user1);

    InputQueue_ptr iq = std::make_shared<svr::datamodel::InputQueue>(
            "tableName", "logicalName", user1->get_name(), "description", bpt::seconds(60), bpt::seconds(5), bpt::hours(-8), std::vector<std::string>{"up", "down", "left", "right"} );
    aci.input_queue_service.save(iq);

    Dataset_ptr ds = std::make_shared<svr::datamodel::Dataset>(0, "DeconQueueTestDataset", user1->get_user_name(), iq, std::vector<InputQueue_ptr>()
            , svr::datamodel::Priority::Normal, "", 4, "sym7");
    ds->set_is_active(true);

    ds->set_max_lookback_time_gap(svr::common::date_time_string_to_seconds ("38,21:22:23"));

    aci.dataset_service.save(ds);

    svr::business::DatasetService::UserDatasetPairs dsu;
    aci.dataset_service.update_active_datasets(dsu);
    ASSERT_EQ(2UL, dsu.size());

    Dataset_ptr &p_dataset = dsu[0].dataset;
    p_dataset->set_input_queue(
                aci.input_queue_service.get_queue_metadata(
                    p_dataset->get_input_queue()->get_table_name()));
    aci.dataset_service.load_ensembles(p_dataset);

    ASSERT_EQ(p_dataset->get_max_lookback_time_gap(), bpt::hours(38*24 + 21) + bpt::minutes(22) + bpt::seconds(23) );

    aci.dataset_service.remove(ds);
    aci.input_queue_service.remove(iq);
    aci.user_service.remove(user1);
}

TEST_F(DaoTestFixture, SelectingActiveDatasets)
{
    User_ptr user1Low = std::make_shared<svr::datamodel::User>(
            bigint(), "User2016-07-20-Low", "User2016-07-20-Low@dkdk.dld", "User2016-07-20-Low", "User2016-07-20-Low", svr::datamodel::User::ROLE::ADMIN, svr::datamodel::Priority::Low) ;

    aci.user_service.save(user1Low);

    InputQueue_ptr iq1 = std::make_shared<svr::datamodel::InputQueue>(
            "InputQueue1", "InputQueue1", user1Low->get_name(), "InputQueue1", bpt::seconds(60), bpt::seconds(5), bpt::hours(-8), std::vector<std::string>{"up", "down", "left", "right"} );
    aci.input_queue_service.save(iq1);

    Dataset_ptr ds1 = std::make_shared<svr::datamodel::Dataset>(0, "Dataset2016-07-20-Low", user1Low->get_user_name(), iq1, std::vector<InputQueue_ptr>()
            , svr::datamodel::Priority::Low, "", 4, "sym7");
    ds1->set_is_active(true);

    aci.dataset_service.save(ds1);

    ////////////////////////////////////////////////////////////////////////////

    svr::business::DatasetService::UserDatasetPairs pairs;
    aci.dataset_service.update_active_datasets(pairs);

    ASSERT_EQ(2UL, pairs.size());
    auto iter = pairs.begin();
    ASSERT_EQ(iter->dataset->get_dataset_name(), "eurusd"); ASSERT_EQ(1UL, iter->users.size()); ASSERT_EQ(iter->users[0]->get_user_name(), "svrwave");
    ++iter;
    ASSERT_EQ(iter->dataset->get_dataset_name(), "Dataset2016-07-20-Low"); ASSERT_EQ(1UL, iter->users.size()); ASSERT_EQ(iter->users[0]->get_user_name(), "User2016-07-20-Low");

    ////////////////////////////////////////////////////////////////////////////

    User_ptr user2Normal = std::make_shared<svr::datamodel::User>(
            bigint(), "User2016-07-20-Normal", "User2016-07-20-Normal@dkdk.dld", "User2016-07-20-Normal", "User2016-07-20-Normal"
            , svr::datamodel::User::ROLE::ADMIN, svr::datamodel::Priority::Normal) ;

    aci.user_service.save(user2Normal);

    InputQueue_ptr iq2 = std::make_shared<svr::datamodel::InputQueue>(
            "InputQueue2", "InputQueue2", user2Normal->get_name(), "InputQueue2", bpt::seconds(60), bpt::seconds(5), bpt::hours(-8), std::vector<std::string>{"up", "down", "left", "right"} );
    aci.input_queue_service.save(iq2);

    Dataset_ptr ds2 = std::make_shared<svr::datamodel::Dataset>(0, "Dataset2016-07-20-Below", user2Normal->get_user_name(), iq1, std::vector<InputQueue_ptr>()
            , svr::datamodel::Priority::BelowNormal, "", 4, "sym7");
    ds2->set_is_active(true);

    aci.dataset_service.save(ds2);

    ////////////////////////////////////////////////////////////////////////////

    aci.dataset_service.update_active_datasets(pairs);

    ASSERT_EQ(3UL, pairs.size());

    iter = pairs.begin();
    ASSERT_EQ(iter->dataset->get_dataset_name(), "Dataset2016-07-20-Below"); ASSERT_EQ(1UL, iter->users.size());ASSERT_EQ(iter->users[0]->get_user_name(), "User2016-07-20-Normal");
    ++iter;
    ASSERT_EQ(iter->dataset->get_dataset_name(), "eurusd"); ASSERT_EQ(1UL, iter->users.size());ASSERT_EQ(iter->users[0]->get_user_name(), "svrwave");
    ++iter;
    ASSERT_EQ(iter->dataset->get_dataset_name(), "Dataset2016-07-20-Low"); ASSERT_EQ(1UL, iter->users.size());ASSERT_EQ(iter->users[0]->get_user_name(), "User2016-07-20-Low");

    ////////////////////////////////////////////////////////////////////////////

    ASSERT_FALSE( aci.dataset_service.unlink_user_from_dataset( user2Normal, ds1 ) );
    ASSERT_TRUE ( aci.dataset_service.link_user_to_dataset( user2Normal, ds1 ) );

    aci.dataset_service.update_active_datasets(pairs);

    ASSERT_EQ(3UL, pairs.size());

    iter = pairs.begin();
    ASSERT_EQ(iter->dataset->get_dataset_name(), "Dataset2016-07-20-Below"); ASSERT_EQ(1UL, iter->users.size());ASSERT_EQ(iter->users[0]->get_user_name(), "User2016-07-20-Normal");
    ++iter;
    ASSERT_EQ(iter->dataset->get_dataset_name(), "Dataset2016-07-20-Low"); ASSERT_EQ(2UL, iter->users.size()); ASSERT_EQ(iter->users[0]->get_user_name(), "User2016-07-20-Normal"); ASSERT_EQ(iter->users[1]->get_user_name(), "User2016-07-20-Low");
    ++iter;
    ASSERT_EQ(iter->dataset->get_dataset_name(), "eurusd"); ASSERT_EQ(1UL, iter->users.size());ASSERT_EQ(iter->users[0]->get_user_name(), "svrwave");

    ////////////////////////////////////////////////////////////////////////////

    Dataset_ptr ds3 = std::make_shared<svr::datamodel::Dataset>(0, "Dataset2016-07-20-High-3", user2Normal->get_user_name(), iq1, std::vector<InputQueue_ptr>()
            , svr::datamodel::Priority::High, "", 4, "sym7");
    ds3->set_is_active(true);

    aci.dataset_service.save(ds3);

    aci.dataset_service.update_active_datasets(pairs);

    ASSERT_EQ(4UL, pairs.size());

    iter = pairs.begin();
    ASSERT_EQ(iter->dataset->get_dataset_name(), "Dataset2016-07-20-High-3"); ASSERT_EQ(1UL, iter->users.size()); ASSERT_EQ(iter->users[0]->get_user_name(), "User2016-07-20-Normal");
    ++iter;
    ASSERT_EQ(iter->dataset->get_dataset_name(), "Dataset2016-07-20-Below"); ASSERT_EQ(1UL, iter->users.size());ASSERT_EQ(iter->users[0]->get_user_name(), "User2016-07-20-Normal");
    ++iter;
    ASSERT_EQ(iter->dataset->get_dataset_name(), "Dataset2016-07-20-Low"); ASSERT_EQ(2UL, iter->users.size()); ASSERT_EQ(iter->users[0]->get_user_name(), "User2016-07-20-Normal"); ASSERT_EQ(iter->users[1]->get_user_name(), "User2016-07-20-Low");
    ++iter;
    ASSERT_EQ(iter->dataset->get_dataset_name(), "eurusd"); ASSERT_EQ(1UL, iter->users.size());ASSERT_EQ(iter->users[0]->get_user_name(), "svrwave");

    ////////////////////////////////////////////////////////////////////////////

    ASSERT_TRUE( aci.dataset_service.unlink_user_from_dataset( user2Normal, ds1 ) );
    ASSERT_FALSE( aci.dataset_service.unlink_user_from_dataset( user2Normal, ds1 ) );

    aci.dataset_service.update_active_datasets(pairs);

    ASSERT_EQ(4UL, pairs.size());

    iter = pairs.begin();
    ASSERT_EQ(iter->dataset->get_dataset_name(), "Dataset2016-07-20-High-3"); ASSERT_EQ(1UL, iter->users.size()); ASSERT_EQ(iter->users[0]->get_user_name(), "User2016-07-20-Normal");
    ++iter;
    ASSERT_EQ(iter->dataset->get_dataset_name(), "Dataset2016-07-20-Below"); ASSERT_EQ(1UL, iter->users.size());ASSERT_EQ(iter->users[0]->get_user_name(), "User2016-07-20-Normal");
    ++iter;
    ASSERT_EQ(iter->dataset->get_dataset_name(), "eurusd"); ASSERT_EQ(1UL, iter->users.size());ASSERT_EQ(iter->users[0]->get_user_name(), "svrwave");
    ++iter;
    ASSERT_EQ(iter->dataset->get_dataset_name(), "Dataset2016-07-20-Low"); ASSERT_EQ(1UL, iter->users.size()); ASSERT_EQ(iter->users[0]->get_user_name(), "User2016-07-20-Low");

    ////////////////////////////////////////////////////////////////////////////

    User_ptr user3High = std::make_shared<svr::datamodel::User>(
        bigint(), "User2016-07-20-High-3", "User2016-07-20-High-3@dkdk.dld", "User2016-07-20-High-3", "User2016-07-20-High-3", svr::datamodel::User::ROLE::ADMIN, svr::datamodel::Priority::High);

    aci.user_service.save(user3High);

    ASSERT_TRUE( aci.dataset_service.link_user_to_dataset( user3High, ds1 ) );
    ASSERT_TRUE( aci.dataset_service.link_user_to_dataset( user3High, ds2 ) );
    ASSERT_TRUE( aci.dataset_service.link_user_to_dataset( user3High, ds3 ) );

    aci.dataset_service.update_active_datasets(pairs);

    ASSERT_EQ(4UL, pairs.size());

    iter = pairs.begin();
    ASSERT_EQ(iter->dataset->get_dataset_name(), "Dataset2016-07-20-High-3"); ASSERT_EQ(2UL, iter->users.size()); ASSERT_EQ(iter->users[0]->get_user_name(), "User2016-07-20-High-3"); ASSERT_EQ(iter->users[1]->get_user_name(), "User2016-07-20-Normal");
    ++iter;
    ASSERT_EQ(iter->dataset->get_dataset_name(), "Dataset2016-07-20-Below"); ASSERT_EQ(2UL, iter->users.size()); ASSERT_EQ(iter->users[0]->get_user_name(), "User2016-07-20-High-3"); ASSERT_EQ(iter->users[1]->get_user_name(), "User2016-07-20-Normal");
    ++iter;
    ASSERT_EQ(iter->dataset->get_dataset_name(), "Dataset2016-07-20-Low"); ASSERT_EQ(2UL, iter->users.size()); ASSERT_EQ(iter->users[0]->get_user_name(), "User2016-07-20-High-3"); ASSERT_EQ(iter->users[1]->get_user_name(), "User2016-07-20-Low");
    ++iter;
    ASSERT_EQ(iter->dataset->get_dataset_name(), "eurusd"); ASSERT_EQ(1UL, iter->users.size()); ASSERT_EQ(iter->users[0]->get_user_name(), "svrwave");

    ////////////////////////////////////////////////////////////////////////////

    aci.user_service.remove(user3High);
    aci.dataset_service.remove(ds3);

    aci.dataset_service.remove(ds2);
    aci.input_queue_service.remove(iq2);
    aci.user_service.remove(user2Normal);


    aci.dataset_service.remove(ds1);
    aci.input_queue_service.remove(iq1);
    aci.user_service.remove(user1Low);
}
