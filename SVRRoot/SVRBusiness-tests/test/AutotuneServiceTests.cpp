#include "include/DaoTestFixture.h"
#include <iostream>
#include <vector>
#include <model/SVRParameters.hpp>
#include <model/Model.hpp>
#include <model/User.hpp>
#include <model/Dataset.hpp>
#include <model/AutotuneTask.hpp>

using svr::datamodel::AutotuneTask;

TEST_F(DaoTestFixture, AutotuneTaskWorkflow)
{
    User_ptr user1 = std::make_shared<svr::datamodel::User>(
            bigint(), "DeconQueueTestUser", "DeconQueueTestUser@email", "DeconQueueTestUser", "DeconQueueTestUser", svr::datamodel::User::ROLE::ADMIN, svr::datamodel::Priority::High) ;

    aci.user_service.save(user1);

    InputQueue_ptr iq = std::make_shared<svr::datamodel::InputQueue>(
            "tableName", "logicalName", user1->get_name(), "description", bpt::seconds(60), bpt::seconds(5), bpt::hours(-8), std::vector<std::string>{"up", "down", "left", "right"} );
    aci.input_queue_service.save(iq);

    Dataset_ptr ds = std::make_shared<svr::datamodel::Dataset>(0, "DeconQueueTestDataset", user1->get_user_name(), iq, std::vector<InputQueue_ptr>()
            , svr::datamodel::Priority::Normal, "", 4, "sym7");
    ds->set_is_active(true);
    aci.dataset_service.save(ds);

    bigint                  result_dataset_id   = 101;
    bpt::ptime              creation_time       = bpt::second_clock::local_time();
    bpt::ptime              done_time           = creation_time + bpt::hours(2);
    std::map<std::string, std::string> parameters = {{"swt_levels", "1,4"}, {"swt_wavelet_name","bior3.1..bior3.5"},
                                                    {"lookback_time", "00:15:00"},  {"svr_c_0", "1.0"}};
    bpt::ptime              start_train_time    = bpt::time_from_string("2015-05-20 10:45");
    bpt::ptime              end_train_time      = bpt::time_from_string("2015-05-20 10:47");
    bpt::ptime              start_validation_time = bpt::time_from_string("2015-05-20 10:48");
    bpt::ptime              end_validation_time = bpt::time_from_string("2015-05-20 10:50");
    int                     status              = 0;         // 0 - new, 1 - in process, 2 - done, 3 - error
    double                  mse                 = 0.123;

    size_t vp_sliding_direction = 0, vp_slide_count = 0;
    bpt::seconds vp_slide_period_sec = bpt::seconds(0);
    size_t pso_best_points_counter = 0, pso_iteration_number  = 0, pso_particles_number = 0, pso_topology = 0, nm_max_iteration_number = 0;
    double nm_tolerance  = 0;

    AutotuneTask_ptr test_autotune_task = std::make_shared<AutotuneTask>(0, ds->get_id(), result_dataset_id,
                                                creation_time, done_time, parameters,
                                                start_train_time, end_train_time,
                                                start_validation_time, end_validation_time,
                                                vp_sliding_direction, vp_slide_count, vp_slide_period_sec,
                                                pso_best_points_counter, pso_iteration_number,
                                                pso_particles_number, pso_topology,
                                                nm_max_iteration_number, nm_tolerance,
                                                status, mse);

    ASSERT_FALSE(aci.autotune_task_service.exists(test_autotune_task));

    ASSERT_EQ(0UL, aci.autotune_task_service.find_all_by_dataset_id(test_autotune_task->get_dataset_id()).size());

    ASSERT_EQ(nullptr, aci.autotune_task_service.get_by_id(test_autotune_task->get_id()).get());

    ASSERT_EQ(0, aci.autotune_task_service.remove(test_autotune_task));

    ASSERT_EQ(1, aci.autotune_task_service.save(test_autotune_task));

    ASSERT_NE(bigint(0), test_autotune_task->get_id());

    ASSERT_TRUE(aci.autotune_task_service.exists(test_autotune_task));

    auto userAutotuneTasks = aci.autotune_task_service.find_all_by_dataset_id(ds->get_id());

    ASSERT_EQ(1UL, userAutotuneTasks.size());

    ASSERT_EQ(parameters, userAutotuneTasks.front()->get_parameters());

    ASSERT_EQ(*test_autotune_task, *userAutotuneTasks.front());
    ASSERT_EQ(*test_autotune_task, *aci.autotune_task_service.get_by_id(test_autotune_task->get_id()));

    ASSERT_EQ(1, aci.autotune_task_service.remove(test_autotune_task));

    ASSERT_FALSE(aci.autotune_task_service.exists(test_autotune_task));

    ASSERT_EQ(0UL, aci.autotune_task_service.find_all_by_dataset_id(test_autotune_task->get_id()).size());

    ASSERT_EQ(nullptr, aci.autotune_task_service.get_by_id(test_autotune_task->get_id()).get());

    ASSERT_EQ(0, aci.autotune_task_service.remove(test_autotune_task));

    aci.autotune_task_service.remove(test_autotune_task);
    aci.dataset_service.remove(ds);
    aci.input_queue_service.remove(iq);
    aci.user_service.remove(user1);
}
