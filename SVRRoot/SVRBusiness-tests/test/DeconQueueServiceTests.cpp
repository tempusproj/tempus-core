#include "include/DaoTestFixture.h"
#include <iostream>
#include <model/User.hpp>
#include <model/InputQueue.hpp>
#include <model/DeconQueue.hpp>
#include <model/Dataset.hpp>
#include "include/InputQueueRowDataGenerator.hpp"

namespace
{
    long const testDataNumberToGenerate = 5000;
}

TEST_F(DaoTestFixture, DeconQueueWorkflow)
{
    User_ptr user1 = std::make_shared<svr::datamodel::User>(
            bigint(), "DeconQueueTestUser", "DeconQueueTestUser@email", "DeconQueueTestUser", "DeconQueueTestUser", svr::datamodel::User::ROLE::ADMIN, svr::datamodel::Priority::High) ;

    aci.user_service.save(user1);

    InputQueue_ptr iq = std::make_shared<svr::datamodel::InputQueue>(
            "tableName", "logicalName", user1->get_name(), "description", bpt::seconds(60), bpt::seconds(5), bpt::hours(-8), std::vector<std::string>{"up", "down", "left", "right"} );
    aci.input_queue_service.save(iq);

    Dataset_ptr ds = std::make_shared<svr::datamodel::Dataset>(0, "DeconQueueTestDataset", user1->get_user_name(), iq, std::vector<InputQueue_ptr>()
            , svr::datamodel::Priority::Normal, "", 4, "sym7");
    ds->set_is_active(true);
    aci.dataset_service.save(ds);

    DeconQueue_ptr dq = std::make_shared<svr::datamodel::DeconQueue>("DeconQueuetableName", iq->get_table_name(), "up", ds->get_id());

    // The decon queue is saved with saving the dataset
    //aci.decon_queue_service.save(dq);

    aci.dataset_service.remove(ds);

    aci.decon_queue_service.remove(dq);

    aci.input_queue_service.remove(iq);
    aci.user_service.remove(user1);
}

// Commented out as this test functionality was replaced by SVRDaemon-whitebox-test
//TEST_F(DaoTestFixture, testDeconstructionOfFrames )
//{
//    User_ptr user1 = std::make_shared<svr::datamodel::User>(
//            bigint(), "DeconQueueTestUser", "DeconQueueTestUser@email", "DeconQueueTestUser", "DeconQueueTestUser", svr::datamodel::User::ROLE::ADMIN, svr::datamodel::Priority::High) ;
//
//    aci.user_service.save(user1);
//
//    InputQueue_ptr iq = std::make_shared<svr::datamodel::InputQueue>(
//            "tableName", "logicalNaDeconme", user1->get_name(), "description", bpt::seconds(60), bpt::seconds(5), bpt::hours(-8), std::vector<std::string>{"up", "down", "left", "right"} );
//    aci.input_queue_service.save(iq);
//
//    Dataset_ptr ds = std::make_shared<svr::datamodel::Dataset>(0, "DeconQueueTestDataset", user1->get_user_name(), iq, std::vector<InputQueue_ptr>()
//            , svr::datamodel::Priority::Normal, "", 4, "sym7");
//    ds->set_is_active(true);
//    aci.dataset_service.save(ds);
//
//
//    InputQueueRowDataGenerator dataGenerator(aci.input_queue_service, iq, iq->get_column_names().size(), testDataNumberToGenerate);
//
//    // generate some random data
//    PROFILE_EXEC_TIME( while ( !dataGenerator.isDone() ) {
//        DataRow_ptr row = dataGenerator();
//        aci.input_queue_service.add_row(iq, row);
//    }, "Generating " << testDataNumberToGenerate << " InputQueue rows");
//
//    // should save all the data
//    PROFILE_EXEC_TIME(ASSERT_EQ(testDataNumberToGenerate, aci.input_queue_service.save(iq)), "Saving InputQueue");
//
//    bpt::ptime startTime = aci.input_queue_service.find_oldest_record(iq)->get_value_time();
//    bpt::ptime endTime = aci.input_queue_service.find_newest_record(iq)->get_value_time();
//
//    ASSERT_FALSE(startTime.is_special());
//    ASSERT_FALSE(endTime.is_special());
//
////    FramesContainer_ptr columnFrames;
//
////    PROFILE_EXEC_TIME(columnFrames = aci.input_queue_service.get_column_in_frames(iq, valueColumns.at(0),
////                                                                             testFrameSize, startTime, endTime, false),
////                      "Getting column into frames");
//
////    LOG4_DEBUG("Got " << columnFrames->get_frames().size() << " each of " << testFrameSize << " rows");
//
////    size_t expectedNoOfFrames = (size_t) testDataNumberToGenerate / testFrameSize;
////    ASSERT_EQ(expectedNoOfFrames, columnFrames->get_frames().size());
//
////    ASSERT_EQ(expectedNoOfFrames * testFrameSize, columnFrames->count_rows());
//
//    DeconQueue_ptr  deconQueue;
//
//    PROFILE_EXEC_TIME(deconQueue = aci.decon_queue_service.deconstruct(iq, ds).at(0),
//                      "Deconstruction of single column containing " + std::to_string(iq->get_data().size()));
//
//    LOG4_DEBUG("Deconstructed Data Queue: " << deconQueue->to_string());
//
////    ASSERT_EQ(deconQueue->get_data().size(), iq->get_data().size()) << "InputQueue size and deconstracted data size aren't equal";
////    size_t decon_levels = ds->get_swt_levels() + 1;
////    for (auto &it : deconQueue->get_data())
////    {
////        ASSERT_EQ(it.second->get_values().size(), decon_levels) << "Decon queue data at "
////                                                                << bpt::to_simple_string(it.first)
////                                                                << " have wrong deconstacted levels";
////    }
//
//    aci.decon_queue_service.save(deconQueue);
//
//    DeconQueue_ptr queue2 = aci.decon_queue_service.get_by_table_name(deconQueue->get_table_name());
//
//    aci.decon_queue_service.load_decon_data(queue2);
//
//    ASSERT_EQ(*deconQueue, *queue2);
//
//    aci.dataset_service.remove(ds);
//
//    aci.decon_queue_service.remove(deconQueue);
//
//    aci.input_queue_service.remove(iq);
//    aci.user_service.remove(user1);
//}

TEST_F(DaoTestFixture, TestSaveDQIntegrity)
{
    User_ptr user1 = std::make_shared<svr::datamodel::User>(
            bigint(), "JamesBond", "JamesBond@email", "JamesBond", "JamesBond", svr::datamodel::User::ROLE::ADMIN, svr::datamodel::Priority::High) ;

    aci.user_service.save(user1);

    InputQueue_ptr iq = std::make_shared<svr::datamodel::InputQueue>(
            "SomeInputQueue", "SomeInputQueue", user1->get_name(), "SomeInputQueue", bpt::seconds(60), bpt::seconds(5), bpt::hours(-8), std::vector<std::string>{"up", "down", "left", "right"} );
    aci.input_queue_service.save(iq);

    Dataset_ptr ds = std::make_shared<svr::datamodel::Dataset>(0, "SomeTestDataset", user1->get_user_name(), iq, std::vector<InputQueue_ptr>()
            , svr::datamodel::Priority::Normal, "", 4, "sym7");
    ds->set_is_active(true);
    aci.dataset_service.save(ds);

    DeconQueue_ptr dq = std::make_shared<svr::datamodel::DeconQueue>("SomeDeconQueuetableName", iq->get_table_name(), "up", ds->get_id());

    bpt::ptime nw = bpt::second_clock::local_time();

    DataRow_ptr row = DataRow_ptr(new svr::datamodel::DataRow(nw));
    row->set_values({0, 1, 2});
    dq->get_data().insert(std::make_pair( nw, row));

    aci.decon_queue_service.save(dq);
    aci.decon_queue_service.save(dq);

    DataRow_ptr row1 = DataRow_ptr(new svr::datamodel::DataRow(nw + bpt::seconds(1)));
    row1->set_values({0, 1, 2});
    dq->get_data().insert(std::make_pair(nw + bpt::seconds(1), row1 ));

    DeconQueue_ptr dq1 = std::make_shared<svr::datamodel::DeconQueue>("SomeDeconQueuetableName", iq->get_table_name(), "up", ds->get_id());

    aci.decon_queue_service.load_decon_data(dq1, nw - bpt::hours(1), nw + bpt::hours(1), 1000);

    ASSERT_EQ(1UL, dq1->get_data().size());

    aci.dataset_service.remove(ds);

    aci.decon_queue_service.remove(dq);

    aci.input_queue_service.remove(iq);
    aci.user_service.remove(user1);
}

TEST_F(DaoTestFixture, TestDQUpdates)
{
    User_ptr user1 = std::make_shared<svr::datamodel::User>(
            bigint(), "WarrenBuffett", "WarrenBuffett@email", "WarrenBuffett", "WarrenBuffett", svr::datamodel::User::ROLE::ADMIN, svr::datamodel::Priority::High) ;

    aci.user_service.save(user1);

    InputQueue_ptr iq = std::make_shared<svr::datamodel::InputQueue>(
            "GatesFoundationIQ", "GatesFoundationIQ", user1->get_name(), "GatesFoundationIQ", bpt::seconds(60), bpt::seconds(5), bpt::hours(-8), std::vector<std::string>{"up", "down", "left", "right"} );
    aci.input_queue_service.save(iq);

    Dataset_ptr ds = std::make_shared<svr::datamodel::Dataset>(0, "GatesFoundationDS", user1->get_user_name(), iq, std::vector<InputQueue_ptr>()
            , svr::datamodel::Priority::Normal, "", 4, "sym7");
    ds->set_is_active(true);
    aci.dataset_service.save(ds);

    DeconQueue_ptr dq = std::make_shared<svr::datamodel::DeconQueue>("GatesFoundationDQ", iq->get_table_name(), "up", ds->get_id());

    bpt::ptime nw = bpt::second_clock::local_time();

    DataRow_ptr row = DataRow_ptr(new svr::datamodel::DataRow(nw));
    row->set_values({0, 1, 2});
    dq->get_data().insert(std::make_pair( nw, row));

    aci.decon_queue_service.save(dq);

    ///////////// No decon queue table recreation
    dq->get_data().clear();

    DataRow_ptr row1 = DataRow_ptr(new svr::datamodel::DataRow(nw + bpt::seconds(60) ));
    row1->set_values({0, 1, 2});
    dq->get_data().insert(std::make_pair( nw + bpt::seconds(60), row1));
    aci.decon_queue_service.save(dq);

    DeconQueue_ptr dq_test1 = aci.decon_queue_service.get_by_table_name(dq->get_table_name());
    aci.decon_queue_service.load_decon_data(dq_test1, nw, nw + bpt::seconds(61), 10);

    ASSERT_EQ(2UL, dq_test1->get_data().size());
    ASSERT_EQ(3UL, dq_test1->get_data().begin()->second->get_values().size() );

    ///////////// Table should be recreated;

    dq->get_data().clear();

    DataRow_ptr row2 = DataRow_ptr(new svr::datamodel::DataRow(nw + bpt::seconds(2*60) ));
    row2->set_values({0, 1, 2, 3});
    dq->get_data().insert(std::make_pair( nw + bpt::seconds(2*60), row2));

    aci.decon_queue_service.save(dq);

    DeconQueue_ptr dq_test2 = aci.decon_queue_service.get_by_table_name(dq->get_table_name());
    aci.decon_queue_service.load_decon_data(dq_test2, nw, nw + bpt::seconds(2 * 60 + 1), 10);

    ASSERT_EQ(1UL, dq_test2->get_data().size());
    ASSERT_EQ(4UL, dq_test2->get_data().begin()->second->get_values().size() );


    aci.dataset_service.remove(ds);

    aci.decon_queue_service.remove(dq);

    aci.input_queue_service.remove(iq);
    aci.user_service.remove(user1);

}
