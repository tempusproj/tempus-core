#pragma once

#include <cppcms/json.h>

#include <view/MainView.hpp>
#include "model/Dataset.hpp"

namespace content {

struct DatasetForm : cppcms::form{

    cppcms::widgets::regex_field               name;
    cppcms::widgets::text               description;
    cppcms::widgets::text               lookback_time;
    cppcms::widgets::select             priority;
    cppcms::widgets::numeric<double>    svr_C;
    cppcms::widgets::numeric<double>    svr_epsilon;
    cppcms::widgets::numeric<double>    svr_kernel_param;
    cppcms::widgets::numeric<double>    svr_kernel_param2;
    cppcms::widgets::numeric<size_t>    svr_decremental_distance;
    cppcms::widgets::numeric<double>    svr_adjacent_levels_ratio;
    cppcms::widgets::text               svr_kernel_type;
    cppcms::widgets::numeric<size_t>    swt_levels;
    cppcms::widgets::select             swt_wavelet;
    cppcms::widgets::submit             submit;

    DatasetForm();

    virtual bool validate() override;

private:

    std::vector<std::pair<std::string, std::string>> get_wavelet_filters();
    bool validate_lookback_time();
    const std::string time_duration_pattern = "[-]h[h][:mm][:ss][.fff]";

};

struct Dataset : public Main {
    std::string dataset_name;
    std::string user_name;
    std::string priority;
    std::string description;
    std::string swt_levels;
    std::string swt_wavelet;
    std::string lookback_time;
    std::string svr_c;
    std::string svr_epsilon;
    std::string svr_kernel_param;
    std::string svr_kernel_param2;
    std::string svr_decremental_distance;
    std::string svr_adjacent_levels_ratio;
};

struct DatasetWithForm : public Main{

    Dataset_ptr object;
    DatasetForm form;

    void load_form_data();
};
}


namespace cppcms {
namespace json {

// We specilize cppcms::json::traits structure to convert
// objects to and from json values

template<>
struct traits<Dataset_ptr> {
    static Dataset_ptr get(value const &v)
    {
        if(v.type()!=is_object) {
            throw bad_value_cast();
        }
        Dataset_ptr dataset;

        dataset->set_dataset_id(v.get<bigint>("dataset_id"));
        dataset->set_dataset_name(v.get<std::string>("dataset_name"));
        dataset->set_user_name(v.get<std::string>("user_name"));
        dataset->set_priority(svr::datamodel::get_priority_from_string(v.get<std::string>("priority")));
        dataset->set_description(v.get<std::string>("description"));
        dataset->set_swt_levels(v.get<size_t>("swt_levels"));
        dataset->set_swt_wavelet_name(v.get<std::string>("swt_wavelet_name"));

        return dataset;
    }
    static void set(value &v,Dataset_ptr const &in) {

        v.set("dataset_id", in->get_id());
        v.set("dataset_name", in->get_dataset_name());
        v.set("user_name", in->get_user_name());
        v.set("priority", svr::datamodel::to_string(in->get_priority()));
        v.set("description", in->get_description());
        v.set("swt_levels", in->get_swt_levels());
        v.set("swt_wavelet_name", in->get_swt_wavelet_name());
    }
};
} // json
} // cppcms
