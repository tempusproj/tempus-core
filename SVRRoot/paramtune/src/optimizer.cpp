#include "optimizer.h"
#include <paramtune.h>
#include "pso.h"

namespace svr {
namespace paramtune {

std::pair<double, std::vector<double>>
nm(const loss_callback_t &f, const std::vector<double>& initial_values, const NM_parameters& nm_parameters)
{
    const size_t n {initial_values.size()}; // number of parameters, i.e. vector length
    std::vector<double> xmin(n);
    double ynewlo;
    size_t ifault {0};
    const std::vector<double> step(n, 1.0);
    const size_t check_for_convergence {4};
    double ccoeff = 0.5;
    double del;
    double dn;
    double dnn;
    double ecoeff {2.0};
    double eps {0.001};
    size_t i;
    int ihi;
    int ilo;
    size_t j;
    int jcount;
    int ell;
    size_t nn;
    std::vector<double> p(n * (n+1));
    std::vector<double> p2star(n);
    std::vector<double> pbar(n);
    std::vector<double> pstar(n);
    double rcoeff {1.0};
    double rq;
    double x;
    std::vector<double> y(n + 1);
    double y2star;
    double ylo;
    double ystar;
    double z;

    size_t icount {0};
    size_t numres {0};
    std::vector<double> start = initial_values;
    jcount = check_for_convergence;
    dn = ( double ) ( n );
    nn = n + 1;
    dnn = ( double ) ( nn );
    del = 1.0;
    rq = nm_parameters.tolerance_ * dn;
    //
    //  Initial or restarted loop.
    //
    LOG4_INFO("NM: initialization");

    while(true)
    {
        for ( i = 0; i < n; i++ )
        {
            p[i+n*n] = start[i];
        }
        y[n] = f( start );
        ++icount;

        for ( j = 0; j < n; j++ )
        {
            x = start[j];
            start[j] = start[j] + step[j] * del;
            for ( i = 0; i < n; i++ )
            {
                p[i+j*n] = start[i];
            }
            y[j] = f( start );
            ++icount;
            start[j] = x;
        }
        //
        //  The simplex construction is complete.
        //
        //  Find highest and lowest Y values.  YNEWLO = Y(IHI) indicates
        //  the vertex of the simplex to be replaced.
        //
        ylo = y[0];
        ilo = 0;

        for ( i = 1; i < nn; i++ )
        {
            if ( y[i] < ylo )
            {
                ylo = y[i];
                ilo = i;
            }
        }
        //
        //  Inner loop.
        //
        while(true)
        {
            if ( nm_parameters.max_iteration_number_ <= icount )
            {
                break;
            }
            ynewlo = y[0];
            ihi = 0;

            for ( i = 1; i < nn; i++ )
            {
                if ( ynewlo < y[i] )
                {
                    ynewlo = y[i];
                    ihi = i;
                }
            }
            //
            //  Calculate PBAR, the centroid of the simplex vertices
            //  excepting the vertex with Y value YNEWLO.
            //
            for ( i = 0; i < n; i++ )
            {
                z = 0.0;
                for ( j = 0; j < nn; j++ )
                {
                    z = z + p[i+j*n];
                }
                z = z - p[i+ihi*n];
                pbar[i] = z / dn;
            }
            //
            //  Reflection through the centroid.
            //
            for ( i = 0; i < n; i++ )
            {
                pstar[i] = pbar[i] + rcoeff * ( pbar[i] - p[i+ihi*n] );
            }
            ystar = f( pstar );
            ++icount;
            //
            //  Successful reflection, so extension.
            //
            if ( ystar < ylo )
            {
                for ( i = 0; i < n; i++ )
                {
                    p2star[i] = pbar[i] + ecoeff * ( pstar[i] - pbar[i] );
                }
                y2star = f( p2star );
                ++icount;
                //
                //  Check extension.
                //
                if ( ystar < y2star )
                {
                    for ( i = 0; i < n; i++ )
                    {
                        p[i+ihi*n] = pstar[i];
                    }
                    y[ihi] = ystar;
                }
                //
                //  Retain extension or contraction.
                //
                else
                {
                    for ( i = 0; i < n; i++ )
                    {
                        p[i+ihi*n] = p2star[i];
                    }
                    y[ihi] = y2star;
                }
            }
            //
            //  No extension.
            //
            else
            {
                ell = 0;
                for ( i = 0; i < nn; i++ )
                {
                    if ( ystar < y[i] )
                    {
                        ell = ell + 1;
                    }
                }

                if ( 1 < ell )
                {
                    for ( i = 0; i < n; i++ )
                    {
                        p[i+ihi*n] = pstar[i];
                    }
                    y[ihi] = ystar;
                }
                //
                //  Contraction on the Y(IHI) side of the centroid.
                //
                else if ( ell == 0 )
                {
                    for ( i = 0; i < n; i++ )
                    {
                        p2star[i] = pbar[i] + ccoeff * ( p[i+ihi*n] - pbar[i] );
                    }
                    y2star = f( p2star );
                    ++icount;
                    //
                    //  Contract the whole simplex.
                    //
                    if ( y[ihi] < y2star )
                    {
                        for ( j = 0; j < nn; j++ )
                        {
                            for ( i = 0; i < n; i++ )
                            {
                                p[i+j*n] = ( p[i+j*n] + p[i+ilo*n] ) * 0.5;
                                xmin[i] = p[i+j*n];
                            }
                            y[j] = f( xmin );
                            ++icount;
                        }
                        ylo = y[0];
                        ilo = 0;

                        for ( i = 1; i < nn; i++ )
                        {
                            if ( y[i] < ylo )
                            {
                                ylo = y[i];
                                ilo = i;
                            }
                        }
                        continue;
                    }
                    //
                    //  Retain contraction.
                    //
                    else
                    {
                        for ( i = 0; i < n; i++ )
                        {
                            p[i+ihi*n] = p2star[i];
                        }
                        y[ihi] = y2star;
                    }
                }
                //
                //  Contraction on the reflection side of the centroid.
                //
                else if ( ell == 1 )
                {
                    for ( i = 0; i < n; i++ )
                    {
                        p2star[i] = pbar[i] + ccoeff * ( pstar[i] - pbar[i] );
                    }
                    y2star = f( p2star );
                    ++icount;
                    //
                    //  Retain reflection?
                    //
                    if ( y2star <= ystar )
                    {
                        for ( i = 0; i < n; i++ )
                        {
                            p[i+ihi*n] = p2star[i];
                        }
                        y[ihi] = y2star;
                    }
                    else
                    {
                        for ( i = 0; i < n; i++ )
                        {
                            p[i+ihi*n] = pstar[i];
                        }
                        y[ihi] = ystar;
                    }
                }
            }
            //
            //  Check if YLO improved.
            //
            if ( y[ihi] < ylo )
            {
                ylo = y[ihi];
                ilo = ihi;
            }
            --jcount;

            if ( 0 < jcount )
            {
                continue;
            }
            //
            //  Check to see if minimum reached.
            //
            if ( icount <= nm_parameters.max_iteration_number_ )
            {
                jcount = check_for_convergence;

                z = 0.0;
                for ( i = 0; i < nn; i++ )
                {
                    z = z + y[i];
                }
                x = z / dnn;

                z = 0.0;
                for ( i = 0; i < nn; i++ )
                {
                    z = z + pow ( y[i] - x, 2 );
                }

                if ( z <= rq )
                {
                    break;
                }
            }
        }
        //
        //  Factorial tests to check that YNEWLO is a local minimum.
        //
        for ( i = 0; i < n; i++ )
        {
            xmin[i] = p[i+ilo*n];
        }
        ynewlo = y[ilo];

        if ( nm_parameters.max_iteration_number_ < icount )
        {
            ifault = 2;
            break;
        }

        ifault = 0;

        for ( i = 0; i < n; i++ )
        {
            del = step[i] * eps;
            xmin[i] = xmin[i] + del;
            z = f( xmin );
            ++icount;
            if ( z < ynewlo )
            {
                ifault = 2;
                break;
            }
            xmin[i] = xmin[i] - del - del;
            z = f( xmin );
            ++icount;
            if ( z < ynewlo )
            {
                ifault = 2;
                break;
            }
            xmin[i] = xmin[i] + del;
        }

        if ( ifault == 0 )
        {
            break;
        }
        //
        //  Restart the procedure.
        //
        for ( i = 0; i < n; i++ )
        {
            start[i] = xmin[i];
        }
        del = eps;
        ++numres;
    }

    LOG4_INFO("NM: number of iterations: " << icount);

    return std::make_pair(ynewlo,xmin);
}

std::vector<std::vector<double>>
pso(const loss_callback_t &f, const PSO_parameters& pso_parameters, const Bounds &bounds)
{
    // set the default settings
    pso_settings_t settings;

    pso_set_default_settings(&settings);
    settings.x_lo = bounds.to_vector(bound_type::min);
    settings.x_hi = bounds.to_vector(bound_type::max);

    // set PSO settings manually
    settings.size = pso_parameters.particles_number_;
    settings.steps = pso_parameters.iteration_number_;
    settings.nhood_strategy = static_cast<size_t>(pso_parameters.pso_topology_);

    // run optimization algorithm
    return pso_solve(f, &settings);
}


}
}
