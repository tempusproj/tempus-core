#include "main-config.hpp"
#include "appcontext.hpp"

#include "CLI.h"

using namespace svr::datamodel;
using namespace svr::dao;
using namespace svr::business;
using namespace svr::context;
using namespace svr::common;
using namespace bpt;
using namespace std;

int main(int argc, char** argv)
{
    static CLI cli;
    if (cli.parse(argc, argv))
        return cli.run_dataset();
}
