#include "paramtune.h"
#include "main-config.hpp"
#include "appcontext.hpp"

using namespace svr::common;
using namespace svr::datamodel;
using namespace svr::business;
using namespace svr::context;

namespace svr {
namespace paramtune {


static size_t total_loss_calls;

double loss(
        const std::vector<double> &parameters,
        Dataset_ptr &p_dataset,
        validation_parameters_t &validation_parameters,
        const score_metric_e score_metric,
        const DeconQueue_ptr &p_decon_queue,
        const std::vector<DeconQueue_ptr> &aux_decon_queues)
{
    ResourceMeasure resource_measure;

    resource_measure.set_start_time();
    static size_t call_number;
    ++call_number;
    if (total_loss_calls) {
        static const bpt::ptime time_first_call(bpt::second_clock::local_time());
        LOG4_INFO("Estimated time left " << (((time_first_call - bpt::second_clock::local_time()) / call_number) * (total_loss_calls - call_number)));
    }

    LOG4_INFO("Calculating loss count " << call_number << " on column " << validation_parameters.column_name_ <<
              " level " << validation_parameters.model_number_);

    AppContext::get_instance().dataset_service.set_svr_parameters(
                p_dataset,
                parameters,
                validation_parameters.model_number_,
                validation_parameters.bounds_,
                validation_parameters.column_name_);

    SVRParameters_ptr p_svr_parameters = p_dataset->get_ensembles_svr_parameters()[
            std::make_pair(p_dataset->get_input_queue()->get_table_name(), validation_parameters.column_name_)][validation_parameters.model_number_];

    double result = 0;
    size_t score_calls_ct = 0;
    auto training_window = validation_parameters.training_range_;
    auto validation_window = validation_parameters.validation_range_;
    for (size_t ix_pos = 0; ix_pos < validation_parameters.validation_slide_count_; ++ix_pos) {
        // TODO Parallelize slide calls
        std::shared_ptr<Vector<double>> p_predicted_data;
        Vector<double> validation_values;
        try {
            PROFILE_EXEC_TIME(
                    p_predicted_data = AppContext::get_instance().dataset_service.run_dataset_column_model(
                            p_dataset,
                            p_svr_parameters,
                            training_window,
                            validation_window,
                            validation_parameters.model_number_,
                            p_decon_queue,
                            aux_decon_queues),
                    "Run dataset on slide " << ix_pos << " training period " << training_window << " validation period "
                                            << validation_window);
        } catch (const svr::common::insufficient_data &ex) {
            LOG4_WARN("Failed processing range " << training_window << " " << validation_window << ". " << ex.what());
            goto __slide;
        }

        if (!p_predicted_data) {
            LOG4_WARN("Building model for slide " << ix_pos << " training period " << training_window << " failed. Skipping validation.");
            goto __slide;
        }
        p_decon_queue->get_column_values(validation_parameters.model_number_, validation_values, validation_window);
        result += score(validation_values, *p_predicted_data, score_metric, false);
        ++score_calls_ct;

        __slide:
        switch (validation_parameters.sliding_direction_) {
            case sliding_direction_e::forward:
                training_window.shift(validation_parameters.validation_slide_period_sec_);
                validation_window.shift(validation_parameters.validation_slide_period_sec_);
                break;

            case sliding_direction_e::backward:
                training_window.expand(validation_parameters.validation_slide_period_sec_);
                validation_window.expand(validation_parameters.validation_slide_period_sec_);
                break;
        }
    }

    // log results
    LOG4_INFO("Loss call count " << call_number << " time taken " << resource_measure.get_time_duration_human() <<
              " score " <<  result /  score_calls_ct << " column " << validation_parameters.column_name_ <<
              " level " << validation_parameters.model_number_ << " on parameters " << p_svr_parameters->to_string());

    return result / double(score_calls_ct);
}


double score(
        const Vector<double> &reference,
        const Vector<double> &predicted,
        const score_metric_e score_metric,
        bool print)
{
    LOG4_BEGIN();

    if (reference.GetLength() != predicted.GetLength())
        LOG4_WARN("Reference vector length " << reference.GetLength() << " and predicted vector length " << predicted.GetLength() << " do not match!");

    double result = 0.;
    off_t ix = 0;
    while (ix < reference.GetLength() && ix < predicted.GetLength())
    {
        if (print) LOG4_DEBUG(ix << " reference value " << reference[ix] << " predicted value " << predicted[ix]);

        /* TODO Replace with ViennaCL vector operations */
        switch(score_metric)
        {
        case score_metric_e::MSE:
            result += std::pow(predicted[ix] - reference[ix], 2);
            break;
        case score_metric_e::MAE:
            result += std::abs(predicted[ix] - reference[ix]);
            break;
        }

        ++ix;
    }

    result /= double(ix);

    switch(score_metric)
    {
    case score_metric_e::MSE:
        result = sqrt(result);
        break;

    case score_metric_e::MAE:
        break;
    }

    LOG4_DEBUG("Score of " << ix << " validations is " << result);
    return result;
}

static double single_progress_step {0.0};
static double current_progress {0.0};
static ResourceMeasure resource_measure;

double score(
        const svr::datamodel::DataRow::Container &ethalon,
        const svr::datamodel::DataRow::Container &predicted,
        const bpt::time_period validation_period,
        const size_t ethalon_idx,
        const size_t predicted_idx,
        const score_metric_e score_metric,
        bool print)
{
    LOG4_BEGIN();

    double result = 0.;
    double compared_data_ct = 0.;
    for (auto pred_it = predicted.lower_bound(validation_period.begin());
         pred_it->first <= validation_period.end() && pred_it != predicted.end();
         ++pred_it)
    {
        const auto etha_it = ethalon.find(pred_it->first);
        if (etha_it == ethalon.end()) {
            LOG4_WARN("Ethalon value for time " << pred_it->first << " not found. Skipping.");
            continue;
        }

        double ethalon_value = etha_it->second->get_values()[ethalon_idx];
        double predicted_value = pred_it->second->get_values()[predicted_idx];
        if (print) LOG4_INFO(
                    "Time " << bpt::to_simple_string(pred_it->first) << " ethalon value " << ethalon_value << " predicted value " << predicted_value);

        switch(score_metric)
        {
        case score_metric_e::MSE:
            result += std::pow(predicted_value - ethalon_value, 2);
            break;

        case score_metric_e::MAE:
            result += std::abs(predicted_value - ethalon_value);
            break;
        }
        compared_data_ct += 1.;
    }
    result /= compared_data_ct;

    switch(score_metric)
    {
    case score_metric_e::MSE:
        result = sqrt(result);
        break;

    case score_metric_e::MAE:
        break;
    }

    LOG4_DEBUG("Score of " << int(compared_data_ct) << " validations is " << result);
    return result;
}


/* This method is expecting input queues with at least frame length data size */
void
prepare_decon_data(
        Dataset_ptr &p_dataset,
        const InputQueue_ptr &p_input_queue,
        const std::vector<InputQueue_ptr> &aux_input_queues,
        std::vector<DeconQueue_ptr> &decon_queues,
        std::vector<DeconQueue_ptr> &aux_decon_queues)
{
    LOG4_DEBUG("Deconstructing data from " << p_input_queue->get_data().begin()->first <<
                                           " until " << p_input_queue->get_data().rbegin()->first);

    decon_queues = AppContext::get_instance().decon_queue_service.deconstruct(p_input_queue, p_dataset);

    for (const InputQueue_ptr &p_aux_input_queue: aux_input_queues)
    {
        /* TODO Decide if we really need this check here
        if (p_aux_queue->get_column_names().size() != input_queue_column_names_size)
        {
            LOG4_ERROR("Columns number in auxiliary queue and in main queue are not equal");
            aux_decon_queues_column_train.clear();
            break;
        }
        */
        std::vector<DeconQueue_ptr> new_aux_decon_queues = AppContext::get_instance().decon_queue_service.deconstruct(
                    p_aux_input_queue, p_dataset);
        aux_decon_queues.insert(aux_decon_queues.end(), new_aux_decon_queues.begin(), new_aux_decon_queues.end());
    }

    for (DeconQueue_ptr &p_main_decon_queue: decon_queues)
        AppContext::get_instance().dq_scaling_factor_service.scale_decon_queue(p_dataset, p_main_decon_queue);

    for (DeconQueue_ptr &p_aux_decon_queue: aux_decon_queues)
        AppContext::get_instance().dq_scaling_factor_service.scale_decon_queue(p_dataset, p_aux_decon_queue);

    LOG4_END();
}


size_t
get_max_lag_count(const Dataset_ptr &p_dataset, const std::vector<Bounds> &all_bounds)
{
    LOG4_BEGIN();

    size_t max_lag_count = 0;
    for (const Bounds &bounds: all_bounds) {
        if (bounds.max_bounds.get_lag_count() > max_lag_count)
            max_lag_count = bounds.max_bounds.get_lag_count();
        if (bounds.min_bounds.get_lag_count() > max_lag_count)
            max_lag_count = bounds.min_bounds.get_lag_count();
    }

    for (const auto &dataset_parameters_pair: p_dataset->get_ensembles_svr_parameters())
        for (const auto &dataset_parameters: dataset_parameters_pair.second)
            if (dataset_parameters->get_lag_count() > max_lag_count)
                max_lag_count = dataset_parameters->get_lag_count();
    LOG4_DEBUG("Max lag count is " << max_lag_count);

    return max_lag_count;
}


std::pair<std::string, std::pair<double, SVRParameters_ptr>>
tune_column(const Dataset_ptr &p_current_dataset, const validation_parameters_t &local_validation_parameters,
            const NM_parameters &nm_parameters, const PSO_parameters &pso_parameters,
            const DeconQueue_ptr &p_decon_queue, const std::vector<DeconQueue_ptr> &aux_decon_queues)
{
    loss_callback_t f = std::bind(
                svr::paramtune::loss,
                std::placeholders::_1,
                p_current_dataset,
                local_validation_parameters,
                DEFAULT_SCORE_METRIC,
                p_decon_queue,
                aux_decon_queues);

    auto idx_level = local_validation_parameters.model_number_;
    auto pso_result = pso(f, pso_parameters, local_validation_parameters.bounds_[idx_level]);

    std::pair<double, std::vector<double>> best_nm_result(
                std::numeric_limits<double>::max(), std::vector<double>());

    for (size_t ix_point = 0; ix_point < local_validation_parameters.best_points_count_; ix_point++)
    {
        auto nm_result = nm(f, pso_result[ix_point] /* initial values */, nm_parameters);
        if (best_nm_result.first > nm_result.first) best_nm_result = nm_result;
    }

    double current_score = best_nm_result.first;
    const auto key = std::make_pair(p_current_dataset->get_input_queue()->get_table_name(), local_validation_parameters.column_name_);
//    if (current_score >= best_mse[key][idx_level]) return;

    LOG4_INFO("Found new best parameters for column " << local_validation_parameters.column_name_ << " level " << idx_level << " with score " <<
              current_score << " parameters " << p_current_dataset->get_ensembles_svr_parameters()[key].at(idx_level)->to_options_string(idx_level));

    auto current_params = std::make_shared<SVRParameters>(
            *(p_current_dataset->get_ensembles_svr_parameters()[key].at(idx_level)));

    current_progress += single_progress_step;
    double timeleft = (100.0 - current_progress) * resource_measure.get_time_duration() / current_progress;
    LOG4_INFO("TUNING: current progress " << current_progress << " % ");
    LOG4_INFO("Expected tunning time: " << resource_measure.time_duration_to_human(timeleft));

    return std::make_pair(local_validation_parameters.column_name_, std::make_pair(current_score, current_params));
}


void prepare_datasets_tweaking(
        const Dataset_ptr &p_current_dataset, std::vector<Dataset_ptr> &datasets_for_tweaking)
{
    // Creating vector of datasets: first - original, all another - for
    datasets_for_tweaking.push_back(p_current_dataset);

    // TODO review aux code creating dataset for tweaking aux input_queues, seems wrong
    for (const InputQueue_ptr &p_aux_input_queue: p_current_dataset->get_aux_input_queues()) {
        Dataset_ptr p_aux_dataset = std::make_shared<Dataset>(*p_current_dataset);

        std::vector<InputQueue_ptr> aux_input_queues;
        // Set main input queue of aux dataset, as well as aux input queues same as main dataset.
        aux_input_queues.push_back(p_current_dataset->get_input_queue());
        for (const InputQueue_ptr &p_aux_aux_input_queue: p_current_dataset->get_aux_input_queues())
            if (p_aux_aux_input_queue->get_table_name() != p_aux_input_queue->get_table_name())
                aux_input_queues.push_back(p_aux_aux_input_queue);

        p_aux_dataset->set_input_queue(p_aux_input_queue);
        p_aux_dataset->set_aux_input_queues(aux_input_queues);

        datasets_for_tweaking.push_back(p_aux_dataset);
    }
}


DeconQueue_ptr
get_decon_queue_column(
        const std::vector<DeconQueue_ptr> &decon_queues,
        const std::string &input_queue_table_name,
        const std::string &input_queue_column_name)
{
    for (const DeconQueue_ptr &p_decon_queue: decon_queues)
        if (p_decon_queue->get_input_queue_column_name() == input_queue_column_name &&
                p_decon_queue->get_input_queue_table_name() == input_queue_table_name)
            return p_decon_queue;

    return nullptr;
}



std::pair<std::map<std::string, double>, std::map<std::string, SVRParameters_ptr>>
tune_level(
        const Dataset_ptr &p_current_dataset,
        const svr::paramtune::validation_parameters_t &validation_params,
        const svr::paramtune::NM_parameters &nm_parameters,
        const svr::paramtune::PSO_parameters &pso_parameters,
        const InputQueue_ptr &p_input_queue,
        const std::vector<DeconQueue_ptr> &decon_queues,
        const std::vector<DeconQueue_ptr> &aux_decon_queues)
{
    LOG4_INFO("tuning dataset " << p_current_dataset->get_dataset_name() << " with level " << validation_params.model_number_);
    size_t level = validation_params.model_number_;

    std::map<std::string, double> best_column_mse;
    std::map<std::string, SVRParameters_ptr> best_column_params;

    std::vector<std::future<std::pair<std::string, std::pair<double, SVRParameters_ptr>>>>  tune_column_futures;

    for (const std::string &column_name: p_current_dataset->get_input_queue()->get_value_columns()) {
        LOG4_INFO("Tuning level " << level <<  " column " << column_name);
        DeconQueue_ptr p_main_decon_queue = get_decon_queue_column(decon_queues, p_input_queue->get_table_name(), column_name);
        if (!p_main_decon_queue) {
            LOG4_ERROR("Could not find decon queue for column " << column_name << " input queue " << p_input_queue->get_table_name() << ". Aborting.");
            continue;
        }

        std::vector<DeconQueue_ptr> this_column_aux_decon_queues(aux_decon_queues);
        if (AppContext::get_instance().app_properties.get_main_columns_aux())
            for (const auto &p_aux_column_decon_queue: decon_queues)
                if (p_aux_column_decon_queue->get_input_queue_column_name() != p_main_decon_queue->get_input_queue_column_name())
                    this_column_aux_decon_queues.push_back(p_aux_column_decon_queue);

        validation_parameters_t local_validation_parameters = validation_params;
        local_validation_parameters.column_name_ = column_name;

        tune_column_futures.push_back(
                std::async(
                        std::launch::async, tune_column, p_current_dataset, local_validation_parameters, nm_parameters,
                        pso_parameters, p_main_decon_queue, aux_decon_queues));
    }

    for(auto &f: tune_column_futures) {
        auto column_tune_result = f.get();
        auto column_name = column_tune_result.first;

        best_column_mse[column_name] = column_tune_result.second.first;
        best_column_params[column_name] = column_tune_result.second.second;
    }

    return std::make_pair(best_column_mse, best_column_params);
}


void
tune_wavelet(
        Dataset_ptr &p_current_dataset,
        const std::vector<std::vector<size_t>> &svr_kernel_types_range,
        const validation_parameters_t &validation_parameters,
        const NM_parameters &nm_parameters,
        const PSO_parameters &pso_parameters,
        best_mse_t &best_mse,
        Dataset_ptr &p_best_dataset,
        const InputQueue_ptr &p_input_queue,
        const std::vector<InputQueue_ptr> &aux_input_queues)
{
    ResourceMeasure resource_measure;
    resource_measure.set_start_time();

    single_progress_step = 100. / std::max(1., static_cast<double>(p_current_dataset->get_swt_levels() + 1)) /
            p_best_dataset->get_input_queue()->get_value_columns().size();
    std::vector<std::future<std::pair<std::map<std::string, double>, std::map<std::string, SVRParameters_ptr>>>>  tune_level_futures;

    std::vector<DeconQueue_ptr> decon_queues, aux_decon_queues;
    prepare_decon_data(p_current_dataset, p_input_queue, aux_input_queues, decon_queues, aux_decon_queues);

    for (size_t idx_level = 0; idx_level <= p_current_dataset->get_swt_levels(); ++idx_level)
    {
        for (const auto svr_kernel_type: svr_kernel_types_range[idx_level])
        {
            // set svr_kernel_type
            for (auto &it: p_current_dataset->get_ensembles_svr_parameters())
                for (auto &p_svr_parameters: it.second)
                    p_svr_parameters->set_kernel_type(static_cast<kernel_type>(svr_kernel_type));

            std::vector<Dataset_ptr> datasets_for_tweaking;
            prepare_datasets_tweaking(p_current_dataset, datasets_for_tweaking);

            for (Dataset_ptr& p_current_tweaking_dataset: datasets_for_tweaking)
            {

                    validation_parameters_t local_validation_parameters(validation_parameters);
                    local_validation_parameters.model_number_ = idx_level;

                tune_level_futures.push_back(std::async(
                        std::launch::async, tune_level, p_current_tweaking_dataset, local_validation_parameters,
                        nm_parameters, pso_parameters, p_input_queue, decon_queues, aux_decon_queues));
                }
            }
    }

    for(auto &f : tune_level_futures)
    {
        auto level_tune_result = f.get();
        for (auto column_name : p_best_dataset->get_input_queue()->get_value_columns()) {
            auto key = std::make_pair(p_best_dataset->get_input_queue()->get_table_name(),
                                      column_name);
            size_t level = level_tune_result.second[column_name]->get_decon_level();
            if (best_mse[key].size() <= level) best_mse[key].resize(level + 1);
            best_mse[key][level] = level_tune_result.first[column_name];
            p_best_dataset->get_ensembles_svr_parameters()[key].at(
                    level) =
                    std::make_shared<SVRParameters>(*(level_tune_result.second[column_name]));
        }
    }
}


void init_best_mse(
        const Dataset_ptr &p_dataset, size_t max_swt_levels, best_mse_t best_mse)
{
    for (const std::string &column_name: p_dataset->get_input_queue()->get_value_columns())
        best_mse[std::make_pair(p_dataset->get_input_queue()->get_table_name(), column_name)] =
                std::vector<double>(max_swt_levels + 1, std::numeric_limits<double>::max());

    for (const std::string &aux_table_name: p_dataset->get_aux_input_queues_table_names())
        for (const std::string &column_name: p_dataset->get_input_queue()->get_value_columns())
           best_mse[std::make_pair(aux_table_name, column_name)] = std::vector<double>(max_swt_levels + 1, std::numeric_limits<double>::max());
}


InputQueue_ptr
get_input_data(
        InputQueue_ptr &p_input_queue,
        const size_t frame_length,
        const size_t max_lag_count,
        const bpt::time_period &needed_data_time_period)
{
    InputQueue_ptr p_res_input_queue = AppContext::get_instance().input_queue_service.clone_with_data(p_input_queue, needed_data_time_period);
    p_res_input_queue->update_data(
                AppContext::get_instance().input_queue_service.get_latest_queue_data(
                    p_res_input_queue->get_table_name(), max_lag_count + 1, needed_data_time_period.begin()));

    if (p_res_input_queue->get_data().size() < frame_length)
        p_res_input_queue->update_data(
                    AppContext::get_instance().input_queue_service.get_latest_queue_data(
                        p_res_input_queue->get_table_name(), frame_length, needed_data_time_period.end()));

    return p_res_input_queue;
}


bpt::time_period
get_full_time_span(
        const svr::paramtune::validation_parameters_t &validation_parameters,
        const bpt::time_duration &slide_duration)
{
    LOG4_DEBUG(
            "Validation range is " << validation_parameters.validation_range_ << " training range " << validation_parameters.training_range_ <<  " slide duration " << slide_duration);

    const bpt::ptime beginnest_begin =
            validation_parameters.validation_range_.begin() < validation_parameters.training_range_.begin() ?
                validation_parameters.validation_range_.begin() : validation_parameters.training_range_.begin();
    const bpt::ptime endest_end =
            validation_parameters.validation_range_.end() > validation_parameters.training_range_.end() ?
                validation_parameters.validation_range_.end() : validation_parameters.training_range_.end();

    return validation_parameters.sliding_direction_ == sliding_direction_e::forward ?
                bpt::time_period(beginnest_begin, endest_end + slide_duration) : bpt::time_period(beginnest_begin - slide_duration, endest_end);
}


InputQueue_ptr
prepare_input_data(
        const Dataset_ptr &p_dataset,
        const size_t max_frame_length,
        const validation_parameters_t &validation_parameters,
        const size_t max_lag_count,
        InputQueue_ptr &p_input_queue, /* out */
        std::vector<InputQueue_ptr> &aux_input_queues) /* out */
{
    LOG4_BEGIN();
    bpt::time_period needed_data_time_period = get_full_time_span(
            validation_parameters,
            bpt::time_duration(0, 0, validation_parameters.validation_slide_count_ * validation_parameters.validation_slide_period_sec_.total_seconds(), 0)
    );

    LOG4_DEBUG("Need input data from " << needed_data_time_period.begin() << " until " << needed_data_time_period.end());

    p_input_queue = get_input_data(p_dataset->get_input_queue(), max_frame_length, max_lag_count, needed_data_time_period);

    for (InputQueue_ptr &p_aux_input_queue: p_dataset->get_aux_input_queues())
        aux_input_queues.push_back(get_input_data(p_aux_input_queue, max_frame_length, max_lag_count, needed_data_time_period));

    LOG4_DEBUG("Prepared data starting from " << p_input_queue->get_data().begin()->first << " ending at " << p_input_queue->get_data().rbegin()->first);
    return p_input_queue;
}


Dataset_ptr
tune_dataset(
        const Dataset_ptr &p_dataset,
        const std::vector<size_t> &swt_levels_range,
        const std::vector<std::string> &swt_wavelet_names_range,
        const std::vector<std::vector<size_t>> &svr_kernel_types_range,
        const svr::paramtune::validation_parameters_t &validation_parameters,
        const svr::paramtune::NM_parameters &nm_parameters,
        const svr::paramtune::PSO_parameters &pso_parameters)
{
    Dataset_ptr p_best_dataset = std::make_shared<Dataset>(*p_dataset);
    size_t max_swt_levels = swt_levels_range.empty() ? 1 : *std::max_element(swt_levels_range.begin(), swt_levels_range.end());
    size_t max_lag_count = get_max_lag_count(p_dataset, validation_parameters.bounds_);
    svr::paramtune::best_mse_t best_mse;

    init_best_mse(p_dataset, max_swt_levels, best_mse);

    total_loss_calls = p_dataset->get_input_queue()->get_value_columns().size();
    for (auto swt_levels: swt_levels_range) total_loss_calls *= swt_levels;
    total_loss_calls *= swt_wavelet_names_range.size() * svr_kernel_types_range.size() *
            nm_parameters.max_iteration_number_ * validation_parameters.best_points_count_ * pso_parameters.iteration_number_ * pso_parameters.particles_number_;
    LOG4_INFO("Total loss calls expected count with current parameters is " << total_loss_calls);

    InputQueue_ptr p_input_queue;
    std::vector<InputQueue_ptr> aux_input_queues;
    prepare_input_data(
                p_dataset, svr::common::swt_levels_to_frame_length(max_swt_levels), validation_parameters, max_lag_count, p_input_queue, aux_input_queues);

    // grid search
    for(const size_t &swt_levels: swt_levels_range) {
        for (const std::string &swt_wavelet_name: swt_wavelet_names_range) {
            Dataset_ptr p_current_dataset = std::make_shared<Dataset>(*p_dataset);
            p_current_dataset->set_swt_levels(swt_levels);
            p_current_dataset->set_swt_wavelet_name(swt_wavelet_name);

            tune_wavelet(
                    p_current_dataset, svr_kernel_types_range, validation_parameters, nm_parameters,
                    pso_parameters, best_mse, p_best_dataset, p_input_queue, aux_input_queues);
        }
    }

    return p_best_dataset;
}


} //paramtune
} //svr
