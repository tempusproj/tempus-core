#ifndef TWEAKINGPARAMS_H
#define TWEAKINGPARAMS_H

#include <vector>
#include <future>
#include "optimizer.h"
#include "model/Dataset.hpp"
#include "model/InputQueue.hpp"
#include "util/ResourceMeasure.hpp"


namespace svr {
namespace paramtune {

using namespace svr::datamodel;


#define DEFAULT_SCORE_METRIC (score_metric_e::MSE)

enum class sliding_direction_e : size_t {forward = 0, backward = 1};
enum class score_metric_e : size_t {MSE = 0, MAE = 1};

typedef std::map<std::pair<std::string, std::string>, std::vector<double>> best_mse_t;

struct validation_parameters_t
{
    bpt::time_period training_range_ {bpt::time_period(bpt::ptime(), bpt::ptime())};
    bpt::time_period validation_range_ {bpt::time_period(bpt::ptime(), bpt::ptime())};

    sliding_direction_e sliding_direction_ {sliding_direction_e::forward};
    size_t validation_slide_count_ {1};
    bpt::seconds validation_slide_period_sec_ {0};

    size_t best_points_count_{1};
    size_t model_number_{0};

    std::string column_name_{""};

    std::vector<Bounds> bounds_;
};

using ValidationParameters_ptr = std::shared_ptr<validation_parameters_t>;

double loss(
        const std::vector<double> &parameters,
        Dataset_ptr &p_dataset,
        validation_parameters_t &validation_parameters,
        const score_metric_e score_metric,
        const DeconQueue_ptr &p_decon_queue,
        const std::vector<DeconQueue_ptr> &aux_decon_queues);

double score(
        const Vector<double> &reference,
        const Vector<double> &predicted,
        const score_metric_e score_metric,
        bool print);

double score(
        const svr::datamodel::DataRow::Container &ethalon,
        const svr::datamodel::DataRow::Container &predicted,
        const bpt::time_period validation_period,
        const size_t ethalon_idx,
        const size_t predicted_idx,
        const score_metric_e score_metric,
        bool print = true);

DeconQueue_ptr get_decon_queue_column(
        const std::vector<DeconQueue_ptr> &decon_queues,
        const std::string &input_queue_table_name,
        const std::string &input_queue_column_name);

void prepare_datasets_tweaking(
        const Dataset_ptr &current_dataset, std::vector<Dataset_ptr> &datasets_for_tweaking);

InputQueue_ptr get_input_data(
        InputQueue_ptr &p_input_queue,
        const size_t frame_length,
        const size_t max_lag_count,
        const bpt::time_period &needed_data_time_period);

InputQueue_ptr prepare_input_data(
        const Dataset_ptr &p_dataset,
        const size_t max_frame_length,
        const svr::paramtune::validation_parameters_t &validation_parameters_t,
        const size_t max_lag_count,
        svr::datamodel::InputQueue_ptr &p_input_queue, /* out */
        std::vector<InputQueue_ptr> &aux_input_queues); /* out */

bpt::time_period get_full_time_span(
        const svr::paramtune::validation_parameters_t &validation_parameters_t,
        const bpt::time_duration &slide_duration);

std::pair<std::string, std::pair<double, SVRParameters_ptr>> tune_column(const Dataset_ptr &p_current_dataset, const validation_parameters_t &local_validation_parameters,
                 const NM_parameters &nm_parameters, const PSO_parameters &pso_parameters,
                 const DeconQueue_ptr &p_decon_queue, const std::vector<DeconQueue_ptr> &aux_decon_queues);

void tune_wavelet(
        Dataset_ptr &p_current_dataset,
        const std::vector<std::vector<size_t>> &svr_kernel_types_range,
        const validation_parameters_t &validation_parameters,
        const NM_parameters &nm_parameters,
        const PSO_parameters &pso_parameters,
        best_mse_t &best_mse,
        Dataset_ptr &p_best_dataset,
        const InputQueue_ptr &p_input_queue,
        const std::vector<InputQueue_ptr> &aux_input_queues);

Dataset_ptr tune_dataset(
        const Dataset_ptr &p_dataset,
        const std::vector<size_t> &swt_levels_range,
        const std::vector<std::string> &swt_wavelet_names_range,
        const std::vector<std::vector<size_t>> &svr_kernel_types_range,
        const svr::paramtune::validation_parameters_t &validation_parameters,
        const svr::paramtune::NM_parameters &nm_parameters,
        const svr::paramtune::PSO_parameters &pso_parameters);

std::pair<std::map<std::string, double>, std::map<std::string, SVRParameters_ptr>> tune_level(
        const Dataset_ptr &p_current_dataset,
        const svr::paramtune::validation_parameters_t &validation_params,
        const svr::paramtune::NM_parameters &nm_parameters,
        const svr::paramtune::PSO_parameters &pso_parameters,
        const InputQueue_ptr &p_input_queue,
        const std::vector<DeconQueue_ptr> &decon_queues,
        const std::vector<DeconQueue_ptr> &aux_decon_queues);


} //paramtune
} //svr


#endif // TWEAKINGPARAMS_H
