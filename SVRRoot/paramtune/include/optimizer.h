#pragma once

#include <vector>
#include <functional>
#include "model/SVRParameters.hpp"

namespace svr {
namespace paramtune {

using namespace svr::datamodel;

typedef std::function<double (const std::vector<double>&)> loss_callback_t;

enum class PsoTopology : size_t {global = 0, ring = 1, random = 2};

struct PSO_parameters
{
    size_t iteration_number_;
    size_t particles_number_;
    PsoTopology pso_topology_;
};

struct NM_parameters
{
    size_t max_iteration_number_;
    double tolerance_;
};


std::pair<double, std::vector<double>>
nm(const loss_callback_t &f, const std::vector<double>& initial_values, const NM_parameters& nm_parameters);

std::vector<std::vector<double>>
pso(const loss_callback_t &f, const PSO_parameters& pso_parameters, const Bounds &bounds);

} //paramtune
} //svr

