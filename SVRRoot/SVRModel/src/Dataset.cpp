#include <model/Dataset.hpp>
#include "appcontext.hpp"

namespace svr{
namespace datamodel{

void Dataset::init_ensembles_svr_parameters(const ensembles_svr_parameters_t ensembles_svr_parameters, bigint id, size_t swt_levels)
{
    for (const auto &svr_parameters_pair: ensembles_svr_parameters)
        this->ensembles_svr_parameters_.emplace(svr_parameters_pair.first, svr::common::clone_shared_ptr_elements(svr_parameters_pair.second));

    if (ensembles_svr_parameters.empty()) {
        ensembles_svr_parameters_.clear();
        for (const std::string &column_name: p_input_queue_->get_value_columns())
        {
            std::vector<SVRParameters_ptr> vec_svr_parameters;
            for (size_t i = 0; i <= swt_levels; ++i)
            {
                auto svr_parameters = std::make_shared<SVRParameters>(SVRParameters());
                svr_parameters->set_decon_level(i);
                svr_parameters->set_dataset_id(id);
                svr_parameters->set_input_queue_table_name(p_input_queue_->get_table_name());
                svr_parameters->set_input_queue_column_name(column_name);
            }
            auto key = std::make_pair(p_input_queue_->get_table_name(), column_name);
            this->ensembles_svr_parameters_.emplace(key, vec_svr_parameters);
        }
    }
}


Dataset::Dataset(
        bigint id,
        const std::string &dataset_name,
        const std::string &user_name,
        const InputQueue_ptr &p_input_queue,
        const std::vector<InputQueue_ptr> &aux_input_queues,
        const Priority &priority,
        const std::string &description,
        size_t swt_levels,
        const std::string& swt_wavelet_name,
        const bpt::time_duration& max_lookback_time_gap,
        const std::vector<Ensemble_ptr> &ensembles,
        bool is_active,
        const std::vector<IQScalingFactor_ptr> iq_scaling_factors,
        const std::vector<DQScalingFactor_ptr> dq_scaling_factors,
        const ensembles_svr_parameters_t ensembles_svr_parameters
        )
    : Entity(id),
      dataset_name_(dataset_name),
      user_name_(user_name),
      p_input_queue_(std::make_shared<InputQueue>(*p_input_queue)),
      aux_input_queues_(svr::common::clone_shared_ptr_elements(aux_input_queues)),
      priority_(priority),
      description_(description),
      swt_levels_(swt_levels),
      swt_wavelet_name_(swt_wavelet_name),
      max_lookback_time_gap_(max_lookback_time_gap),
      ensembles_(svr::common::clone_shared_ptr_elements(ensembles)),
      is_active_(is_active),
      iq_scaling_factors_(svr::common::clone_shared_ptr_elements(iq_scaling_factors)),
      dq_scaling_factors_(svr::common::clone_shared_ptr_elements(dq_scaling_factors))

{
    if (p_input_queue_ == nullptr) LOG4_WARN("Input queue is null!");

    init_ensembles_svr_parameters(ensembles_svr_parameters, id, swt_levels);
}


Dataset::Dataset(
        bigint id,
        const std::string &dataset_name,
        const std::string &user_name,
        const std::string &input_queue_table_name,
        const std::vector<std::string> &aux_input_queues_table_names,
        const Priority &priority,
        const std::string &description, /* This is description */
        size_t swt_levels,
        const std::string& swt_wavelet_name,
        const bpt::time_duration& max_lookback_time_gap,
        const std::vector<Ensemble_ptr> &ensembles,
        bool is_active,
        const std::vector<IQScalingFactor_ptr> iq_scaling_factors,
        const std::vector<DQScalingFactor_ptr> dq_scaling_factors,
        const ensembles_svr_parameters_t ensembles_svr_parameters)
    : Entity(id),
      dataset_name_(dataset_name),
      user_name_(user_name),
          priority_(priority),
          description_(description),
          swt_levels_(swt_levels),
          swt_wavelet_name_(swt_wavelet_name),
          max_lookback_time_gap_(max_lookback_time_gap),
          ensembles_(ensembles),
          is_active_(is_active),
          iq_scaling_factors_(svr::common::clone_shared_ptr_elements(iq_scaling_factors)),
          dq_scaling_factors_(svr::common::clone_shared_ptr_elements(dq_scaling_factors))
{
    if (!input_queue_table_name.empty())
        p_input_queue_ = svr::context::AppContext::get_instance().input_queue_service.get_queue_metadata(input_queue_table_name);
    else
        LOG4_WARN("Input queue table name is empty!");

    for (const std::string &table_name : aux_input_queues_table_names)
        if (table_name.empty())
            aux_input_queues_.push_back(
                    svr::context::AppContext::get_instance().input_queue_service.get_queue_metadata(table_name));

    init_ensembles_svr_parameters(ensembles_svr_parameters, id, swt_levels);
}


Dataset::Dataset(const Dataset &dataset):
    Dataset(0, dataset.dataset_name_, dataset.user_name_,
            dataset.p_input_queue_, dataset.aux_input_queues_,
            dataset.priority_, dataset.description_,
            dataset.swt_levels_, dataset.swt_wavelet_name_, dataset.max_lookback_time_gap_,
            dataset.ensembles_, dataset.is_active_,
            dataset.iq_scaling_factors_, dataset.dq_scaling_factors_,
            dataset.ensembles_svr_parameters_)
{
}


bool Dataset::operator == (const Dataset& other) const
{
    return get_id() == other.get_id()
            && get_dataset_name() == other.get_dataset_name()
            && get_user_name() == other.get_user_name()
            && get_input_queue()->get_table_name() == other.get_input_queue()->get_table_name()
            && aux_input_queues_.size() == other.get_aux_input_queues().size()
            && std::equal(aux_input_queues_.begin(), aux_input_queues_.end(),
                          other.get_aux_input_queues().begin(),
                          [](const InputQueue_ptr &i, const InputQueue_ptr &j)
                            {return i->get_table_name() == j->get_table_name();})
            && get_priority() == other.get_priority()
            && get_swt_levels() == other.get_swt_levels()
            && get_swt_wavelet_name() == other.get_swt_wavelet_name()
            && get_max_lookback_time_gap() == other.get_max_lookback_time_gap()
//          && ensembles_.size() == other.get_ensembles().size()
//                && std::equal(ensembles.begin(), ensembles.end(),
//                                other.get_ensembles().begin())
            && get_is_active() == other.get_is_active();
}

void Dataset::set_dataset_id(bigint id)
{
    this->set_id(id);
    for (Ensemble_ptr p_ensemble : ensembles_)
        p_ensemble->set_dataset_id(id);

    for (auto &vec_svr_parameters : ensembles_svr_parameters_)
        for (auto svr_parameters : vec_svr_parameters.second)
            svr_parameters->set_dataset_id(id);
}

std::string Dataset::to_string() const {
    std::stringstream ss;

    ss  << "DatasetId: " << get_id()
        << ", DatasetName: " << get_dataset_name()
        << ", QueueTableName: " << get_input_queue()->get_table_name()
        << ", UserName: " << get_user_name()
        << ", Priority: " << svr::datamodel::to_string(get_priority())
        << ", Description: " << get_description()
        << ", SWTLevels: " << get_swt_levels()
        << ", SWTWaveletName: " << get_swt_wavelet_name()
        << ", Max Lookback Time Gap: " << bpt::to_simple_string(get_max_lookback_time_gap())
        << ", is active: " << get_is_active();

    return ss.str();
}

std::string Dataset::parameters_to_string() const
{
    std::string s{"{"};

    s += "\"swt_levels\":\"" + std::to_string(swt_levels_) + "\",";
    s += "\"swt_wavelet_name\":\"" + swt_wavelet_name_ + "\",";

    // ensemples[0] is used since each ensemble has the same parameters
    const std::vector<SVRParameters_ptr>& vec_parameters {ensembles_svr_parameters_.begin()->second};
    size_t model_number {0};

    for(auto& parameters : vec_parameters)
        s += parameters->to_options_string(model_number++) + ",";

    // remove last character, i.e. ","
    s.pop_back();

    return s + "}";
}

}
}
