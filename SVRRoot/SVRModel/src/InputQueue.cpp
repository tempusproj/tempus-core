#include "model/InputQueue.hpp"

namespace svr {
namespace datamodel {


InputQueue::InputQueue(
        const std::string& table_name,
        const std::string& logical_name,
        const std::string& owner_user_name,
        const std::string& description,
        const bpt::time_duration& resolution,
        const bpt::time_duration& legal_time_deviation,
        const bpt::time_duration& time_zone_offset,
        const std::vector<std::string> value_columns,
        const DataRowContainer &rows)
    :
    Queue(table_name, rows),
    logical_name_(logical_name),
    owner_user_name_(owner_user_name),
    description_(description),
    resolution_(resolution),
    legal_time_deviation_(legal_time_deviation),
    time_zone_offset_(time_zone_offset),
    value_columns_(value_columns)
{
    if (table_name.empty()) reinit_table_name();
}


InputQueue InputQueue::get_copy_metadata() const
{
    return InputQueue(
                get_table_name(), logical_name_, owner_user_name_, description_, resolution_,
                legal_time_deviation_, time_zone_offset_, value_columns_);
}


InputQueue_ptr InputQueue::clone_empty() const
{
    return std::make_shared<InputQueue>(
                get_table_name(), logical_name_, owner_user_name_, description_, resolution_,
                legal_time_deviation_, time_zone_offset_, value_columns_);
}


void InputQueue::set_logical_name(const std::string& logical_name)
{
    logical_name_ = logical_name;
    reinit_table_name();
}


void InputQueue::set_owner_user_name(const std::string& owner_user_name)
{
    owner_user_name_ = owner_user_name;
    reinit_table_name();
}


void InputQueue::set_resolution(const bpt::time_duration& resolution)
{
    this->resolution_ = resolution;
    reinit_table_name();
}


void InputQueue::reinit_table_name()
{
    set_table_name(make_queue_table_name(get_owner_user_name(), get_logical_name(), get_resolution()));
}


std::string InputQueue::metadata_to_string() const {
    std::stringstream ss;
    ss << "Table name " << this->table_name_
            << " logical name " << this->logical_name_
            << " owner user name " << this->owner_user_name_
            << " description " << this->description_
            << " resolution " << bpt::to_simple_string(this->resolution_)
            << " legal time deviation " << bpt::to_simple_string(this->legal_time_deviation_)
            << " time zone offset " << bpt::to_simple_string(this->time_zone_offset_)
            << " columns ";

    for (const std::string &column: this->value_columns_)
        ss << ", " << column;

    return ss.str();
}

std::string InputQueue::make_queue_table_name(const std::string &user_name, const std::string &logical_name, const bpt::time_duration &resolution)
{
    std::string result = svr::common::make_table_identifier(
                svr::common::INPUT_QUEUE_TABLE_NAME_PREFIX + "_" + user_name + "_" + logical_name + "_" + std::to_string(resolution.total_seconds()));
    std::transform(result.begin(), result.end(), result.begin(), ::tolower);
    return result;
}

} // namespace svr
} //namespace datamodel
