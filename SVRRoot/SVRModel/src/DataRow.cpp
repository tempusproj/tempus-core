#include "model/DataRow.hpp"
#include "appcontext.hpp"


svr::datamodel::DataRow::Container::iterator
find_nearest_before(
        svr::datamodel::DataRow::Container &data,
        const boost::posix_time::ptime &time,
        const boost::posix_time::time_duration &max_gap,
        const size_t lag_count)
{
    svr::datamodel::DataRow::Container::iterator iter = data.upper_bound(time);
    if (iter == data.begin()) {
        throw svr::common::insufficient_data(
                "No value before or at " + bpt::to_simple_string(time) +
                ", data available is from " + bpt::to_simple_string(data.begin()->first) + " until " +
                bpt::to_simple_string(data.rbegin()->first));
    }

    --iter;
    if (iter->first - time > max_gap)
        throw svr::common::insufficient_data(
                "Difference between " + bpt::to_simple_string(time) + " and " + bpt::to_simple_string(iter->first) +
                " is greater than max gap time " + bpt::to_simple_string(max_gap) +
                ", data available is from " + bpt::to_simple_string(data.begin()->first) + " until " +
                bpt::to_simple_string(data.rbegin()->first));

    if (lag_count == std::numeric_limits<size_t>::max()) return iter;

    off_t dist = std::distance(data.begin(), iter);
    if (dist < off_t(lag_count))
        throw svr::common::insufficient_data(
                "Distance from beginning " + std::to_string(dist) + " is less than needed lag count " + std::to_string(lag_count) +
                ", data available is from " + bpt::to_simple_string(data.begin()->first) + " until " + bpt::to_simple_string(data.rbegin()->first));

    return iter;
}


svr::datamodel::DataRow::Container::const_iterator
find_nearest_before(
        const svr::datamodel::DataRow::Container &data,
        const boost::posix_time::ptime &time,
        const boost::posix_time::time_duration &max_gap,
        const size_t lag_count)
{
    svr::datamodel::DataRow::Container::const_iterator iter = data.upper_bound(time);
    if (iter == data.begin()) {
        throw svr::common::insufficient_data(
                "No value before or at " + bpt::to_simple_string(time) +
                ", data available is from " + bpt::to_simple_string(data.begin()->first) + " until " +
                bpt::to_simple_string(data.rbegin()->first));
    }

    --iter;
    if (iter->first - time > max_gap)
        throw svr::common::insufficient_data(
                "Difference between " + bpt::to_simple_string(time) + " and " + bpt::to_simple_string(iter->first) +
                " is greater than max gap time " + bpt::to_simple_string(max_gap) +
                ", data available is from " + bpt::to_simple_string(data.begin()->first) + " until " +
                bpt::to_simple_string(data.rbegin()->first));

    if (lag_count == std::numeric_limits<size_t>::max()) return iter;

    off_t dist = std::distance(data.begin(), iter);
    if (dist < off_t(lag_count))
        throw svr::common::insufficient_data(
                "Distance from beginning " + std::to_string(dist) + " is less than needed lag count " + std::to_string(lag_count) +
                ", data available is from " + bpt::to_simple_string(data.begin()->first) + " until " + bpt::to_simple_string(data.rbegin()->first));

    return iter;
}
