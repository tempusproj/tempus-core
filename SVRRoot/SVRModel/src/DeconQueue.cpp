#include "model/DeconQueue.hpp"

namespace svr {
namespace datamodel {

DeconQueue::DeconQueue(): Queue() {}

DeconQueue::DeconQueue(
        std::string const &table_name,
        std::string const &input_queue_table_name,
        std::string const &input_queue_column_name,
        const bigint dataset_id,
        const DataRowContainer &data)
        : Queue(table_name, data),
          input_queue_table_name_(input_queue_table_name),
          input_queue_column_name_(input_queue_column_name),
          dataset_id_(dataset_id)
{
    if (table_name.empty()) reinit_table_name();
}


DeconQueue_ptr DeconQueue::clone_empty() const
{
    return std::make_shared<DeconQueue>(
                table_name_, input_queue_table_name_, input_queue_column_name_, dataset_id_);
}


void DeconQueue::set_input_queue_table_name(const std::string &input_queue_table_name)
{
    this->input_queue_table_name_ = input_queue_table_name;
    reinit_table_name(); // because table_name created from dataset_id, input_queue_column_name and input_queue_table_name
}


void DeconQueue::set_input_queue_column_name(const std::string &input_queue_column_name)
{
    this->input_queue_column_name_ = input_queue_column_name;
    reinit_table_name(); // because table_name created from dataset_id, input_queue_column_name and input_queue_table_name
}


void DeconQueue::set_dataset_id(bigint dataset_id) {
    this->dataset_id_ = dataset_id;
    reinit_table_name(); // because table_name created from dataset_id, input_queue_column_name and input_queue_table_name
}


std::string DeconQueue::make_queue_table_name(const std::string &input_queue_table_name, const bigint dataset_id, const std::string &input_queue_column_name)
{
    std::string result = svr::common::make_table_identifier(
            svr::common::DECON_QUEUE_TABLE_NAME_PREFIX + "_" +
            input_queue_table_name + "_" +
            std::to_string(dataset_id) + "_" +
            input_queue_column_name);
    std::transform(result.begin(), result.end(), result.begin(), ::tolower);
    return result;
}

void DeconQueue::reinit_table_name()
{
    set_table_name(
            svr::common::make_table_identifier(
                    svr::common::DECON_QUEUE_TABLE_NAME_PREFIX
                    + "_" + input_queue_table_name_
                    + "_" + std::to_string(dataset_id_)
                    + "_" + input_queue_column_name_)
    );
}


std::string DeconQueue::metadata_to_string() const {
    std::stringstream ss;
    ss << "Table name " << this->table_name_
            << ", input queue table name " << this->input_queue_table_name_
            << ", input queue column name " << this->input_queue_column_name_
            << ", dataset id " << this->dataset_id_
            << ", rows count " << this->data_.size();

    return ss.str();
}


bool DeconQueue::operator==(const DeconQueue &other) const {
    if (this == &other) return true;

    return table_name_ == other.table_name_
            && input_queue_table_name_ == other.input_queue_table_name_
            && input_queue_column_name_ == other.input_queue_column_name_
            && dataset_id_ == other.dataset_id_;
}

std::string DeconQueue::data_to_string() const {
    return data_to_string(25);
}

std::string DeconQueue::data_to_string(size_t data_size) const {
    size_t rowId = 0;
    std::stringstream ss;
    for (const auto &row : get_data()) {
        if (rowId++ > data_size) {
            ss << ". . . " << (get_data().size() - data_size) << " more" << std::endl;
            break;
        }
        ss << row.second->to_string() << '\n';
    }
    return ss.str();
}

}
}
