#include <model/DBTable.hpp>

#include <algorithm>
#include <cctype>

namespace svr {
namespace datamodel {

Queue::Queue(const Queue &rhs) : table_name_(rhs.table_name_)
{
    this->data_ = svr::common::clone_shared_ptr_elements(rhs.data_);
}


Queue::Queue(const std::string& table_name, const DataRowContainer &data):
    table_name_(table_name)
{
    this->data_ = svr::common::clone_shared_ptr_elements(data);
}


void Queue::set_table_name(const std::string& table_name)
{
    table_name_ = table_name;
}


std::string Queue::to_string() const
{
    return "table name " + this->table_name_ + "\n" + metadata_to_string() + "\n\t" + data_to_string();
}


void Queue::update_data(const DataRow::Container &data, const bool overwrite)
{
    LOG4_BEGIN();

    for (const DataRow::Container::value_type &elem: data) {
        auto iter = this->data_.find(elem.first);
        if (iter != this->data_.end() && !overwrite) continue;
        else if (iter == this->data_.end()) this->data_.insert(elem);
        else iter->second = elem.second;
    }

    LOG4_END();
}


std::string Queue::data_to_string() const
{
    size_t rows_ct = 0;
    std::stringstream ss;
    for (const auto &row : this->data_) {
        if (rows_ct++ > DISPLAY_ROWS_LIMIT) {
            ss << ". . . " << (get_data().size() - DISPLAY_ROWS_LIMIT) << " more" << std::endl;
            break;
        }
        ss << row.second->to_string() << "\n\t";
    }
    return ss.str();
}


}
}
