#pragma once

#include "common/types.hpp"
#include "common/Logging.hpp"
#include "model/Entity.hpp"
#include "model/Priority.hpp"


namespace svr{
namespace datamodel{


class User : public Entity {

public:
    enum class ROLE{ ADMIN, USER };

private:
    std::string user_name;
    std::string email;
    std::string name;
    std::string password;
    ROLE role;
    Priority priority;

public:
    bool operator==(User const & other) const {return user_name == other.user_name && email == other.email && name == other.name && password == other.password && role == other.role;}

    User(){}
    User(bigint user_id,
        const std::string& username,
        const std::string& email,
        const std::string& password,
        const std::string& name = "",
        const ROLE& _role = ROLE::USER,
        Priority _priority = Priority::Normal
        ):
            Entity(user_id),
            user_name(username),
            email(email),
            name(name),
            password(password),
            role(_role),
    		priority(_priority)
    {
        LOG4_TRACE("User created: " << this->to_string());
    }

public:
    virtual ~User(){}
    void set_email(const std::string& email) {	this->email = email;}
    void set_name(const std::string& name) {this->name = name;}
    void set_role(ROLE role) {this->role = role;}
    void set_password(const std::string& password) {this->password = password;}
    void set_user_name(const std::string& userName) {user_name = userName;}
    std::string get_name() const {return name;}
    std::string get_email() const {	return email;}
    std::string get_user_name() const {return user_name;}
    ROLE get_role() const{	return role;}
    std::string get_password() const{return password;}
    Priority const &get_priority() const { return priority; }
    void set_priority(Priority const &priority) { this->priority = priority; }

    std::string to_string() const override
    {
        std::stringstream ss;
        ss << "User={EntityId:" << get_id()
                << ",UserName:" << get_user_name()
                << ",email:" << get_email()
                << ",password:" << get_password()
                << ",name:" << get_name()
                << ",role:" << (get_role() == ROLE::ADMIN ? "Admin" : "User")
                << ",priority: " << svr::datamodel::to_string(get_priority())
                << "}";
        return ss.str();
    }

};
}
}

using User_ptr = std::shared_ptr<svr::datamodel::User>;
