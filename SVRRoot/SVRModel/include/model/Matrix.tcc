#pragma once

#include "model/Matrix.h"

namespace svr{
namespace datamodel{
////////////////////////////
// METHODS IMPLEMENTATION //
////////////////////////////

// INITIALIZATION
template<class T>
Matrix<T>::Matrix() {
    this->StepSize = 100;
}

template<class T>
Matrix<T>::Matrix(double **X, int Rows, int Cols) {
    this->StepSize = 100;
    this->Values.resize(Rows);
    for (int i = 0; i < Rows; i++) {
        this->Values.push_back(std::make_shared<Vector<T> >(Vector<T>(X[i], Cols)));
    }
}

template<class T>
Matrix<T>::Matrix(const Matrix<T> &m): Matrix() {
    for (int i = 0; i < m.GetLengthRows(); i++)
        AddRowCopy(m.GetRowRef(i));
}

template<class T>
Matrix<T>::~Matrix() {
    this->Clear();
}

template<class T>
bool Matrix<T>::operator==(Matrix<T> const & o)const
{
    for(size_t i = 0; i < Values.size(); ++i)
        if(! ( *(Values[i]) == *(o.Values[i])))
            return false;
    return true;
}

template<class T>
viennacl::matrix<T> Matrix<T>::ViennaClone() const
{
    viennacl::matrix<T> vcl_matrix(GetLengthRows(), GetLengthCols());
    ViennaClone(vcl_matrix);
    return vcl_matrix;
}

template<class T>
void Matrix<T>::ViennaClone(viennacl::matrix<T> & vcl_matrix) const
{
    // TODO: Improve...

    vcl_matrix.resize(GetLengthRows(), GetLengthCols());
    std::vector<std::vector<T>> std_x(GetLengthRows());
    for (long i = 0; i < GetLengthRows(); i++) {
        datamodel::Vector<T> * row = GetRowRef(i);
        row->copy_to(std_x[i]);
    }
    viennacl::copy(std_x, vcl_matrix);
}

template<class T>
Matrix<T> *Matrix<T>::Clone() {
    Matrix<T> *M = new Matrix<T>();
    for (int i = 0; i < this->GetLengthRows(); i++)
        M->AddRowCopy(this->GetRowRef(i));
    return M;
}

template<class T>
int Matrix<T>::GetLengthRows() const {
    return this->Values.size();
}

template<class T>
int Matrix<T>::GetLengthCols() const {
    if (this->Values.size() == 0) {
        return 0;
    } else {
        return this->Values[0]->GetLength();
    }
}

// Selection Operations
template<class T>
Vector<T> *Matrix<T>::GetRowRef(int Index) const {
    if (Index >= 0 && Index < this->GetLengthRows()) {
        return this->Values[Index].get();
    } else {
        LOG4_ERROR(
                "Error! It's impossible to get an row from the matrix that doesn't exist.");
        return new Vector<T>();
    }
}

template<class T>
Vector<T> *Matrix<T>::GetRowCopy(int Index) {
    if (Index >= 0 && Index < this->GetLengthRows()) {
        return this->Values[Index]->Clone();
    } else {
        LOG4_ERROR(
                "Error! It's impossible to get an row from the matrix that doesn't exist.");
        return new Vector<T>();
    }
}

template<class T>
Vector<T> *Matrix<T>::GetColCopy(int Index) {
    if (Index >= 0 && Index < this->GetLengthCols()) {
        Vector<T> *V = new Vector<T>();
        for (int i = 0; i < this->GetLengthRows(); i++) {
            V->Add(this->Values[i]->GetValue(Index));
        }
        return V;
    } else {
        LOG4_ERROR(
                "Error! It's impossible to get an row from the matrix that doesn't exist.");
        return new Vector<T>();
    }
}

template<class T>
T Matrix<T>::GetValue(int RowIndex, int ColIndex) {
    return this->Values[RowIndex]->GetValue(ColIndex);
}

template<class T>
void Matrix<T>::SetValue(int RowIndex, int ColIndex, T Value) {
    (*this->Values[RowIndex])[ColIndex] = Value;
}

template<class T>
int Matrix<T>::IndexOf(Vector<T> *V) {
    for (int i = 0; i < this->GetLengthRows(); i++) {
        bool Found = true;
        for (int j = 0; j < this->GetLengthCols(); j++) {
            if (V->GetValue(j) != this->GetValue(i, j)) {
                Found = false;
                break;
            }
        }
        if (Found)
            return i;
    }
    return -1;
}

// Add/Remove Operations
template<class T>
void Matrix<T>::Clear() {
    this->Values.clear();
}

template<class T>
void Matrix<T>::AddRowRef(Vector<T> *V) {
    if (this->GetLengthRows() == 0 && this->GetLengthCols() == 0) {
        this->Values.push_back(std::shared_ptr<Vector<T> >(V));
    } else if (this->GetLengthCols() == V->GetLength()) {
        this->Values.push_back(std::shared_ptr<Vector<T> >(V));
    } else {
        LOG4_ERROR("Error! It's impossible to add a row of different lengths. source:" << V->GetLength() << " required: " << this->Values.size());
    }
}

template<class T>
void Matrix<T>::AddRowCopy(const Vector<T> *V) {
    if (this->GetLengthRows() == 0 && this->GetLengthCols() == 0) {
        this->Values.push_back(std::shared_ptr<Vector<T> >(V->Clone()));
    } else if (this->GetLengthCols() == V->GetLength()) {
        this->Values.push_back(std::shared_ptr<Vector<T> >(V->Clone()));
    } else {
        LOG4_ERROR("Error! It's impossible to add a row of different length.");
    }
}

template<class T>
void Matrix<T>::AddRowCopy(const Vector<T> &V) {
    if (this->GetLengthRows() == 0 && this->GetLengthCols() == 0) {
        this->Values.push_back(std::shared_ptr<Vector<T> >(V.Clone()));
    } else if (this->GetLengthCols() == V.GetLength()) {
        this->Values.push_back(std::shared_ptr<Vector<T> >(V.Clone()));
    } else {
        LOG4_ERROR("Error! It's impossible to add a row of different length.");
    }
}

template<class T>
void Matrix<T>::AddRowCopy(const T *V, int N) {
    if (this->GetLengthRows() == 0 && this->GetLengthCols() == 0) {
        Vector<T> *NewV = new Vector<T>(V, N);
        this->Values.push_back(std::shared_ptr<Vector<T> >(NewV));
    } else if (this->GetLengthCols() == V->Length) {
        Vector<T> *NewV = new Vector<T>(V, N);
        this->Values.push_back(std::shared_ptr<Vector<T> >(NewV));
    } else {
        LOG4_ERROR("Error! It's impossible to add a row of different length.");
    }
}

template<class T>
void Matrix<T>::AddRowRef(T *V, int N) /* TODO Convert to shared_ptr */
{
    if (this->GetLengthRows() == 0 && this->GetLengthCols() == 0) {
        this->Values.push_back(std::shared_ptr<Vector<T> >(V));
    } else if (this->GetLengthCols() == V->Length) {
        this->Values.push_back(std::shared_ptr<Vector<T> >(V));
    } else {
        LOG4_ERROR("Error! It's impossible to add a row of different length.");
    }
}

template<class T>
void Matrix<T>::AddRowRefAt(Vector<T> *V, int Index) {
    if (this->GetLengthRows() == 0 && this->GetLengthCols() == 0
            && Index == 0) {
        this->Values.insert(V, std::next(this->Values.begin(), Index));
    } else if (this->GetLengthCols() == V->GetLength() && Index >= 0
            && Index <= this->GetLengthRows()) {
        this->Values.insert(V, std::next(this->Values.begin(), Index));
    } else {
        LOG4_ERROR(
                "Error! It's impossible to add a row of different length or in a bad index.");
    }
}

template<class T>
void Matrix<T>::AddRowCopyAt(Vector<T> *V, int Index) {
    if (this->GetLengthRows() == 0 && this->GetLengthCols() == 0
            && Index == 0) {
        this->Values.insert(V->Clone(), std::next(this->Values.begin(), Index));
    } else if (this->GetLengthCols() == V->GetLength() && Index >= 0
            && Index <= this->GetLengthRows()) {
        this->Values.insert(V->Clone(), std::next(this->Values.begin(), Index));
    } else {
        LOG4_ERROR(
                "Error! It's impossible to add a row of different length or in a bad index.");
    }
}

template<class T>
void Matrix<T>::AddRowCopyAt(T *V, int N, int Index) {
    if (this->GetLengthRows() == 0 && this->GetLengthCols() == 0
            && Index == 0) {
        this->Values.insert(new Vector<T>(V, N), std::next(this->Values.begin(), Index));
    } else if (this->GetLengthCols() == V->Length && Index >= 0
            && Index <= this->GetLengthRows()) {
        this->Values.insert(new Vector<T>(V, N), std::next(this->Values.begin(), Index));
    } else {
        LOG4_ERROR(
                "Error! It's impossible to add a row of different length or in a bad index.");
    }
}

template<class T>
void Matrix<T>::AddColCopy(Vector<T> *V) {
    if (this->GetLengthRows() == 0 && this->GetLengthCols() == 0) {
        for (int i = 0; i < V->GetLength(); i++) {
            Vector<T> *V3 = new Vector<T>();
            V3->Add(V->GetValue(i));
            this->Values.push_back(std::shared_ptr<Vector<T> >(V3));
        }
    } else if (this->GetLengthRows() == V->GetLength()) {
        for (int i = 0; i < V->GetLength(); i++) {
            this->Values[i]->Add(V->GetValue(i));
        }
    } else {
        LOG4_ERROR(
                "Error! It's impossible to add a column of different length.");
    }
}

template<class T>
void Matrix<T>::AddColCopy(T *V, int N) {
    if (this->GetLengthRows() == 0 && this->GetLengthCols() == 0) {
        for (int i = 0; i < N; i++) {
            Vector<T> *V3 = new Vector<T>();
            V3->Add(V[i]);
            this->Values.push_back(std::shared_ptr<Vector<T> >(V3));
        }
    } else if (this->GetLengthRows() == N) {
        for (int i = 0; i < N; i++) {
            this->Values[i]->Add(V[i]);
        }
    } else {
        LOG4_ERROR(
                "Error! It's impossible to add a column of different length.");
    }
}

template<class T>
void Matrix<T>::AddColCopyAt(Vector<T> *V, int Index) {
    if (this->GetLengthRows() == 0 && this->GetLengthCols() == 0
            && Index == 0) {
        this->AddColCopy(V);
    } else if (this->GetLengthRows() == V->GetLength() && Index >= 0
            && Index <= this->GetLengthRows()) {
        for (int i = 0; i < V->GetLength(); i++) {
            this->Values[i]->AddAt(V->GetValue(i), Index);
        }
    } else {
        LOG4_ERROR(
                "Error! It's impossible to add a row of different length or in a bad index.");
    }
}

template<class T>
void Matrix<T>::AddColCopyAt(T *V, int N, int Index) {
    if (this->GetLengthRows() == 0 && this->GetLengthCols() == 0
            && Index == 0) {
        this->AddCol(V, N);
    } else if (this->GetLengthRows() == N && Index >= 0
            && Index <= this->GetLengthRows()) {
        for (int i = 0; i < N; i++) {
            this->Values[i]->AddAt(V[i], Index);
        }
    } else {
        LOG4_ERROR(
                "Error! It's impossible to add a row of different length or in a bad index.");
    }
}

template<class T>
void Matrix<T>::RemoveRow(int Index) {
    if (Index >= 0 && Index < this->GetLengthRows()) {
        this->Values.erase(std::next(this->Values.begin(), Index));
    } else {
        LOG4_ERROR(
                "Error! It's impossible to remove an element from the matrix that doesn't exist.");
    }
}

template<class T>
void Matrix<T>::RemoveCol(int Index) {
    if (Index >= 0 && Index < this->GetLengthCols()) {
        for (int i = 0; i < this->GetLengthRows(); i++) {
            this->Values[i]->RemoveAt(Index);
        }
    } else {
        LOG4_ERROR(
                "Error! It's impossible to remove an element from the matrix that doesn't exist.");
    }
}

template<class T>
Matrix<T> *Matrix<T>::ExtractRows(int FromRowIndex, int ToRowIndex) {
    if (FromRowIndex >= 0 && ToRowIndex <= this->GetLengthRows() - 1
            && FromRowIndex <= ToRowIndex) {
        Matrix<T> *M = new Matrix<T>();
        for (int i = FromRowIndex; i <= ToRowIndex; i++)
            M->AddRowRef(this->GetRowCopy(i));
        return M;
    } else {
        LOG4_ERROR(
                "Error! It's impossible to extract the rows: invalid indexes");
        return new Matrix<T>();
    }
}

template<class T>
Matrix<T> *Matrix<T>::ExtractCols(int FromColIndex, int ToColIndex) {
    if (FromColIndex >= 0 && ToColIndex <= this->GetLengthCols() - 1
            && FromColIndex <= ToColIndex) {
        Matrix<T> *M = new Matrix<T>();
        for (int i = 0; i < this->GetLengthRows(); i++)
            M->AddRowRef(this->GetRowRef(i)->Extract(FromColIndex, ToColIndex));
        return M;
    } else {
        LOG4_ERROR(
                "Error! It's impossible to extract the columns: invalid indexes");
        return new Matrix<T>();
    }
}

// Pre-built Matrix
template<class T>
Matrix<double> *Matrix<T>::ZeroMatrix(int RowsNumber, int ColsNumber) {
    Matrix<double> *M = new Matrix<double>();
    for (int i = 0; i < RowsNumber; i++) {
        Vector<double> *V = Vector<double>::ZeroVector(ColsNumber);
        M->AddRowRef(V);
    }
    return M;
}

template<class T>
Matrix<double> *Matrix<T>::RandMatrix(int RowsNumber, int ColsNumber) {
    Matrix<double> *M = new Matrix<double>();
    for (int i = 0; i < RowsNumber; i++) {
        Vector<double> *V = Vector<double>::RandVector(ColsNumber);
        M->AddRowRef(V);
    }
    return M;
}

// Mathematical Operations
template<class T>
void Matrix<T>::SumScalar(T X) {
    for (int i = 0; i < this->GetLengthRows(); i++) {
        this->Values[i]->SumScalar(X);
    }
}

template<class T>
void Matrix<T>::ProductScalar(T X) {
    for (int i = 0; i < this->GetLengthRows(); i++) {
        this->Values[i]->ProductScalar(X);
    }
}

template<class T>
void Matrix<T>::DivideScalar(T X) {
    for (int i = 0; i < this->GetLengthRows(); i++) {
        this->Values[i]->DivideScalar(X);
    }
}

template<class T>
void Matrix<T>::PowScalar(T X) {
    for (int i = 0; i < this->GetLengthRows(); i++) {
        this->Values[i]->PowScalar(X);
    }
}

template<class T>
void Matrix<T>::SumMatrix(Matrix<T> *M) {
    for (int i = 0; i < this->GetLengthRows(); i++) {
        this->Values[i]->SumVector(M->Values[i].get());
    }
}

template<class T>
void Matrix<T>::SubtractMatrix(Matrix<T> *M) {
    for (int i = 0; i < this->GetLengthRows(); i++) {
        this->Values[i]->SubtractVector(M->Values[i].get());
    }
}

template<class T>
Vector<T> *Matrix<T>::ProductVector(Vector<T> *V) {
    if (this->GetLengthCols() == 0 || this->GetLengthCols() == V->GetLength()) {
        Vector<T> *V2 = new Vector<T>(this->GetLengthRows());
        // TODO Parallelize
        for (int i = 0; i < this->GetLengthRows(); i++) {
            V2->Add(V->ProductVectorScalar(this->Values[i].get(), V));
        }
        return V2;
    } else {
        LOG4_ERROR(
                "Error! It's impossible to multiply a matrix and a vector with different length.");
        return new Vector<T>();
    }
}

template<class T>
Vector<T> *Matrix<T>::ProductVector(Matrix *M, Vector<T> *V) {
    if (M->GetLengthCols() == 0 || M->GetLengthCols() == V->GetLength()) {
        Vector<T> *V2 = new Vector<T>(M->GetLengthRows());
        for (int i = 0; i < M->GetLengthRows(); i++) {
            V2->Add(V->ProductVectorScalar(M->Values[i].get(), V));
        }
        return V2;
    } else {
        LOG4_ERROR(
                "Error! It's impossible to multiply a matrix and a vector with different length.");
        return new Vector<T>();
    }
}

template<class T>
Matrix<T> *Matrix<T>::ProductVectorVector(Vector<T> *V1, Vector<T> *V2) {
    if (V1->GetLength() == V2->GetLength()) {
        Matrix<T> *M = new Matrix();
        for (int i = 0; i < V1->GetLength(); i++) {
            Vector<T> *V4 = V2->Clone();
            V4->ProductScalar(V1->GetValue(i));
            M->AddRowRef(V4);
        }
        return M;
    } else {
        LOG4_ERROR(
                "Error! It's impossible to multiply two vectors with different length.");
        return new Matrix<T>();
    }
}

template<class T>
Matrix<T> *Matrix<T>::ProductMatrixMatrix(Matrix<T> *M1, Matrix<T> *M2) {
    if (M1->GetLengthCols() == M2->GetLengthCols()) {
        Matrix<T> *M3 = new Matrix<T>();
        for (int i = 0; i < M1->GetLengthRows(); i++) {
            Vector<T> *V = new Vector<T>(M2->GetLengthRows());
            for (int j = 0; j < M2->GetLengthRows(); j++) {
                V->Add(
                        V->ProductVectorScalar(M1->Values[i].get(),
                                M2->Values[i].get()));
            }
            M3->AddRowRef(V);
        }
        return M3;
    } else {
        LOG4_ERROR(
                "Error! It's impossible to multiply two matrix with not compatiple size.");
        return new Matrix<T>();
    }

}

// I/O Operations
template<class T>
Matrix<double> *Matrix<T>::Load(char const *Filename) {
    // Open the file
    std::ifstream File(Filename, std::ios::in);
    if (!File) {
        LOG4_ERROR("Error. It's impossible to open the file.");
        return new Matrix<double>();
    }
    Matrix<double> *M = new Matrix<double>();
    // Save the vector
    try {
        int RowsNumber, ColsNumber;
        double Value;
        File >> RowsNumber >> ColsNumber;
        for (int i = 0; i < RowsNumber; i++) {
            Vector<double> *V = new Vector<double>(ColsNumber);
            for (int j = 0; j < ColsNumber; j++) {
                File >> Value;
                V->Add(Value);
            }
            M->AddRowRef(V);
        }
    } catch (...) {
        LOG4_ERROR("Error. It's impossible to complete the save.");
    }
    // Close the file
    File.close();

    return M;
}

template<class T>
void Matrix<T>::Save(char const * Filename) {
    // Open the file
    std::ofstream File(Filename, std::ios::out);
    if (!File) {
        LOG4_ERROR("Error. It's impossible to create the file.");
        return;
    }
    File.precision(30);
    // Save the matrix
    try {
        File << this->GetLengthRows() << " " << this->GetLengthCols() << std::endl;
        for (int i = 0; i < this->GetLengthRows(); i++) {
            for (int j = 0; j < this->GetLengthCols(); j++)
                File << this->Values[i]->GetValue(j) << " ";
            File << std::endl;
        }
    } catch (...) {
        LOG4_ERROR("Error. It's impossible to complete the save.");
    }
    // Close the file
    File.close();
}

template<class T>
void Matrix<T>::Print() {
    for (int i = 0; i < this->GetLengthRows(); i++) {
        this->Values[i]->Print();
    }
}

template<class T>
void Matrix<T>::Print(char *MatrixName) {
    LOG4_INFO(MatrixName);
    this->Print();
}

// Operators Redefinition
template<class T>
Vector<T> Matrix<T>::operator[](int Index) {
    return (*this->Values[Index]);
}

}
}
