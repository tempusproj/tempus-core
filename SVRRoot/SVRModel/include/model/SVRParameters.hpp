#pragma once

#include "model/Entity.hpp"
#include "common/Logging.hpp"

#define SMO_EPSILON 1e-6

namespace svr{
namespace datamodel{



typedef enum class kernel_type : int
{
    LINEAR = 0,
    POLYNOMIAL = 1,
    RBF = 2,
    RBF_GAUSSIAN = 3,
    RBF_EXPONENTIAL = 4,
    MLP = 5,
    number_of_kernel_types
} kernel_type_e, *kernel_type_e_ptr;

    enum class bound_type : int
    {
        min = 0,
        max = 1
    };

typedef std::tuple<
    std::string, /* input_queue_table_name; */
    std::string, /* input_queue_column_name; */
    size_t /* decon_level */> svr_parameters_index_t;

class SVRParameters: public Entity
{

private:
    bigint dataset_id; /* TODO Replace with pointer to dataset id */

    std::string input_queue_table_name; // TODO Replace with pointer to Input Queue
    std::string input_queue_column_name; // TODO Replace with pointer to Input Queue
    size_t decon_level_;

    double svr_C;
    double svr_epsilon;
    double svr_kernel_param;
    double svr_kernel_param2;
    u_int64_t svr_decremental_distance;
    double svr_adjacent_levels_ratio;
    kernel_type_e kernel_type;
    size_t lag_count;

public:

    SVRParameters(): SVRParameters(0, 0, "", "", 0, 0.0, 0.0, 0.0, 0.0, 0, 0.0, kernel_type_e::RBF, 0)
    {}

    SVRParameters(
            const bigint id, const bigint dataset_id, const std::string &input_queue_table_name,
            const std::string &input_queue_column_name, const size_t decon_level, double svr_C,
            const double svr_epsilon, const double svr_kernel_param, const double svr_kernel_param2,
            const u_int64_t svr_decremental_distance, const double svr_adjacent_levels_ratio,
            const kernel_type_e kernel_type, const size_t lag_count)
        : Entity(id),
          dataset_id(dataset_id),
              input_queue_table_name(input_queue_table_name),
              input_queue_column_name(input_queue_column_name),
              decon_level_(decon_level),
              svr_C(svr_C),
              svr_epsilon(svr_epsilon),
              svr_kernel_param(svr_kernel_param),
              svr_kernel_param2(svr_kernel_param2),
              svr_decremental_distance(svr_decremental_distance),
              svr_adjacent_levels_ratio(svr_adjacent_levels_ratio),
              kernel_type(kernel_type),
              lag_count(lag_count) {
        this->set_kernel_type(kernel_type);
    }

    SVRParameters(const SVRParameters &params):
        SVRParameters(
            0, params.get_dataset_id(), params.get_input_queue_table_name(),
            params.get_input_queue_column_name(),
            params.get_decon_level(), params.get_svr_C(),
            params.get_svr_epsilon(), params.get_svr_kernel_param(),
            params.get_svr_kernel_param2(), params.get_svr_decremental_distance(),
            params.get_svr_adjacent_levels_ratio(), params.get_kernel_type(),
            params.get_lag_count())
    {
    }

    bool operator == (const SVRParameters& other) const {

        return get_dataset_id() == other.get_dataset_id()
                && get_input_queue_table_name() == other.get_input_queue_table_name()
                && get_input_queue_column_name() == other.get_input_queue_column_name()
                && get_decon_level() == other.get_decon_level()
                && get_svr_C() == other.get_svr_C()
                && get_svr_epsilon() == other.get_svr_epsilon()
                && get_svr_kernel_param() == other.get_svr_kernel_param()
                && get_svr_kernel_param2() == other.get_svr_kernel_param2()
                && get_svr_decremental_distance() == other.get_svr_decremental_distance()
                && get_svr_adjacent_levels_ratio() == other.get_svr_adjacent_levels_ratio()
                && get_kernel_type() == other.get_kernel_type()
                && get_lag_count() == other.get_lag_count();
    }

    bigint get_dataset_id() const
    {
        return dataset_id;
    }

    void set_dataset_id(bigint value)
    {
        dataset_id = value;
    }

    std::string get_input_queue_column_name() const
    {
        return input_queue_column_name;
    }

    void set_input_queue_column_name(const std::string &value)
    {
        input_queue_column_name = value;
    }

    std::string get_input_queue_table_name() const
    {
        return input_queue_table_name;
    }

    void set_input_queue_table_name(const std::string &value)
    {
        input_queue_table_name = value;
    }

    size_t get_decon_level() const
    {
        return decon_level_;
    }

    void set_decon_level(size_t decon_level)
    {
        this->decon_level_ = decon_level;
    }

    double get_svr_C() const {
        return svr_C;
    }

    void set_svr_C(double svr_C) {
        if(svr_C <= 0.0)
            throw std::invalid_argument("Cost parameter for SVM is less or equal than zero");
        else
            this->svr_C = svr_C;
    }

    double get_svr_epsilon() const {
        return svr_epsilon;
    }

    void set_svr_epsilon(double svr_epsilon) {
        if(svr_epsilon <= 0.0)
            throw std::invalid_argument("Epsilon parameter for SVM is less or equal than zero");
        else
            this->svr_epsilon = svr_epsilon;
    }

    double get_svr_kernel_param() const {
        return svr_kernel_param;
    }

    void set_svr_kernel_param(double svr_kernel_param) {
        if(svr_kernel_param <= 0.0)
            throw std::invalid_argument("Kernel parameter for SVM is less or equal than zero");
        else
            this->svr_kernel_param = svr_kernel_param;
    }

    double get_svr_kernel_param2() const {
        return svr_kernel_param2;
    }

    void set_svr_kernel_param2(double svr_kernel_param2) {
        this->svr_kernel_param2 = svr_kernel_param2;
    }

    u_int64_t get_svr_decremental_distance() const {
        return svr_decremental_distance;
    }

    void set_svr_decremental_distance(u_int64_t svr_decremental_distance) {
        this->svr_decremental_distance = svr_decremental_distance;
    }

    double get_svr_adjacent_levels_ratio() const {
        return svr_adjacent_levels_ratio;
    }

    void set_svr_adjacent_levels_ratio(double svr_adjacent_levels_ratio) {
        if(svr_adjacent_levels_ratio < 0.0 || svr_adjacent_levels_ratio > 1.0)
            throw std::invalid_argument("Adjacent levels ration is out of [0, 1]");
        else
            this->svr_adjacent_levels_ratio = svr_adjacent_levels_ratio;
    }

    kernel_type_e get_kernel_type() const {
        return kernel_type;
    }

    void set_kernel_type(kernel_type_e kernel_type) {
        if (kernel_type < kernel_type_e::number_of_kernel_types)
            this->kernel_type = kernel_type;
        else
            throw std::invalid_argument("wrong type of kernel");
    }

    size_t get_lag_count() const {
        return lag_count;
    }

    void set_lag_count(size_t lag_count) {
        if(lag_count <= 0)
            throw std::invalid_argument("Lag count parameter is less or equal than zero");
        else
            this->lag_count = lag_count;
    }

    std::string to_string() const {
        std::stringstream ss;

        ss      << "Id: " << get_id()
                << ", C: " << get_svr_C()
                << ", epsilon: " << get_svr_epsilon()
                << ", kernel_param: " << get_svr_kernel_param()
                << ", kernel_param2: " << get_svr_kernel_param2()
                << ", decremental_distance: " << get_svr_decremental_distance()
                << ", svr_adjacent_levels_ratio: " << get_svr_adjacent_levels_ratio()
                << ", kernel_type: " << static_cast<int>(get_kernel_type())
                << ", lag_count: " << get_lag_count() ;

        return ss.str();
    }

    std::string to_options_string(const size_t model_number) const
    {
        std::stringstream ss;

        ss << "\"svr_c_" << model_number << "\":\"" << get_svr_C() << "\","
           << "\"svr_epsilon_" << model_number << "\":\"" << get_svr_epsilon() << "\","
           << "\"svr_kernel_param_" << model_number << "\":\"" << get_svr_kernel_param() << "\","
           << "\"svr_kernel_param2_" << model_number << "\":\"" << get_svr_kernel_param2() << "\","
           << "\"svr_decremental_distance_" << model_number << "\":\"" << get_svr_decremental_distance() << "\","
           << "\"svr_svr_adjacent_levels_ratio_" << model_number << "\":\"" << get_svr_adjacent_levels_ratio() << "\","
           << "\"svr_kernel_type_" << model_number << "\":\"" << static_cast<int>(get_kernel_type()) << "\","
           << "\"svr_error_tolerance_" << model_number << "\":\"" << "tune" << "\","
           << "\"lag_count_" << model_number << "\":\"" << get_lag_count() << "\"";

        return ss.str();
    }

    std::vector<double> to_vector() const
    {
        // TODO
        return std::vector<double>();
    }
};

struct TuneSVRParameters
{
    bool svr_C{false};
    bool svr_epsilon{false};
    bool svr_kernel_param{false};
    bool svr_kernel_param2{false};
    bool svr_adjacent_levels_ratio{false};
    bool lag_count{false};
    bool kernel_type{false};
};

struct Bounds
{
    datamodel::SVRParameters min_bounds;
    datamodel::SVRParameters max_bounds;
    TuneSVRParameters is_tuned;

    std::vector<double> to_vector(const bound_type & bt) const
    {
        const datamodel::SVRParameters * current_parameters;
        switch(bt)
        {
            case bound_type::min :
                current_parameters = &min_bounds;
                break;
            case bound_type::max :
                current_parameters = &max_bounds;
                break;
        }

        std::vector<double> result;
        result.push_back(current_parameters->get_svr_C());
        result.push_back(current_parameters->get_svr_epsilon());
        result.push_back(current_parameters->get_svr_kernel_param());
        result.push_back(current_parameters->get_svr_kernel_param2());
        result.push_back(current_parameters->get_svr_adjacent_levels_ratio());
        result.push_back(static_cast<double>(current_parameters->get_lag_count()));

        return result;
    }
};

} // datamodel namespace
} // svr namespace

using SVRParameters_ptr = std::shared_ptr<svr::datamodel::SVRParameters>;
