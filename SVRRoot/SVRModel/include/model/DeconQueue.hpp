#pragma once

#include <common/types.hpp>
#include <util/StringUtils.hpp>
#include "DBTable.hpp"
#include "DataRow.hpp"
#include "StoreBufferPushMerge.hpp"

namespace svr {
namespace datamodel {

class DeconQueue;
using DeconQueue_ptr = std::shared_ptr<DeconQueue>;

class DeconQueue: public Queue
{
private:
    std::string input_queue_table_name_; // TODO Replace with pointer to Input Queue
    std::string input_queue_column_name_; // TODO Replace with pointer to Input Queue
    bigint dataset_id_; // TODO Replace with pointer to Dataset

    void reinit_table_name();

public:

    DeconQueue();

    DeconQueue(
            std::string const &table_name,
            std::string const &input_queue_table_name_,
            std::string const &input_queue_column_name_,
            const bigint dataset_id_,
            const DataRowContainer &data = DataRowContainer());

    DeconQueue_ptr clone_empty() const;

    inline std::string get_input_queue_table_name() const
    {
        return input_queue_table_name_;
    }
    void set_input_queue_table_name(const std::string &input_queue_table_name_);

    inline std::string get_input_queue_column_name() const
    {
        return input_queue_column_name_;
    }
    void set_input_queue_column_name(const std::string &input_queue_column_name_);

    inline bigint get_dataset_id() const
    {
        return dataset_id_;
    }
    void set_dataset_id(bigint dataset_id_);

    virtual std::string metadata_to_string() const override;

    bool operator==(const DeconQueue &other) const;
    
    std::string data_to_string() const;
    
    std::string data_to_string(size_t data_size) const;
    
    static std::string make_queue_table_name(const std::string &input_queue_table_name_, const bigint dataset_id_, const std::string &input_queue_column_name_);
};

}
}

template<>
inline void store_buffer_push_merge<svr::datamodel::DeconQueue_ptr>(svr::datamodel::DeconQueue_ptr &dest, svr::datamodel::DeconQueue_ptr const &src)
{
    for(auto const &row: src->get_data())
        dest->get_data().insert(row);
    dest->set_table_name(src->get_table_name());
    dest->set_input_queue_table_name(src->get_input_queue_table_name());
    dest->set_input_queue_column_name(src->get_input_queue_column_name());
    dest->set_dataset_id(src->get_dataset_id());
}
