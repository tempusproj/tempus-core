/******************************************************************************
 *                       ONLINE SUPPORT VECTOR REGRESSION                      *
 *                      Copyright 2006 - Francesco Parrella                    *
 *                                                                             *
 *This program is distributed under the terms of the GNU General Public License*
 ******************************************************************************/

#pragma once

#include <memory>
#include <iostream>
#include <fstream>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <algorithm>

#include "viennacl/scalar.hpp"
#include "viennacl/vector.hpp"
#include "viennacl/matrix.hpp"
#include "viennacl/matrix_proxy.hpp"
#include "viennacl/linalg/inner_prod.hpp"
#include "viennacl/linalg/norm_1.hpp"
#include "viennacl/linalg/prod.hpp"
#include "viennacl/linalg/maxmin.hpp"
#include "viennacl/linalg/sum.hpp"


namespace svr {
namespace datamodel {


template<class T>
class Vector {
public:

    // Initialization
    Vector();
    explicit Vector(int Length);
    explicit Vector(const Vector<T> *X);
    explicit Vector(const std::vector<T>& X);
    Vector(const Vector<T> &v);
    Vector(T *X, int N);
    Vector(const Vector<T> *X, int N);
    Vector(T X, int N);
    Vector(const viennacl::vector<T>& X, int N);

    ~Vector();

    bool operator==(Vector<T> const &)const;

    void copy_to(std::vector<T>& stl_vec);
    std::shared_ptr<viennacl::vector<T> > vcl();
    Vector<T> *Clone() const;

    int GetLength() const;

    T GetValue(int Index) const;

    void SetValue(int Index, T Value);

    bool Contains(T Value) const;

    // Add/Remove Operations
    void Clear();

    void Add(T X);

    void AddAt(T X, int Index);

    void RemoveAt(int Index);

    Vector<T> *Extract(int FromIndex, int ToIndex);

    // Pre-built Vectors
    static Vector<double> *ZeroVector(int Length);

    static Vector<double> *RandVector(int Length);

    static Vector<T> *GetSequence(T Start, T Step, T End);

    // Mathematical Operations
    void SumScalar(T X);

    void ProductScalar(T X);

    void DivideScalar(T X);

    void PowScalar(T X);
    void SquareScalar();

    void SumVector(Vector<T> *V);

    static Vector<T> *SumVector(Vector<T> *V1, Vector<T> *V2);

    void SubtractVector(Vector<T> *V);

    static Vector<T> *SubtractVector(Vector<T> *V1, Vector<T> *V2);

    void ProductVector(Vector<T>& V);
    static Vector<T> ProductVector(Vector<T>& V1, Vector<T>& V2);

    T ProductVectorScalar(Vector<T> *V);
    static T ProductVectorScalar(Vector<T> *V1, Vector<T> *V2);

    T Sum();

    T AbsSum();

    Vector<T>* AbsList();

    void Abs();

    // Comparison Operations
    T Min();

    void Min(T *MinValue, int *MinIndex);

    T MinAbs();

    void MinAbs(T *MinValue, int *MinIndex);

    T Max();

    void Max(T *MaxValue, int *MaxIndex);

    T MaxAbs();

    void MaxAbs(T *MaxValue, int *MaxIndex);

    T Mean();

    T MeanAbs();

    T Variance();

    // Sorting Operations
    void Sort();

    void RemoveDuplicates();

    int Find(T X);

    // I/O Operations
    static Vector<T> *Load(const char *Filename);

    void Save(const char *Filename);

    void Print();

    void Print(char *VectorName);

    // Operators Redefinition
    T& operator[](int Index);
    const T& operator[](int Index) const;
    void operator=(const viennacl::vector<T>& vcl);

    void Resize();
    void Resize(size_t NewSize);
    void Resize(size_t NewSize, T FillValue);

private:
    void update_values_ptr();
    size_t new_size(size_t size);

    viennacl::vector<T> Values;
    int Length;
    int MaxLength;

    // TODO: Raw ptr to viennacl vector data chunk
    // TODO: Ensure only host memory data pointed, but not GPU memory...
    T* values_ptr;

}; // Vector



} // model
} // svr
