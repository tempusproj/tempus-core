#pragma once

#include "model/Entity.hpp"
#include "model/Priority.hpp"
#include "model/Ensemble.hpp"
#include "model/InputQueue.hpp"
#include "model/IQScalingFactor.hpp"
#include "model/DQScalingFactor.hpp"
#include "util/StringUtils.hpp"
#include <algorithm>
#include <unordered_map>

namespace svr{
namespace datamodel{

static const std::vector<std::string> swt_wavelet_names
    // 0 - 14
    {"db1", "db2", "db3", "db4", "db5", "db6", "db7", "db8", "db9", "db10", "db11", "db12", "db13", "db14", "db15",
    // 15 - 29
     "bior1.1", "bior1.3", "bior1.5", "bior2.2", "bior2.4", "bior2.6", "bior2.8",
     "bior3.1", "bior3.3", "bior3.5", "bior3.7", "bior3.9", "bior4.4", "bior5.5", "bior6.8",
    // 30 - 34
     "coif1", "coif2", "coif3", "coif4", "coif5",
    // 35 - 44
     "sym1", "sym2", "sym3", "sym4", "sym5", "sym6", "sym7", "sym8", "sym9", "sym10",
    // 45 - 54
     "sym11", "sym12", "sym13", "sym14", "sym15", "sym16", "sym17", "sym18", "sym19", "sym20"};

typedef std::map<
/* input queue table name */ std::pair<std::string,
/* input queue column name */ std::string>,
/* svr parameters */ std::vector<SVRParameters_ptr> >
    ensembles_svr_parameters_t;

class Dataset : public Entity{

private:
    Ensemble_ptr null_ensemble = std::shared_ptr<Ensemble>(nullptr);

    std::string dataset_name_;
    std::string user_name_;
    InputQueue_ptr p_input_queue_;
    std::vector<InputQueue_ptr> aux_input_queues_;
    Priority priority_;
    std::string description_;
    size_t swt_levels_;
    std::string swt_wavelet_name_;
    bpt::time_duration max_lookback_time_gap_;
    std::vector<Ensemble_ptr> ensembles_;
    bool is_active_;
    std::vector<IQScalingFactor_ptr> iq_scaling_factors_;
    std::vector<DQScalingFactor_ptr> dq_scaling_factors_;
    ensembles_svr_parameters_t ensembles_svr_parameters_;

    void init_ensembles_svr_parameters(const ensembles_svr_parameters_t ensembles_svr_parameters, bigint id, size_t swt_levels);

public:

    Dataset() : Entity() {}

    Dataset(
            bigint id,
            const std::string &dataset_name_,
            const std::string &user_name_,
            const InputQueue_ptr &p_input_queue,
            const std::vector<InputQueue_ptr> &aux_input_queues_,
            const Priority &priority_,
            const std::string &description_,
            size_t swt_levels_,
            const std::string& swt_wavelet_name_,
            const bpt::time_duration& max_lookback_time_gap_ = bpt::hours(23),
            const std::vector<Ensemble_ptr> &ensembles_ = std::vector<Ensemble_ptr>(),
            bool is_active_ = false,
            const std::vector<IQScalingFactor_ptr> iq_scaling_factors = std::vector<IQScalingFactor_ptr>(),
            const std::vector<DQScalingFactor_ptr> dq_scaling_factors = std::vector<DQScalingFactor_ptr>(),
            const ensembles_svr_parameters_t ensembles_svr_parameters = ensembles_svr_parameters_t()
            );

    Dataset(
            bigint id,
            const std::string &dataset_name_,
            const std::string &user_name_,
            const std::string &input_queue_table_name,
            const std::vector<std::string> &aux_input_queues_table_names,
            const Priority &priority_,
            const std::string &description_,
            size_t swt_levels_,
            const std::string& swt_wavelet_name_,
            const bpt::time_duration& max_lookback_time_gap_ = bpt::hours(23),
            const std::vector<Ensemble_ptr> &ensembles_ = std::vector<Ensemble_ptr>(),
            bool is_active_ = false,
            const std::vector<IQScalingFactor_ptr> iq_scaling_factors = std::vector<IQScalingFactor_ptr>(),
            const std::vector<DQScalingFactor_ptr> dq_scaling_factors = std::vector<DQScalingFactor_ptr>(),
            const ensembles_svr_parameters_t ensembles_svr_parameters = ensembles_svr_parameters_t()
            );

    Dataset(const Dataset &dataset);

    bool operator == (const Dataset& other) const;

    void set_dataset_id(bigint id);

    std::string get_dataset_name() const {
        return dataset_name_;
    }

    void set_dataset_name(const std::string &dataset_name) {
        Dataset::dataset_name_ = dataset_name;
    }

    std::string get_user_name() const {
        return user_name_;
    }

    void set_user_name(const std::string &user_name) {
        this->user_name_ = user_name;
    }

    const InputQueue_ptr &get_input_queue() const {
        return p_input_queue_;
    }

    InputQueue_ptr &get_input_queue() {
        return p_input_queue_;
    }

    void set_input_queue(InputQueue_ptr p_input_queue) {
        this->p_input_queue_ = p_input_queue;
    }

    std::vector<InputQueue_ptr>& get_aux_input_queues() {
        return aux_input_queues_;
    }

    const std::vector<InputQueue_ptr>& get_aux_input_queues() const {
        return aux_input_queues_;
    }

    void set_aux_input_queues(const std::vector<InputQueue_ptr> &aux_input_queues) {
        this->aux_input_queues_ = aux_input_queues;
    }

    std::vector<std::string> get_aux_input_queues_table_names() const {
        std::vector<std::string> table_names;
        for (InputQueue_ptr p_input_queue : aux_input_queues_)
            table_names.push_back(p_input_queue->get_table_name());
        return table_names;
    }

    Priority const &get_priority() const {
        return priority_;
    }

    void set_priority(Priority const &priority) {
        this->priority_ = priority;
    }

    std::string get_description() const {
        return description_;
    }

    void set_description(const std::string &description) {
        this->description_ = description;
    }

    size_t get_swt_levels() const {
        return swt_levels_;
    }

    void set_swt_levels(size_t swt_levels) {
        this->swt_levels_ = swt_levels;
    }

    std::string get_swt_wavelet_name() const {
        return this->swt_wavelet_name_;
    }

    void set_swt_wavelet_name(const std::string& swt_wavelet_name) {
        assert(validate_swt_wavelet_name(swt_wavelet_name));
        this->swt_wavelet_name_ = swt_wavelet_name;
    }

    bool validate_swt_wavelet_name(const std::string& swt_wavelet_name) const {
        return std::find(swt_wavelet_names.begin(), swt_wavelet_names.end(), swt_wavelet_name) != swt_wavelet_names.end();
    }
    
    const bpt::time_duration& get_max_lookback_time_gap() const {
        return max_lookback_time_gap_;
    }

    void set_max_lookback_time_gap(const bpt::time_duration& max_lookback_time_gap) {
        this->max_lookback_time_gap_ = max_lookback_time_gap;
    }

    std::vector<Ensemble_ptr>& get_ensembles()
    {
        return ensembles_;
    }

    Ensemble_ptr &get_ensemble(const std::string &column_name)
    {
        for (Ensemble_ptr &p_ensemble: this->ensembles_)
            if (p_ensemble->get_decon_queue()->get_input_queue_column_name() == column_name)
                return p_ensemble;
        return null_ensemble;
    }

    void set_ensembles(const std::vector<Ensemble_ptr> &ensembles)
    {
        this->ensembles_ = ensembles;
        for (const Ensemble_ptr &p_ensemble: this->ensembles_) p_ensemble->set_dataset_id(get_id());
    }

    bool get_is_active() const {
        return is_active_;
    }

    void set_is_active(bool is_active) {
        this->is_active_ = is_active;
    }

    std::vector<IQScalingFactor_ptr> get_iq_scaling_factors()
    {
        return iq_scaling_factors_;
    }

    void set_iq_scaling_factors(const std::vector<IQScalingFactor_ptr>& iq_scaling_factors)
    {
        iq_scaling_factors_ = iq_scaling_factors;
    }

    std::vector<DQScalingFactor_ptr> &get_dq_scaling_factors()
    {
        return dq_scaling_factors_;
    }

    void set_dq_scaling_factors(const std::vector<DQScalingFactor_ptr>& dq_scaling_factors)
    {
        dq_scaling_factors_ = dq_scaling_factors;
    }
    
    void set_ensembles_svr_parameters(const ensembles_svr_parameters_t &ensembles_svr_parameters)
    {
        ensembles_svr_parameters_ = ensembles_svr_parameters;
    }

    ensembles_svr_parameters_t& get_ensembles_svr_parameters()
    {
        return ensembles_svr_parameters_;
    }

    const ensembles_svr_parameters_t& get_ensembles_svr_parameters() const
    {
        return ensembles_svr_parameters_;
    }

    virtual std::string to_string() const override;

    std::string parameters_to_string() const;
};


}
}

using Dataset_ptr = std::shared_ptr<svr::datamodel::Dataset>;
