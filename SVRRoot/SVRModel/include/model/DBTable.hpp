#pragma once

#include <string>
#include <memory>

#include "DataRow.hpp"
#include "Vector.h"

#define DISPLAY_ROWS_LIMIT (25)


namespace svr {
namespace datamodel {

class Queue {
protected:
    std::string table_name_;
    DataRowContainer data_;

public:
    Queue() = default;

    Queue(const Queue &rhs);

    Queue(const std::string& table_name, const DataRowContainer &data = DataRowContainer());

    inline const std::string& get_table_name() const {return table_name_;}
    void set_table_name(const std::string& tableName);

    virtual std::string to_string() const;

    std::string data_to_string() const;
    virtual std::string metadata_to_string() const = 0;

    void update_data(const DataRow::Container &data, const bool overwrite = true);

    inline const DataRow::Container& get_data() const { return data_; }
    inline DataRow::Container& get_data() { return data_; }
    inline void set_data(const DataRow::Container& data) { this->data_ = data; }

    bool get_column_values(
            const size_t column_index,
            svr::datamodel::Vector<double> &output_values,
            const bpt::time_period range)
    {
        if (this->data_.empty() || this->data_.begin()->second->get_values().size() <= column_index) {
            LOG4_WARN("No data for column index " << column_index);
            return false;
        }

        auto iter_start = this->data_.lower_bound(range.begin());
        if (iter_start == this->data_.end()) {
            LOG4_WARN("Couldn't find range " << range);
            return false;
        }
        for_each(iter_start,
                 this->data_.lower_bound(range.end()),
                 [&column_index, &output_values](const std::pair<bpt::ptime, DataRow_ptr> &row_pair) { output_values.Add(row_pair.second->get_values()[column_index]); }
                );

        return true;
    }

    inline void trim(const bpt::ptime &start_time, const bpt::ptime &end_time)
    {
        data_.erase(data_.begin(), data_.lower_bound(start_time));
        data_.erase(data_.upper_bound(end_time), data_.end());
    }

    inline void trim(const bpt::time_period &time_range)
    {
        data_.erase(data_.begin(), data_.lower_bound(time_range.begin()));
        data_.erase(data_.upper_bound(time_range.end()), data_.end());
    }

    inline std::vector<double> get_tick_volume() const
    {
        std::vector<double> result;
        for(const auto& row: data_)
            result.push_back(row.second->get_tick_volume());
        return result;
    }

};

} /* namespace model */
} /* namespace svr */
