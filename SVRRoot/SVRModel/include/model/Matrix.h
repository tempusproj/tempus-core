/******************************************************************************
 *                       ONLINE SUPPORT VECTOR REGRESSION                      *
 *                      Copyright 2006 - Francesco Parrella                    *
 *                                                                             *
 *This program is distributed under the terms of the GNU General Public License*
 ******************************************************************************/

#pragma once

#include <iostream>
#include "model/Vector.tcc"

namespace svr {
namespace datamodel {

template<class T>
class Matrix {
public:
    // Attributes
    std::vector<std::shared_ptr<Vector<T> > > Values;

    // Initialization
    Matrix();

    Matrix(double **X, int Rows, int Cols);

    Matrix(const Matrix<T> &m);

    ~Matrix();

    bool operator==(Matrix<T> const &) const;

    viennacl::matrix<T> ViennaClone() const;
    void ViennaClone(viennacl::matrix<T> & vcl_matrix) const;

    Matrix<T> *Clone();

    int GetLengthRows() const;

    int GetLengthCols() const;

    // Selection Operations
    Vector<T> *GetRowRef(int Index) const;

    Vector<T> *GetRowCopy(int Index);

    Vector<T> *GetColCopy(int Index);

    T GetValue(int RowIndex, int ColIndex);

    void SetValue(int RowIndex, int ColIndex, T Value);

    int IndexOf(Vector<T> *V);

    // Add/Remove Operations
    void Clear();

    void AddRowRef(T *V, int N);

    void AddRowRef(Vector<T> *V);

    void AddRowRefAt(Vector<T> *V, int Index);

    void AddRowCopy(const Vector<T> *V);

    void AddRowCopy(const Vector<T> &V);

    void AddRowCopy(const T *V, int N);

    void AddRowCopyAt(Vector<T> *V, int Index);

    void AddRowCopyAt(T *V, int N, int Index);

    void AddColCopy(Vector<T> *V);

    void AddColCopy(T *V, int N);

    void AddColCopyAt(Vector<T> *V, int Index);

    void AddColCopyAt(T *V, int N, int Index);

    void RemoveRow(int Index);

    void RemoveCol(int Index);

    Matrix<T> *ExtractRows(int FromRowIndex, int ToRowIndex);

    Matrix<T> *ExtractCols(int FromColIndex, int ToColIndex);

    // Pre-built Matrix
    static Matrix<double> *ZeroMatrix(int RowsNumber, int ColsNumber);

    static Matrix<double> *RandMatrix(int RowsNumber, int ColsNumber);

    // Mathematical Operations
    void SumScalar(T X);

    void ProductScalar(T X);

    void DivideScalar(T X);

    void PowScalar(T X);

    void SumMatrix(Matrix<T> *M);

    void SubtractMatrix(Matrix<T> *M);

    Vector<T> *ProductVector(Vector<T> *V);

    static Vector<T> *ProductVector(Matrix *M, Vector<T> *V);

    static Matrix<T> *ProductVectorVector(Vector<T> *V1, Vector<T> *V2);

    static Matrix<T> *ProductMatrixMatrix(Matrix<T> *M1, Matrix<T> *M2);

    // I/O Operations
    static Matrix<double> *Load(char const *Filename);

    void Save(char const * Filename);

    void Print();

    void Print(char *MatrixName);

    // Operators Redefinition
    Vector<T> operator[](int Index);

private:
    // Private Attributes
    int StepSize;

    // Private Methods
    void Resize();

    void Resize(int NewRowNumber, int NewColNumber);
};

}
}
