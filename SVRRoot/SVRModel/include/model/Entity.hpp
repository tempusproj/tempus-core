#pragma once

#include "common/types.hpp"

namespace svr{
namespace datamodel{

class Entity
{
	bigint id;

public:
    Entity() : id(0){}
	Entity(bigint id) : id(id){}
	virtual ~Entity(){}

	bigint get_id() const {return id;}

	void set_id(bigint id) {this->id = id;}

	virtual std::string to_string() const = 0;
};

}
}
