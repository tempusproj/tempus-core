#pragma once

#include "model/Entity.hpp"
#include "model/Model.hpp"
#include "model/DeconQueue.hpp"
#include "model/SVRParameters.hpp"
#include "util/StringUtils.hpp"


namespace svr{
namespace datamodel{

class Ensemble : public Entity{

private:
    bigint dataset_id; /* TODO Replace with pointer to a dataset */
//    std::vector<SVRParameters_ptr> vec_svr_parameters;
    std::vector<Model_ptr> models;
    DeconQueue_ptr p_decon_queue;
    std::vector<DeconQueue_ptr> aux_decon_queues;

public:
    bool operator==(Ensemble const & o) {return dataset_id==o.dataset_id && models==o.models && p_decon_queue==o.p_decon_queue && aux_decon_queues==o.aux_decon_queues;}

    Ensemble() : Entity() {
    }

    Ensemble(bigint id,
             bigint dataset_id,
             std::string decon_queue_table_name,
             std::vector<std::string> aux_decon_queue_table_names) : Entity(id), dataset_id(dataset_id)
    {
        if (!decon_queue_table_name.empty()) {
            p_decon_queue = std::make_shared<DeconQueue>(DeconQueue());
            p_decon_queue->set_table_name(decon_queue_table_name);
        }
        for (const std::string &aux_decon_queue_table_name : aux_decon_queue_table_names) {
            DeconQueue_ptr p_aux_decon_queue = std::make_shared<DeconQueue>(DeconQueue());
            p_aux_decon_queue->set_table_name(aux_decon_queue_table_name);
            aux_decon_queues.push_back(p_aux_decon_queue);
        }
    }

    Ensemble(bigint id, bigint dataset_id, std::vector<Model_ptr> models,
             DeconQueue_ptr p_decon_queue, std::vector<DeconQueue_ptr> aux_decon_queues = std::vector<DeconQueue_ptr>())
            : Entity(id), dataset_id(dataset_id), models(models),
              p_decon_queue(p_decon_queue), aux_decon_queues(aux_decon_queues)
    {
        p_decon_queue->set_dataset_id(dataset_id);
    }

    void set_dataset_id(bigint dataset_id) {
        this->dataset_id = dataset_id;
        if (this->p_decon_queue != nullptr) {
            this->p_decon_queue->set_dataset_id(dataset_id);
        }
    }

    bigint get_dataset_id() const {
        return dataset_id;
    }

//    std::vector<SVRParameters_ptr>& get_vec_svr_parameters()
//    {
//        return vec_svr_parameters;
//    }

//    const std::vector<SVRParameters_ptr>& get_vec_svr_parameters() const
//    {
//        return vec_svr_parameters;
//    }

//    void set_vec_svr_parameters(const std::vector<SVRParameters_ptr> &vec_svr_parameters)
//    {
//        this->vec_svr_parameters = vec_svr_parameters;
//        for (SVRParameters_ptr p_svr_parameters : vec_svr_parameters) {
//            p_svr_parameters->set_ensemble_id(get_id());
//        }
//    }

    std::vector<Model_ptr>& get_models() {
        return models;
    }

    const std::vector<Model_ptr>& get_models() const {
        return models;
    }

    std::vector<bigint> get_models_ids() {
        std::vector<bigint> res;
        res.reserve(models.size());
        for (Model_ptr model : models) {
            res.push_back(model->get_id());
        }
        return res;
    }

    void set_models(std::vector<Model_ptr> models) {
        this->models = models;
    }

    DeconQueue_ptr &get_decon_queue() {
        return p_decon_queue;
    }

    void set_decon_table(DeconQueue_ptr p_decon_queue) {
        this->p_decon_queue = p_decon_queue;
    }

    std::vector<DeconQueue_ptr>& get_aux_decon_queues() {
        return aux_decon_queues;
    }

    const std::vector<DeconQueue_ptr>& get_aux_decon_queues() const {
        return aux_decon_queues;
    }

    std::vector<std::string> get_aux_decon_table_names() const {
        std::vector<std::string> res;
        res.reserve(aux_decon_queues.size());
        std::transform(aux_decon_queues.begin(), aux_decon_queues.end(),
                       std::back_inserter(res),
                       [](const DeconQueue_ptr &decon){return decon->get_table_name();});
        return res;
    }

    void set_aux_decon_queues(const std::vector<DeconQueue_ptr> &aux_decon_queues)
    {
        this->aux_decon_queues = aux_decon_queues;
    }

    std::pair<std::string, std::string> get_key_pair()
    {
        return std::make_pair(this->p_decon_queue->get_input_queue_table_name(),
                              this->p_decon_queue->get_input_queue_column_name());
    }

    virtual std::string to_string() const override{
        std::stringstream ss;

        ss <<  "Ensemble ID: " << get_id()
           << "Models: ";
        for (Model_ptr model : models) {
            ss << model->to_string() << " ";
        }
        ss      << "DeconQueue: " << p_decon_queue->to_string()
                << "AuxDeconQueues: ";
        for (DeconQueue_ptr p_aux_decon_queue : aux_decon_queues) {
            ss << p_aux_decon_queue->to_string() << " ";
        }
        return ss.str();

    }

};


}
}

using Ensemble_ptr = std::shared_ptr<svr::datamodel::Ensemble>;
