#pragma once

#include <model/Entity.hpp>

namespace svr{
namespace datamodel{

class MultivalRequest : public Entity{
public:

    MultivalRequest() : Entity() {}
    MultivalRequest( bigint request_id, std::string user_name, bigint dataset_id, bpt::ptime request_time,
        bpt::ptime value_time_start, bpt::ptime value_time_end, size_t resolution, std::string const & value_columns )
    : Entity(request_id), dataset_id(dataset_id), user_name(std::move(user_name)), request_time(request_time),
        value_time_start(value_time_start), value_time_end(value_time_end), resolution(resolution), value_columns(value_columns) {}

    bigint dataset_id;
    std::string user_name;
    bpt::ptime request_time;
    bpt::ptime value_time_start;
    bpt::ptime value_time_end;
    size_t resolution;
    std::string value_columns;


    virtual std::string to_string() const{
        std::stringstream ss;
        ss  << "RequestID: " << get_id()
            << ", User: " << user_name
            << ", DatasetID: " << dataset_id
            << ", Request Time: " << bpt::to_simple_string(request_time)
            << ", Value time start: " << bpt::to_simple_string(value_time_start)
            << ", Value time end: " << bpt::to_simple_string(value_time_end)
            << ", Resolution: " << resolution
            << ", value columns" << value_columns;


        return ss.str();
    }

    bool operator==(MultivalRequest const & o) const {return dataset_id==o.dataset_id && user_name==o.user_name && request_time==o.request_time
            && value_time_start==o.value_time_start && value_time_end==o.value_time_end && resolution==o.resolution && value_columns==o.value_columns; }

};

class MultivalResponse : public Entity{
public:
    bigint request_id;
    bpt::ptime value_time;
    std::string value_column;
    double value;

    MultivalResponse(): Entity() {}
    MultivalResponse(bigint response_id, bigint request_id, bpt::ptime value_time, std::string value_column, double value)
    : Entity(response_id), request_id(request_id), value_time(std::move(value_time)), value_column(std::move(value_column)), value(value) {}

    virtual std::string to_string() const{
        std::stringstream ss;
        ss  << "ResponseID: " << get_id()
            << ", RequestID: " << request_id
            << ", ValueTime: " << bpt::to_simple_string(value_time)
            << ", ValueColumn: " << value_column
            << ", Value: " << value
        ;
        return ss.str();
    }

    bool operator==(MultivalResponse const & o) const {return request_id==o.request_id && value_time==o.value_time
            && value_column==o.value_column && value==o.value; }
};

class ValueRequest : public Entity{
public:
    bpt::ptime request_time;
    bpt::ptime value_time;
    std::string value_column;

    bool operator==(ValueRequest const & o){ return request_time == o.request_time && value_time == o.value_time && value_column == o.value_column; }

    virtual std::string to_string() const{
        std::stringstream ss;
        ss  << "ResponseID: " << get_id()
            << ", RequestTime: " << bpt::to_simple_string(request_time)
            << ", value_time: " << bpt::to_simple_string(value_time)
            << ", ValueColumn: " << value_column
        ;
        return ss.str();
    }
};

} // namespace datamodel
} // namespace svr

using MultivalRequest_ptr = std::shared_ptr<svr::datamodel::MultivalRequest>;
using MultivalResponse_ptr = std::shared_ptr<svr::datamodel::MultivalResponse>;
using ValueRequest_ptr = std::shared_ptr<svr::datamodel::ValueRequest>;

