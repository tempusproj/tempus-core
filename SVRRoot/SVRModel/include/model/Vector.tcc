#pragma once
#include "model/Vector.h"
#include "common/Logging.hpp"
#include "util/MathUtils.hpp"
#include <cmath>
#include <limits>
#include <thread>
#include <future>
#include <unordered_set>

namespace svr{
namespace datamodel{

////////////////////////////
// METHODS IMPLEMENTATION //
////////////////////////////

static const int step_size = 64;

// INITIALIZATION
template<class T>
Vector<T>::Vector()
: Vector(step_size)
{
    Length = 0;
}

template<class T>
Vector<T>::Vector(T *X, int N)
: Vector(N)
{
    Length = N;
    std::memcpy(&(values_ptr[0]),
                &(X[0]),
                sizeof(T) * N);
}

template<class T>
Vector<T>::Vector(const Vector<T> & v)
: Vector(&v)
{
}

template<class T>
Vector<T>::Vector(const Vector<T> *X)
: Vector(X, X->Length)
{
}

template<class T>
Vector<T>::Vector(const Vector<T> *X, int N)
: Values(X->Values.size(), viennacl::context(viennacl::MAIN_MEMORY)),
  Length(N), MaxLength(X->MaxLength)
{
    Length = N;
    if (!X->Values.empty()) {
        this->Values = X->Values;
    }
    update_values_ptr();
}

template<class T>
Vector<T>::Vector(const std::vector<T>& X)
: Vector(X.size())
{
    Length = X.size();
    viennacl::fast_copy(X.begin(), X.end(), this->Values.begin());
}

template<class T>
Vector<T>::Vector(const viennacl::vector<T>& X, int N)
: Values(X.size(), viennacl::context(viennacl::MAIN_MEMORY)),
  Length(N), MaxLength(X.size())
{
    if (!X.empty()) {
        this->Values = X;
    }
    Resize(X.size());
}

template<class T>
Vector<T>::Vector(T X, int N)
: Vector(N)
{
    Length = N;
    std::vector<T> cpu_vec(N, X);
    viennacl::fast_copy(cpu_vec.begin(), cpu_vec.end(), this->Values.begin());
}

template<class T>
Vector<T>::Vector(int Length)
: Values(new_size(Length), viennacl::context(viennacl::MAIN_MEMORY)), Length(0), MaxLength(new_size(Length))
{
    update_values_ptr();
}

template<class T>
Vector<T>::~Vector() {
    //this->Clear();
}

namespace
{

    template<class T>
    bool comparer(T const & t1, T const & t2)
    {
        return t1 == t2;
    }

    template<>
    bool comparer(double const & t1, double const & t2)
    {
        return fabs(t1 - t2) < std::numeric_limits<double>::epsilon();
    }
}

template<class T>
bool Vector<T>::operator==(Vector<T> const & o) const
{
    bool result = Length == o.Length;
    if(!result)
        return false;

    for(size_t i = 0; i < size_t(Length); ++i)
        if(!comparer(Values[i], o.Values[i]))
            return false;

    return true;
}

#define THIS_VCL VCL((*this))
#define VCL(V) viennacl::vector_base<T>(V.Values.handle(), V.GetLength(), 0, 1)

template<class T>
std::shared_ptr<viennacl::vector<T> > Vector<T>::vcl()
{
    // Makes a clone without actual copy (using the same memory chunk).
    return std::make_shared<viennacl::vector<T> >(THIS_VCL);
}

template<class T>
void Vector<T>::copy_to(std::vector<T>& stl_vec)
{
    stl_vec.resize(GetLength());
    viennacl::vector_base<T> vcl(Values.handle(), GetLength(), 0, 1);
    viennacl::fast_copy(vcl.begin(), vcl.end(), stl_vec.begin());
}

template<class T>
Vector<T> *Vector<T>::Clone() const {
    return new Vector<T>(this, this->Length);
}

template<class T>
int Vector<T>::GetLength() const {
    return this->Length;
}

template<class T>
void Vector<T>::Resize() {
    this->Resize((this->MaxLength + step_size) * 2); // Multiply by 2 like stl vector does.
}

template<class T>
void Vector<T>::Resize(size_t size) {
    size = new_size(size);
    this->Values.resize(size);
    this->MaxLength = size;
    update_values_ptr();
}

template<class T>
void Vector<T>::Resize(size_t new_size, T fill_value) {
    size_t old_size = Values.size();
    Resize(new_size);
    if (new_size > old_size) {
        std::vector<T> cpu_vec(new_size - old_size, fill_value);
        viennacl::fast_copy(cpu_vec.begin(), cpu_vec.end(), std::next(this->Values.begin(), old_size));
    }
}

template<class T>
size_t Vector<T>::new_size(size_t size)
{
    return (size / step_size + 1) * step_size;
}

template<class T>
void Vector<T>::update_values_ptr()
{
    values_ptr = reinterpret_cast<T*>(this->Values.handle().ram_handle().get());
}

template<class T>
T Vector<T>::GetValue(int Index) const {
    if (Index < 0 || Index >= this->Length) {
        throw std::runtime_error("Vector index out of bounds");
    }
    return values_ptr[Index];
}

template<class T>
void Vector<T>::SetValue(int Index, T Value) {
    if (Index < 0 || Index >= this->Length) {
        throw std::runtime_error("Vector index out of bounds");
    }
    values_ptr[Index] = Value;
}

template<class T>
bool Vector<T>::Contains(T Value) const {
    for (int i = 0; i < Length; i++) {
        if (values_ptr[i] == Value)
            return true;
    }
    return false;
}

// Add/Remove Operations
template<class T>
void Vector<T>::Clear() {
    Length = 0;
}

template<class T>
void Vector<T>::Add(T X) {
    if (this->Length == this->MaxLength) {
        this->Resize();
    }

    values_ptr[this->Length++] = X;
}

template<class T>
void Vector<T>::AddAt(T X, int Index) {
    if (Index >= 0 && Index <= this->Length) {
        if (this->Length == this->MaxLength) {
            this->Resize();
        }

        std::memmove(&(values_ptr[Index + 1]),
                    &(values_ptr[Index]),
                    sizeof(T) * (Length - Index));
        values_ptr[Index] = X;
        this->Length++;
    } else {
        LOG4_ERROR(
                "Error! It's impossible to add an element in an invalid index.");
    }
}

template<class T>
void Vector<T>::RemoveAt(int Index) {
    if (this->Length > 0 && Index >= 0 && Index < this->Length) {
        std::memmove(&(values_ptr[Index]),
                    &(values_ptr[Index + 1]),
                    sizeof(T) * (Length - Index - 1));
        this->Length--;
    } else {
        LOG4_ERROR(
                "Error! It's impossible to remove an element from the vector that doesn't exist.");
    }
}

template<class T>
Vector<T> *Vector<T>::Extract(int FromIndex, int ToIndex) {
    if (FromIndex >= 0 && ToIndex <= this->Length - 1 && FromIndex <= ToIndex) {
        Vector<T> *V = new Vector<T>(std::vector<T>(values_ptr + FromIndex, values_ptr + ToIndex + 1));
        return V;
    } else {
        LOG4_ERROR(
                "Error! It's impossible to extract the vector: invalid indexes");
        return new Vector<T>();
    }
}

// Pre-built Vectors
template<class T>
Vector<double> *Vector<T>::ZeroVector(int Length) {
    Vector<double> *V = new Vector<double>(0.0, Length);
    return V;
}

template<class T>
Vector<double> *Vector<T>::RandVector(int Length) {
    Vector<double> *V = new Vector<double>(Length);
    for (int i = 0; i < Length; i++)
        V->Add(static_cast<double>(rand()) / static_cast<double>(RAND_MAX));

    return V;
}

template<class T>
Vector<T> *Vector<T>::GetSequence(T Start, T Step, T End) {
    Vector<T> *V = new Vector<T>();
    if (Start < End) {
        for (T i = Start; i <= End; i += Step) {
            V->Add(i);
        }
    } else {
        for (T i = Start; i >= End; i -= Step) {
            V->Add(i);
        }
    }
    return V;
}

// Mathematical Operations
template<class T>
void Vector<T>::SumScalar(T X) {
    Vector v2(X, this->GetLength());
    SumVector(&v2);
}

template<class T>
void Vector<T>::ProductScalar(T X) {
    THIS_VCL *= X;
}

template<class T>
void Vector<T>::DivideScalar(T X) {
    if (X != 0) {
        THIS_VCL /= X;
    } else {
        for (int i = 0; i < this->GetLength(); i++) {
            this->Values[i] = svr::common::SIGN<T>(this->Values[i]) * INF;
        }
    }
}

template<class T>
void Vector<T>::PowScalar(T X)
{
    // TODO: Check for better element wise power...
    Vector<T> v2(X, GetLength());
    THIS_VCL = viennacl::linalg::element_pow(THIS_VCL, VCL(v2));
}

template<class T>
void Vector<T>::SquareScalar()
{
    PowScalar(2);
}

template<class T>
void Vector<T>::SumVector(Vector<T> *V) {
    if (this->GetLength() == V->GetLength()) {
        THIS_VCL += VCL((*V));
    } else {
        LOG4_ERROR(
                "Error! It's impossible to sum two vectors with different length.");
    }
}

template<class T>
Vector<T> *Vector<T>::SumVector(Vector<T> *V1, Vector<T> *V2) {
    if (V1->GetLength() == V2->GetLength()) {
        Vector *V3 = new Vector(V1->Values, V1->GetLength());
        VCL((*V3)) += VCL((*V2));
        return V3;
    } else {
        LOG4_ERROR(
                "Error! It's impossible to sum two vectors with different length.");
        return new Vector<T>();
    }
}

template<class T>
void Vector<T>::SubtractVector(Vector<T> *V) {
    if (this->GetLength() == V->GetLength()) {
        THIS_VCL -= VCL((*V));
    } else {
        LOG4_ERROR(
                "Error! It's impossible to sum two vectors with different length.");
    }
}

template<class T>
Vector<T> *Vector<T>::SubtractVector(Vector<T> *V1, Vector<T> *V2) {
    if (V1->GetLength() == V2->GetLength()) {
        Vector *V3 = V1->Clone();
        VCL((*V3)) -= VCL((*V2));
        return V3;
    } else {
        LOG4_ERROR(
                "Error! It's impossible to subtract two vectors with different length.");
        return new Vector<T>();
    }
}

template<class T>
void Vector<T>::ProductVector(Vector<T>& V) {
    THIS_VCL = VCL(ProductVector(*this, V));
}

template<class T>
Vector<T> Vector<T>::ProductVector(Vector<T>& V1, Vector<T>& V2) {
    if (V1.GetLength() == V2.GetLength()) {
        Vector V3(V1);
        VCL(V3) = viennacl::linalg::element_prod(VCL(V1), VCL(V2));
        return V3;
    } else {
        LOG4_ERROR(
                "Error! It's impossible to multiply two vectors with different length.");
        return Vector<T>();
    }
}

template<class T>
T Vector<T>::ProductVectorScalar(Vector<T> *V) {
    return ProductVectorScalar(this, V);
}

template<class T>
T Vector<T>::ProductVectorScalar(Vector<T> *V1, Vector<T> *V2) {
    T Product = 0;
    if (V1->GetLength() == V2->GetLength()) {
        Product = viennacl::linalg::inner_prod(VCL((*V1)), VCL((*V2)));
        return Product;
    } else {
        LOG4_ERROR(
                "Error! It's impossible to multiply two vectors with different length.");
        return Product;
    }
}

template<class T>
T Vector<T>::Sum() {
    return viennacl::linalg::sum(THIS_VCL);
}

template<class T>
T Vector<T>::AbsSum() {
    return viennacl::linalg::norm_1(THIS_VCL);
}

template<typename T>
Vector<T> *Vector<T>::AbsList() {
    Vector<T> *X = new Vector<T>(this);
    X->Abs();
    return X;
}

template<typename T>
void Vector<T>::Abs() {
    THIS_VCL = viennacl::linalg::element_fabs(THIS_VCL);
}

// Comparison Operations
template<class T>
T Vector<T>::Min() {
    T MinValue = 0;
    if (this->Length > 0) {
        MinValue = viennacl::linalg::min(THIS_VCL);
    }
    return MinValue;
}

template<class T>
void Vector<T>::Min(T *MinValue, int *MinIndex) {
    if (this->Length > 0) {
        (*MinValue) = this->Values[0];
        (*MinIndex) = 0;
        for (int i = 1; i < this->Length; i++) {
            if (this->Values[i] < (*MinValue)) {
                (*MinValue) = this->Values[i];
                (*MinIndex) = i;
            }
        }
    } else {
        (*MinValue) = -1;
        (*MinIndex) = -1;
    }
}

template<class T>
T Vector<T>::MinAbs() {
    if (this->Length > 0) {
        Vector<T> V2(*this);
        V2.Abs();
        T MinValue = V2.Min();
        return MinValue;
    } else {
        T MinValue;
        return MinValue;
    }
}

template<class T>
void Vector<T>::MinAbs(T *MinValue, int *MinIndex) {
    if (this->Length > 0) {
        (*MinValue) = svr::common::ABS<T>(this->Values[0]);
        (*MinIndex) = 0;
        for (int i = 1; i < this->Length; i++) {
            if (svr::common::ABS<T>(this->Values[i]) < (*MinValue)) {
                (*MinValue) = svr::common::ABS<T>(this->Values[i]);
                (*MinIndex) = i;
            }
        }
    } else {
        (*MinValue) = -1;
        (*MinIndex) = -1;
    }
}

template<class T>
T Vector<T>::Max() {
    T MaxValue = 0;
    if (this->Length > 0) {
        MaxValue = viennacl::linalg::max(THIS_VCL);
    }
    return MaxValue;
}

template<class T>
void Vector<T>::Max(T *MaxValue, int *MaxIndex) {
    if (this->Length > 0) {
        (*MaxValue) = this->Values[0];
        (*MaxIndex) = 0;
        for (int i = 1; i < this->Length; i++) {
            if (this->Values[i] > (*MaxValue)) {
                (*MaxValue) = this->Values[i];
                (*MaxIndex) = i;
            }
        }
    } else {
        (*MaxValue) = -1;
        (*MaxIndex) = -1;
    }
}

template<class T>
T Vector<T>::MaxAbs() {
    if (this->Length > 0) {
        Vector<T> V2(*this);
        V2.Abs();
        T MaxValue = V2.Max();
        return MaxValue;
    } else {
        T MaxValue;
        return MaxValue;
    }
}

template<class T>
void Vector<T>::MaxAbs(T *MaxValue, int *MaxIndex) {
    if (this->Length > 0) {
        (*MaxValue) = svr::common::ABS<T>(this->Values[0]);
        (*MaxIndex) = 0;
        for (int i = 1; i < this->Length; i++) {
            if (svr::common::ABS<T>(this->Values[i]) > (*MaxValue)) {
                (*MaxValue) = svr::common::ABS<T>(this->Values[i]);
                (*MaxIndex) = i;
            }
        }
    } else {
        (*MaxValue) = -1;
        (*MaxIndex) = -1;
    }
}

template<class T>
T Vector<T>::Mean() {
    T MeanValue = 0;

    if (this->Length > 0) {
        MeanValue = Sum() / this->Length;
    }

    return MeanValue;
}

template<class T>
T Vector<T>::MeanAbs() {
    T MeanValue = 0;

    if (this->Length > 0) {
        MeanValue = AbsSum() / this->Length;
    }

    return MeanValue;
}

// TODO: Population or Sample Variance?
// TODO: Previous impl probably tried Sample Variance, however it was wrong and
// the result was Population Variance with trimmed first sample.
template<class T>
T Vector<T>::Variance() {
    T Variance = 0;
    if (this->Length > 0) {
        Vector v2(*this);
        v2.SumScalar( - v2.Mean() );
        v2.PowScalar(2);
        Variance = v2.Sum() / v2.GetLength();
    }

    return Variance;
}

// Sorting Operations
// TODO: Check if these could be impl entirely with viennacl...
template<class T>
void Vector<T>::Sort() {
    std::sort(values_ptr, values_ptr + Length);
}

template<class T>
void Vector<T>::RemoveDuplicates() {
    std::vector<T> cpu_vec_unique;
    std::unordered_set<T> uniques;

    for (int i = 0; i < GetLength(); ++i) {
       if (uniques.count(values_ptr[i]) == 0) {
           uniques.insert(values_ptr[i]);
           cpu_vec_unique.push_back(values_ptr[i]);
       }
    }

    viennacl::fast_copy(cpu_vec_unique.begin(), cpu_vec_unique.end(), this->Values.begin());
    this->Length = cpu_vec_unique.size();
}

template<class T>
int Vector<T>::Find(T X) {
    for (int i = 0; i < this->Length; i++) {
        if (this->Values[i] == X) {
            return i;
        }
    }
    return -1;
}

// I/O Operations
template<class T>
Vector<T> *Vector<T>::Load(const char *Filename) {
    // Open the file
    std::ifstream File(Filename,std::ios::in);
    if (!File) {
        LOG4_ERROR("Error. It's impossible to open the file.");
        return new Vector<T>();
    }
    Vector<T> *V;
    V = new Vector<T>();
    // Load the vector
    try {
        T Value;
        while (!File.eof()) {
            File >> Value;
            if(File.good())
                V->Add(Value);
        }
    } catch (...) {
        //LOG4_ERROR("Error. It's impossible to complete the save.");
    }
    // Close the file
    File.close();

    return V;
}

template<class T>
void Vector<T>::Save(const char *Filename) {
    // Open the file
    std::ofstream File(Filename, std::ios::out);
    if (!File) {
        LOG4_ERROR("Error. It's impossible to create the file.");
        return;
    }
    File.precision(30);
    // Save the vector
    try {
        for (int i = 0; i < this->Length; i++)
            File << this->Values[i] << " ";
        File << std::endl;
    } catch (...) {
        LOG4_ERROR("Error. It's impossible to complete the save.");
    }
    // Close the file
    File.close();
}

template<class T>
void Vector<T>::Print() {
    for (int i = 0; i < this->Length; i++) {
        LOG4_INFO(this->Values[i]);
    }
    std::cout << std::endl;
}

template<class T>
void Vector<T>::Print(char *VectorName) {
    LOG4_INFO(VectorName);
    Print();
}

// Operators Redefinition
template<class T>
T& Vector<T>::operator[](int Index) {
    if (Index < 0 || Index >= this->Length) {
        throw std::runtime_error("Vector index out of bounds");
    }
    return values_ptr[Index];
}

template<class T>
const T& Vector<T>::operator[](int Index) const
{
    if (Index < 0 || Index >= this->Length) {
        throw std::runtime_error("Vector index out of bounds");
    }
    return values_ptr[Index];
}

template<class T>
void Vector<T>::operator=(const viennacl::vector<T>& vcl) {
    Values.resize(vcl.size());
    Values = vcl;
    Length = vcl.size();
    Resize(vcl.size());
}

}
}
