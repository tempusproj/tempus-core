#pragma once

#include "model/Entity.hpp"
#include "OnlineSVR.h"
#include "util/StringUtils.hpp"
#include "model/SVRParameters.hpp"

namespace svr{
namespace datamodel{

class Model : public Entity{

private:
    bigint ensemble_id; /* TODO Replace with pointer to Ensemble */
    size_t decon_level;
    size_t samples_trained_count_;
    std::vector<size_t> learning_levels;
    OnlineSVR_ptr svr_model;
    bpt::ptime last_modified {bpt::max_date_time};
    bpt::ptime last_modeled_value_time {bpt::min_date_time};

public:

    bool operator==(Model const & o) const {return ensemble_id == o.ensemble_id && decon_level == o.decon_level
            && learning_levels == o.learning_levels && last_modified == o.last_modified
            && last_modeled_value_time == o.last_modeled_value_time && svr_model && o.svr_model && *svr_model == *o.svr_model ;}
    Model() : Entity() {}

    //TODO: constructor and training if we have train_data
    /** Create a model
     * \param id the model ID used in database
     * \param ensemble_id the ensemble ID of the ensemble this model belongs to
     * \param decon_level wavelet deconstruction level index this model is to model and predict
     * \param learning_levels indexes of the adjacent wavelet levels this model is to include in training, always in diapason between 0 and (wavelet levels - 1)
     * \param lag_count count of historical value rows per every wavelet level to include in training a model
     * \param svr_model pointer to a OnlineSVR instance this model object is wrapping, can be nullptr, in which case a new OnlineSVR instance is created
     * \param last_modified time this model class was last updated (modified). Used to keep track how recent this model instance is.
     * \param last_modeled_value_time the value time of the last value this model has been trained against with.
     */
    Model(bigint id, bigint ensemble_id, size_t decon_level, const std::vector<size_t> &learning_levels,
          OnlineSVR_ptr svr_model, bpt::ptime const &last_modified,
          bpt::ptime const &last_modeled_value_time)
            : Entity(id),
              ensemble_id(ensemble_id),
              decon_level(decon_level),
              learning_levels(learning_levels),
              svr_model(svr_model),
              last_modified(last_modified),
              last_modeled_value_time(last_modeled_value_time)
    {}

    void reset()
    {
        this->samples_trained_count_ = 0;
        this->last_modeled_value_time = bpt::min_date_time;
        this->last_modified = bpt::min_date_time;
        this->svr_model.reset();
    }

    size_t get_samples_trained_count() {
        return this->samples_trained_count_;
    }

    /** Get ensemble database ID this model is part of */
    bigint get_ensemble_id() const {
        return ensemble_id;
    }

    /** Change the ensemble this model is part of
     * \param ensemble_id Database ensemble ID this model is to become part of.
     */
    void set_ensemble_id(bigint ensemble_id) {
        this->ensemble_id = ensemble_id;
    }

    /** Get the wavelet deconstruction level this model is predicting. */
    size_t get_decon_level() const {
        return decon_level;
    }

    /** Set the decon level this model is predicting */
    void set_decon_level(size_t decon_level) {
        this->decon_level = decon_level;
    }

    /** Get training wavelet deconstruction level indexes this model is trained against. */
    std::vector<size_t> get_learning_levels() const {
        return learning_levels;
    }

    /** Set learning wavelet deconstruction level indexes this model is trained against. */
    void set_learning_levels(std::vector<size_t> learning_levels) {
        this->learning_levels = learning_levels;
    }

    /** Get pointer to an OnlineSVR model instance */
    OnlineSVR_ptr &get_svr_model()
    {
        return svr_model;
    }

    OnlineSVR_ptr get_svr_model() const
    {
        return svr_model;
    }

    /** Set member svr model point to a OnlineSVR instance
     * \param svr_model new OnlineSVR instance
     */
    void set_svr_model(OnlineSVR_ptr svr_model) {
        this->svr_model = svr_model;
    }

    /** Get last time model was updated */
    bpt::ptime const &get_last_modified() const {
        return last_modified;
    }

    /** Set time model was last updated
     * \param last_modified time model was last modified
     */
    void set_last_modified(bpt::ptime const &last_modified) {
        this->last_modified = last_modified;
    }

    /** Get value time of the latest row this model was trained against. */
    bpt::ptime const &get_last_modeled_value_time() const {
        return last_modeled_value_time;
    }

    /** Set value time of the latest row this model was trained against. */
    void set_last_modeled_value_time(bpt::ptime const &last_modeled_value_time) {
        this->last_modeled_value_time = last_modeled_value_time;
    }

    /** Get a string representation of this class instance. */
    virtual std::string to_string() const override{
        std::stringstream ss;

        ss << "Model id: " << get_id()
            << ", Ensemble id: " << get_ensemble_id()
            << ", Decon Level: " << get_decon_level()
            << ", Learning levels: [";
        if (!learning_levels.empty()) {

            std::copy(learning_levels.begin(), learning_levels.end() - 1,
                      std::ostream_iterator<size_t>(ss, ","));
            ss << learning_levels.back();
        }
        ss << "]"
            << ", Last modified time: " << bpt::to_simple_string(get_last_modified())
            << ", Last modeled value time: " << bpt::to_simple_string(get_last_modeled_value_time());

        return ss.str();
    }
};
}
}

using Model_ptr = std::shared_ptr<svr::datamodel::Model>;
using Model_vector = std::vector<Model_ptr>;
