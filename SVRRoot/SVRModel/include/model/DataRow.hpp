#pragma once

#include "common/compatibility.hpp"
#include "common/Logging.hpp"
#include "common/constants.hpp"
#include "common/exceptions.hpp"

namespace svr{
namespace datamodel{


class DataRow
{
private:
    bpt::ptime value_time_;
    bpt::ptime update_time_;
    double tick_volume_;
    std::vector<double> values_;

public:
    using Container = std::map<bpt::ptime, std::shared_ptr<DataRow>>;

    DataRow()
    {}

    DataRow(
            const bpt::ptime& value_time,
            const bpt::ptime& update_time = bpt::second_clock::local_time(),
            double tick_volume = svr::common::DEFAULT_VALUE_TICK_VOLUME,
            const std::vector<double> values = std::vector<double>{}) :
        value_time_(value_time),
        update_time_(update_time),
        tick_volume_(tick_volume),
        values_(values)
    {}

    std::vector<double>& get_values() { return values_; }
    void set_values(const std::vector<double>& values) { values_ = values; }

    const bpt::ptime& get_update_time() const {return update_time_;}
    void set_update_time(const bpt::ptime& update_time) {update_time_ = update_time;}

    const bpt::ptime& get_value_time() const {return value_time_;}
    void set_value_time(const bpt::ptime& value_time) {value_time_ = value_time;}

    double get_tick_volume() const { return tick_volume_; }
    void set_tick_volume(double weight) { tick_volume_ = weight; }

    std::string to_string() const
    {
        std::stringstream ss;

        ss.precision(30);

        ss << "ValueTime: " << bpt::to_simple_string(value_time_)
           << ", UpdateTime: " << bpt::to_simple_string(update_time_)
           << ", " << tick_volume_;

        for (const double &val : values_)
            ss << ", " << val;

        return ss.str();
    }

    std::vector <std::string> to_tuple() const
    {
        std::vector<std::string> result;

        result.push_back(bpt::to_simple_string(this->value_time_));
        result.push_back(bpt::to_simple_string(this->update_time_));
        result.push_back(std::to_string(this->tick_volume_));

        for(const double v: this->values_) result.push_back(std::to_string(v));

        return result;
    }

    bool operator == (const DataRow& other) const
    {
        return this->value_time_ == other.value_time_
                && this->tick_volume_ == other.tick_volume_
                && this->values_.size() == other.values_.size()
                && std::equal(values_.begin(), values_.end(), other.values_.begin());
    }

    /* TODO Move to DataRow::Container once class is implemented */
    DataRow::Container::const_iterator static find(
            const DataRow::Container &data,
            const boost::posix_time::ptime &row_time,
            const boost::posix_time::time_duration &deviation)
    {
        auto it_row = data.lower_bound(row_time);
        if (it_row->first == row_time) return it_row;
        if (std::prev(it_row)->first - row_time > it_row->first - row_time) std::advance(it_row, -1);
        if (it_row->first - row_time > deviation) return data.end();
        return it_row;
    }
};


class datarow_range: protected std::pair<DataRow::Container::iterator /* start */, DataRow::Container::iterator /* end */>
{
    size_t distance_;
    DataRow::Container::iterator rbegin_;
    DataRow::Container &container_;
    datarow_range() = default;

public:

    void reinit()
    {
        this->distance_ = static_cast<size_t>(std::abs(std::distance(this->first, this->second)));
        this->rbegin_ = std::prev(this->second);
    }

    datarow_range(
            const DataRow::Container::iterator &start,
            const DataRow::Container::iterator &end,
            DataRow::Container &container
    ) : container_(container)
    {
        this->first = start;
        this->second = end;
        reinit();
    }

    const size_t distance() const { return distance_; }

    const DataRow::Container::iterator &begin() const { return this->first; }
    const DataRow::Container::iterator &rbegin() const { return this->rbegin_; }
    const DataRow::Container::iterator &end() const { return this->second; }
    DataRow::Container::iterator &begin() { return this->first; }
    DataRow::Container::iterator &rbegin() { return this->rbegin_; }
    DataRow::Container::iterator &end() { return this->second; }

    DataRow::Container &get_container() const { return this->container_; }

    void set_begin(const DataRow::Container::iterator &begin)
    {
        this->first = begin;
        reinit();
    }

    void set_end(const DataRow::Container::iterator &end)
    {
        this->second = end;
        reinit();
    }

    datarow_range &operator=(const datarow_range &rhs)
    {
        this->first = rhs.first;
        this->second = rhs.second;
        this->container_ = rhs.container_;
        reinit();
        return *this;
    }
};

}
}

svr::datamodel::DataRow::Container::iterator
find_nearest_before(
        svr::datamodel::DataRow::Container &data,
        const boost::posix_time::ptime &time,
        const boost::posix_time::time_duration &max_gap,
        const size_t lag_count = std::numeric_limits<size_t>::max());

svr::datamodel::DataRow::Container::const_iterator
find_nearest_before(
        const svr::datamodel::DataRow::Container &data,
        const boost::posix_time::ptime &time,
        const boost::posix_time::time_duration &max_gap,
        const size_t lag_count = std::numeric_limits<size_t>::max());


using DataRow_ptr = std::shared_ptr<svr::datamodel::DataRow>;
using DataRowContainer = svr::datamodel::DataRow::Container;
using DataRowContainer_ptr = std::shared_ptr<svr::datamodel::DataRow::Container>;
