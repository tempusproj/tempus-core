#pragma once

#include <common/types.hpp>
#include <util/StringUtils.hpp>
#include "DBTable.hpp"
#include "DataRow.hpp"
#include "StoreBufferPushMerge.hpp"


#define DEFAULT_INPUT_QUEUE_RESOLUTION (60) /* seconds */


namespace svr {
namespace datamodel {

class InputQueue;
using InputQueue_ptr = std::shared_ptr<InputQueue>;

class InputQueue : public Queue
{
private:
    std::string logical_name_;
    std::string owner_user_name_;
    std::string description_;

    bpt::time_duration resolution_;
    bpt::time_duration legal_time_deviation_;

    bpt::time_duration time_zone_offset_;
    std::vector<std::string> value_columns_;

public:

    InputQueue(
            const std::string& table_name = std::string(),
            const std::string& logical_name = std::string(),
            const std::string& owner_user_name = std::string(),
            const std::string& description = std::string(),
            const bpt::time_duration& resolution = bpt::seconds(DEFAULT_INPUT_QUEUE_RESOLUTION),
            const bpt::time_duration& legal_time_deviation = bpt::seconds(0),
            const bpt::time_duration& time_zone_offset = bpt::seconds(0),
            const std::vector<std::string> value_columns = std::vector<std::string>(),
            const DataRowContainer &rows = DataRowContainer());

    inline const std::string& get_description() const { return description_; }
    inline void set_description(const std::string& description) { this->description_ = description; }

    inline const bpt::time_duration& get_legal_time_deviation() const {	return legal_time_deviation_;}
    inline void set_legal_time_deviation(const bpt::time_duration& legalTimeDeviation) {legal_time_deviation_ = legalTimeDeviation;}

    inline const std::string& get_logical_name() const { return logical_name_; }
    void set_logical_name(const std::string& logicalName);

    inline const std::string& get_owner_user_name() const { return owner_user_name_; }
    void set_owner_user_name(const std::string& ownerUserName);

    inline const bpt::time_duration& get_resolution() const { return resolution_; }
    void set_resolution(const bpt::time_duration& resolution);

    inline const bpt::time_duration& get_time_zone_offset() const {return time_zone_offset_;}
    inline void set_time_zone_offset(const bpt::time_duration& time_zone_offset) {time_zone_offset_ = time_zone_offset;}

    inline const std::vector<std::string>& get_value_columns() const {return value_columns_;}
    inline void set_value_columns(const std::vector<std::string>& value_columns){ value_columns_ = value_columns; }

    /* TODO Implement
    inline const std::vector<std::string> &get_feature_columns() const;
    inline void set_feature_columns(const std::vector<std::string> &feature_columns) { feature_columns_ = feature_columns; }
    */

	// end getters and setters

    InputQueue get_copy_metadata() const;
    InputQueue_ptr clone_empty() const;

    std::string metadata_to_string() const override;

    void reinit_table_name();

    static std::string make_queue_table_name(const std::string &user_name, const std::string &logical_name, const bpt::time_duration &resolution);

};

} /* namespace model */
} /* namespace svr */


template<>
inline void store_buffer_push_merge<svr::datamodel::InputQueue_ptr>(svr::datamodel::InputQueue_ptr & dest, svr::datamodel::InputQueue_ptr const & src)
{
    for(auto const & row : src->get_data())
        dest->get_data().insert(row);
    dest->set_value_columns(src->get_value_columns());
    dest->set_description(src->get_description());
    dest->set_legal_time_deviation(src->get_legal_time_deviation());
    dest->set_logical_name(src->get_logical_name());
    dest->set_owner_user_name(src->get_owner_user_name());
    dest->set_resolution(src->get_resolution());
    dest->set_table_name(src->get_table_name());
    dest->set_time_zone_offset(src->get_time_zone_offset());
}
