#include "TestSuite.hpp"
#include "../include/model/Vector.h"
#include "../include/model/Vector.tcc"
#include <gtest/gtest.h>
#include <limits>
#include <random>
#include <chrono>
#include <unordered_set>

using Vector = svr::datamodel::Vector<double>;

std::vector<double> initVector(size_t len)
{
    std::vector<double> v(len, 0);
    double a = -10;

    for (size_t i = 0; i < v.size(); ++i) {
        v[i] = a;
        a += 0.2;
    }

    return v;
}

std::vector<double> initVectorRandom(size_t len)
{
    std::vector<double> v(len, 0);
    const int precision = 1e3;
    const int maxVal = 100;

    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine generator(seed);
    std::uniform_int_distribution<int> distribution(- maxVal * precision, maxVal * precision);

    for (size_t i = 0; i < v.size(); ++i) {
        v[i] = double(distribution(generator)) / precision;
    }

    return v;
}

TEST(VectorTests, PowBasicTests)
{
    Vector v1(0.0, 77), v2(0.0, 77);
    for(int i = 0; i < v1.GetLength(); ++i)
        v1[i] = double(i);

    v1.PowScalar(2);
    for(int i = 0; i < v1.GetLength(); ++i)
        ASSERT_LE(fabs(v1[i] - pow(double(i), 2)), std::numeric_limits<double>::epsilon());

    for(int i = 0; i < v2.GetLength(); ++i)
        v2[i] = double(i);

    v2.SquareScalar();
    for(int i = 0; i < v2.GetLength(); ++i)
        ASSERT_LE(fabs(v2[i] - pow(double(i), 2)), std::numeric_limits<double>::epsilon());
}

TEST(VectorTests, PowPerformanceTests)
{
    if ( !TestSuite::doPerformanceTests() )
        return;

    const size_t len = 1e+7;
    const int maxVal = 1000;

    std::default_random_engine generator;
    std::uniform_int_distribution<int> distribution(1, maxVal);

    Vector v1(0.0, len), v2(0.0, len);


    using namespace std::chrono;

    for(size_t i = 0; i < len; ++i)
    {
        double u = double(distribution(generator));
        v1[i] = u / maxVal;
        v2[i] = u / maxVal;
    }

    /*  Test 1   **************************************************************/
    high_resolution_clock::time_point start = high_resolution_clock::now();

    v1.PowScalar(2);

    high_resolution_clock::time_point finish = high_resolution_clock::now();

    duration<double> time_span = duration_cast<duration<double>>(start - finish);
    std::cout << "TEST(VectorTests, PowPerformanceTests).PowScalar(2):" << " length: " << len << " took " << time_span.count() << "sec." << std::endl;

    /*  Test 2   **************************************************************/

    start = high_resolution_clock::now();

    v2.SquareScalar();

    finish = high_resolution_clock::now();

    time_span = duration_cast<duration<double>>(start - finish);
    std::cout << "TEST(VectorTests, PowPerformanceTests).SquareScalar():" << " length: " << len << " took " << time_span.count() << "sec." << std::endl;

    /*  Test 3   **************************************************************/

    start = high_resolution_clock::now();

    v2.SquareScalar();

    finish = high_resolution_clock::now();

    time_span = duration_cast<duration<double>>(start - finish);
    std::cout << "TEST(VectorTests, PowPerformanceTests).SquareScalar():" << " length: " << len << " took " << time_span.count() << "sec." << std::endl;

    /*  Test 4   **************************************************************/

    start = high_resolution_clock::now();

    v1.PowScalar(2);

    finish = high_resolution_clock::now();

    time_span = duration_cast<duration<double>>(start - finish);
    std::cout << "TEST(VectorTests, PowPerformanceTests).PowScalar(2):" << " length: " << len << " took " << time_span.count() << "sec." << std::endl;

    /*  Done   ****************************************************************/

    ASSERT_EQ(v1, v2);

}

TEST(VectorTests, Sort)
{
    Vector v1(initVectorRandom(11));
    v1.Sort();
    for (int i = 1; i < v1.GetLength(); ++i) {
        ASSERT_TRUE(v1[i - 1] <= v1[i]);
    }
}

TEST(VectorTests, Container)
{
    Vector v1(initVectorRandom(111));

    Vector * vClone = v1.Clone();
    for (int i = 0; i < v1.GetLength(); ++i) {
        ASSERT_EQ(v1[i], vClone->GetValue(i));
    }

    // Copy to STL Vector
    std::vector<double> stl_vec(v1.GetLength());
    v1.copy_to(stl_vec);
    for (int i = 0; i < v1.GetLength(); ++i) {
        ASSERT_EQ(v1[i], stl_vec[i]);
    }

    // Resize
    Vector vResize(v1);
    ASSERT_EQ(vResize.GetLength(), v1.GetLength());
    vResize.Resize(v1.GetLength() * 4);
    for (int i = 0; i < v1.GetLength(); ++i) {
        ASSERT_EQ(v1[i], vResize[i]);
    }

    // Add
    Vector vAdd(v1);
    vAdd.Add(3.14);
    for (int i = 0; i < v1.GetLength(); ++i) {
        ASSERT_EQ(v1[i], vAdd[i]);
    }
    ASSERT_EQ(vAdd[v1.GetLength()], 3.14);

    // AddAt
    Vector vAddAt({1.0, 2, 3, 4, 5, 6, 7});
    Vector v2(vAddAt);
    vAddAt.AddAt(M_PI, 3);
    vAddAt.AddAt(M_PI, 0);
    vAddAt.AddAt(M_PI, 9);
    // TODO: Add more asserts to ensure add/remove correctness...
    // RemoveAt
    vAddAt.RemoveAt(9);
    vAddAt.RemoveAt(0);
    vAddAt.RemoveAt(3);
    for (int i = 0; i < vAddAt.GetLength(); ++i) {
        ASSERT_LE(fabs(vAddAt[i] - v2[i]), std::numeric_limits<double>::epsilon());
    }

    // Set/Get
    v1.SetValue(11, M_PI);
    ASSERT_LE(M_PI - v1.GetValue(11),  std::numeric_limits<double>::epsilon());
}

TEST(VectorTests, CTOR)
{
    const size_t n = 10;
    Vector* v1 = new Vector(n + 1);
    for (size_t i = 0; i < n + 1; i++) {
        v1->Add(1);
    }
    Vector* v2 = new Vector(1, n + 1);
    for (int i = 0; i < v1->GetLength(); ++i) {
        ASSERT_LE(fabs(v1->GetValue(i) - v2->GetValue(i)), std::numeric_limits<double>::epsilon());
    }

    // TODO: Add more CTOR tests...
}

TEST(VectorTests, RemoveDuplicates)
{
    Vector v1({1, 5, 3, 8, 34, 8, 12, 5, 14, 15, 3, 4, 5, 6, 7, 55, 4});
    std::unordered_set<double> uniques;

    v1.RemoveDuplicates();

    for (int i = 0; i < v1.GetLength(); ++i) {
        ASSERT_EQ(uniques.count(v1[i]), static_cast<size_t>(0));
        uniques.insert(v1[i]);
    }
}

TEST(VectorTests, Extract)
{
    Vector v1(initVectorRandom(33));

    const int from = 5;
    const int to = 15;

    Vector * v2 = v1.Extract(from, to);
    ASSERT_EQ(v2->GetLength(), to - from + 1);

    for (int i = from; i <= to; ++i) {
        ASSERT_LE(fabs(v2->GetValue(i - from) - v1[i]), std::numeric_limits<double>::epsilon());
    }
}

TEST(VectorTests, ABS)
{
    Vector v1(initVector(111));
    Vector v2(v1);

    v1.Abs();

    for (int i = 0; i < v1.GetLength(); ++i) {
        ASSERT_LE(fabs(fabs(v2[i]) - v1[i]), std::numeric_limits<double>::epsilon());
    }
}

TEST(VectorTests, Sum)
{
    Vector v1(initVectorRandom(33));

    double sum = v1.Sum();
    double sum2 = 0;

    for (int i = 0; i < v1.GetLength(); ++i) {
        sum2 += v1[i];
    }

    ASSERT_LE(fabs(sum - sum2), std::numeric_limits<double>::epsilon());
}

TEST(VectorTests, AbsSum)
{
    Vector v1(initVectorRandom(33));
    Vector v2(v1);

    double sum = v1.AbsSum();
    double sum2 = 0;

    for (int i = 0; i < v1.GetLength(); ++i) {
        sum2 += fabs(v2[i]);
    }

    ASSERT_LE(fabs(sum - sum2), std::numeric_limits<double>::epsilon());
}

TEST(VectorTests, SumVector)
{
    Vector v1(initVectorRandom(33));
    Vector v2(initVectorRandom(33));
    Vector v3(v1);

    v1.SumVector(&v2);

    for (int i = 0; i < v1.GetLength(); ++i) {
        ASSERT_LE(fabs(v1[i] - (v2[i] + v3[i])), std::numeric_limits<double>::epsilon());
    }
}

TEST(VectorTests, SumScalar)
{
    Vector v1(initVectorRandom(33));
    Vector v3(v1);
    const double scalar = 5.5;

    v1.SumScalar(scalar);

    for (int i = 0; i < v1.GetLength(); ++i) {
        ASSERT_LE(fabs(v1[i] - (v3[i] + scalar)), std::numeric_limits<double>::epsilon());
    }
}

TEST(VectorTests, ProductVector)
{
    Vector v1(initVectorRandom(55));
    Vector v2(initVectorRandom(55));
    Vector v3(v1);

    v1.ProductVector(v2);

    for (int i = 0; i < v1.GetLength(); ++i) {
        double prod2 = v2[i] * v3[i];
        ASSERT_LE(fabs(prod2 - v1[i]), std::numeric_limits<double>::epsilon());
    }
}

TEST(VectorTests, ProductVectorScalar)
{
    Vector v1(initVectorRandom(55));
    Vector v2(initVectorRandom(55));
    Vector v3(v1);

    double prod = v1.ProductVectorScalar(&v2);
    double prod2 = 0;

    for (int i = 0; i < v1.GetLength(); ++i) {
        prod2 += v2[i] * v3[i];
    }

    ASSERT_LE(fabs(prod2 - prod), std::numeric_limits<double>::epsilon());
}

TEST(VectorTests, Variance)
{
    Vector v1(initVectorRandom(111));

    double variance = v1.Variance();
    double variance2 = 0;
    double mean = v1.Mean();

    for (int i = 0; i < v1.GetLength(); ++i) {
        variance2 += (fabs(v1[i] - mean)) * (fabs(v1[i] - mean));
    }
    variance2 /= v1.GetLength();

    ASSERT_LE(fabs(variance2 - variance), std::numeric_limits<double>::epsilon());
}

TEST(VectorTests, MinMax)
{
    Vector v1(initVectorRandom(55));

    double min = v1.Min();
    double max = v1.Max();

    double min2 = v1[0];
    double max2 = v1[0];

    for (int i = 1; i < v1.GetLength(); ++i) {
        if (min2 > v1[i]) {
            min2 = v1[i];
        }
        if (max2 < v1[i]) {
            max2 = v1[i];
        }
    }

    ASSERT_EQ(min, min2);
    ASSERT_EQ(max, max2);
}
