#include "gpu_handler.h" 
#include "common/common.hpp"

namespace svr {
namespace batch {

using namespace boost::interprocess;

std::shared_ptr<gpu_handler> gpu_handler::p_instance = nullptr;
std::once_flag gpu_handler::only_one;

gpu_handler::gpu_handler() : thread_pool(8)
{
    // TODO: Activate when needed.
    //thread_pool.start();
#ifdef VIENNACL_WITH_OPENCL
    try {
        init_devices(CL_DEVICE_TYPE_GPU);
    } catch (const viennacl::ocl::device_not_found &ex) {
        LOG4_WARN("No GPU device could be found. Using CPUs instead.");
        //free_gpus = {};
#endif // VIENNACL_WITH_OPENCL
        init_devices(CL_DEVICE_TYPE_CPU);
#ifdef VIENNACL_WITH_OPENCL
    }
#endif // VIENNACL_WITH_OPENCL

    p_gpu_sem = std::make_unique<named_semaphore>(
            open_or_create_t(), SVRWAVE_GPU_SEM, max_running_gpu_threads_number, permissions(0x1FF));

    // This hack is needed as the semaphore created with sudo privileges, does not have W rights.
    char cstr[256];
    snprintf(cstr, ELEMCOUNT(cstr), "chmod a+rw /dev/shm/sem.%s", SVRWAVE_GPU_SEM);
    std::shared_ptr<FILE> pipe_gpu(popen(cstr, "r"), pclose);
}


gpu_handler::~gpu_handler()
{
    /*
    if (!named_semaphore::remove(SVRWAVE_GPU_SEM))
        LOG4_WARN("Failed removing named semaphore " << SVRWAVE_GPU_SEM);
    */
}


void gpu_handler::gpu_sem_enter()
{
    if (!p_gpu_sem) {
        LOG4_WARN("No semaphore initialized! Aborting.");
        return;
    }
    p_gpu_sem->wait();
}


void gpu_handler::gpu_sem_leave()
{
    if (!p_gpu_sem) {
        LOG4_WARN("No semaphore initialized! Aborting.");
        return;
    }
    p_gpu_sem->post(); // Finished with GPU
}


gpu_handler &gpu_handler::get_instance()
{
    std::call_once(
            gpu_handler::only_one,
            [] () { gpu_handler::p_instance.reset(new gpu_handler()); }
    );
    return *gpu_handler::p_instance;
}


void gpu_handler::init_devices(const int device_type)
{
    viennacl::ocl::platform pf;
    LOG4_INFO("Platform info " << pf.info());
    std::vector<viennacl::ocl::device> const &devices = pf.devices(device_type);
    max_running_gpu_threads_number = devices.size();
    max_gpu_data_chunk_size = std::numeric_limits<size_t>::max();
    for (const auto &device: devices)
        if (device.max_mem_alloc_size() > 0 && device.max_mem_alloc_size() / sizeof(double) < max_gpu_data_chunk_size)
            max_gpu_data_chunk_size = device.max_mem_alloc_size() / sizeof(double);
    LOG4_INFO("Devices available " << max_running_gpu_threads_number << ", device max allocate size is " <<
                                   max_gpu_data_chunk_size << " elements of size " << sizeof(double));
    for (size_t i = 0; i < devices.size(); ++i) {
        setup_context(i, devices[i]);
        free_gpus.push(i);
    }
}


size_t gpu_handler::get_free_gpu()
{
    std::lock_guard<std::mutex> guard(free_gpu_mutex);
    size_t free_gpu_index = free_gpus.top();
    free_gpus.pop();
    return free_gpu_index;
}


void gpu_handler::return_gpu(size_t gpu_index)
{
    std::lock_guard<std::mutex> guard(free_gpu_mutex);

    free_gpus.push(gpu_index);
}


size_t gpu_handler::get_max_running_gpu_threads_number()
{
    return max_running_gpu_threads_number;
}


size_t gpu_handler::get_max_gpu_data_chunk_size()
{
    return max_gpu_data_chunk_size;
}


}
}
