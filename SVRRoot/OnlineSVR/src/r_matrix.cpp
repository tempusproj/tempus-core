#include "r_matrix.h"

namespace svr {

namespace batch {

using namespace datamodel;


double R_matrix::getQ(int index1, int index2)
{
    return Q.GetValue(index1, index2);
}

Vector<double>* R_matrix::getQsi(std::vector<int> sv_indexes, int SampleIndex)
{
    Vector<double>* V2 = new Vector<double>(sv_indexes.size() - 1);
    for (size_t i = 0; i < sv_indexes.size() - 1; i++)
    {
        V2->Add(Q.GetValue(SampleIndex, sv_indexes[i]));
    }
    return V2;
}

Vector<double>* R_matrix::getQxi(int SampleIndex)
{
    Vector<double>* V = new Vector<double>(SampleIndex + 1);
    for (int i = 0; i < SampleIndex + 1; i++) {
        V->Add(Q.GetValue(SampleIndex, i));
    }

    return V;
}

Matrix<double>* R_matrix::getQxs(std::vector<int> sv_indexes, int SamplesTrainedNumber)
{
    Matrix<double>* M = new Matrix<double>();
    for (int i = 0; i < SamplesTrainedNumber; i++) {
        Vector<double>* V2 = new Vector<double>(sv_indexes.size() - 1);
        for (size_t j = 0; j < sv_indexes.size() - 1; j++) {
            V2->Add(Q.GetValue(i, sv_indexes[j]));
        }
        M->AddRowRef(V2);
    }
    return M;
}


void R_matrix::update_R_matrix(std::vector<int> sv_indexes, int SampleIndex)
{
    if (!update_r_matrix_enabled_) {
        return;
    }

    LOG4_DEBUG("Running update R matrix on " << sv_indexes.size() << " instances, sample index " << SampleIndex);
    //cacl beta
    Vector<double>* Qsi = getQsi(sv_indexes, SampleIndex);
    Qsi->AddAt(1, 0);
    Vector<double>* Beta = this->R->ProductVector(Qsi);
    Beta->ProductScalar(-1);
    delete Qsi;

    Vector<double>* Gamma;
    //calc gamma
    if (sv_indexes.size() - 1 == 0) {
        Gamma = new Vector<double>(1, SampleIndex + 1);
    } else {
        Vector<double>* Qxi = this->getQxi(SampleIndex);
        Matrix<double>* Qxs = this->getQxs(sv_indexes, SampleIndex + 1);
        Vector<double>* Ones = new Vector<double>(1, SampleIndex + 1);
        Qxs->AddColCopyAt(Ones, 0);
        Gamma = Qxs->ProductVector(Beta);
        Gamma->SumVector(Qxi);
        delete Ones;
        delete Qxi;
        delete Qxs;
    }

    //add sample to R
    if (this->R->GetLengthRows() == 0) {
        Vector<double>* V1 = new Vector<double>(2);
        V1->Add(-this->getQ(SampleIndex, SampleIndex));
        V1->Add(1);
        this->R->AddRowRef(V1);
        Vector<double>* V2 = new Vector<double>(2);
        V2->Add(1);
        V2->Add(0);
        this->R->AddRowRef(V2);
    } else {
        Vector<double>* Zeros = Vector<double>::ZeroVector(this->R->GetLengthCols());
        this->R->AddColCopy(Zeros);
        Zeros->Add(0);
        this->R->AddRowRef(Zeros);
        if (Gamma->GetValue(SampleIndex) != 0) {
            Beta->Add(1);
            Matrix<double>* BetaMatrix = Matrix<double>::ProductVectorVector(
                    Beta, Beta);
            BetaMatrix->DivideScalar(Gamma->GetValue(SampleIndex));
            this->R->SumMatrix(BetaMatrix);
            delete BetaMatrix;
        }
    }

    delete Gamma;
    delete Beta;
}

} //batch

} //svr
