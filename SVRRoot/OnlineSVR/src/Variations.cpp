/******************************************************************************
 *                       ONLINE SUPPORT VECTOR REGRESSION                      *
 *                      Copyright 2006 - Francesco Parrella                    *
 *                                                                             *
 *This program is distributed under the terms of the GNU General Public License*
 ******************************************************************************/

#include <string.h>
#include "OnlineSVR.h"

using namespace svr::common;
using namespace std;

namespace svr {

// Variation Operations
double OnlineSVR::FindVariationLc1(Vector<double>* H, Vector<double>* Gamma,
		int SampleIndex, int Direction) {
	double Weightc = this->Weights->GetValue(SampleIndex);
	double Hc = H->GetValue(SampleIndex);
	double Gammac = Gamma->GetValue(SampleIndex);
    double Epsilon = this->svr_parameters.get_svr_epsilon();
    double C = this->svr_parameters.get_svr_C();

	if (Gammac <= 0)
		return Direction * INF;

	if (Hc > +Epsilon && -C < Weightc && Weightc <= 0)
		return (-Hc + Epsilon) / Gammac;

	else if (Hc < -Epsilon && 0 <= Weightc && Weightc < C)
		return (-Hc - Epsilon) / Gammac;

	else
		return Direction * INF;

}

double OnlineSVR::FindVariationLc2(int SampleIndex, int Direction) {
	double Weightsc = this->Weights->GetValue(SampleIndex);
    double C = this->svr_parameters.get_svr_C();

	if (this->GetSupportSetElementsNumber() == 0)
		return Direction * INF;
	else if (Direction > 0)
		return -Weightsc + C;
	else
		return -Weightsc - C;
}

double OnlineSVR::FindVariationLc(int SampleIndex) {
	return -this->Weights->GetValue(SampleIndex);
}

Vector<double>* OnlineSVR::FindVariationLs(Vector<double>* H,
		Vector<double>* Beta, int Direction) {
	Vector<double>* Ls = new Vector<double>(
			this->GetSupportSetElementsNumber());
	for (int i = 0; i < this->GetSupportSetElementsNumber(); i++) {

		double Weightsi = this->Weights->GetValue(
				this->SupportSetIndexes->GetValue(i));
		double Hi = H->GetValue(this->SupportSetIndexes->GetValue(i));
		double Betai = Beta->GetValue(i + 1);
		double Lsi = Direction * INF;
        double C = this->svr_parameters.get_svr_C();

		if (Betai == 0)
			Lsi = Direction * INF;

		else if (Direction * Betai > 0) {

			if (Hi > 0) { // Hi == Epsilon
				if (Weightsi < -C)
					Lsi = (-Weightsi - C) / Betai;
				else if (Weightsi <= 0)
					Lsi = -Weightsi / Betai;
				else
					Lsi = Direction * INF;

			} else { // Hi == -Epsilon
				if (Weightsi < 0)
					Lsi = -Weightsi / Betai;
				else if (Weightsi <= +C)
					Lsi = (-Weightsi + C) / Betai;
				else
					Lsi = Direction * INF;
			}

		} else {

			if (Hi > 0) { // Hi == Epsilon

				if (Weightsi > 0)
					Lsi = -Weightsi / Betai;
				else if (Weightsi >= -C)
					Lsi = (-Weightsi - C) / Betai;
				else
					Lsi = Direction * INF;

			} else { // Hi == -Epsilon

				if (Weightsi > +C)
					Lsi = (-Weightsi + C) / Betai;
				else if (Weightsi >= 0)
					Lsi = -Weightsi / Betai;
				else
					Lsi = Direction * INF;
			}
		}

		Ls->Add(Lsi);
	}

	return Ls;
}

Vector<double>* OnlineSVR::FindVariationLe(Vector<double>* H,
		Vector<double>* Gamma, int Direction) {
	Vector<double>* Le = new Vector<double>(this->GetErrorSetElementsNumber());
	for (int i = 0; i < this->GetErrorSetElementsNumber(); i++) {

		double Weightsi = this->Weights->GetValue(
				this->ErrorSetIndexes->GetValue(i));
		double Hi = H->GetValue(this->ErrorSetIndexes->GetValue(i));
		double Gammai = Gamma->GetValue(this->ErrorSetIndexes->GetValue(i));
		double Lei = Direction * INF;
        double Epsilon = this->svr_parameters.get_svr_epsilon();

		if (Gammai == 0)
			Lei = Direction * INF;

		else if (Direction * Gammai > 0) {

			if (Weightsi > 0) { // Weightsi == +C
				if (Hi < -Epsilon)
					Lei = (-Hi - Epsilon) / Gammai;
				else
					Lei = Direction * INF;

			} else { // Weightsi == -C
				if (Hi < +Epsilon)
					Lei = (-Hi + Epsilon) / Gammai;
				else
					Lei = Direction * INF;
			}

		} else {

			if (Weightsi > 0) { // Weightsi == +C
				if (Hi > -Epsilon)
					Lei = (-Hi - Epsilon) / Gammai;
				else
					Lei = Direction * INF;
			} else { // Weightsi == -C

				if (Hi > +Epsilon)
					Lei = (-Hi + Epsilon) / Gammai;
				else
					Lei = Direction * INF;
			}

		}

		Le->Add(Lei);
	}

	return Le;
}

Vector<double>* OnlineSVR::FindVariationLr(Vector<double>* H,
		Vector<double>* Gamma, int Direction) {
	Vector<double>* Lr = new Vector<double>(
			this->GetRemainingSetElementsNumber());
	for (int i = 0; i < this->GetRemainingSetElementsNumber(); i++) {

		double Hi = (*H)[(*this->RemainingSetIndexes)[i]];
		double Gammai = (*Gamma)[(*this->RemainingSetIndexes)[i]];
		double Lri = Direction * INF;
        double Epsilon = this->svr_parameters.get_svr_epsilon();

		if (Gammai == 0) {
			Lri = Direction * INF;
		} else if (Direction * Gammai > 0) {
			if (Hi < -Epsilon) {
				Lri = (-Hi - Epsilon) / Gammai;
			} else if (Hi < +Epsilon) {
				Lri = (-Hi + Epsilon) / Gammai;
			} else {
				Lri = Direction * INF;
			}
		} else {
			if (Hi > +Epsilon) {
				Lri = (-Hi + Epsilon) / Gammai;
			} else if (Hi > -Epsilon) {
				Lri = (-Hi - Epsilon) / Gammai;
			} else {
				Lri = Direction * INF;
			}
		}

		Lr->Add(Lri);
	}

	return Lr;
}

void OnlineSVR::FindLearningMinVariation(Vector<double>* H,
		Vector<double>* Beta, Vector<double>* Gamma, int SampleIndex,
		double* MinVariation, int* MinIndex, int* Flag) {

	// Find Samples Variations
	int Direction;
	/*
	 if ((*this->Weights)[SampleIndex]>0 &&  (-this->Epsilon<H->Values[SampleIndex] && H->Values[SampleIndex]<0))
	 Direction = SIGN(H->Values[SampleIndex]);
	 else if ((*this->Weights)[SampleIndex]<0 &&  (0<H->Values[SampleIndex] && H->Values[SampleIndex]<this->Epsilon))
	 Direction = SIGN(H->Values[SampleIndex]*Gammac);
	 else
	 Direction = SIGN(-H->Values[SampleIndex]);
	 */

	double Weightc = this->Weights->GetValue(SampleIndex);
	double Hc = H->GetValue(SampleIndex);
	double Gammac = Gamma->GetValue(SampleIndex);

	/*
	 if (Weightc!=0 && SIGN(Hc*Weightc)<0 && (0<ABS(Hc) && ABS(Hc)<Epsilon))
	 Direction = SIGN(H->Values[SampleIndex]*Gamma->Values[SampleIndex]);
	 else
	 */
	Direction = (int)SIGN(-H->GetValue(SampleIndex));

	double Lc1 = this->FindVariationLc1(H, Gamma, SampleIndex, Direction);

    Direction = (int)SIGN(Lc1);

	if (SIGN(Lc1) != SIGN(Direction)) {
		LOG4_INFO("Direction = " << Direction);
		LOG4_INFO("Weightc   = " << Weightc);
		LOG4_INFO("Hc        = " << Hc);
		LOG4_INFO("Gammac    = " << Gammac);

		cout << "ERROR SIGN!!";
	}

	double Lc2 = this->FindVariationLc2(SampleIndex, Direction);
	Vector<double> & Ls = *this->FindVariationLs(H, Beta, Direction);
	Vector<double> & Le = *this->FindVariationLe(H, Gamma, Direction);
	Vector<double> & Lr = *this->FindVariationLr(H, Gamma, Direction);

	// Check Values
	if (Gammac < 0) {
		// Avoid loops
		int i;
		for (i = 0; i < Ls.GetLength(); i++) {
			if (Ls[i] == 0)
				Ls[i] = Direction * INF;
		}
		for (i = 0; i < Le.GetLength(); i++) {
			if (Le[i] == 0)
				Le[i] = Direction * INF;
		}
		for (i = 0; i < Lr.GetLength(); i++) {
			if (Lr[i] == 0)
				Lr[i] = Direction * INF;
		}
	}

	// Find Min Variation
	double MinLsValue, MinLeValue, MinLrValue;
	int MinLsIndex, MinLeIndex, MinLrIndex;
    if (this->GetSupportSetElementsNumber() > 0)
		Ls.MinAbs(&MinLsValue, &MinLsIndex);
	else
		MinLsValue = Direction * INF;
	if (this->GetErrorSetElementsNumber() > 0)
		Le.MinAbs(&MinLeValue, &MinLeIndex);
	else
		MinLeValue = Direction * INF;
	if (this->GetRemainingSetElementsNumber() > 0)
		Lr.MinAbs(&MinLrValue, &MinLrIndex);
	else
		MinLrValue = Direction * INF;
	Vector<double> Variations(5);
	Variations.Add(ABS(Lc1));
	Variations.Add(ABS(Lc2));
	Variations.Add(MinLsValue);
	Variations.Add(MinLeValue);
	Variations.Add(MinLrValue);
	Variations.MinAbs(MinVariation, Flag);

	// Find Sample Index Variation
	(*MinVariation) *= Direction;
	(*Flag)++;
	switch (*Flag) {
	case 1:
	case 2:
		(*MinIndex) = 0;
		break;
	case 3:
		(*MinIndex) = MinLsIndex;
		break;
	case 4:
		(*MinIndex) = MinLeIndex;
		break;
	case 5:
		(*MinIndex) = MinLrIndex;
		break;
	}

	// Clear
	delete &Ls;
	delete &Le;
	delete &Lr;

	// PROVE
	static int Counts = 0;
	if (*MinVariation == 0)
		Counts++;
	else
		Counts = 0;

}

void OnlineSVR::FindUnlearningMinVariation(Vector<double>* H,
		Vector<double>* Beta, Vector<double>* Gamma, int SampleIndex,
		double* MinVariation, int* MinIndex, int* Flag) {

	// Find Samples Variations
    int Direction = (int)SIGN(-this->Weights->GetValue(SampleIndex));
	double Lc = this->FindVariationLc(SampleIndex);
	Vector<double>* Ls = this->FindVariationLs(H, Beta, Direction);
	Vector<double>* Le = this->FindVariationLe(H, Gamma, Direction);
	Vector<double>* Lr = this->FindVariationLr(H, Gamma, Direction);

	// Find Min Variation
	double MinLsValue, MinLeValue, MinLrValue;
	int MinLsIndex, MinLeIndex, MinLrIndex;
	if (this->GetSupportSetElementsNumber() > 0)
		Ls->MinAbs(&MinLsValue, &MinLsIndex);
	else
		MinLsValue = Direction * INF;
	if (this->GetErrorSetElementsNumber() > 0)
		Le->MinAbs(&MinLeValue, &MinLeIndex);
	else
		MinLeValue = Direction * INF;
	if (this->GetRemainingSetElementsNumber() > 0)
		Lr->MinAbs(&MinLrValue, &MinLrIndex);
	else
		MinLrValue = Direction * INF;
	Vector<double>* Variations = new Vector<double>(5);
	Variations->Add(Lc);
	Variations->Add(Direction * INF);
	Variations->Add(MinLsValue);
	Variations->Add(MinLeValue);
	Variations->Add(MinLrValue);
	Variations->MinAbs(MinVariation, Flag);

	// Find Sample Index Variation
	(*MinVariation) *= Direction;
	(*Flag)++;
	switch (*Flag) {
	case 1:
	case 2:
		(*MinIndex) = 0;
		break;
	case 3:
		(*MinIndex) = MinLsIndex;
		break;
	case 4:
		(*MinIndex) = MinLeIndex;
		break;
	case 5:
		(*MinIndex) = MinLrIndex;
		break;
	}

	// Clear
	delete Ls;
	delete Le;
	delete Lr;
	delete Variations;

	// PROVE
	static int Counts = 0;
	if (*MinVariation == 0)
		Counts++;
	else
		Counts = 0;
	if (Counts >= 10) {
        LOG4_INFO("ERROR! Cycle found! (FORGET)");
	}

	if (*MinVariation == 0) {
        LOG4_INFO("ATTENTION");
	}

}

// Matrix R Operations
void OnlineSVR::UpdateWeightsAndBias(Vector<double>** H, Vector<double>* Beta,
		Vector<double>* Gamma, int SampleIndex, double MinVariation) {

	// Update Weights and Bias
	if (this->GetSupportSetElementsNumber() > 0) {

		// Update Weights New Sample
		(*this->Weights)[SampleIndex] += MinVariation;

		// Update Bias
		Vector<double>* DeltaWeights = Beta->Clone();
		DeltaWeights->ProductScalar(MinVariation);
		this->Bias += DeltaWeights->GetValue(0);

		// Update Weights Support Set
		int i;
		for (i = 0; i < this->GetSupportSetElementsNumber(); i++) {
			(*this->Weights)[this->SupportSetIndexes->GetValue(i)] +=
					DeltaWeights->GetValue(i + 1);
		}
		delete DeltaWeights;

		// Update H
		Vector<double>* DeltaH = Gamma->Clone();
		DeltaH->ProductScalar(MinVariation);
		for (i = 0; i < this->GetSamplesTrainedNumber(); i++) {
			//if (this->SupportSetIndexes->Contains(i))
			//	LOG4_INFO("S(" << i << ") before=" << (**H)[i] << "   variation=" << DeltaH->Values[i] << "   Gamma = " << Gamma->Values[i]);
			(**H)[i] += DeltaH->GetValue(i);
		}
		delete DeltaH;
	} else {

		// Update Bias
		this->Bias += MinVariation;

		// Update H
		(*H)->SumScalar(MinVariation);
	}
}

void OnlineSVR::AddSampleToRemainingSet(int SampleIndex) {
	this->RemainingSetIndexes->Add(SampleIndex);
}

// Set Operations
void OnlineSVR::AddSampleToSupportSet(Vector<double>** H, Vector<double>* Beta,
		Vector<double>* Gamma, int SampleIndex, double ) {
	// Update H and Sets
    (**H)[SampleIndex] = SIGN<double>((**H)[SampleIndex]) * this->svr_parameters.get_svr_epsilon();
	this->SupportSetIndexes->Add(SampleIndex);
    this->AddSampleToR(SampleIndex, set_type_e::SUPPORT_SET, Beta, Gamma);
}

void OnlineSVR::AddSampleToErrorSet(int SampleIndex, double ) {
	// Update H and Sets
	(*this->Weights)[SampleIndex] = SIGN<double>(
            (*this->Weights)[SampleIndex]) * this->svr_parameters.get_svr_C();
	this->ErrorSetIndexes->Add(SampleIndex);
}

void OnlineSVR::MoveSampleFromSupportSetToErrorRemainingSet(int MinIndex,
		double ) {
	int Index = this->SupportSetIndexes->GetValue(MinIndex);
	double Weightsi = (*this->Weights)[Index];

    if (ABS(Weightsi) < ABS(this->svr_parameters.get_svr_C() - ABS(Weightsi))) {
		(*this->Weights)[Index] = 0;
	} else {
        (*this->Weights)[Index] = SIGN(Weightsi) * this->svr_parameters.get_svr_C();
	}

	if ((*this->Weights)[Index] == 0) {

		// CASE 3a: Move Sample from SupportSet to RemainingSet
		// Update H and Sets
		this->RemainingSetIndexes->Add(Index);
		this->SupportSetIndexes->RemoveAt(MinIndex);
		this->RemoveSampleFromR(MinIndex);
	} else {
		// CASE 3b: Move Sample from SupportSet to ErrorSet
		// Update H and Sets
		this->ErrorSetIndexes->Add(Index);
		this->SupportSetIndexes->RemoveAt(MinIndex);
		this->RemoveSampleFromR(MinIndex);
	}
}

void OnlineSVR::MoveSampleFromErrorSetToSupportSet(Vector<double>** H,
		Vector<double>* Beta, Vector<double>* Gamma, int MinIndex,
		double ) {
	int Index = this->ErrorSetIndexes->GetValue(MinIndex);
	// Update H and Sets
    (**H)[Index] = SIGN<double>((**H)[Index]) * this->svr_parameters.get_svr_epsilon();
	this->SupportSetIndexes->Add(Index);
	this->ErrorSetIndexes->RemoveAt(MinIndex);
    this->AddSampleToR(Index, set_type_e::ERROR_SET, Beta, Gamma);
}

void OnlineSVR::MoveSampleFromRemainingSetToSupportSet(Vector<double>** H,
		Vector<double>* Beta, Vector<double>* Gamma, int MinIndex,
		double ) {
	int Index = this->RemainingSetIndexes->GetValue(MinIndex);
	// Update H and Sets
    (**H)[Index] = SIGN<double>((**H)[Index]) * this->svr_parameters.get_svr_epsilon();
	this->SupportSetIndexes->Add(Index);
	this->RemainingSetIndexes->RemoveAt(MinIndex);
    this->AddSampleToR(Index, set_type_e::REMAINING_SET, Beta, Gamma);
}

void OnlineSVR::RemoveSampleFromSupportSet(int SampleSetIndex) {
	this->SupportSetIndexes->RemoveAt(SampleSetIndex);
	this->RemoveSampleFromR(SampleSetIndex);
}

void OnlineSVR::RemoveSampleFromErrorSet(int SampleSetIndex) {
	this->ErrorSetIndexes->RemoveAt(SampleSetIndex);
}

void OnlineSVR::RemoveSampleFromRemainingSet(int SampleSetIndex) {
	int SampleIndex = this->RemainingSetIndexes->GetValue(SampleSetIndex);
	this->RemainingSetIndexes->RemoveAt(SampleSetIndex);
	this->X->RemoveRow(SampleIndex);
	this->Y->RemoveAt(SampleIndex);
	this->Weights->RemoveAt(SampleIndex);
	if (this->SaveKernelMatrix) {
		this->RemoveSampleFromKernelMatrix(SampleIndex);
	}
	int i;
	for (i = 0; i < this->GetSupportSetElementsNumber(); i++) {
		if (this->SupportSetIndexes->GetValue(i) > SampleIndex) {
			(*this->SupportSetIndexes)[i] -= 1;
		}
	}
	for (i = 0; i < this->GetErrorSetElementsNumber(); i++) {
		if ((*this->ErrorSetIndexes)[i] > SampleIndex) {
			(*this->ErrorSetIndexes)[i] -= 1;
		}
	}
	for (i = 0; i < this->GetRemainingSetElementsNumber(); i++) {
		if ((*this->RemainingSetIndexes)[i] > SampleIndex) {
			(*this->RemainingSetIndexes)[i] -= 1;
		}
	}
	this->SamplesTrainedNumber--;
	if (this->SamplesTrainedNumber == 0) {
		this->Bias = 0;
	}
}

void OnlineSVR::RemoveSample(int SampleIndex) {
    this->X->RemoveRow(SampleIndex);
    this->Y->RemoveAt(SampleIndex);
    this->Weights->RemoveAt(SampleIndex);
    if (this->SaveKernelMatrix) {
        this->RemoveSampleFromKernelMatrix(SampleIndex);
    }

    int i;
    for (i = 0; i < this->GetSupportSetElementsNumber(); i++) {
        if ((*this->SupportSetIndexes)[i] > SampleIndex) {
            (*this->SupportSetIndexes)[i] -= 1;
        }
    }
    for (i = 0; i < this->GetErrorSetElementsNumber(); i++) {
        if ((*this->ErrorSetIndexes)[i] > SampleIndex) {
            (*this->ErrorSetIndexes)[i] -= 1;
        }
    }
    for (i = 0; i < this->GetRemainingSetElementsNumber(); i++) {
        if ((*this->RemainingSetIndexes)[i] > SampleIndex) {
            (*this->RemainingSetIndexes)[i] -= 1;
        }
    }

    this->SamplesTrainedNumber--;
    if (this->SamplesTrainedNumber == 1
            && this->GetErrorSetElementsNumber() > 0) {
        this->ErrorSetIndexes->RemoveAt(0);
        this->RemainingSetIndexes->Add(0);
        (*this->Weights)[0] = 0;
        this->Bias = this->Margin(this->X->Values[0].get(), (*this->Y)[0]);
    }
    if (this->SamplesTrainedNumber == 0) {
        this->Bias = 0;
    }
}

} // svr
