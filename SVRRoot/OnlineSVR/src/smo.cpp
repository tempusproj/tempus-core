#include "smo.h"
#include <boost/interprocess/sync/named_semaphore.hpp>

namespace svr {
namespace smo {



int smo_solver::solve(
        const viennacl::matrix<double> &kernel_matrix,
        const viennacl::vector<double> &linear_term,
        const viennacl::vector<double> &y_,
        const SVRParameters &param,
        viennacl::vector<double> &alpha2,
        viennacl::scalar<double> &rho)
{
    viennacl::matrix<double> quadratic_matrix(kernel_matrix.size1() * 2, kernel_matrix.size2() * 2);
    prepare_quadratic_matrix(kernel_matrix, quadratic_matrix);

    quadratic_matrix.switch_memory_context(viennacl::context(viennacl::MAIN_MEMORY));
    viennacl::backend::default_memory_type(viennacl::MAIN_MEMORY);

    viennacl::vector<double> qDiagonal = viennacl::diag(quadratic_matrix);

    return solve(quadratic_matrix, qDiagonal, linear_term, y_, param, alpha2, rho);
}


int smo_solver::solve(
        const viennacl::matrix<double> &quadratic_matrix,
        const viennacl::vector<double> &q_diagonal,
        const viennacl::vector<double> &linear_term,
        const viennacl::vector<double> &y_,
        const SVRParameters &param,
        viennacl::vector<double> &alpha2,
        viennacl::scalar<double> &rho)
{
    l = quadratic_matrix.size1();
    QD = q_diagonal;
    Q = quadratic_matrix;
    sign = viennacl::scalar_vector<double>(l, 1);

    viennacl::range sign_range_right(l / 2, l);

    viennacl::vector_range<viennacl::vector<double>> sign_subvec_right(sign, sign_range_right);
    sign_subvec_right *= -1;

    p = linear_term;
    alpha = alpha2;
    y = y_;
    this->C = param.get_svr_C();

    // initialize alpha_status
    alpha_status = viennacl::scalar_vector<char>(l, FREE);
    for(size_t i = 0; i < alpha_status.size(); i++)
    {
        if(alpha[i] >= C)
            alpha_status[i] = UPPER_BOUND;
        else if(alpha[i] <= 0)
            alpha_status[i] = LOWER_BOUND;
        else alpha_status[i] = FREE;
    }

    // initialize active set (for shrinking)
    active_set = viennacl::scalar_vector<int>(l, 0);
    for(size_t i = 0; i < active_set.size(); i++)
        active_set[i] = i;
    active_size = l;

    // initialize gradient
    gradient_m = viennacl::matrix<double>(1, l);
    for (size_t k = 0; k < l; k++)
        gradient_m(0,k) = linear_term[k];

    gradient_bar_m = viennacl::matrix<double>(1, l);


//    gradient = linear_term;
//    gradient_bar = viennacl::scalar_vector<double>(l, 0);

    viennacl::range qmatrix_row_range(0, l);
    for(size_t i = 0; i < l; i++) //TODO  optimize for matrix
        if(alpha_status[i] != LOWER_BOUND)
        {
            //get i-th row from KM
            viennacl::matrix_range<viennacl::matrix<double>> q_row(
                    Q, viennacl::range(i, i+1), qmatrix_row_range);
            double alpha_i = alpha[i];
            gradient_m += alpha_i * q_row;
            if(alpha_status[i] == UPPER_BOUND)
                gradient_bar_m += param.get_svr_C() * q_row;
        }

    // optimization step

    int iter = 0;
//    int max_iter = std::max<size_t>(300000, l>INT_MAX/100 ? INT_MAX : 100*l);
//    int counter = std::min<size_t>(l, 1000) + 1;
    while(iter < off_t(max_iter_))
    {
        // show progress
/*
        if(--counter == 0)
        {
            counter = std::min<size_t>(l,1000);
            LOG4_DEBUG(". " << iter);
        }
*/
        int i,j;
        if(select_working_set(i,j) != 0)
        {
            // reset active set size and check
            active_size = l;
            if(select_working_set(i,j) != 0)
                break;
//            else
//                counter = 1;	// do shrinking next iteration
        }
        if((-1 == i) || (-1 == j))
        {
            LOG4_ERROR("SMO select_working_set returned a -1 as an index, stopping SMO.");
            break;
        }

        ++iter;
        if(iter % 5000 == 0)
            LOG4_INFO("SMO working long ... ¯\\_(ツ)_/¯ iterations : " << iter);

        // update alpha[i] and alpha[j], handle bounds carefully
        viennacl::matrix_range<viennacl::matrix<double>> q_i_row(
                Q, viennacl::range(i, i+1), qmatrix_row_range);
        viennacl::matrix_range<viennacl::matrix<double>> q_j_row(
                Q, viennacl::range(j, j+1), qmatrix_row_range);
//        viennacl::vector<double> q_i_row = viennacl::row<double>(Q, i);
//        viennacl::vector<double> q_j_row = viennacl::row<double>(Q, j);

        double C_i = C, C_j = C;
        double old_alpha_i = alpha[i];
        double old_alpha_j = alpha[j];

        if(y[i]!=y[j])
        {
            double quad_coef = QD[i]+QD[j] + 2.0 * Q(i,j);

            if (quad_coef <= 0)
                quad_coef = TAU;

            double delta = (-gradient_m(0,i) - gradient_m(0,j)) / quad_coef;

            double diff = alpha[i] - alpha[j];

            alpha[i] += delta;
            alpha[j] += delta;
            if(diff > 0)
            {
                if(alpha[j] < 0)
                {
                    alpha[j] = 0;
                    alpha[i] = diff;
                }
            }
            else
            {
                if(alpha[i] < 0)
                {
                    alpha[i] = 0;
                    alpha[j] = -diff;
                }
            }
            if(diff > C_i - C_j) //diff > 0
            {
                if(alpha[i] > C_i)
                {
                    alpha[i] = C_i;
                    alpha[j] = C_i - diff;
                }
            }
            else
            {
                if(alpha[j] > C_j)
                {
                    alpha[j] = C_j;
                    alpha[i] = C_j + diff;
                }
            }
        }
        else
        {
            double quad_coef = QD[i] + QD[j] - 2.0 * Q(i,j);
            if (quad_coef <= 0)
                quad_coef = TAU;
            double delta = (gradient_m(0,i) - gradient_m(0,j)) / quad_coef;
            double sum = alpha[i] + alpha[j];

            alpha[i] -= delta;
            alpha[j] += delta;

            if(sum > C_i)
            {
                if(alpha[i] > C_i)
                {
                    alpha[i] = C_i;
                    alpha[j] = sum - C_i;
                }
            }
            else
            {
                if(alpha[j] < 0)
                {
                    alpha[j] = 0;
                    alpha[i] = sum;
                }
            }
            if(sum > C_j)
            {
                if(alpha[j] > C_j)
                {
                    alpha[j] = C_j;
                    alpha[i] = sum - C_j;
                }
            }
            else
            {
                if(alpha[i] < 0)
                {
                    alpha[i] = 0;
                    alpha[j] = sum;
                }
            }
        }

        // update gradient

        double delta_alpha_i = alpha[i] - old_alpha_i;
        double delta_alpha_j = alpha[j] - old_alpha_j;
        gradient_m += q_i_row * delta_alpha_i + q_j_row * delta_alpha_j;

        // update alpha_status and gradient_bar


        bool ui = (alpha_status[i] == UPPER_BOUND);
        bool uj = (alpha_status[j] == UPPER_BOUND);
        update_alpha_status(i);
        update_alpha_status(j);

        if(ui != (alpha_status[i] == UPPER_BOUND))
        {
            if(ui)
            {
                gradient_bar_m -= C_i * q_i_row;
            }
            else
            {
                gradient_bar_m += C_i * q_i_row;
            }
        }

        if(uj != (alpha_status[j] == UPPER_BOUND))
        {
            if(uj)
            {
                gradient_bar_m -= C_j * q_j_row;
            }
            else
            {
                gradient_bar_m += C_j * q_j_row;
            }
        }

    }

    if(iter >= off_t(max_iter_))
    {
        if(active_size < l)
        {
            active_size = l;
        }
        LOG4_WARN("Reached max number of iterations!");
    }

    // calculate rho

    rho = calculate_rho();

    // put back the solution
    {
        for(size_t i = 0; i < l; i++)
            alpha2[active_set[i]] = alpha[i];
    }
//    LOG4_DEBUG("\noptimization finished, #iter = " << iter);

    return iter;

}


/* TODO Move this to a custom ViennaCL kernel */
// return 1 if already optimal, return 0 otherwise
int smo_solver::select_working_set(int &out_i, int &out_j)
{
    // return i,j such that
    // i: maximizes -y_i * grad(f)_i, i in I_up(\alpha)
    // j: minimizes the decrease of obj value
    //    (if quadratic coefficeint <= 0, replace it with tau)
    //    -y_j*grad(f)_j < -y_i*grad(f)_i, j in I_low(\alpha)

    double Gmax = -INF;
    double Gmax2 = -INF;
    int Gmax_idx = -1;
    int Gmin_idx = -1;
    double obj_diff_min = INF;

    viennacl::vector<double> gradient = viennacl::row(gradient_m, 0);
    auto iter_y = y.begin();
    auto iter_alpha_status = alpha_status.begin();
    size_t t = 0;
    for (auto iter_gradient = gradient.begin(); iter_gradient != gradient.end(); iter_gradient++)
    {
        if(*iter_y == +1)
        {
            if(*iter_alpha_status != UPPER_BOUND)
                if(-(*iter_gradient) >= Gmax)
                {
                    Gmax = -(*iter_gradient);
                    Gmax_idx = t;
                }
        }
        else
        {
            if(*iter_alpha_status != LOWER_BOUND)
                if((*iter_gradient) >= Gmax)
                {
                    Gmax = (*iter_gradient);
                    Gmax_idx = t;
                }
        }
        t++;
        iter_y++;
        iter_alpha_status++;
    }

    int i = Gmax_idx;


    double yi = y(0);
    double QDi = QD(0);
    viennacl::vector<double> Qi_row;
    if(i != -1)
    {
        Qi_row = viennacl::row(Q, i);
        QDi = QD(i);
        yi = y(i);
    }


    t = 0;

    auto iter_y2 = y.begin();
    auto iter_alpha_status2 = alpha_status.begin();
    auto iter_QD = QD.begin();
    auto iter_Qi_row = Qi_row.begin();

    for (auto iter_gradient = gradient.begin(); iter_gradient != gradient.end(); iter_gradient++)
    {
        double grad_cur_value = *iter_gradient;
        if(*iter_y2 == +1)
        {
            if (*iter_alpha_status2 != LOWER_BOUND)
            {
                double grad_diff= Gmax + grad_cur_value;
                if (grad_cur_value >= Gmax2)
                    Gmax2 = grad_cur_value;

                if (grad_diff > 0)
                {
                    double obj_diff;
                    double quad_coef = QDi + *iter_QD - 2.0 * yi * (*iter_Qi_row);

                    if (quad_coef > 0)
                        obj_diff = -(grad_diff * grad_diff) / quad_coef;
                    else
                        obj_diff = -(grad_diff * grad_diff) / TAU;

                    if (obj_diff <= obj_diff_min)
                    {
                        Gmin_idx=t;
                        obj_diff_min = obj_diff;
                    }
                }
            }
        }
        else
        {
            if (*iter_alpha_status2 != UPPER_BOUND)
            {
                double grad_diff = Gmax - grad_cur_value;
                if (-grad_cur_value >= Gmax2)
                    Gmax2 = -grad_cur_value;

                if (grad_diff > 0)
                {
                    double obj_diff;
                    double quad_coef = QDi + *iter_QD + 2.0 * yi * (*iter_Qi_row);

                    if (quad_coef > 0)
                        obj_diff = -(grad_diff * grad_diff) / quad_coef;
                    else
                        obj_diff = -(grad_diff * grad_diff)/TAU;

                    if (obj_diff <= obj_diff_min)
                    {
                        Gmin_idx=t;
                        obj_diff_min = obj_diff;
                    }
                }
            }
        }

        t++;
        iter_y2++;
        iter_alpha_status2++;
        iter_QD++;
        iter_Qi_row++;
    }

    if(Gmax+Gmax2 < SMO_EPSILON)
        return 1;

    out_i = Gmax_idx;
    out_j = Gmin_idx;
    return 0;
}


double smo_solver::calculate_rho()
{
    double r;
    int nr_free = 0;
    double ub = INF, lb = -INF, sum_free = 0;
    viennacl::vector<double> gradient(gradient_m.size2());
    for (size_t k = 0; k < gradient_m.size2(); k++)
        gradient[k] = gradient_m(0,k);
    viennacl::vector<double> y_gradient = viennacl::linalg::element_prod(y, gradient);
    for(size_t i=0;i<active_size;i++)
    {
        if(alpha_status[i] == UPPER_BOUND)
        {
            if(y[i]==-1)
                ub = std::min<double>(ub, y_gradient[i]);
            else
                lb = std::max<double>(lb, y_gradient[i]);
        }
        else if(alpha_status[i] == LOWER_BOUND)
        {
            if(y[i]==+1)
                ub = std::min<double>(ub, y_gradient[i]);
            else
                lb = std::max<double>(lb, y_gradient[i]);
        }
        else
        {
            ++nr_free;
            sum_free += y_gradient[i];
        }
    }

    if(nr_free>0)
        r = sum_free/nr_free;
    else
        r = (ub+lb)/2;

    return r;
}


void prepare_kernel_matrix(
        datamodel::Matrix<double> &learning_data,
        const SVRParameters &param,
        viennacl::matrix<double> &kernel_matrix)
{
    LOG4_BEGIN();
    if (learning_data.GetLengthRows() < 1 || learning_data.GetLengthCols() < 1) {
        LOG4_ERROR("Learning data empty. Returning.");
        return;
    }

#ifdef VIENNACL_WITH_OPENCL
    LOG4_INFO("using opencl for kernel matrix computations");
    viennacl::backend::default_memory_type(viennacl::OPENCL_MEMORY);
#else
    viennacl::backend::default_memory_type(viennacl::MAIN_MEMORY);
#endif
    // TODO Do not clone once Matrix is viennacl::matrix backend
    kernel_matrix = svr::OnlineSVR::Kernel(learning_data.ViennaClone(), param);

    LOG4_END();
}


void prepare_quadratic_matrix(const viennacl::matrix<double> &kernel_matrix, viennacl::matrix<double> &quadratic_matrix)
{
    //form quadratic matrix
    viennacl::range q_m_row_top_range(0, quadratic_matrix.size1() / 2);
    viennacl::range q_m_row_bottom_range(quadratic_matrix.size1() / 2, quadratic_matrix.size1());
    viennacl::range q_m_col_left_range(0, quadratic_matrix.size2() / 2);
    viennacl::range q_m_cols_right_range(quadratic_matrix.size2() / 2, quadratic_matrix.size2());

    viennacl::matrix_range<viennacl::matrix<double>> quadratic_matrix_top_left(
            quadratic_matrix, q_m_row_top_range, q_m_col_left_range);
    viennacl::matrix_range<viennacl::matrix<double>> quadratic_matrix_top_right(
            quadratic_matrix, q_m_row_top_range, q_m_cols_right_range);

    viennacl::matrix_range<viennacl::matrix<double>> quadratic_matrix_bottom_left(
            quadratic_matrix,  q_m_row_bottom_range, q_m_col_left_range);
    viennacl::matrix_range<viennacl::matrix<double>> quadratic_matrix_bottom_right(
            quadratic_matrix, q_m_row_bottom_range, q_m_cols_right_range);
    quadratic_matrix_top_left = kernel_matrix;
    quadratic_matrix_top_right = kernel_matrix * viennacl::scalar<double>(-1.0);
    quadratic_matrix_bottom_left = kernel_matrix * viennacl::scalar<double>(-1.0);
    quadratic_matrix_bottom_right = kernel_matrix;
}

#ifdef VIENNACL_WITH_OPENCL
void
prepare_kernel_quadratic_matrices(
        const datamodel::Matrix<double> &learning_data, const SVRParameters &param, viennacl::matrix<double> &quadratic_matrix_cpu)
{
    svr::batch::gpu_handler::get_instance().gpu_sem_enter();
    size_t context_id = batch::gpu_handler::get_instance().get_free_gpu();

    viennacl::context ctx(viennacl::ocl::get_context(static_cast<long>(context_id)));
    viennacl::matrix<double> vcl_x(learning_data.GetLengthRows(), learning_data.GetLengthCols(), ctx);
    viennacl::matrix<double> kernel_matrix;
    learning_data.ViennaClone(vcl_x);
    svr::OnlineSVR::Kernel(vcl_x, param, ctx, kernel_matrix);
    viennacl::matrix<double> quadratic_matrix(kernel_matrix.size1() * 2, kernel_matrix.size2() * 2, viennacl::context(ctx));
    //quadratic_matrix.resize(kernel_matrix.size1() * 2, kernel_matrix.size2() * 2, true);
    viennacl::range q_m_row_top_range(0, quadratic_matrix.size1() / 2);
    viennacl::range q_m_row_bottom_range(quadratic_matrix.size1() / 2, quadratic_matrix.size1());
    viennacl::range q_m_col_left_range(0, quadratic_matrix.size2() / 2);
    viennacl::range q_m_cols_right_range(quadratic_matrix.size2() / 2, quadratic_matrix.size2());

    viennacl::matrix_range<viennacl::matrix<double>> quadratic_matrix_top_left(
            quadratic_matrix, q_m_row_top_range, q_m_col_left_range);
    viennacl::matrix_range<viennacl::matrix<double>> quadratic_matrix_top_right(
            quadratic_matrix, q_m_row_top_range, q_m_cols_right_range);
    viennacl::matrix_range<viennacl::matrix<double>> quadratic_matrix_bottom_left(
            quadratic_matrix, q_m_row_bottom_range, q_m_col_left_range);
    viennacl::matrix_range<viennacl::matrix<double>> quadratic_matrix_bottom_right(
            quadratic_matrix, q_m_row_bottom_range, q_m_cols_right_range);
    quadratic_matrix_top_left = kernel_matrix;
    quadratic_matrix_top_right = kernel_matrix * (-1.0);//viennacl::scalar<double>(-1.0);
    quadratic_matrix_bottom_left = kernel_matrix * (-1.0);//viennacl::scalar<double>(-1.0);
    quadratic_matrix_bottom_right = kernel_matrix;
    quadratic_matrix_cpu.resize(kernel_matrix.size1() * 2, kernel_matrix.size2() * 2, true);
    viennacl::fast_copy(quadratic_matrix, (double *)quadratic_matrix_cpu.handle().ram_handle().get());
    batch::gpu_handler::get_instance().return_gpu(context_id);

    svr::batch::gpu_handler::get_instance().gpu_sem_leave();
}

#endif //VIENNACL_WITH_OPENCL



} //smo
} //svr
