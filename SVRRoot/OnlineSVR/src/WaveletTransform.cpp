/*
 * Wavelet.cpp
 *
 *  The wavelib has few minor changes to remove the levels limitation.
 *  CMake's also changed to add proper install target and the PIC flag needed.
 *  
 *  The chosen convolution method is fft convolution but not direct conv, as the
 *  removed levels limitation makes the lib unstable when using direct conv.
 *  So only fft conv should be used.
 * 
 *  There were done few simple tests using fft convolution, which showed quite
 *  good (~4x) improvement over the old wavelet2d lib which used fftw lib.
 * 
 *  Created on: Jun 17, 2016
 *      Author: sgeorgiev
 */

#include "WaveletTransform.hpp"

namespace svr {


WaveletTransform::WaveletTransform(const std::string& name, const std::string& method, const size_t signal_len, const size_t levels)
: signal_len_(signal_len), levels_(levels)
{
    // Const_cast needed due to wavelib.
    wave_ = wave_init(const_cast<char*>(name.c_str()));
    wt_ = wt_init(wave_, const_cast<char*>(method.c_str()), signal_len, levels);

    // Using fft conv mode as the wavelib's levels limitation is removed, and
    // only fft conv is working properly.
    setWTConv(wt_, const_cast<char*>("fft"));
}

WaveletTransform::~WaveletTransform()
{
    wave_free(wave_);
    wt_free(wt_);
}

SWTransform::SWTransform(const std::string& name, const size_t signal_len, const size_t levels)
: WaveletTransform(name, "swt", signal_len, levels)
{

}

SWTransform::~SWTransform()
{

}

void SWTransform::swt_frame(const std::vector<double>& frame, std::vector<std::vector<double> >& result)
{
    result.clear();
    result.resize(frame.size(), std::vector<double>(levels_ + 1));

    swt(wt_, const_cast<double*>(&frame[0]));

    for(size_t frame_index = 0; frame_index < frame.size(); frame_index++) {
        for(size_t level = 0; level <= levels_; level++) {
            size_t index = frame.size() * level + frame_index;
            result[frame_index][level] = wt_->output[index];
        }
    }
}

void SWTransform::iswt_frame(const std::vector<double>& frame, std::vector<double>& result) {
    result.clear();
    result.resize(signal_len_);

    // Initialize input signal.
    wt_->output = const_cast<double*>(&frame[0]);
    wt_->outlength = frame.size();

    iswt(wt_, const_cast<double*>(&result[0]));
}

void WaveletTransform::summary() const {
    wt_summary(wt_);
}


}

