/******************************************************************************
 *                       ONLINE SUPPORT VECTOR REGRESSION                      *
 *                      Copyright 2006 - Francesco Parrella                    *
 *                                                                             *
 *This program is distributed under the terms of the GNU General Public License*
 ******************************************************************************/
#include "OnlineSVR.h"

namespace svr {

// Kernel Operations
double OnlineSVR::Kernel(Vector<double> *V1, Vector<double> *V2) {
    double K;
    Vector<double> *V;

    switch (this->svr_parameters.get_kernel_type()) {

        case kernel_type_e::LINEAR:
            // K = V1 * V2'
            return Vector<double>::ProductVectorScalar(V1, V2);
            break;

        case kernel_type_e::POLYNOMIAL:
            // K = (V1*V2' + 1) ^ KernelParam
            K = Vector<double>::ProductVectorScalar(V1, V2);
            return pow(K + 1, this->svr_parameters.get_svr_kernel_param());
            break;

        case kernel_type_e::RBF:
            // K = exp (-KernelParam * sum(dist(V1,V2)^2))
            V = Vector<double>::SubtractVector(V1, V2);
            V->SquareScalar();
            K = V->Sum();
            K *= -this->svr_parameters.get_svr_kernel_param();
            delete V;
            return exp(K);
            break;

        case kernel_type_e::RBF_GAUSSIAN:
            // K = exp (-sum(dist(V1,V2)^2 / 2*(KernelParam^2))
            V = Vector<double>::SubtractVector(V1, V2);
            V->SquareScalar();
            K = V->Sum();
            if (this->svr_parameters.get_svr_kernel_param() != 0)
                K /= -(2 * pow(this->svr_parameters.get_svr_kernel_param(), 2));
            else
                K /= -2;
            delete V;
            return exp(K);
            break;

        case kernel_type_e::RBF_EXPONENTIAL:
            // K = exp (-sum(dist(V1,V2) / 2*(KernelParam^2))
            V = Vector<double>::SubtractVector(V1, V2);
            K = V->AbsSum();
            if (this->svr_parameters.get_svr_kernel_param() != 0)
                K /= -(2 * pow(this->svr_parameters.get_svr_kernel_param(), 2));
            else
                K /= -2;
            delete V;
            return exp(K);
            break;

        case kernel_type_e::MLP:
            // K = tanh((V1*V2')*KernelParam + KernelParam2)
            K = Vector<double>::ProductVectorScalar(V1, V2);
            K = tanh(K * this->svr_parameters.get_svr_kernel_param() +
                     this->svr_parameters.get_svr_kernel_param2());
            return K;
            break;
        default:
//            LOG4_ERROR("incorrect kernel type" << this->kernel_type_);
            throw std::invalid_argument("incorrect kernel type");
            break;
    }

    return -1;
}

viennacl::matrix<double> OnlineSVR::Kernel(const viennacl::matrix<double> &train_data, const SVRParameters &svr_parameters)
{
    viennacl::matrix<double> kernel_matrix = viennacl::linalg::prod(train_data, viennacl::trans(train_data));
    switch (svr_parameters.get_kernel_type()) {
    case kernel_type_e::LINEAR:
    {
        break;
    }

    case kernel_type_e::POLYNOMIAL:
    {
        viennacl::matrix<double> I = viennacl::identity_matrix<double>(kernel_matrix.size1());
        kernel_matrix *= svr_parameters.get_svr_kernel_param();
        kernel_matrix += I * svr_parameters.get_svr_kernel_param2();
//        dotMatrix = viennacl::linalg::element_pow(dotMatrix, 1);
        break;
    }

    case kernel_type_e::RBF:
    {
        viennacl::vector<double> diagonal = viennacl::diag(kernel_matrix,0);
        viennacl::vector<double> i = viennacl::scalar_vector<double>(diagonal.size(), 1.);
        viennacl::matrix<double> temp = viennacl::linalg::outer_prod(i, diagonal);

        kernel_matrix = (temp + viennacl::trans(temp) - 2. * kernel_matrix) * -svr_parameters.get_svr_kernel_param();
        kernel_matrix = viennacl::linalg::element_exp(kernel_matrix);
        break;
    }

    case kernel_type_e::RBF_GAUSSIAN:
    {
        viennacl::vector<double> diagonal = viennacl::diag(kernel_matrix,0);
        viennacl::vector<double> i = viennacl::scalar_vector<double>(diagonal.size(), 1.);
        viennacl::matrix<double> temp = viennacl::linalg::outer_prod(i, diagonal);
        kernel_matrix = (temp + viennacl::trans(temp) - 2. * kernel_matrix) /
                (-2.*std::pow(svr_parameters.get_svr_kernel_param(),2.));
        kernel_matrix = viennacl::linalg::element_exp(kernel_matrix);
        break;
    }

    case kernel_type_e::RBF_EXPONENTIAL:
    {
        viennacl::vector<double> diagonal = viennacl::diag(kernel_matrix,0);
        viennacl::vector<double> i = viennacl::scalar_vector<double>(diagonal.size(), 1.);
        viennacl::matrix<double> temp = viennacl::linalg::outer_prod(i, diagonal);
        kernel_matrix = temp + viennacl::trans(temp) - 2. * kernel_matrix;
        kernel_matrix = viennacl::linalg::element_sqrt(kernel_matrix);
        kernel_matrix = kernel_matrix / (-2.*std::pow(svr_parameters.get_svr_kernel_param(),2.));
        break;
    }

    case kernel_type_e::MLP:
    {
        viennacl::matrix<double> I = viennacl::identity_matrix<double>(kernel_matrix.size1());
        kernel_matrix = kernel_matrix * svr_parameters.get_svr_kernel_param();
        kernel_matrix += svr_parameters.get_svr_kernel_param2() * I;
        kernel_matrix = viennacl::linalg::element_tanh(kernel_matrix);
        break;
    }
    default:
//        LOG4_ERROR("incorrect kernel type" << this->kernel_type_); TODO: print_kernel
        throw std::invalid_argument("incorrect kernel type");
        break;
    }
    return kernel_matrix;
}

viennacl::matrix<double> OnlineSVR::Kernel(const viennacl::matrix<double> &train_data, const SVRParameters &svr_parameters,
                                           viennacl::context &ctx, viennacl::matrix<double> &kernel_matrix)
{
    kernel_matrix = viennacl::linalg::prod(train_data, viennacl::trans(train_data));
    switch (svr_parameters.get_kernel_type()) {
        case kernel_type_e::LINEAR:
        {
            break;
        }

        case kernel_type_e::POLYNOMIAL:
        {
            viennacl::matrix<double> I = viennacl::identity_matrix<double>(kernel_matrix.size1(), ctx);
            kernel_matrix *= svr_parameters.get_svr_kernel_param();
            kernel_matrix += I * svr_parameters.get_svr_kernel_param2();
//        dotMatrix = viennacl::linalg::element_pow(dotMatrix, 1);
            break;
        }

        case kernel_type_e::RBF:
        {
            viennacl::vector<double> diagonal = viennacl::diag(kernel_matrix,0);
            viennacl::vector<double> i = viennacl::scalar_vector<double>(diagonal.size(), 1, ctx);
            viennacl::matrix<double> temp = viennacl::linalg::outer_prod(i, diagonal);

            kernel_matrix = (temp + viennacl::trans(temp) - 2 * kernel_matrix) * -svr_parameters.get_svr_kernel_param();
            kernel_matrix = viennacl::linalg::element_exp(kernel_matrix);
            break;
        }

        case kernel_type_e::RBF_GAUSSIAN:
        {
            viennacl::vector<double> diagonal = viennacl::diag(kernel_matrix,0);
            viennacl::vector<double> i = viennacl::scalar_vector<double>(diagonal.size(), 1, ctx);
            viennacl::matrix<double> temp = viennacl::linalg::outer_prod(i, diagonal);
            kernel_matrix = (temp + viennacl::trans(temp) - 2 * kernel_matrix) /
                            (-2*std::pow(svr_parameters.get_svr_kernel_param(),2));
            kernel_matrix = viennacl::linalg::element_exp(kernel_matrix);
            break;
        }

        case kernel_type_e::RBF_EXPONENTIAL:
        {
            viennacl::vector<double> diagonal = viennacl::diag(kernel_matrix,0);
            viennacl::vector<double> i = viennacl::scalar_vector<double>(diagonal.size(), 1, ctx);
            viennacl::matrix<double> temp = viennacl::linalg::outer_prod(i, diagonal);
            kernel_matrix = temp + viennacl::trans(temp) - 2 * kernel_matrix;
            kernel_matrix = viennacl::linalg::element_sqrt(kernel_matrix);
            kernel_matrix = kernel_matrix / (-2*std::pow(svr_parameters.get_svr_kernel_param(),2));
            break;
        }

        case kernel_type_e::MLP:
        {
            viennacl::matrix<double> I = viennacl::identity_matrix<double>(kernel_matrix.size1(), ctx);
            kernel_matrix = kernel_matrix * svr_parameters.get_svr_kernel_param();
            kernel_matrix += svr_parameters.get_svr_kernel_param2() * I;
            kernel_matrix = viennacl::linalg::element_tanh(kernel_matrix);
            break;
        }
        default:
//        LOG4_ERROR("incorrect kernel type" << this->kernel_type_); TODO: print_kernel
            throw std::invalid_argument("incorrect kernel type");
            break;
    }
    return kernel_matrix;
}

viennacl::matrix<double> OnlineSVR::Kernel(Matrix<double> * X, const SVRParameters &svr_parameters)
{
    return Kernel(X->ViennaClone(), svr_parameters);
}

std::vector<std::vector<double>> OnlineSVR::BuildKernelMatrix(Matrix<double> *X)
{
    Matrix<double> kernelMatrix;
    for (int i = 0; i < X->GetLengthRows(); i++)
    {
        Vector<double>* Xcur = X->GetRowRef(i);
        Vector<double>* V = new Vector<double>();
        if (X->GetLengthRows() > 1) {
            for (int i = 0; i < kernelMatrix.GetLengthRows(); i++)
                V->Add(this->Kernel(X->GetRowRef(i), Xcur));
            kernelMatrix.AddColCopy(V);
        }
        V->Add(this->Kernel(Xcur, Xcur));
        kernelMatrix.AddRowRef(V);
    }
    std::vector<std::vector<double>> stdX(kernelMatrix.GetLengthRows());
    for (long i = 0; i < kernelMatrix.GetLengthRows(); i++)
    {
        Vector<double> * row = kernelMatrix.GetRowRef(i);
        row->copy_to(stdX[i]);
    }

    return stdX;
}

} // svr
