/******************************************************************************
*                       ONLINE SUPPORT VECTOR REGRESSION                      *
*                      Copyright 2006 - Francesco Parrella                    *
*                                                                             *
*This program is distributed under the terms of the GNU General Public License*
******************************************************************************/


#include <iostream>
#include "OnlineSVR.h"

namespace svr {

// Learning Operations
int OnlineSVR::Stabilize() {
	// Initialization
	int Flops = 0;
    LOG4_DEBUG("Starting Stabilize...");

	// Stabilizing
    std::vector<double> current_unstabilized_sample_indexes;
	int CurrentSampleIndex = 0;
	int TrueSampleIndex = 0;
	int LastSampleIndex = this->GetSamplesTrainedNumber() - 1;
	while (CurrentSampleIndex <= LastSampleIndex) {
		if (!this->VerifyKKTConditions(CurrentSampleIndex)) {
            current_unstabilized_sample_indexes.push_back(TrueSampleIndex);
            LOG4_DEBUG("Stabilizing " << TrueSampleIndex + 1 << "/" << X->GetLengthRows());
			Vector<double>* X = this->X->GetRowCopy(CurrentSampleIndex);
			double Y = this->Y->GetValue(CurrentSampleIndex);
			Flops += this->Unlearn(CurrentSampleIndex);
			Flops += this->Learn(X, Y);
			delete X;
			LastSampleIndex--;
		} else {
			CurrentSampleIndex++;
		}
		TrueSampleIndex++;
	}
    update_stabilization_convergence_status(current_unstabilized_sample_indexes);

	if (this->VerifyKKTConditions())
        LOG4_DEBUG("Stabilized " << X->GetLengthRows() << " elements correctly");
	else
        LOG4_DEBUG("Some elements cannot be stabilized");

	return Flops;
}

bool OnlineSVR::VerifyKKTConditions() {
	Vector<double>* H = this->Margin(this->X, this->Y);
	bool ris = this->VerifyKKTConditions(H);
	delete H;
	return ris;
}

bool OnlineSVR::VerifyKKTConditions(Vector<double>* H)
{
    int SampleIndex, SampleSetIndex; //TODO: remove this
    return this->VerifyKKTConditions(H, &SampleIndex, &SampleSetIndex);
}

bool OnlineSVR::VerifyKKTConditions(Vector<double>* H, int* SampleIndex,
     int* SampleSetIndex)
{
    double C = this->svr_parameters.get_svr_C();
    double Epsilon = this->svr_parameters.get_svr_epsilon();
    double Error = SMO_EPSILON;

	// Support Set
	int i;
	for (i = 0; i < this->GetSupportSetElementsNumber(); i++) {
		(*SampleIndex) = this->SupportSetIndexes->GetValue(i);
		double Weightsi = this->Weights->GetValue(*SampleIndex);
		double Hi = H->GetValue(*SampleIndex);
		if (!((OnlineSVR::IsContained(Weightsi, -C, 0, Error)
				&& OnlineSVR::IsEquals(Hi, Epsilon, Error))
				|| ((OnlineSVR::IsContained(Weightsi, 0, C, Error)
						&& OnlineSVR::IsEquals(Hi, -Epsilon, Error))))) {
			(*SampleSetIndex) = i;
			return false;
		}
	}

	// Error Set
	for (i = 0; i < this->GetErrorSetElementsNumber(); i++) {
		(*SampleIndex) = this->ErrorSetIndexes->GetValue(i);
		double Weightsi = this->Weights->GetValue(*SampleIndex);
		double Hi = H->GetValue(*SampleIndex);
		if (!((OnlineSVR::IsEquals(Weightsi, -C, Error)
				&& OnlineSVR::IsBigger(Hi, Epsilon, Error))
				|| ((OnlineSVR::IsEquals(Weightsi, C, Error)
						&& OnlineSVR::IsLesser(Hi, -Epsilon, Error))))) {
			(*SampleSetIndex) = i;
			return false;
		}
	}

	// Remaining Set
	for (i = 0; i < this->GetRemainingSetElementsNumber(); i++) {
		(*SampleIndex) = this->RemainingSetIndexes->GetValue(i);
		double Weightsi = this->Weights->GetValue(*SampleIndex);
		double Hi = H->GetValue(*SampleIndex);
		if (!(OnlineSVR::IsEquals(Weightsi, 0, Error)
				&& OnlineSVR::IsContained(Hi, -Epsilon, +Epsilon, Error))) {
			(*SampleSetIndex) = i;
			return false;
		}
	}

	return true;
}

bool OnlineSVR::VerifyKKTConditions(int SampleIndex) {
    double C = this->svr_parameters.get_svr_C();
    double Epsilon = this->svr_parameters.get_svr_epsilon();
    double Error = SMO_EPSILON;
	double Hi = this->Margin(this->X->GetRowRef(SampleIndex),
			this->Y->GetValue(SampleIndex));
	double Weightsi = this->Weights->GetValue(SampleIndex);

	// Support Set
	if (this->SupportSetIndexes->Find(SampleIndex) >= 0) {
		if (!((OnlineSVR::IsContained(Weightsi, -C, 0, Error)
				&& OnlineSVR::IsEquals(Hi, Epsilon, Error))
				|| ((OnlineSVR::IsContained(Weightsi, 0, C, Error)
						&& OnlineSVR::IsEquals(Hi, -Epsilon, Error)))))
			return false;
		else
			return true;
	}

	// Error Set
	if (this->ErrorSetIndexes->Find(SampleIndex) >= 0) {
		if (!((OnlineSVR::IsEquals(Weightsi, -C, Error)
				&& OnlineSVR::IsBigger(Hi, Epsilon, Error))
				|| ((OnlineSVR::IsEquals(Weightsi, C, Error)
						&& OnlineSVR::IsLesser(Hi, -Epsilon, Error)))))
			return false;
		else
			return true;
	}

	// Remaining Set
	if (this->RemainingSetIndexes->Find(SampleIndex) >= 0) {
		if (!(OnlineSVR::IsEquals(Weightsi, 0, Error)
				&& OnlineSVR::IsContained(Hi, -Epsilon, +Epsilon, Error)))
			return false;
		else
			return true;
	}

	return true;
}

bool OnlineSVR::IsEquals(double Value1, double Value2, double Error) {
	//if (abs(Value1-Value2)<=Error)
	double Diff = Value1 - Value2;
	Diff = Diff > 0 ? Diff : -Diff;
	if (Diff <= Error)
		return true;
	else
		return false;
}

bool OnlineSVR::IsLesser(double Value1, double Value2, double Error) {
	if (Value1 - Error <= Value2)
		return true;
	else
		return false;
}

bool OnlineSVR::IsBigger(double Value1, double Value2, double Error) {
	if (Value1 + Error >= Value2)
		return true;
	else
		return false;
}
bool OnlineSVR::IsContained(double Value, double From, double To,
		double Error) {
	if (From - Error <= Value && Value <= To + Error)
		return true;
	else
		return false;
}

void OnlineSVR::update_stabilization_convergence_status(const std::vector<double> & unstabilized_sample_indexes)
{
    LOG4_DEBUG("unstabilized_sample_indexes " << unstabilized_sample_indexes.size());
    if (unstabilized_sample_indexes.empty() || unstabilized_sample_indexes.size() <= this->unstabilized_sample_indexes_.size())
        this->is_stabilization_converging_ = true;
    else
        this->is_stabilization_converging_ = false;
   this->unstabilized_sample_indexes_ = unstabilized_sample_indexes;
}

} // svr
