/******************************************************************************
*                       ONLINE SUPPORT VECTOR REGRESSION                      *
*                      Copyright 2006 - Francesco Parrella                    *
*                                                                             *
*This program is distributed under the terms of the GNU General Public License*
******************************************************************************/

#include <iostream>
#include <fstream>
#include "OnlineSVR.tcc"

using namespace std;

namespace svr {

// I/O Operations
void OnlineSVR::LoadOnlineSVR(const char *Filename) {
    // Open the file
    ifstream File(Filename, ios::in);
    if (!File) {
        LOG4_ERROR("Error. File not found.");
        return;
    }

    if (LoadOnlineSVR(*this, File) == false)
        LOG4_ERROR("Failed loading model from " << Filename);

    // Close the file
    File.close();
}

void OnlineSVR::LoadOnlineSVR(std::stringstream &input_stream) {
    if (LoadOnlineSVR(*this, input_stream) == false)
        throw std::runtime_error("Failed loading model from stream.");
}


void OnlineSVR::SaveOnlineSVR(const char *Filename) const {
    // Open the file
    ofstream File(Filename, ios::out);
    if (!File) {
        LOG4_ERROR("Error. It's impossible to create the file.");
        return;
    }

    if (SaveOnlineSVR(*this, File) == false)
        LOG4_ERROR("Failed saving model to " << Filename);

    // Close the file
    File.close();
}

void OnlineSVR::SaveOnlineSVR(std::stringstream &output_stream) const {
    OnlineSVR::SaveOnlineSVR(*this, output_stream);
}

void OnlineSVR::Import(char *Filename, Matrix<double> **X, Vector<double> **Y) {
    // Open the file
    ifstream File(Filename, ios::in);
    if (!File) {
        LOG4_ERROR("Error. File not found.");
        return;
    }

    // Time
    LOG4_DEBUG("Starting import new data...");
    int RowsNumber;

    try {
        int ColsNumber;

        // Reading the parameters
        File >> RowsNumber >> ColsNumber;

        // Import the data
        (*X) = new Matrix<double>();
        (*Y) = new Vector<double>(RowsNumber);
        double Value;
        for (int i = 0; i < RowsNumber; i++) {
            // Add Y
            File >> Value;
            (*Y)->Add(Value);
            // Add X
            Vector<double> *Line = new Vector<double>(ColsNumber - 1);
            for (int j = 0; j < ColsNumber - 1; j++) {
                File >> Value;
                Line->Add(Value);
            }
            (*X)->AddRowRef(Line);
        }

    }
    catch (...) {
        LOG4_ERROR("Error. The file is probably corrupted.");
    }

    // Close the file
    File.close();
}

void OnlineSVR::Import(char *Filename, Matrix<double> **AngularPositions, Matrix<double> **MotorCurrents, Matrix<double> **AppliedVoltages) {
    // Open the file
    ifstream File(Filename, ios::in);
    if (!File) {
        LOG4_ERROR("Error. File not found.");
        return;
    }

    // Time
    LOG4_DEBUG("Starting import new data...");
    int RowsNumber = 0;

    try {

        (*AngularPositions) = new Matrix<double>();
        (*MotorCurrents) = new Matrix<double>();
        (*AppliedVoltages) = new Matrix<double>();
        char X[80];
        double X1, X2, X3, X4;

        File >> X >> X;
        while (!File.eof()) {
            RowsNumber++;
            // Import Angular Position
            File >> X1 >> X >> X2 >> X >> X3 >> X >> X4 >> X >> X >> X;
            Vector<double> *Line = new Vector<double>(4);
            Line->Add(X1);
            Line->Add(X2);
            Line->Add(X3);
            Line->Add(X4);
            (*AngularPositions)->AddRowRef(Line);
            // Import Motor Currents
            File >> X1 >> X >> X2 >> X >> X3 >> X >> X4 >> X >> X >> X;
            Line = new Vector<double>(4);
            Line->Add(X1);
            Line->Add(X2);
            Line->Add(X3);
            Line->Add(X4);
            (*MotorCurrents)->AddRowRef(Line);
            // Import Applied Voltages
            File >> X1 >> X >> X2 >> X >> X3 >> X >> X4 >> X >> X >> X;
            Line = new Vector<double>(4);
            Line->Add(X1);
            Line->Add(X2);
            Line->Add(X3);
            Line->Add(X4);
            (*AppliedVoltages)->AddRowRef(Line);
        }
    }
    catch (...) {
        LOG4_ERROR("Error. Data is corrupted.");
    }

    // Close the file
    File.close();
}

    void OnlineSVR::LoadOnlineSVR(svm_model * libsvmModel, Matrix<double> * learningData, Vector<double> * referenceData) {
        if (svm_get_svm_type(libsvmModel) != EPSILON_SVR)
            throw std::logic_error("trying to load a libsvm model of incorrect type");
        this->SamplesTrainedNumber = libsvmModel->l;
        //read X and Y sets
        const int *sv_indices = libsvmModel->sv_indices;

        for (int i = 0; i < this->SamplesTrainedNumber; i++) {
            int currentSvIndex = sv_indices[i];
            this->Y->Add(referenceData->GetValue(currentSvIndex - 1));
            this->X->AddRowCopy(learningData->GetRowRef(currentSvIndex - 1));
            this->SupportSetIndexes->Add(i);
        }
        //read weights
        for (int i = 0; i < this->SamplesTrainedNumber; i++) {
            this->Weights->Add(1.0);
        }
        //read bias
        this->Bias = -libsvmModel->rho[0]; //it's negative

        // Kernel Matrix
        if (this->SaveKernelMatrix)
            this->BuildKernelMatrix();
    }

} // svr
