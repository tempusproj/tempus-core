/******************************************************************************
 *                       ONLINE SUPPORT VECTOR REGRESSION                      *
 *                      Copyright 2006 - Francesco Parrella                    *
 *                                                                             *
 *This program is distributed under the terms of the GNU General Public License*
 ******************************************************************************/

#include "OnlineSVR.tcc"

namespace svr {

kernel_type_e get_kernel_type_from_string(const std::string & kernel_type_str)
{
    kernel_type_e kernel_type;
    if (kernel_type_str == "LINEAR")
        kernel_type = kernel_type_e::LINEAR;
    else if (kernel_type_str == "POLYNOMIAL")
        kernel_type = kernel_type_e::POLYNOMIAL;
    else if (kernel_type_str == "RBF")
        kernel_type = kernel_type_e::RBF;
    else if (kernel_type_str == "RBF_GAUSSIAN")
        kernel_type = kernel_type_e::RBF_GAUSSIAN;
    else if (kernel_type_str == "RBF_EXPONENTIAL")
        kernel_type = kernel_type_e::RBF_EXPONENTIAL;
    else if (kernel_type_str == "MLP")
        kernel_type = kernel_type_e::MLP;
    else {
        LOG4_ERROR("Incorrect kernel type");
        throw std::invalid_argument("Incorrect kernel type");
    }
    return kernel_type;
}

std::string kernel_type_to_string(const kernel_type_e kernel_type)
{
    std::string kernel_type_str;
    if (kernel_type == kernel_type_e::LINEAR)
        kernel_type_str = "LINEAR";
    else if (kernel_type == kernel_type_e::POLYNOMIAL)
        kernel_type_str = "POLYNOMIAL";
    else if (kernel_type == kernel_type_e::RBF)
        kernel_type_str = "RBF";
    else if (kernel_type == kernel_type_e::RBF_GAUSSIAN)
        kernel_type_str = "RBF_GAUSSIAN";
    else if (kernel_type == kernel_type_e::RBF_EXPONENTIAL)
        kernel_type_str = "RBF_EXPONENTIAL";
    else if (kernel_type == kernel_type_e::MLP)
        kernel_type_str = "MLP";
    else {
        LOG4_ERROR("Incorrect kernel type");
        throw std::invalid_argument("Incorrect kernel type");
    }
    return kernel_type_str;
}

// Initialization
OnlineSVR::OnlineSVR()
{
	this->svr_parameters = SVRParameters();
	this->X = new Matrix<double>();
	this->Y = new Vector<double>();
	this->Weights = new Vector<double>();
	this->Bias = .0;
	this->SamplesTrainedNumber = 0;
	this->SupportSetIndexes = new Vector<int>();
	this->ErrorSetIndexes = new Vector<int>();
	this->RemainingSetIndexes = new Vector<int>();
	this->R = new Matrix<double>();
	this->KernelMatrix = new Matrix<double>();
}

// Initialization
OnlineSVR::OnlineSVR(const SVRParameters &svr_parameters_) :
        svr_parameters(svr_parameters_)
{
	this->X = new Matrix<double>();
	this->Y = new Vector<double>();
	this->Weights = new Vector<double>();
	this->Bias = .0;
	this->SamplesTrainedNumber = 0;
	this->SupportSetIndexes = new Vector<int>();
	this->ErrorSetIndexes = new Vector<int>();
	this->RemainingSetIndexes = new Vector<int>();
	this->R = new Matrix<double>();
	this->KernelMatrix = new Matrix<double>();
}

OnlineSVR::OnlineSVR(std::stringstream &input_stream) : OnlineSVR() {
	if (LoadOnlineSVR(*this, input_stream) == false)
		throw std::runtime_error("Failed loading model from stream.");
}

OnlineSVR::~OnlineSVR() {
	delete this->X;
	delete this->Y;
	delete this->Weights;
	delete this->SupportSetIndexes;
	delete this->ErrorSetIndexes;
	delete this->RemainingSetIndexes;
	delete this->R;
	delete this->KernelMatrix;
}

void OnlineSVR::Clear() {
	this->SamplesTrainedNumber = 0;
	this->Bias = 0;
	this->X->Clear();
	this->Y->Clear();
	this->Weights->Clear();
	this->SupportSetIndexes->Clear();
	this->ErrorSetIndexes->Clear();
	this->RemainingSetIndexes->Clear();
	this->R->Clear();
	this->KernelMatrix->Clear();
}

OnlineSVR* OnlineSVR::Clone() {
	OnlineSVR* p_onlinesvr = new OnlineSVR(this->svr_parameters);
    p_onlinesvr->svr_parameters = this->svr_parameters;
	p_onlinesvr->StabilizedLearning = this->StabilizedLearning;
	p_onlinesvr->SaveKernelMatrix = this->SaveKernelMatrix;
	p_onlinesvr->X = this->X->Clone();
	p_onlinesvr->Y = this->Y->Clone();
	p_onlinesvr->Weights = this->Weights->Clone();
	p_onlinesvr->Bias = this->Bias;
	p_onlinesvr->SupportSetIndexes = this->SupportSetIndexes->Clone();
	p_onlinesvr->ErrorSetIndexes = this->ErrorSetIndexes->Clone();
	p_onlinesvr->RemainingSetIndexes = this->RemainingSetIndexes->Clone();
	p_onlinesvr->R = this->R->Clone();
	p_onlinesvr->KernelMatrix = this->KernelMatrix->Clone();
	p_onlinesvr->SamplesTrainedNumber = this->SamplesTrainedNumber;
	return p_onlinesvr;
}

// Attributes Operations
SVRParameters &OnlineSVR::get_svr_parameters() {
    return this->svr_parameters;
}

void OnlineSVR::set_svr_parameters(const SVRParameters &svr_parameters_) {
    this->svr_parameters = svr_parameters_;
}

int OnlineSVR::GetSamplesTrainedNumber() {
	return this->SamplesTrainedNumber;
}

int OnlineSVR::GetSupportSetElementsNumber() const {
	return this->SupportSetIndexes->GetLength();
}

int OnlineSVR::GetErrorSetElementsNumber() const {
	return this->ErrorSetIndexes->GetLength();
}

int OnlineSVR::GetRemainingSetElementsNumber() const {
	return this->RemainingSetIndexes->GetLength();
}

bool OnlineSVR::GetStabilizedLearning() {
	return StabilizedLearning;
}

void OnlineSVR::SetStabilizedLearning(bool StabilizedLearning) {
	this->StabilizedLearning = StabilizedLearning;
}
bool OnlineSVR::GetSaveKernelMatrix() {
	return SaveKernelMatrix;
}

void OnlineSVR::SetSaveKernelMatrix(bool SaveKernelMatrix) {
	if (!this->SaveKernelMatrix && SaveKernelMatrix) {
		this->BuildKernelMatrix();
	} else if (!SaveKernelMatrix) {
		this->KernelMatrix->Clear();
	}
	this->SaveKernelMatrix = SaveKernelMatrix;
}

Vector<int>* OnlineSVR::GetSupportSetIndexes() const {
	return this->SupportSetIndexes;
}

Vector<int>* OnlineSVR::GetErrorSetIndexes() const {
	return this->ErrorSetIndexes;
}
Vector<int>* OnlineSVR::GetRemainingSetIndexes() const {
	return this->RemainingSetIndexes;
}

void OnlineSVR::set_samples_trained_number(size_t samples_num)
{
    this->SamplesTrainedNumber = samples_num;
}

void OnlineSVR::add_index_to_support_set(int index)
{
    this->SupportSetIndexes->Add(index);
}

void OnlineSVR::set_bias(double bias)
{
    this->Bias = bias;
}

void OnlineSVR::add_weight(double weight)
{
    this->Weights->Add(weight);
}

void OnlineSVR::set_kernel_matrix(Matrix<double> *matrix)
{
    delete KernelMatrix;
    this->KernelMatrix = matrix;
}

void OnlineSVR::set_weights(Vector<double> *weights) {
    delete Weights;
    Weights = weights;
}

void OnlineSVR::set_x(Matrix<double> *x) {
    delete X;
    X = x;
}

void OnlineSVR::set_y(Vector<double> *y) {
    delete Y;
    Y = y;
}

void OnlineSVR::set_R_matrix(Matrix<double>* r_matrix)
{
    delete R;
    R = r_matrix;
}


// Predict/Margin Operations
double OnlineSVR::Predict(int Index) {
	double PredictedValue = 0;
	for (int i = 0; i < this->GetSamplesTrainedNumber(); i++) {
		PredictedValue += this->Weights->GetValue(i)
				* this->KernelMatrix->GetValue(i, Index);
	}

	// Bias
	PredictedValue += this->Bias;
	return PredictedValue;
}

// TODO Why a pointer argument here? Replace with reference.
double OnlineSVR::Predict(Vector<double>* V) {
	// Trained Elements
	double PredictedValue = 0;
	for (int i = 0; i < this->GetSamplesTrainedNumber(); i++) {
		PredictedValue += this->Weights->GetValue(i)
				* this->Kernel(this->X->GetRowRef(i), V);
	}

	// Bias
	PredictedValue += this->Bias;
	return PredictedValue;
}

/* TODO Optimize, replace with real batch predict implementation */
Vector<double>* OnlineSVR::Predict(Matrix<double>* X) {
	if (this->X == X && this->SaveKernelMatrix) {
		Vector<double>* V = new Vector<double>(X->GetLengthRows());
		for (int i = 0; i < X->GetLengthRows(); i++) {
			V->Add(this->Predict(i));
		}
		return V;
	} else {
		Vector<double>* V = new Vector<double>(X->GetLengthRows());
		for (int i = 0; i < X->GetLengthRows(); i++) {
			V->Add(this->Predict(X->GetRowRef(i)));
		}
		return V;
	}
}

double OnlineSVR::Margin(Vector<double>* X, double Y) {
	return this->Predict(X) - Y;
}

Vector<double>* OnlineSVR::Margin(Matrix<double>* X, Vector<double>* Y) {
	Vector<double>* V = this->Predict(X);
	V->SubtractVector(Y);
	return V;
}

double OnlineSVR::Predict(double* X, int ElementsSize) {
	Vector<double> V(X, ElementsSize);
	double PredictedValue = this->Predict(&V);
	return PredictedValue;
}

Vector<double>* OnlineSVR::Predict(double** X, int ElementsNumber, int ElementsSize) {
	Matrix<double>* M = new Matrix<double>(X, ElementsNumber, ElementsSize);
	Vector<double>* V = this->Predict(M);
	delete M;
	return V;
}

double OnlineSVR::Margin(double* X, double Y, int ElementsSize) {
	return this->Predict(X, ElementsSize) - Y;
}

Vector<double>* OnlineSVR::Margin(double** X, double* Y, int ElementsNumber,
		int ElementsSize) {
	Vector<double>* V1 = new Vector<double>(
			this->Predict(X, ElementsNumber, ElementsSize), ElementsNumber);
	Vector<double>* V2 = new Vector<double>(Y, ElementsNumber);
	V1->SubtractVector(V2);
	delete V2;
	return V1;
}

// Other Kernel Operations
Matrix<double>* OnlineSVR::Q(Vector<int>* V1, Vector<int>* V2) {
	if (!SaveKernelMatrix) {
		Matrix<double>* M = new Matrix<double>();
		for (int i = 0; i < V1->GetLength(); i++) {
			Vector<double>* V = new Vector<double>(V1->GetLength());
			for (int j = 0; j < V2->GetLength(); j++) {
				V->Add(
						this->Kernel(this->X->GetRowRef(V1->GetValue(i)),
								this->X->GetRowRef(V2->GetValue(j))));
			}
			M->AddRowRef(V);
		}
		return M;
	} else {
		Matrix<double>* M = new Matrix<double>();
		for (int i = 0; i < V1->GetLength(); i++) {
			Vector<double>* V = new Vector<double>(V1->GetLength());
			for (int j = 0; j < V2->GetLength(); j++) {
				V->Add(
						this->KernelMatrix->GetValue(V1->GetValue(i),
								V2->GetValue(j)));
			}
			M->AddRowRef(V);
		}
		return M;
	}
}

Matrix<double>* OnlineSVR::Q(Vector<int>* V) {
	if (!SaveKernelMatrix) {
		Matrix<double>* M = new Matrix<double>();
		for (int i = 0; i < this->GetSamplesTrainedNumber(); i++) {
			Vector<double>* V2 = new Vector<double>(V->GetLength());
			for (int j = 0; j < V->GetLength(); j++) {
				V2->Add(
						this->Kernel(this->X->GetRowRef(i),
								this->X->GetRowRef(V->GetValue(j))));
			}
			M->AddRowRef(V2);
		}
		return M;
	} else {
		Matrix<double>* M = new Matrix<double>();
		for (int i = 0; i < this->GetSamplesTrainedNumber(); i++) {
			Vector<double>* V2 = new Vector<double>(V->GetLength());
			for (int j = 0; j < V->GetLength(); j++) {
				V2->Add(this->KernelMatrix->GetValue(i, V->GetValue(j)));
			}
			M->AddRowRef(V2);
		}
		return M;
	}
}

Vector<double>* OnlineSVR::Q(Vector<int>* V, int Index) {
	if (!SaveKernelMatrix) {
		Vector<double>* V2 = new Vector<double>(V->GetLength());
		for (int i = 0; i < V->GetLength(); i++) {
			V2->Add(
					this->Kernel(this->X->GetRowRef(V->GetValue(i)),
							this->X->GetRowRef(Index)));
		}
		return V2;
	} else {
		Vector<double>* V2 = new Vector<double>(V->GetLength());
		for (int i = 0; i < V->GetLength(); i++) {
			V2->Add(this->KernelMatrix->GetValue(V->GetValue(i), Index));
		}
		return V2;
	}
}

Vector<double>* OnlineSVR::Q(int Index) {
	if (!SaveKernelMatrix) {
		Vector<double>* V = new Vector<double>(this->GetSamplesTrainedNumber());
		for (int i = 0; i < this->GetSamplesTrainedNumber(); i++) {
			V->Add(
					this->Kernel(this->X->GetRowRef(i),
							this->X->GetRowRef(Index)));
		}
		return V;
	} else {
		Vector<double>* V = new Vector<double>(this->GetSamplesTrainedNumber());
		for (int i = 0; i < this->GetSamplesTrainedNumber(); i++) {
			V->Add(this->KernelMatrix->GetValue(i, Index));
		}
		return V;
	}
}

double OnlineSVR::Q(int Index1, int Index2) {
	if (!SaveKernelMatrix) {
		return this->Kernel(this->X->GetRowRef(Index1),
				this->X->GetRowRef(Index2));
	} else {
		return this->KernelMatrix->GetValue(Index1, Index2);
	}
}

// Matrix R Operations
void OnlineSVR::AddSampleToR(int SampleIndex, set_type_e SampleOldSet,
		Vector<double>* Beta, Vector<double>* Gamma) {
	if (this->R->GetLengthRows() == 0) {
		Vector<double>* V1 = new Vector<double>(2);
		V1->Add(-this->Q(SampleIndex, SampleIndex));
		V1->Add(1);
		this->R->AddRowRef(V1);
		Vector<double>* V2 = new Vector<double>(2);
		V2->Add(1);
		V2->Add(0);
		this->R->AddRowRef(V2);
	} else {
		Vector<double>* NewBeta;
		Vector<double>* NewGamma;
        if (SampleOldSet == set_type_e::ERROR_SET
                || SampleOldSet == set_type_e::REMAINING_SET) {
			// TODO: We only need of beta and Gamma(SampleIndex)
			int LastSupport = this->SupportSetIndexes->GetValue(
					this->GetSupportSetElementsNumber() - 1);
			this->SupportSetIndexes->RemoveAt(
					this->GetSupportSetElementsNumber() - 1);
			NewBeta = this->FindBeta(SampleIndex);
			NewGamma = Gamma;
			NewGamma->SetValue(SampleIndex,
					this->FindGammaSample(NewBeta, SampleIndex));
			this->SupportSetIndexes->Add(LastSupport);
		} else {
			NewBeta = Beta->Clone();
			NewGamma = Gamma;
		}
		Vector<double>* Zeros = new Vector<double>(
				this->R->GetLengthCols() + 1);
		for (int i = 0; i < this->R->GetLengthCols(); i++) {
			Zeros->Add(0);
		}
		this->R->AddColCopy(Zeros);
		Zeros->Add(0);
		this->R->AddRowRef(Zeros);
		if (NewGamma->GetValue(SampleIndex) != 0) {
			NewBeta->Add(1);
			Matrix<double>* BetaMatrix = Matrix<double>::ProductVectorVector(
					NewBeta, NewBeta);
			BetaMatrix->DivideScalar(NewGamma->GetValue(SampleIndex));
			this->R->SumMatrix(BetaMatrix);
			delete BetaMatrix;
		}
		delete NewBeta;
	}
}

void OnlineSVR::RemoveSampleFromR(int SampleIndex) {
	Vector<double>* Row = this->R->GetRowCopy(SampleIndex + 1);
	Row->RemoveAt(SampleIndex + 1);
	Vector<double>* Col = this->R->GetColCopy(SampleIndex + 1);
	Col->RemoveAt(SampleIndex + 1);
	double Rii = R->GetValue(SampleIndex + 1, SampleIndex + 1);
	this->R->RemoveRow(SampleIndex + 1);
	this->R->RemoveCol(SampleIndex + 1);
	if (Rii != 0) {
		Matrix<double>* RVariations = Matrix<double>::ProductVectorVector(Col,
				Row);
		RVariations->DivideScalar(Rii);
		this->R->SubtractMatrix(RVariations);
		delete RVariations;
	}
	delete Row;
	delete Col;
	if (this->R->GetLengthRows() == 1) {
		this->R->Clear();
	}
}

Vector<double>* OnlineSVR::FindBeta(int SampleIndex) {
	Vector<double>* Qsi = this->Q(this->SupportSetIndexes, SampleIndex);
	Qsi->AddAt(1, 0);
	Vector<double>* Beta = this->R->ProductVector(Qsi);
	Beta->ProductScalar(-1);
	delete Qsi;
	return Beta;
}

Vector<double>* OnlineSVR::FindGamma(Vector<double>* Beta, int SampleIndex) {
	if (this->GetSupportSetElementsNumber() == 0) {
		Vector<double>* Gamma = new Vector<double>(
				this->GetSamplesTrainedNumber());
		for (int i = 0; i < this->GetSamplesTrainedNumber(); i++) {
			Gamma->Add(1);
		}
		return Gamma;
	} else {
		Vector<double>* Qxi = this->Q(SampleIndex);
		Matrix<double>* Qxs = this->Q(this->SupportSetIndexes);
		Vector<double>* Ones = new Vector<double>(
				this->GetSamplesTrainedNumber());
		for (int i = 0; i < this->GetSamplesTrainedNumber(); i++) {
			Ones->Add(1);
		}
		Qxs->AddColCopyAt(Ones, 0);
		Vector<double>* Gamma = Qxs->ProductVector(Beta);
		Gamma->SumVector(Qxi);
		delete Ones;
		delete Qxi;
		delete Qxs;
		return Gamma;
	}
}

double OnlineSVR::FindGammaSample(Vector<double>* Beta, int SampleIndex) {
	if (this->GetSupportSetElementsNumber() == 0) {
		return 1;
	} else {
		double Qii = this->Q(SampleIndex, SampleIndex);
		Vector<double>* Qsi = this->Q(this->SupportSetIndexes, SampleIndex);
		Qsi->AddAt(1, 0);
		double Gamma = Qii + Beta->ProductVectorScalar(Qsi);
		delete Qsi;
		return Gamma;
	}
}

// KernelMatrix Operations
void OnlineSVR::AddSampleToKernelMatrix(Vector<double>* X) {
	Vector<double>* V = new Vector<double>();
	if (this->SamplesTrainedNumber > 1) {
		for (int i = 0; i < this->KernelMatrix->GetLengthRows(); i++)
			V->Add(this->Kernel(this->X->GetRowRef(i), X));
		this->KernelMatrix->AddColCopy(V);
	}
	V->Add(this->Kernel(X, X));
	this->KernelMatrix->AddRowRef(V);
}

void OnlineSVR::RemoveSampleFromKernelMatrix(int SampleIndex) {
	if (this->KernelMatrix->GetLengthRows() > 1) {
		this->KernelMatrix->RemoveRow(SampleIndex);
		this->KernelMatrix->RemoveCol(SampleIndex);
	} else {
		this->KernelMatrix->RemoveRow(SampleIndex);
	}
}

void OnlineSVR::BuildKernelMatrix() {
	if (this->KernelMatrix->GetLengthRows() != this->SamplesTrainedNumber) {
		KernelMatrix->Clear();
		for (int i = 0; i < this->SamplesTrainedNumber; i++)
			this->AddSampleToKernelMatrix(this->X->GetRowRef(i));
	} else {
		for (int i = 0; i < this->SamplesTrainedNumber; i++) {
			for (int j = 0; j <= i; j++) {
				double Value = this->Kernel(this->X->GetRowRef(i),
						this->X->GetRowRef(j));
				this->KernelMatrix->SetValue(i, j, Value);
				this->KernelMatrix->SetValue(j, i, Value);
			}
		}
	}
}

bool OnlineSVR::operator==(OnlineSVR const & o)const
{
    return
    SamplesTrainedNumber == o.SamplesTrainedNumber
    && StabilizedLearning == o.StabilizedLearning
    && SaveKernelMatrix ==  o.SaveKernelMatrix

    && X && o.X && *X == *o.X
    && Y && o.Y && *Y == *o.Y
    && Weights && o.Weights && *Weights == *o.Weights
    && Bias == o.Bias

    && SupportSetIndexes && o.SupportSetIndexes && *SupportSetIndexes == *o.SupportSetIndexes
    && ErrorSetIndexes && o.ErrorSetIndexes && *ErrorSetIndexes == *o.ErrorSetIndexes
    && RemainingSetIndexes && o.RemainingSetIndexes && *RemainingSetIndexes == *o.RemainingSetIndexes
    && R && o.R && *R == *o.R
    && KernelMatrix && o.KernelMatrix && *KernelMatrix == *o.KernelMatrix

    && unstabilized_sample_indexes_==o.unstabilized_sample_indexes_
    && svr_parameters.get_svr_C() == o.svr_parameters.get_svr_C()
    && svr_parameters.get_svr_epsilon() == svr_parameters.get_svr_epsilon()
    && svr_parameters.get_kernel_type() == svr_parameters.get_kernel_type()
    && svr_parameters.get_svr_kernel_param() == svr_parameters.get_svr_kernel_param()
    && svr_parameters.get_svr_kernel_param2() == svr_parameters.get_svr_kernel_param2()
    ;
}


} // svr
