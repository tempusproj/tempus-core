#include "vcl_svm.h"
#include "smo.h"

namespace svr {
namespace batch {


// TODO Create a cascaded class and move this property into it as a member field.
static bool update_r_matrix_ = false;
static size_t max_smo_iterations_;


void set_max_smo_iterations(const size_t max_smo_iterations)
{
    LOG4_DEBUG("Setting maximum SMO iterations to " << max_smo_iterations_);
    max_smo_iterations_ = max_smo_iterations;
}


void set_update_r_matrix(const bool update_r_matrix)
{
    LOG4_DEBUG("Setting update R matrix to " << update_r_matrix);
    update_r_matrix_ = update_r_matrix;
}


int solve_epsilon_svr(
        const viennacl::matrix<double> &q_matrix,
        const viennacl::vector<double> &y,
        const SVRParameters &param,
        viennacl::vector<double> &alpha,
        viennacl::scalar<double> &rho,
        int loo)
{
    int l = y.size();
    viennacl::vector<double> q_diagonal = viennacl::diag(q_matrix);
    viennacl::vector<double> alpha2 = viennacl::scalar_vector<double>(2 * l, 0);
    viennacl::vector<double> linear_term = viennacl::scalar_vector<double>(2 * l, param.get_svr_epsilon());
    viennacl::vector<double> y_ = viennacl::scalar_vector<double>(2 * l, 1);

    //extract subvectors, operate over them
    viennacl::range linear_term_range_left(0, l);
    viennacl::range linear_term_range_right(l, 2 * l);

    viennacl::vector_range<viennacl::vector<double>> linear_term_subvec_left(
            linear_term, linear_term_range_left);
    viennacl::vector_range<viennacl::vector<double>> linear_term_subvec_right(
            linear_term, linear_term_range_right);
    linear_term_subvec_left -= y;
    linear_term_subvec_right += y;

    if (loo != -1) // TODO Why LOO in Epsilon SVR?
    {
        for(int i = 0; i < l; i++)
        {
            if (alpha[i] > 0) alpha2[i] = alpha[i];
            if (alpha[i] < 0) alpha2[i+l] = -alpha[i];
        }
        linear_term[loo] = linear_term[loo+l] = INF;
    }

    viennacl::range y_range_right(l, 2 * l);
    viennacl::vector_range<viennacl::vector<double>> y_subvec_right(y_, y_range_right);
    y_subvec_right *= -1;

    svr::smo::smo_solver smo(max_smo_iterations_);
    int iter_num = smo.solve(q_matrix, q_diagonal, linear_term, y_, param, alpha2, rho);

    viennacl::range alpha2_range_left(0, l);
    viennacl::range alpha2_range_right(l, 2 * l);

    viennacl::vector_range<viennacl::vector<double>> alpha2_subvec_left(alpha2, alpha2_range_left);
    viennacl::vector_range<viennacl::vector<double>> alpha2_subvec_right(alpha2, alpha2_range_right);

    alpha = alpha2_subvec_left - alpha2_subvec_right;
    return iter_num;
}


void
svm_train(
        Matrix<double> &learning_data,
        Vector<double> &reference_data,
        OnlineSVR &model,
        const bool only_sv)
{
//    std::lock_guard<std::mutex> guard(batch::gpu_handler::get_instance().work_gpu_mutex);
//    viennacl::matrix<double> kernel_matrix;
//    smo::prepare_kernel_matrix(learning_data, param, kernel_matrix);
//    viennacl::matrix<double> quadratic_matrix(kernel_matrix.size1() * 2, kernel_matrix.size2() * 2);
//    smo::prepare_quadratic_matrix(kernel_matrix, quadratic_matrix);
    LOG4_DEBUG("Training on " << learning_data.GetLengthCols() << " columns and " << learning_data.GetLengthRows() << " rows.");
#ifdef VIENNACL_WITH_OPENCL
    viennacl::backend::default_memory_type(viennacl::MAIN_MEMORY);
    viennacl::matrix<double> quadratic_matrix;
    smo::prepare_kernel_quadratic_matrices(learning_data, model.get_svr_parameters(), quadratic_matrix);
//    quadratic_matrix.switch_memory_context(viennacl::context(viennacl::MAIN_MEMORY));
#else //VIENNACL_WITH_OPENCL
    viennacl::matrix<double> kernel_matrix;
    smo::prepare_kernel_matrix(learning_data, param, kernel_matrix);
    viennacl::matrix<double> quadratic_matrix(kernel_matrix.size1() * 2, kernel_matrix.size2() * 2);
    smo::prepare_quadratic_matrix(kernel_matrix, quadratic_matrix);
    quadratic_matrix.switch_memory_context(viennacl::context(viennacl::MAIN_MEMORY));
#endif //VIENNACL_WITH_OPENCL

    viennacl::vector<double> decision_func = viennacl::scalar_vector<double>(reference_data.GetLength(), 0.);
    viennacl::scalar<double> rho;
    int iter_num = 0;
    viennacl::vector<double> vcl_y = *reference_data.vcl();
    iter_num = solve_epsilon_svr(quadratic_matrix, vcl_y, model.get_svr_parameters(), decision_func, rho, -1);
    LOG4_INFO("SMO iterations count " << iter_num);

    viennacl::vector<double> fabs_decision_func = viennacl::linalg::element_fabs(decision_func);
    //get SV, fill into onlineSVR model
    Matrix<double> *X = new Matrix<double>();
    Vector<double> *Y = new Vector<double>();

    std::vector<int> sv_indices;
    R_matrix r_matrix(quadratic_matrix, update_r_matrix_);
    size_t j = 0;
    if (only_sv) {
        for (size_t i = 0; i < decision_func.size(); i++) {
            if (fabs_decision_func[i] > 0) {
                X->AddRowCopy(learning_data.GetRowRef(i)); // TODO Reference not copy
                Y->Add(reference_data.GetValue(i));
                model.add_index_to_support_set(j);
                sv_indices.push_back(j);
                r_matrix.update_R_matrix(sv_indices, j);
                model.add_weight(decision_func[i]);
                j++;
            }
        }
    } else {
        for (size_t i = 0; i < decision_func.size(); i++) {
            X->AddRowCopy(learning_data.GetRowRef(i));
            Y->Add(reference_data.GetValue(i));
            if (fabs_decision_func[i] > 0) {
                model.add_index_to_support_set(i);
                sv_indices.push_back(i);
                r_matrix.update_R_matrix(sv_indices, i);
                j++;
            }
            model.add_weight(decision_func[i]);
        }
        LOG4_INFO("SV #" << j);
    }
    model.set_R_matrix(r_matrix.getR());
    model.set_samples_trained_number(Y->GetLength());
    model.set_y(Y);
    model.set_x(X);
    model.set_bias(rho * viennacl::scalar<double>(-1.)); // It's negative

    // Kernel Matrix
    if (model.GetSaveKernelMatrix()) model.BuildKernelMatrix();
}

} //batch
} //svr
