#include "cascaded_svm.h"

namespace svr {
namespace cascaded {


static double cascade_layer_reduce_ratio_ = DEFAULT_CASCADE_LAYER_REDUCE_RATIO;

void set_cascade_layer_reduce_ratio(const double cascade_layer_reduce_ratio)
{
    cascade_layer_reduce_ratio_ = cascade_layer_reduce_ratio;
}


size_t max_segment_size_ = DEFAULT_MAX_SEGMENT_SIZE;

void set_max_segment_size(const size_t max_segment_size)
{
    max_segment_size_ = max_segment_size;
}

void
cascade_iteration(
        Matrix<double> &features, Vector<double> &labels, OnlineSVR &model)
{
    layer_data top_layer;
    top_layer.training_matrix = &features;
    top_layer.reference_vector = &labels;

    size_t max_segment_size = max_segment_size_;
#ifdef VIENNACL_WITH_OPENCL
    max_segment_size = batch::gpu_handler::get_instance().get_max_gpu_data_chunk_size();
#endif //VIENNACL_WITH_OPENCL
    size_t segment_size_limit = std::min(static_cast<size_t>(labels.GetLength() / MIN_SEGMENT_DIVIDER), static_cast<size_t>(max_segment_size)); // TODO Rework, major issue, we should calculate segment size limit from rows and columns count not number of labels

    top_layer = train_segment(top_layer, model.get_svr_parameters(), segment_size_limit);
    if (top_layer.reference_vector->GetLength() != 0) {
        batch::svm_train(*top_layer.training_matrix, *top_layer.reference_vector, model);
    } else {
        delete top_layer.training_matrix;
        delete top_layer.reference_vector;
        top_layer.training_matrix = features.Clone();
        top_layer.reference_vector = labels.Clone();
        layer_data final_layer = get_subsegment(top_layer, cascade_layer_reduce_ratio_);

        batch::svm_train(*final_layer.training_matrix, *final_layer.reference_vector, model);
        delete final_layer.training_matrix;
        delete final_layer.reference_vector;
    }
    
    delete top_layer.training_matrix;
    delete top_layer.reference_vector;
}


layer_data train_segment(layer_data &input_segment, const SVRParameters &parameters, const size_t max_segment_size)
{
    std::vector<layer_data> sub_segments = divide_segment(input_segment);
    std::vector<layer_data> trained_segments;
    std::vector<std::future<layer_data>> futures;
    size_t subsegments_size {0};

    for (auto &segment: sub_segments) {
        if(!segment.is_calculated) {
            subsegments_size += segment.reference_vector->GetLength();
            if (size_t(segment.reference_vector->GetLength()) > max_segment_size)
            {
                // TODO Currently recursive train segment is not suitable for parallelizing.
                //auto fn = std::bind(train_segment, std::ref(segment), std::ref(parameters), max_segment_size);
                futures.push_back(
                    std::async(std::launch::deferred, train_segment, std::ref(segment), std::ref(parameters), max_segment_size)
                    //svr::batch::gpu_handler::get_instance().thread_pool.post<layer_data>(fn)
                );
            } else {
                segment.is_calculated = true;
                trained_segments.push_back(segment);
            }
        }
    }

    for(auto &f: futures) {
        layer_data data = f.get();
        data.is_calculated = false;
        trained_segments.push_back(data);
    }

    layer_data supersegment = merge_segments(trained_segments);
    
    for (auto &segment: trained_segments) {
        if (!segment.is_calculated) {
            delete segment.training_matrix;
            delete segment.reference_vector;
        }
    }
    
    if (
            ((supersegment.reference_vector->GetLength() > (subsegments_size * cascade_layer_reduce_ratio_)) &&
                (size_t(supersegment.reference_vector->GetLength()) != subsegments_size)) ||
            supersegment.reference_vector->GetLength() == 0 )
    {
        if (supersegment.reference_vector->GetLength()) LOG4_DEBUG(
                "Labels count is " << supersegment.reference_vector->GetLength() << " trimming to " <<
                                   subsegments_size * cascade_layer_reduce_ratio_);
        else LOG4_DEBUG("Labels count is zero.");
        delete supersegment.training_matrix;
        delete supersegment.reference_vector;
        
        layer_data current_segment = get_subsegment(input_segment, cascade_layer_reduce_ratio_);
        supersegment.training_matrix = current_segment.training_matrix->Clone();
        supersegment.reference_vector = current_segment.reference_vector->Clone();
        delete current_segment.training_matrix;
        delete current_segment.reference_vector;
    }

    for (auto &segment: sub_segments)
    {
        delete segment.training_matrix;
        delete segment.reference_vector;
    }

    layer_data result = batch_train_segment(
            *supersegment.training_matrix, *supersegment.reference_vector, parameters);

    delete supersegment.reference_vector;
    delete supersegment.training_matrix;

    return result;
}


std::vector<layer_data> divide_segment(layer_data &supersegment, size_t number_of_segments)
{
    std::vector<layer_data> subsegments;
    const size_t subsegment_max_num = number_of_segments;
    size_t chunk_size = supersegment.reference_vector->GetLength() / subsegment_max_num;
    if (chunk_size < 1) {
        LOG4_ERROR("Chunk size is 0. Skipping.");
        return subsegments;
    }
    const size_t last_chunk_additional_size = supersegment.reference_vector->GetLength() % chunk_size;
    for (size_t subsegment_index = 0; subsegment_index < subsegment_max_num; subsegment_index++)
    {
        layer_data subsegment;
        subsegment.training_matrix = new Matrix<double>();
        subsegment.reference_vector = new Vector<double>();

        size_t i = subsegment_index * chunk_size;
        size_t current_chunk_size = chunk_size;
        if (subsegment_index == subsegment_max_num - 1)
            current_chunk_size += last_chunk_additional_size;
        for ( size_t j = 0; j < current_chunk_size; j++, i++)
        {
            subsegment.reference_vector->Add(supersegment.reference_vector->GetValue(i));
            subsegment.training_matrix->AddRowCopy(supersegment.training_matrix->GetRowRef(i));
        }
        subsegments.push_back(subsegment);
    }

    return subsegments;
}


layer_data get_subsegment(layer_data &supersegment, double ratio)
{
    size_t chunk_size = (size_t) std::round(supersegment.reference_vector->GetLength() * ratio);

    layer_data subsegment;
    subsegment.training_matrix = new Matrix<double>();
    subsegment.reference_vector = new Vector<double>();

    size_t j = supersegment.reference_vector->GetLength() - (chunk_size + 1);
    for ( ; j < size_t(supersegment.reference_vector->GetLength()); j++)
    {
        subsegment.reference_vector->Add(supersegment.reference_vector->GetValue(j));
        subsegment.training_matrix->AddRowCopy(supersegment.training_matrix->GetRowRef(j));
    }
    return subsegment;
}


layer_data merge_segments(std::vector<layer_data> &subsegments)
{
    layer_data supersegment;

    supersegment.training_matrix = new Matrix<double>();
    supersegment.reference_vector = new Vector<double>();
    for(auto & segment : subsegments)
    {
        for (long row_num = 0; row_num < segment.reference_vector->GetLength(); row_num++)
        {
            supersegment.training_matrix->AddRowCopy(segment.training_matrix->GetRowRef(row_num));
            supersegment.reference_vector->Add(segment.reference_vector->GetValue(row_num));
        }
    }
    supersegment.is_calculated = true;
    return supersegment;
}


layer_data batch_train_segment(
        Matrix<double> &learning_data, Vector<double> &reference_data, const SVRParameters &parameters)
{
    OnlineSVR model(parameters);
    batch::svm_train(learning_data, reference_data, model, true);
    layer_data result;
    result.training_matrix = model.get_x()->Clone();
    result.reference_vector = model.get_y()->Clone();
    result.is_calculated = true;

    return result;
}

} // cascaded
} // svr
