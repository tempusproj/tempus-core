#include <gtest/gtest.h>
#include "../include/OnlineSVR.h"
#include <sstream>
#include <util/CompressionUtils.hpp>
#include <util/ThreadPoolAsio.hpp>

namespace
{
    using TheMatrix = Matrix<double>;
    using TheVector = Vector<double>;

    TheVector * fill( size_t n)
    {
        TheVector * result = new Vector<double>();
        for(size_t i = 0; i < n; ++i)
            result->Add( 0.3 + i);
        return result;
    }

    void fill(TheMatrix * m1, size_t n, size_t m)
    {
        for(size_t i = 0; i < n; ++i)
        {
            m1->Values.push_back(std::make_shared<Vector<double> >());
            for(size_t j = 0; j < m; ++j)
                m1->Values[i]->Add(0.4 + i*j);
        }
    }

}

TEST(OnlineSVR, SaveLoad)
{
    svr::OnlineSVR osvr;

    TheMatrix * m1 {new TheMatrix()}, * m2{new TheMatrix()}, * m3{new TheMatrix()};
    fill(m1, 3, 4);
    fill(m2, 4, 5);
    fill(m3, 5 ,6);

    TheVector * v1 = fill(3), * v2 = fill(4);

    svr::datamodel::SVRParameters sp1(
            0, 0, "", "", 0, 1, 2.0, 3.0, 4.0, 5, 0.5, kernel_type_e::RBF, 9);

    std::stringstream str;

    osvr.set_R_matrix(m1);
    osvr.set_bias(1.23);
    osvr.set_kernel_matrix(m2);
    osvr.set_samples_trained_number(2);
    osvr.set_svr_parameters( sp1 );
    osvr.set_weights(v1);
    osvr.set_x(m3);
    osvr.set_y(v2);
    osvr.SetSaveKernelMatrix(false);
    osvr.SetStabilizedLearning(true);

    osvr.SaveOnlineSVR(str);

    std::string compressed = svr::common::compress(str.str().c_str(), str.str().size());
    std::string decompressed = svr::common::decompress(compressed.c_str(), compressed.size());

    std::stringstream ostr;
    ostr.rdbuf()->pubsetbuf(const_cast<char *>(decompressed.c_str()), decompressed.size());

    svr::OnlineSVR osvr1;
    osvr1.LoadOnlineSVR(ostr);

    ASSERT_EQ(osvr, osvr1);
}

TEST(OnlineSVR, ThreadPoolAsio)
{
    const size_t size = 10;
    svr::common::ThreadPoolAsio pool(size);
    ASSERT_EQ(pool.size(), size_t(0));
    pool.start();
    ASSERT_EQ(pool.size(), size);
    pool.increment();
    ASSERT_EQ(pool.size(), (size + 1));

    std::atomic<int> sum(0);
    std::vector<std::future<int> > futures;

    int num_tasks = 1000;
    for (int i = 0; i < num_tasks; i++) {
        futures.push_back(pool.post<int>([i, &sum]() -> int {
            sum += 1;
            return i;
        }));
    }

    int sum2 = 0;
    for (auto & future : futures) {
        sum2 += future.get();
    }

    ASSERT_EQ(sum, num_tasks);
    ASSERT_EQ(sum2, (0 + num_tasks - 1) * num_tasks / 2);
}