/*
 * Wavelet.hpp
 *
 *  Wrapper of wavelib. https://github.com/rafat/wavelib
 * 
 *  Provides convenient way to make forward and inverse wavelet transformations
 *  hiding wavelib's internal objects.
 * 
 *  Created on: Jun 17, 2016
 *      Author: sgeorgiev
 */

#pragma once

#include <vector>
#include <string>

#include <wavelib/header/wavelib.h>

namespace svr {


class WaveletTransform {
public:
    WaveletTransform(const std::string& name, const std::string& method, const size_t signal_len, const size_t levels);
    virtual ~WaveletTransform();

    void summary() const;

protected:
    wave_object wave_;  // wavelib wavelet object
    wt_object wt_;      // wavelib wavelet transform object

    size_t signal_len_;
    size_t levels_;
};

class SWTransform : public WaveletTransform {
public:
    SWTransform(const std::string& name, const size_t signal_len, const size_t levels);
    virtual ~SWTransform();

    void swt_frame(const std::vector<double>& frame, std::vector<std::vector<double> >& result);
    void iswt_frame(const std::vector<double>& frame, std::vector<double>& result);
};


}
