#ifndef _CASCADED_SVM_H_
#define _CASCADED_SVM_H_

#include "vcl_svm.h"
#include "model/DataRow.hpp"
#include "model/Model.hpp"
#include <future>
#include <boost/interprocess/sync/named_semaphore.hpp>
#include "gpu_handler.h"



namespace svr {
namespace cascaded {

#define DEFAULT_MAX_SEGMENT_SIZE 2000
#define MIN_SEGMENT_DIVIDER 8
#define DEFAULT_CASCADE_LAYER_REDUCE_RATIO 0.6


/* TODO Rewrite struct so that it will contain bounds to training and reference arrays and a pointer to the actual data
 * holding memory chunk and its size
 */
struct layer_data
{
    /* TODO Replace with std::shared_ptr or implement destructor */
    Matrix<double>* training_matrix = nullptr;
    Vector<double>* reference_vector = nullptr;
    bool is_calculated {false};
};

layer_data
batch_train_segment(
        Matrix<double> &learning_data,
        Vector<double> &reference_data,
        const SVRParameters &parameters);

void
cascade_iteration(
        Matrix<double>& features,
        Vector<double>& labels,
        OnlineSVR &model);

layer_data train_segment(layer_data & subsegments, const SVRParameters &parameters, const size_t max_segment_size);

std::vector<layer_data> divide_segment( layer_data & supersegment, size_t number_of_segments = 2);
layer_data get_subsegment(layer_data &supersegment, double ratio);

layer_data merge_segments( std::vector<layer_data> &subsegments);

void set_max_segment_size(const size_t max_segment_size);
void set_cascade_layer_reduce_ratio(const double cascade_layer_reduce_ratio);

} //cascaded
} //svr


#endif /* _CASCADED_SVM_H_ */
