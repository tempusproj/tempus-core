#ifndef _SMO_H
#define _SMO_H

#include "viennacl/scalar.hpp"
#include "viennacl/vector.hpp"
#include "viennacl/matrix.hpp"
#include "viennacl/matrix_proxy.hpp"
#include "viennacl/linalg/inner_prod.hpp"
#include "viennacl/linalg/norm_1.hpp"
#include "viennacl/linalg/prod.hpp"

//#include "model/Matrix.h"
#include "OnlineSVR.h"

#include <algorithm>

#include "model/SVRParameters.hpp"
#include "gpu_handler.h"
#include "gpu_handler.h"

namespace svr {
namespace smo {

#define TAU 1e-12
#define MAX_ITER (300000)

class smo_solver
{
    size_t max_iter_;
public:
    smo_solver(const size_t max_iter = (size_t)MAX_ITER) : max_iter_(max_iter) {}
    virtual ~smo_solver() {}

    int solve(const viennacl::matrix<double> &kernel_matrix,
              const viennacl::vector<double> &linear_term,
              const viennacl::vector<double> &y_,
              const SVRParameters &param,
              viennacl::vector<double> &alpha2,
              viennacl::scalar<double> &rho);

    int solve(
            const viennacl::matrix<double> &quadratic_matrix,
            const viennacl::vector<double> &q_diagonal,
            const viennacl::vector<double> &linear_term,
            const viennacl::vector<double> &y_,
            const SVRParameters &param,
            viennacl::vector<double> &alpha2,
            viennacl::scalar<double> &rho);

private:
    size_t active_size;
    viennacl::vector<double> y;
    viennacl::matrix<double> gradient_m;
    enum { LOWER_BOUND, UPPER_BOUND, FREE };
    viennacl::vector<char> alpha_status;	// LOWER_BOUND, UPPER_BOUND, FREE
    viennacl::vector<double> alpha;
    viennacl::matrix<double> Q;
    viennacl::vector<double> QD;
    viennacl::vector<double> sign;
    double C;
    viennacl::vector<double> p;
    viennacl::vector<int> active_set;
    viennacl::matrix<double> gradient_bar_m;
    size_t l;

    inline void update_alpha_status(int i)
    {
        if(alpha[i] >= C)
            alpha_status[i] = UPPER_BOUND;
        else if(alpha[i] <= 0)
            alpha_status[i] = LOWER_BOUND;
        else alpha_status[i] = FREE;
    }
    int select_working_set(int &i, int &j);
    double calculate_rho();
};

void prepare_kernel_matrix(
        datamodel::Matrix<double> &learning_data,
        const SVRParameters &param,
        viennacl::matrix<double> &kernel_matrix);
void prepare_quadratic_matrix(
        const viennacl::matrix<double> &kernel_matrix,
        viennacl::matrix<double> &quadratic_matrix);
void prepare_kernel_quadratic_matrices(
        const datamodel::Matrix<double> &learning_data, const SVRParameters &param, viennacl::matrix<double> &quadratic_matrix_cpu);

}
}


#endif /* _SMO_H */
