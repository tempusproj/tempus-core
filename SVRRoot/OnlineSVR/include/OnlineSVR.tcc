#pragma once
#include "OnlineSVR.h"
namespace svr {

kernel_type_e get_kernel_type_from_string(const std::string & kernel_type_str);

std::string kernel_type_to_string(const kernel_type_e kernel_type);

template<typename S>
bool OnlineSVR::LoadOnlineSVR(OnlineSVR &osvr, S &input_stream)
{
    osvr.Clear();

    try {
        // Title
        std::string trash;
        double X;
        int X2;
        int SamplesDimension;
        double C, epsilon, kp1, kp2;
        std::string kernel_type;

        input_stream >> trash >> osvr.SamplesTrainedNumber;
        input_stream >> trash >> SamplesDimension;
        input_stream >> trash >> C; osvr.svr_parameters.set_svr_C(C);
        input_stream >> trash >> epsilon; osvr.svr_parameters.set_svr_epsilon(epsilon);
        input_stream >> trash >> kernel_type; osvr.svr_parameters.set_kernel_type(get_kernel_type_from_string(kernel_type));
        input_stream >> trash >> kp1; osvr.svr_parameters.set_svr_kernel_param(kp1);
        input_stream >> trash >> kp2; osvr.svr_parameters.set_svr_kernel_param2(kp2);
        input_stream >> trash >> X2; osvr.StabilizedLearning = X2 > 0;
        input_stream >> trash >> X2; osvr.SaveKernelMatrix = X2 > 0;

        if(trash != "SaveKernelMatrix:")
            throw std::runtime_error("OnlineSVR::LoadOnlineSVR: error during loading 1");

        int xrows, xcols;
        input_stream >> trash >> xrows;
        input_stream >> trash >> xcols;

        int i;
        for (i = 0; i < xrows; i++) {
            Vector<double> *Sample = new Vector<double>(xrows);
            for (int j = 0; j < xcols; j++) {
                input_stream >> X;
                Sample->Add(X);
            }
            osvr.X->AddRowRef(Sample);
        }

        input_stream >> trash >> xrows;
        if(trash != "Y:")
            throw std::runtime_error("OnlineSVR::LoadOnlineSVR: error during loading 2");

        for (i = 0; i < xrows; i++) {
            input_stream >> X;
            osvr.Y->Add(X);
        }

        int tmp;

        input_stream >> trash >> tmp;
        for (i = 0; i < tmp; i++) {
            input_stream >> X2;
            osvr.SupportSetIndexes->Add(X2);
        }

        input_stream >> trash >> tmp;
        for (i = 0; i < tmp; i++) {
            input_stream >> X2;
            osvr.ErrorSetIndexes->Add(X2);
        }

        input_stream >> trash >> tmp;
        for (i = 0; i < tmp; i++) {
            input_stream >> X2;
            osvr.RemainingSetIndexes->Add(X2);
        }
        if(trash != "RemainingSet:")
            throw std::runtime_error("OnlineSVR::LoadOnlineSVR: error during loading 4");

        input_stream >> trash >> tmp;
        for (i = 0; i < tmp; i++) {
            input_stream >> X;
            osvr.Weights->Add(X);
        }

        input_stream >> trash >> osvr.Bias;

        input_stream >> trash >> xrows;
        input_stream >> trash >> xcols;

        for (i = 0; i < xrows; i++) {
            Vector<double> *Sample = new Vector<double>(xrows);
            for (int j = 0; j < xcols; j++) {
                input_stream >> X;
                Sample->Add(X);
            }
            osvr.R->AddRowRef(Sample);
        }

        if(trash != "RmCols:")
            throw std::runtime_error("OnlineSVR::LoadOnlineSVR: error during loading 3");

        if (osvr.SaveKernelMatrix)
            osvr.BuildKernelMatrix();
    }
    catch (...) {
        LOG4_ERROR("Error. Data corrupted.");
        osvr.Clear();
        return false;
    }

    return true;
}

template<typename S>
bool OnlineSVR::SaveOnlineSVR(const OnlineSVR &osvr, S &output_stream) {
    output_stream.precision(30);

    try {
        output_stream << "SamplesTrainedNumber: " << osvr.SamplesTrainedNumber << std::endl;
        output_stream << "SamplesDimension: " << osvr.X->GetLengthCols() << std::endl;
        output_stream << "C: " << osvr.svr_parameters.get_svr_C() << std::endl;
        output_stream << "Epsilon: " << osvr.svr_parameters.get_svr_epsilon() << std::endl;
        output_stream << "KernelType: " << kernel_type_to_string(osvr.svr_parameters.get_kernel_type()) << std::endl;
        output_stream << "KernelParam: " << osvr.svr_parameters.get_svr_kernel_param() << std::endl;
        output_stream << "KernelParam2: " << osvr.svr_parameters.get_svr_kernel_param2() << std::endl;
        output_stream << "StabilizedLearning: " << (osvr.StabilizedLearning ? 1 : 0) << std::endl;
        output_stream << "SaveKernelMatrix: " << (osvr.SaveKernelMatrix ? 1 : 0) << std::endl;

        output_stream << "X.rows: " << osvr.X->GetLengthRows() << " ";
        output_stream << "X.cols: " << osvr.X->GetLengthCols() << " ";

        int i;
        for (i = 0; i < osvr.X->GetLengthRows(); i++)
            for (int j = 0; j < osvr.X->GetLengthCols(); j++)
                output_stream << osvr.X->GetValue(i, j) << " ";

        output_stream << "Y: " << osvr.Y->GetLength() << " ";

        for (i = 0; i < osvr.Y->GetLength(); i++)
            output_stream << osvr.Y->GetValue(i) << " ";

        output_stream << "SupportSet: " << osvr.GetSupportSetElementsNumber() << " ";
        for (i = 0; i < osvr.GetSupportSetElementsNumber(); i++)
            output_stream << osvr.SupportSetIndexes->GetValue(i) << " ";

        output_stream << "ErrorSet: " << osvr.GetErrorSetElementsNumber() << " ";
        for (i = 0; i < osvr.GetErrorSetElementsNumber(); i++)
            output_stream << osvr.ErrorSetIndexes->GetValue(i) << " ";

        output_stream << "RemainingSet: " << osvr.GetRemainingSetElementsNumber() << " ";
        for (i = 0; i < osvr.GetRemainingSetElementsNumber(); i++)
            output_stream << osvr.RemainingSetIndexes->GetValue(i) << " ";

        output_stream << "Weights: " << osvr.Weights->GetLength() << " ";
        for (i = 0; i < osvr.Weights->GetLength(); i++)
            output_stream << osvr.Weights->GetValue(i) << " ";

        output_stream << "Bias: " << osvr.Bias  << " ";

        output_stream << "RmRows: " << osvr.R->GetLengthRows() << " ";
        output_stream << "RmCols: " << osvr.R->GetLengthCols() << " ";
        for (i = 0; i < osvr.R->GetLengthRows(); i++)
            for (int j = 0; j < osvr.R->GetLengthCols(); j++)
                output_stream << osvr.R->GetValue(i, j) << " ";

    }
    catch (...) {
        LOG4_ERROR("Error. It's impossible to complete the save.");
    }
    return true;
}
}
