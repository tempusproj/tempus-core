#pragma once
 
#include <boost/interprocess/sync/named_semaphore.hpp> 
#include <boost/interprocess/sync/named_mutex.hpp> 
#include <boost/interprocess/sync/scoped_lock.hpp> 
 
#ifdef VIENNACL_WITH_OPENCL
#include "viennacl/ocl/device.hpp" 
#include "viennacl/ocl/platform.hpp" 
#include "viennacl/ocl/backend.hpp" 
#endif // VIENNACL_WITH_OPENCL
#include "common/Logging.hpp" 
#include <stack>
#include <mutex>
#include <thread>
#include "util/ThreadPoolAsio.hpp"


namespace svr { 
namespace batch {

#define SVRWAVE_GPU_SEM ("svrwave_gpu_sem")

class gpu_handler {
public:
    gpu_handler();
    ~gpu_handler();

    size_t get_free_gpu();
    void return_gpu(size_t gpu_index);
    size_t get_max_running_gpu_threads_number();
    size_t get_max_gpu_data_chunk_size();
    void gpu_sem_enter();
    void gpu_sem_leave();

    static gpu_handler& get_instance();
    std::mutex work_gpu_mutex;
    common::ThreadPoolAsio thread_pool;

private:
    std::unique_ptr<boost::interprocess::named_semaphore> p_gpu_sem = nullptr;
    std::size_t max_running_gpu_threads_number;
    std::size_t max_gpu_data_chunk_size;
    std::stack<size_t> free_gpus;
    std::mutex free_gpu_mutex;
    void init_devices(const int device_type);

    static std::shared_ptr<gpu_handler> p_instance;
    static std::once_flag only_one;
};
 
} 
} 
 
