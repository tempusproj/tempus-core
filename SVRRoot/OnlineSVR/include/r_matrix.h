#pragma once

#include "model/Matrix.h"
#include "model/Matrix.tcc"
#include "model/Vector.h"
#include "viennacl/vector.hpp"
#include "viennacl/matrix.hpp"
#include <vector>


namespace svr {

namespace batch {


class R_matrix
{
public:
    explicit R_matrix(const viennacl::matrix<double> & quadratic_matrix, bool update_r_matrix_enabled = true)
    : update_r_matrix_enabled_(update_r_matrix_enabled)
    {
        R = new svr::datamodel::Matrix<double>;

        // Needed as viennacl copy_vec api is non-const.
        viennacl::matrix<double> tmpQM = quadratic_matrix;
        for (size_t i = 0; i < tmpQM.size1(); i++) {
            svr::datamodel::Vector<double> row(0.0, tmpQM.size2());
            viennacl::linalg::copy_vec(tmpQM, *row.vcl(), i, 0, false);
            Q.AddRowCopy(row);
        }
    }
    void update_R_matrix(std::vector<int> sv_indexes, int SampleIndex);
    svr::datamodel::Matrix<double> *getR() {return R;}

private:
    double getQ(int index1, int index2);
    svr::datamodel::Vector<double>* getQsi(std::vector<int> sv_indexes, int SampleIndex);
    svr::datamodel::Vector<double>* getQxi(int SampleIndex);
    svr::datamodel::Matrix<double>* getQxs(std::vector<int> sv_indexes, int SamplesTrainedNumber);

    svr::datamodel::Matrix<double> * R;
    svr::datamodel::Matrix<double> Q;

    bool update_r_matrix_enabled_;
};


} //batch
} //svr


