/******************************************************************************
 *                       ONLINE SUPPORT VECTOR REGRESSION                      *
 *                      Copyright 2006 - Francesco Parrella                    *
 *                                                                             *
 *This program is distributed under the terms of the GNU General Public License*
 ******************************************************************************/

#pragma once

#include "svm.h"
#include "model/Vector.tcc"
#include "model/Matrix.tcc"
#include "model/SVRParameters.hpp"
#include "util/MathUtils.hpp"
#include <stdarg.h>

#ifndef VIENNACL_WITH_OPENCL
//#define VIENNACL_WITH_OPENCL
#endif

#include "viennacl/scalar.hpp"
#include "viennacl/vector.hpp"
#include "viennacl/matrix.hpp"
#include "viennacl/linalg/inner_prod.hpp"
#include "viennacl/linalg/norm_1.hpp"
#include "viennacl/linalg/prod.hpp"

#define MAX_STABILIZATION_ITERS_WITHOUT_CONVERGENCE 10

using svr::datamodel::Vector;
using svr::datamodel::Matrix;
using svr::datamodel::SVRParameters;
using svr::datamodel::kernel_type_e;

namespace svr {

typedef enum class set_type_{
    NO_SET,
    SUPPORT_SET,
    ERROR_SET,
    REMAINING_SET
} set_type_e, *set_type_e_ptr;

class OnlineSVR {

public:
    // Initialization
    OnlineSVR();
    OnlineSVR(const SVRParameters &svr_parameters_);
    OnlineSVR(std::stringstream &input_stream);
    ~OnlineSVR();

    void Clear();

    OnlineSVR *Clone();

    // Attributes Operations
    SVRParameters &get_svr_parameters();

    void set_svr_parameters(const SVRParameters &);

    bool GetStabilizedLearning();

    void SetStabilizedLearning(bool StabilizedLearning);

    bool GetSaveKernelMatrix();

    void SetSaveKernelMatrix(bool SaveKernelMatrix);

    int GetSamplesTrainedNumber();

    int GetSupportSetElementsNumber() const;

    int GetErrorSetElementsNumber() const;

    int GetRemainingSetElementsNumber() const;

    Vector<int> *GetSupportSetIndexes() const;

    Vector<int> *GetErrorSetIndexes() const;

    Vector<int> *GetRemainingSetIndexes() const;

    void set_samples_trained_number(size_t samples_num);

    void add_index_to_support_set(int index);

    void set_bias(double bias);

    void add_weight(double weight);

    void set_kernel_matrix(Matrix<double> * matrix);

    void set_weights(Vector<double> *weights);

    void set_x(Matrix<double> *x);

    void set_y(Vector<double> *y);

    void set_R_matrix(Matrix<double>* r_matrix);

    // Learning Operations
    int Train(Matrix<double> *X, Vector<double> *Y);

    int Train(Matrix<double> *X, Vector<double> *Y, Matrix<double> *TestSetX,
            Vector<double> *TestSetY);

    int Train(Matrix<double> *X, Vector<double> *Y, int TrainingSize,
            int TestSize);

    int Train(double **X, double *Y, int ElementsNumber, int ElementsSize);

    int Train(Vector<double> *X, double Y);

    int Forget(Vector<int> *Indexes);

    int Forget(int *Indexes, int N);

    int Forget(int Index);

    int Forget(Vector<double> *Sample);

    int Stabilize();

    void update_stabilization_convergence_status(const std::vector<double> & unstabilized_sample_indexes);

    void SelfLearning(Matrix<double> *TrainingSetX,
            Vector<double> *TrainingSetY, Matrix<double> *ValidationSetX,
            Vector<double> *ValidationSetY, double ErrorTollerance);

    static void CrossValidation(Matrix<double> *TrainingSetX,
            Vector<double> *TrainingSetY, Vector<double> *EpsilonList,
            Vector<double> *CList, Vector<double> *KernelParamList,
            int SetNumber, char *ResultsFileName);

    static double CrossValidation(Vector<Matrix<double> *> *SetX,
            Vector<Vector<double> *> *SetY, double Epsilon, double C,
            double KernelParam);

    static void LeaveOneOut(Matrix<double> *TrainingSetX,
            Vector<double> *TrainingSetY, Vector<double> *EpsilonList,
            Vector<double> *CList, Vector<double> *KernelParamList,
            char *ResultsFileName);

    static double LeaveOneOut(Matrix<double> *SetX, Vector<double> *SetY,
            double Epsilon, double C, double KernelParam);

    // Predict/Margin Operations
    double Predict(Vector<double> *X);

    double Predict(double *X, int ElementsSize);

    Vector<double> *Predict(Matrix<double> *X);

    Vector<double> *Predict(double **X, int ElementsNumber, int ElementsSize);

    double Margin(Vector<double> *X, double Y);

    double Margin(double *X, double Y, int ElementsSize);

    Vector<double> *Margin(Matrix<double> *X, Vector<double> *Y);

    Vector<double> *Margin(double **X, double *Y, int ElementsNumber, int ElementsSize);

    // Control Operations
    bool VerifyKKTConditions();

    void FindError(Matrix<double> *ValidationSetX,
            Vector<double> *ValidationSetY, double *MinError, double *MeanError,
            double *MaxError);

    // I/O Operations
    void ShowInfo();

    void ShowInfo(std::ostream &str_output);

    void LoadOnlineSVR(const char *Filename);

    void LoadOnlineSVR(svm_model *libsvmModel);

    void LoadOnlineSVR(svm_model * libsvmModel, Matrix<double> * learningData, Vector<double> *referenceData);

    void LoadOnlineSVR(std::stringstream &input_stream);

    void SaveOnlineSVR(const char *Filename) const;

    void SaveOnlineSVR(std::stringstream &output_stream) const;

    // static
    template<typename S>
    static bool LoadOnlineSVR(OnlineSVR &osvr,
            S &input_stream);

    template<typename S>
    static bool SaveOnlineSVR(const OnlineSVR &osvr,
            S &output_stream);

    static void Import(char *Filename, Matrix<double> **X, Vector<double> **Y);

    static void Import(char *Filename, Matrix<double> **AngularPositions,
            Matrix<double> **MotorCurrents, Matrix<double> **AppliedVoltages);

    Vector<double> *get_weights() const {
        return Weights;
    }

    Matrix<double> *get_x() const {
        return X;
    }

    Vector<double> *get_y() const {
        return Y;
    }

    Matrix<double> *get_kernel_matrix() const {
        return KernelMatrix;
    }

    static viennacl::matrix<double> Kernel(const viennacl::matrix<double> &train_data, const SVRParameters &svr_parameters);

    static viennacl::matrix<double> Kernel(const viennacl::matrix<double> &train_data, const SVRParameters &svr_parameters, viennacl::context & ctx, viennacl::matrix<double> &kernel_matrix);

    static viennacl::matrix<double> Kernel(Matrix<double> *X, const SVRParameters &svr_parameters);

    std::vector<std::vector<double> > BuildKernelMatrix(Matrix<double> *X);

    void BuildKernelMatrix();

    int get_samples_trained_number() const
    {
        return SamplesTrainedNumber;
    }

    bool operator==(OnlineSVR const &)const;

private:

    // Parameter Attributes
    SVRParameters svr_parameters;
    int SamplesTrainedNumber {0}; // how much observations were used in train
    bool StabilizedLearning {false};
    bool SaveKernelMatrix{false};

    // Training Set Attributes
    Matrix<double> *X = NULL;
    Vector<double> *Y = NULL;
    Vector<double> *Weights = NULL;
    double Bias {0.0};

    // Work Set Attributes
    Vector<int> *SupportSetIndexes = NULL;
    Vector<int> *ErrorSetIndexes = NULL;
    Vector<int> *RemainingSetIndexes = NULL;
    Matrix<double> *R = NULL;
    Matrix<double> *KernelMatrix = NULL;

    std::vector<double> unstabilized_sample_indexes_;
    bool is_stabilization_converging_ {false};

    // Private Learning Operations
    int Learn(Vector<double> *X, double Y);

    int Unlearn(int Index);

    // Kernel Operations
    double Kernel(Vector<double> *V1, Vector<double> *V2);

    Matrix<double> *Q(Vector<int> *V1, Vector<int> *V2);

    Matrix<double> *Q(Vector<int> *V);

    Vector<double> *Q(Vector<int> *V, int Index);

    Vector<double> *Q(int Index);

    double Q(int Index1, int Index2);

    // Matrix R Operations
    void AddSampleToR(int SampleIndex, set_type_e SampleOldSet,
            Vector<double> *Beta, Vector<double> *Gamma);

    void RemoveSampleFromR(int SampleIndex);

    Vector<double> *FindBeta(int SampleIndex);

    Vector<double> *FindGamma(Vector<double> *Beta, int SampleIndex);

    double FindGammaSample(Vector<double> *Beta, int SampleIndex);

    // KernelMatrix Operations
    void AddSampleToKernelMatrix(Vector<double> *X);

    void RemoveSampleFromKernelMatrix(int SampleIndex);


    double Predict(int Index);

    // Variation Operations
    double FindVariationLc1(Vector<double> *H, Vector<double> *Gamma,
            int SampleIndex, int Direction);

    double FindVariationLc2(int SampleIndex, int Direction);

    double FindVariationLc(int SampleIndex);

    Vector<double> *FindVariationLs(Vector<double> *H, Vector<double> *Beta,
            int Direction);

    Vector<double> *FindVariationLe(Vector<double> *H, Vector<double> *Gamma,
            int Direction);

    Vector<double> *FindVariationLr(Vector<double> *H, Vector<double> *Gamma,
            int Direction);

    void FindLearningMinVariation(Vector<double> *H, Vector<double> *Beta,
            Vector<double> *Gamma, int SampleIndex, double *MinVariation,
            int *MinIndex, int *Flag);

    void FindUnlearningMinVariation(Vector<double> *H, Vector<double> *Beta,
            Vector<double> *Gamma, int SampleIndex, double *MinVariation,
            int *MinIndex, int *Flag);

    // Set Operations
    void UpdateWeightsAndBias(Vector<double> **H, Vector<double> *Beta,
            Vector<double> *Gamma, int SampleIndex, double MinVariation);

    void AddSampleToRemainingSet(int SampleIndex);

    void AddSampleToSupportSet(Vector<double> **H, Vector<double> *Beta,
            Vector<double> *Gamma, int SampleIndex, double MinVariation);

    void AddSampleToErrorSet(int SampleIndex, double MinVariation);

    void MoveSampleFromSupportSetToErrorRemainingSet(int MinIndex,
            double MinVariation);

    void MoveSampleFromErrorSetToSupportSet(Vector<double> **H,
            Vector<double> *Beta, Vector<double> *Gamma, int MinIndex,
            double MinVariation);

    void MoveSampleFromRemainingSetToSupportSet(Vector<double> **H,
            Vector<double> *Beta, Vector<double> *Gamma, int MinIndex,
            double MinVariation);

    void RemoveSampleFromSupportSet(int SampleSetIndex);

    void RemoveSampleFromErrorSet(int SampleSetIndex);

    void RemoveSampleFromRemainingSet(int SampleSetIndex);

    void RemoveSample(int SampleIndex);

    // Other Control Operations
    bool VerifyKKTConditions(Vector<double> *H);

    bool VerifyKKTConditions(Vector<double> *H, int *SampleIndex,
            int *SampleSetIndex);

    bool VerifyKKTConditions(int SampleIndex);

    static bool IsEquals(double Value1, double Value2, double Error);

    static bool IsLesser(double Value1, double Value2, double Error);

    static bool IsBigger(double Value1, double Value2, double Error);

    static bool IsContained(double Value, double From, double To, double Error);

};
} // svr

using OnlineSVR_ptr = std::shared_ptr<svr::OnlineSVR>;
