#ifndef _SVM_VCL_H
#define _SVM_VCL_H



#include "viennacl/scalar.hpp"
#include "viennacl/vector.hpp"
#include "viennacl/matrix.hpp"
#include "viennacl/matrix_proxy.hpp"
#include "viennacl/linalg/inner_prod.hpp"
#include "viennacl/linalg/norm_1.hpp"
#include "viennacl/linalg/prod.hpp"

#include <algorithm>


#include "OnlineSVR.h"
#include "model/SVRParameters.hpp"
#include "r_matrix.h"
#include <boost/interprocess/sync/named_semaphore.hpp>

/* TODO Wrap svr::batch in a class */
namespace svr {

namespace batch {
#define TAU 1e-12

void set_update_r_matrix(const bool update_r_matrix);
void set_max_smo_iterations(const size_t max_smo_iterations);

void
svm_train(
        Matrix<double> &learning_data,
        Vector<double> &reference_data,
        OnlineSVR &model,
        const bool only_sv = false);

} //batch
} //svr


#endif /* _SVM_VCL_H */
